# Supporting code: Overcoming the Central Planner Approach – Bilevel Optimization of the European Energy Transition
This code modifies the SecMOD framework to model a bilevel optimization of the European energy system.

## Referencing

If you use our software or any part of it, please cite [Shu et al. (2023)](). The full information on the publication is also shown below:

Shu, D.Y. \*; Reinert, C. \*; Mannhardt, J.\*; Leenders, L.; Lüthje, J.; Mitsos, A.; and Bardow, A. (2023): "Overcoming the Central Planner Approach – Bilevel Optimization of the European Energy Transition". Submitted. 

Since the publication above and this repository mainly describe the extensions compared to the linear SecMOD, please also refer to 
[Reinert et al. (2022)](https://www.frontiersin.org/articles/10.3389/fenrg.2022.884525/abstract) for further information. The full information on the SecMOD publication is also shown below:

Reinert, C.; Schellhas, L.; Mannhardt, J.; Shu, D.; Kämper, A.; Baumgärtner, N.; Deutz, S., and Bardow, A. (2022): "SecMOD: An open-source modular framework
combining multi-sector system optimization and life-cycle assessment". Frontiers in Energy Research. DOI: https://doi.org/10.3389/fenrg.2022.884525.

## License
This project is licensed under the MIT license, for more information please refer to the license file.

## Documentation and Support 

Please find the full documentation of the SecMOD LP framework [here](https://ltt.pages.git-ce.rwth-aachen.de/opt/secmod/secmod/).

You can further find a video about SecMOD and some example applications [here](https://www.youtube.com/watch?v=wXFocTL95hs).

In case you need help using Git, please refer to the git documentation [here](https://git-scm.com/docs).

## Installation
A brief instruction to install SecMOD can also be found below:
Clone a copy of the whole repository to your computer:
```
git clone git@git-ce.rwth-aachen.de:ltt/secmod-bilevel.git
```

Open a terminal with a python enviroment (e.g. Anaconda promt) and install the secmod package with:
```shell
pip install --user -e '<Path\to\cloned\repository>'
``` 
The path should point to the directory where secmod is saved (repository folder).
Make sure that the setup.py file is located in your repository folder. You can use this installation for several projects in multiple working directories.

For further installation instruction please go [here](https://ltt.pages.git-ce.rwth-aachen.de/opt/secmod/secmod/usage/installation.html). The code was tested with Python 3.7.5. 
## First Steps

Create a working directory for the optimization framework. Next, cd to that directory and enter
```shell
python -m secmod.setup
```
in the terminal. This sets up the right folder structure. 
Start the optimization with double clicking the start.bat file, where you can choose your enviroment or 
alternatively use 

```shell
python -m secmod
```
to start. 
