# README - 01-GRID

This directory contains all data packages which define
a grid model. A grid definition contains theinformation
about the nodes of the grid, their geo coordinates
and connections between the nodes. A connection is only
defined by the two nodes it connects.
