# README - ECOINVENT SUBASSEMBLIES

This directory contains all subassemblies used for the ecoinvent
based processes. A subassembly has a unique name and contains
one or multiple ecoinvent processes, which then can be used in
the impact definition of all processes.

Note that the values of the subassemblies were replaced with placeholders.
