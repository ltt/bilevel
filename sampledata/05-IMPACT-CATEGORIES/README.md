# README - IMPACT CATEGORIES

This directory contains all impact categories used in the optimization.
A impact category has a unique name and a corresponding name used
in the ecoinvent database. Every impact category has data about
the nodal and total impact limits for operation, invest and total impact.
