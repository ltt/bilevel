# SecMOD Data

This project includes all the data used in the modeling framework SecMOD for the publication:
Shu, D.Y. *; Reinert, C. *; Mannhardt, J.*; Leenders, L.; Lüthje, J.; Mitsos, A.; and Bardow, A. (2023): "Overcoming the Central Planner Approach – Bilevel Optimization of the European Energy Transition". Submitted.

Due to license restrictions, we deleted all proprietary data. In particular, data for environmental assessment stored in `04-ECOINVENT/ecoinvent.csv` does not contain meaningful values. However, the structure of the database is shown to allow users with access to the proprietary data to reproduce the model, by updating the ecoinvent file `04-ECOINVENT/ecoinvent.csv`.
