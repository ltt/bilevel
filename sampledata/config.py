grid_name       = "EU"
invest_years_per_optimization = 1
invest_years = [2015,2020,2025,2030,2035,2040,2045,2050]
interest_rate   = 0.05
economic_period = "30 years"
# Choose LCA framework - "ReCiPe Midpoint (H)", "ILCD 2.0 2018 midpoint" or "cumulative energy demand"
LCA_framework   = "ReCiPe Midpoint (H)"
# Override used impact categories to reduce preparation time - "None" if FRAMEWORK should be used
LCA_manual_impact_categories = ["cost", "ILCD 2.0 2018 midpoint:climate change:climate change total"]


# Set parameters for the timeseries aggregation, done by TSAM
# Documentation of all parameters is here https://github.com/FZJ-IEK3-VSA/tsam/blob/master/tsam/timeseriesaggregation.py
# See "class TimeSeriesAggregation"
time_series_aggregation_config = {
    "resolution"                : None,
    "noTypicalPeriods"          : 10,
    "hoursPerPeriod"            : 1,
    "clusterMethod"             : "hierarchical",
    "evalSumPeriods"            : False,
    "sortValues"                : False,
    "sameMean"                  : False,
    "rescaleClusterPeriods"     : True,
    "extremePeriodMethod"       : 'None',
    "predefClusterOrder"        : None,
    "predefClusterCenterIndices": None,

    "solver_tsa"                : "gurobi",
    "roundOutput"               : None,
    "addPeakMin"                : None,
    "addPeakMax"                : None,
    "addMeanMin"                : None,
    "addMeanMax"                : None,
    "use_time_series_aggregation": True,
    "correct_dateformat"        : '%d.%m.%Y %H:%M', # desired format of timeseries indizes
    "partially_correct_dateformat"  : '%d.%m.%Y %H:%M:%S', # partially correct format of timeseries indizes that is meant to be changed
    "wrong_dateformat"          : '%d-%b-%Y %H:%M:%S' # current format of timeseries indizes that is meant to be changed
}

# scaling parameters
scaling_options = {
    'algorithm':            'geom_mean', # 'equi': Equilibration, 'geom_mean': Geometric Mean, 'approx_geom_mean': Approximated Geometric Mean, 'arith_mean': Arithmetic Mean
    'Round_to_power_of_2':  True, # rounds scaling factors to power of two (often numerically better)
    'scale_constraints':    False, # scaled after model instantiated 
    'scale_variables':      False, # scaled after model instatiated
    'scale_objective':      False, # scaled after model instatiated
    'analyze_numerics':     False, # retrieves information about numerics
    'eps_bound':            1e-8  # threshold for printing constraint bound
}

# Spatial aggregation configuration
# aggregation = True, if aggregation should be used
# Network_optimization/Operational_optimization = True, if used
# Number_of_processors = maximal used processors during parallel optimization (count processors with multiprocessing package and mp.cpu_count())
# cluster_number_setter = "fast_forward", "plus_one", "continious_increase" (only for convergence test)
# clustering_method = "kmedoids","kmeans", "hierarchical_clustering"
# start_number_of_cluster = Number of clusters to start aggregation with
# epsilon = abort criterion for aggregation
# convergence_test = can be used to check the convergence of the aggregation method (epsilon is neglected and no reaggregation is done).
#                    Continious_increase should be used as cluster_number_setter
# cluster_number_setter_evaluation = At the moment only fast_forward and plus_one can be compared. To compare more methods, they have
#                    to be implemented first into the method and the evaluation function has to be adjusted
# method_quality_evaluation = Value should not be changed. Quality and runtime difference between optimization with additional network and 
#                    additional operational optimization is evaluated
# time_slices_evaluation = time slices that should be evaluated for runtime benefit evaluation
spatial_aggregation_settings ={
    "aggregation" : False,
    "network_optimization" : True,
    "operational_optimization" : False,
    "number_of_processors" : 2,
    "cluster_number_setter" : "continious_increase",
    "continious_increase_number" : 20,
    "clustering_method" : "kmedoids",
    "start_number_of_cluster" : 2,
    "epsilon" : 0.02,
    "convergence_test" : False,
    "cluster_number_setter_evaluation" : ["fast_forward","plus_one"],
    "method_quality_evaluation" : ["NetO","OpO"],
    "time_slices_evaluation" : [10,20,30,40,50,100,150,200]
}

# choose between gurobi_persistent, cplex, and other solvers
solver_centralized = "gurobi_persistent"

solver_options_centralized = {
            "ObjScale": -0.5,
            "LogToConsole": 1,
            "NumericFocus": 2,
            "ScaleFlag": -1,
            # "Method": 0,
            # "Presolve": 0,
            "BarHomogeneous": 1,
            "InfUnbdInfo": 1,
            "DualReductions": 1,
            "Threads": 6
            }

solver_options_BO_gurobi = {
            "ObjScale": -0.5,
            "LogToConsole": 1,
            "NumericFocus": 2,
            "ScaleFlag": 2,
            # "Method": 4,
            # "NodeMethod": 0,
            # "ConcurrentMIP":2,
            "Presolve": 2,
            # "NonConvex": 2,
            # "BarHomogeneous": 1,
            "InfUnbdInfo": 1,
            "DualReductions": 0,
            # "NoRelHeurTime": 10,
            # "MIPFocus": 3,
            # "BranchDir": -1,
            # "Heuristics": 0,
            # "CutPasses": 10,
            "GomoryPasses": 15,
            "AggFill": 10,
            "VarBranch": 3,
            # "DegenMoves" : 9,
            # "ImproveStartGap": 2, 
            "ImproveStartTime": 2000,
            "Cuts": 2,
            "TimeLimit": 5000,
            # "MIPGap": 0.0005,
            "Threads": 8
            }   

solver_options_BO_gams = {
    "option threads": 8,
    "option MIP": "cplex",
}
solver_BO = "gurobi_persistent"
solver_gams_BO = "baron"
solver_options_BO = {
            }
if solver_BO == "gurobi_persistent":
    solver_options_BO = solver_options_BO_gurobi 
elif solver_BO == "gams":
    solver_options_BO = solver_options_BO_gams 

# set options for bilevel and diagonalization approach
bilevel_options = {
            "convergence_criteria": 2e-2,               # maxmimum norm for convergence of outer iteration
            "max_iterations": 100,                      # maximum number of iterations of outer iteration
            "num_hold_iterations": 2,                   # iterations held after convergence is achieved (to avoid accidental nash stationary point)
            "eps_new_capacity": 1e-4,                   # absolute threshold under which the new_capacity is set to zero
            "update_Big_M": 5,                          # factor by which the Big_M constants are increased
            "iterations_run_again": 5,                  # maximum iterations to run a node again
            "eps_Big_M": 1e-6,                          # threshold at which dual variable is assumed to equal Big_M constant
            "sampling_points_binary_expansion": 5,      # number of sampling points in binary expansion
            "set_termination_gap": False,               # set a termination gap, based on the binary expansion gap 
            "damping_factor_diagonalization": 0.65,     # damping factor of outer diagonalization algorithm: predicted_new_capacity^{k+1} = dfd*predicted_new_capacity^{k} + (1-dfd)*new_capacity{k}
            "sort_countries_by_demand": False           # sort order of countries by demand, descending
}

debug_optimization = True

load_raw_input = True
load_existing_input_dict = False
load_existing_results = False
new_pint_units = "./SecMOD/00-INPUT/00-RAW-INPUT/00-UNITS-DEFINITIONS.txt"

# fixes the impact overshoot variables to 0, thus impact limits must be obeyed
fix_impact_overshoot = True
fix_operational_impact_overshoot = False

fix_slack_variables = False #, if the non_served_demand of the in config.fix_products specified products should be fixed to 0.
# default is fix_slack_variables = False to keep slack variable functionality
# fix_slack_variables=False
fix_products = ["electricity","exhaust gas","mobility","heat civil","heat civil from district heating","heat high temperature",
            "heat high temperature from district heating","heat low temperature","heat low temperature from district heating",
            "heat medium temperature","heat medium temperature from district heating"]

