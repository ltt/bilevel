from pathlib import Path
import logging
import os
from pyomo.opt import TerminationCondition
import pickle
import copy
import numpy as np
import pyomo.environ as pe
log_format = '%(asctime)s %(filename)s: %(levelname)s: %(message)s'
if not os.path.exists("logs"):
            os.mkdir("logs")
logging.basicConfig(filename='logs/secmod.log', level=logging.INFO, format=log_format,
                    datefmt='%Y-%m-%d %H:%M:%S')
logging.getLogger().addHandler(logging.StreamHandler())
logging.propagate = False

import secmod.setup as setup
import secmod.helpers as helpers
import secmod.data_processing as data_processing
from secmod.classes import ImpactCategory, ProcessImpacts, units, config
from secmod.optimization_BO_SDE import Optimization as Optimization_BO
from secmod.optimization_centralized import Optimization as Optimization_centralized
from secmod.optimization_BO_centralized_solution_SDE import Optimization as Optimization_BO_centralized_solution

# Get current working directory
working_directory = Path.cwd()
setup.setup(working_directory, reset=False)

def run_bilevel_optimization(input_dictionary, previous_optimization, b_fix_node: bool = False, Optimization = Optimization_BO):
    """ This method runs the bilevel_optimization repeatedly.
    Accepted when no dual variables exceeds its Big_M constant"""
    # set _b_run_again to True
    iteration_run_again = 0
    _b_run_again = True
    while _b_run_again and iteration_run_again <= config.bilevel_options["iterations_run_again"]:
        _b_run_again = False
        # Initialize Optimization class
        thisInvestmentOptimization = Optimization()
        Optimization._dual_constraint_expression = {}
        thisInvestmentOptimization.run(input_dict=input_dictionary, solver=config.solver_BO, solver_options=config.solver_options_BO, debug=config.debug_optimization, previous_optimization = previous_optimization, b_fix_node = b_fix_node)
        # if infeasible, compute and write Irreducible Inconsistent Subsystem (subset model with infeasible constraints)
        if config.debug_optimization and (thisInvestmentOptimization.results.solver.termination_condition == getattr(TerminationCondition,'infeasible') or thisInvestmentOptimization.results.solver.termination_condition == getattr(TerminationCondition,'infeasibleOrUnbounded')):
            logging.error("Problem infeasible for node {}".format(thisInvestmentOptimization.model_instance.current_node.value))
            if config.solver_BO == 'gurobi_persistent': 
                thisInvestmentOptimization.mysolver._solver_model.computeIIS()
                thisInvestmentOptimization.mysolver._solver_model.write("{0}/SecMOD/01-MODEL-RESULTS/gurobi_model_{1}.ilp".format(working_directory, min(ProcessImpacts.invest_years)))
            break
        
        # if any Big_M parameter changed 
        if thisInvestmentOptimization.b_Big_M_changed:
            for big_M in thisInvestmentOptimization.increased_big_M_values:
                for data in thisInvestmentOptimization.increased_big_M_values[big_M]:
                    try:
                        input_dictionary[None][big_M][data] = thisInvestmentOptimization.increased_big_M_values[big_M][data]
                    except:
                        input_dictionary[None][big_M] = {data: thisInvestmentOptimization.increased_big_M_values[big_M][data]}
        if thisInvestmentOptimization.b_Big_M_too_small:
            # run again
            _b_run_again = True
            iteration_run_again += 1
            if iteration_run_again > config.bilevel_options["iterations_run_again"]:
                logging.info("Maximum number of iterations of {} to update Big-M constants reached. Will continue with next node".format(config.bilevel_options["iterations_run_again"]))
        else:
            # don't run again and go to next node
            _b_run_again = False

    return thisInvestmentOptimization,input_dictionary

def main():
    """This is the main method of SecMOD.

    This method is called when SecMOD is called as a script instead of being imported,
    e.g. using the following command line in the command line::

        python -m secmod

    It is the main entrance point for starting a run of SecMOD.
    """

    helpers.log_heading("SecMOD - Startup")
    
    computed_input_path = working_directory / "SecMOD" / "00-INPUT" / "01-COMPUTED-INPUT"
    
    # Setup static variables
    ProcessImpacts.interest_rate = config.interest_rate
    ProcessImpacts.economic_period = units(config.economic_period)
    
    # Choose LCA framework - "ReCiPe Midpoint (H)", "ILCD 1.0.8 2016 midpoint" or "cumulative energy demand"
    ImpactCategory.FRAMEWORK = config.LCA_framework

    # Override used impact categories to reduce preparation time - "None" if FRAMEWORK should be used
    if hasattr(config, "LCA_manual_impact_categories"):
        ImpactCategory.MANUAL_IMPACT_CATEGORY_SELECTION = config.LCA_manual_impact_categories

    # Load raw data
    if config.load_raw_input:
        data_processing.load_raw_input(working_directory / "SecMOD" / "00-INPUT" / "00-RAW-INPUT")
    
    # Sort invest years in case someone put them in the wrong order
    config.invest_years.sort()

    # create results folder
    if not os.path.exists(working_directory / "SecMOD" / "01-MODEL-RESULTS"):
        os.mkdir(working_directory / "SecMOD" / "01-MODEL-RESULTS")

    # Go over all years using a rolling horizon for the optimization
    for reference_year in config.invest_years:
        # Select invest years for the current optimization horizon
        ProcessImpacts.invest_years = [year for year in config.invest_years if (year >= reference_year) and ((config.invest_years.index(year) - config.invest_years.index(reference_year)) < (config.invest_years_per_optimization))]
        helpers.log_heading("Start optimization horizon of {0}, including {1}".format(reference_year, ProcessImpacts.invest_years))
            
        # Generate input dictionary or start with an existing one (only senseful for debugging)
        input_dictionary = data_processing.generate_input_dictionary(computed_input_path, config.load_existing_input_dict)
        input_dictionary[None]['type_of_optimization'] = {None: ['InvestmentOptimization']}
        # Make sure that in the next optimization horizon a new input dictionary is created
        config.load_existing_input_dict = False

        # reorder nodes by total demand
        if config.bilevel_options["sort_countries_by_demand"]:
            total_demand = {key:sum(sum(input_dictionary[None]["demand"][(product,reference_year,ts,key)]*input_dictionary[None]["time_slice_yearly_weight"][ts] for ts in input_dictionary[None]["time_slices"][None]) for product in input_dictionary[None]["products"][None]) for key in input_dictionary[None]["nodes"][None]}
            sorted_demand_nodes = sorted(total_demand,key=total_demand.get,reverse=True)
            # overwrite connected nodes
            input_dictionary[None]["connected_nodes"] = {key:sorted_demand_nodes.index(input_dictionary[None]["nodes"][None][input_dictionary[None]["connected_nodes"][key]-1])+1 for key in input_dictionary[None]["connected_nodes"]}
            # overwrite nodes
            input_dictionary[None]["nodes"][None] = sorted_demand_nodes
        # set first current node
        input_dictionary[None]['current_node'] = {None: 1}

        # ------------------------------------------------------- #
        # solve centralized optimization for centralized pathway
        # ------------------------------------------------------- #
        input_dictionary_centralized = copy.deepcopy(input_dictionary)
        # overwrite existing_capacity_production with existing_capacity_production_centralized
        input_dictionary_centralized[None]["existing_capacity_production"] = input_dictionary_centralized[None]["existing_capacity_production_centralized"]
        thisCentralizedPathwayInvestmentOptimization = Optimization_centralized()
        thisCentralizedPathwayInvestmentOptimization.run(input_dict=input_dictionary_centralized, solver=config.solver_centralized, solver_options=config.solver_options_centralized, debug=config.debug_optimization)
        if config.debug_optimization and (thisCentralizedPathwayInvestmentOptimization.results.solver.termination_condition == getattr(TerminationCondition,'infeasible') or thisCentralizedPathwayInvestmentOptimization.results.solver.termination_condition == getattr(TerminationCondition,'infeasibleOrUnbounded')):
            logging.error("Centralized problem infeasible")
            if config.solver_BO == 'gurobi_persistent': 
                thisCentralizedPathwayInvestmentOptimization.mysolver._solver_model.computeIIS()
                thisCentralizedPathwayInvestmentOptimization.mysolver._solver_model.write("{0}/SecMOD/01-MODEL-RESULTS/centralized_gurobi_model_{1}.ilp".format(working_directory, min(ProcessImpacts.invest_years)))
            break
        with open(working_directory / "SecMOD" / "01-MODEL-RESULTS" / "CentralizedPathwayInvestmentModel_{0}.pickle".format(min(ProcessImpacts.invest_years)), "wb") as input_file:
            pickle.dump(thisCentralizedPathwayInvestmentOptimization.model_instance, input_file)
        input_dictionary_centralized[None]["predicted_new_capacity_production"] = thisCentralizedPathwayInvestmentOptimization.model_instance.new_capacity_production.get_values()
        previous_optimization = thisCentralizedPathwayInvestmentOptimization
        # get BO solution with new_capacity_production from thisCentralizedPathwayInvestmentOptimization
        
        thisCentralizedPathwayResultInvestmentOptimization, input_dictionary_centralized = run_bilevel_optimization(input_dictionary = input_dictionary_centralized,previous_optimization = previous_optimization,b_fix_node = True, Optimization = Optimization_BO_centralized_solution)
        with open(working_directory / "SecMOD" / "01-MODEL-RESULTS" / "CentralizedPathwayResultInvestmentModel_{0}.pickle".format(min(ProcessImpacts.invest_years)), "wb") as input_file:
            pickle.dump(thisCentralizedPathwayResultInvestmentOptimization.model_instance, input_file)
        # add new capacity for centralized pathway
        data_processing.add_new_capacity_centralized_from_results(thisCentralizedPathwayInvestmentOptimization)

        # ------------------------------------------------------- #
        # solve centralized optimization for bilevel pathway
        # ------------------------------------------------------- #
        # get initial solution for new_capacity_production
        thisCentralizedInvestmentOptimization = Optimization_centralized()
        thisCentralizedInvestmentOptimization.run(input_dict=input_dictionary, solver=config.solver_centralized, solver_options=config.solver_options_centralized, debug=config.debug_optimization)
        if config.debug_optimization and (thisCentralizedInvestmentOptimization.results.solver.termination_condition == getattr(TerminationCondition,'infeasible') or thisCentralizedInvestmentOptimization.results.solver.termination_condition == getattr(TerminationCondition,'infeasibleOrUnbounded')):
            logging.error("Centralized problem infeasible")
            if config.solver_BO == 'gurobi_persistent': 
                thisCentralizedInvestmentOptimization.mysolver._solver_model.computeIIS()
                thisCentralizedInvestmentOptimization.mysolver._solver_model.write("{0}/SecMOD/01-MODEL-RESULTS/centralized_gurobi_model_{1}.ilp".format(working_directory, min(ProcessImpacts.invest_years)))
            break
        with open(working_directory / "SecMOD" / "01-MODEL-RESULTS" / "CentralizedInvestmentModel_{0}.pickle".format(min(ProcessImpacts.invest_years)), "wb") as input_file:
            pickle.dump(thisCentralizedInvestmentOptimization.model_instance, input_file)
        input_dictionary[None]["predicted_new_capacity_production"] = thisCentralizedInvestmentOptimization.model_instance.new_capacity_production.get_values()
        previous_optimization = thisCentralizedInvestmentOptimization
        # get BO solution with new_capacity_production from thisCentralizedInvestmentOptimization
        
        thisCentralizedResultInvestmentOptimization, input_dictionary = run_bilevel_optimization(input_dictionary = input_dictionary,previous_optimization = previous_optimization,b_fix_node = True, Optimization = Optimization_BO_centralized_solution)
        with open(working_directory / "SecMOD" / "01-MODEL-RESULTS" / "CentralizedResultInvestmentModel_{0}.pickle".format(min(ProcessImpacts.invest_years)), "wb") as input_file:
            pickle.dump(thisCentralizedResultInvestmentOptimization.model_instance, input_file)
        

        # ------------------------------------------------------- #
        # iterate through players
        # ------------------------------------------------------- #
        # initialize convergence norm, iteration, and held iteration
        delta_new_capacity_production = {}  # empty dict for sampling range of binary expansion 
        iterative_objective_values = {}     # empty dict for objective values of players 
        solution_dict = {}                  # write essential variables to dict
        convergence_norm = np.inf           # initial value convergence norm 
        iterative_convergence_norm = np.inf # initial value convergence norm of iterative objective values
        iteration = 0                       # initial value iteration
        hold_iteration = 0                  # initial value of held iterations
        
        logging.info("Previous new capacity before iteration {} is {}".format(iteration,input_dictionary[None]["predicted_new_capacity_production"]))
        # diagonalization
        while not (convergence_norm < config.bilevel_options["convergence_criteria"] and hold_iteration > config.bilevel_options["num_hold_iterations"]) and iteration <= config.bilevel_options["max_iterations"]:
            helpers.log_heading("Start Iteration number {}".format(iteration))
            
            # copy previous predicted_new_capacity
            predicted_new_capacity_old = copy.deepcopy(input_dictionary[None]["predicted_new_capacity_production"])
            for node in input_dictionary[None]["nodes"][None]:
                # create entry in solution_dict 
                solution_dict[node] = {}
                # current_node (i.e., player)
                input_dictionary[None]['current_node'] = {None: input_dictionary[None]["nodes"][None].index(node)+1}
                helpers.log_heading("Start Optimization for iteration {} for current node: {} (node number {})".format(iteration,node,input_dictionary[None]['current_node'][None]))
                # run bilevel optimization
                thisInvestmentOptimization, input_dictionary = run_bilevel_optimization(input_dictionary,previous_optimization)
                # get new_capacity_production
                new_capacity_production = thisInvestmentOptimization.model_instance.new_capacity_production.get_values()
                # set numerically negative new capacity to 0
                for _index_production in new_capacity_production:
                    if new_capacity_production[_index_production] <= config.bilevel_options["eps_new_capacity"] and abs(new_capacity_production[_index_production]) != 0:
                        logging.info("Value for new capacity production with index {} has a small absolute value of {}. It is set to zero.".format(_index_production,new_capacity_production[_index_production]))
                        new_capacity_production[_index_production] = 0
                # overwrite predicted_new_capacity_production
                # with damping factor diagonalization
                alpha_dfd = config.bilevel_options["damping_factor_diagonalization"]
                # save solution variables
                solution_dict[node]["iterative_objective"] = pe.value(thisInvestmentOptimization.model_instance.Objective/thisInvestmentOptimization.model_instance.scaling_objective)
                solution_dict[node]["objective_all_nodes"] = thisInvestmentOptimization.model_instance.ShortULObjective
                solution_dict[node]["binary_expansion_gap"] = pe.value(thisInvestmentOptimization.model_instance.BinaryExpansionGap)
                solution_dict[node]["predicted_new_capacity"] = {}
                solution_dict[node]["new_capacity"] = {}
                solution_dict[node]["old_predicted_new_capacity"] = predicted_new_capacity_old
                solution_dict[node]["delta_new_capacity"] = thisInvestmentOptimization.model_instance.delta_new_capacity_production.extract_values()
                solution_dict[node]["bound_new_capacity_production"] = thisInvestmentOptimization._bound_new_capacity_production

                for index in new_capacity_production:
                    updated_predicted_new_capacity = alpha_dfd*input_dictionary[None]["predicted_new_capacity_production"][(node,)+index] + \
                        (1-alpha_dfd)*new_capacity_production[index]*thisInvestmentOptimization.model_instance.scaling_new_capacity_production[index].value
                    if updated_predicted_new_capacity <= config.bilevel_options["eps_new_capacity"] and abs(updated_predicted_new_capacity) != 0:
                        updated_predicted_new_capacity = 0
                    input_dictionary[None]["predicted_new_capacity_production"][(node,)+index] = updated_predicted_new_capacity
                    solution_dict[node]["predicted_new_capacity"][index] = updated_predicted_new_capacity
                    solution_dict[node]["new_capacity"][index] = new_capacity_production[index]*thisInvestmentOptimization.model_instance.scaling_new_capacity_production[index].value
                previous_optimization = copy.copy(thisInvestmentOptimization)
                # get delta_new_capacity_production
                for index in thisInvestmentOptimization.model_instance.delta_new_capacity_production:
                    delta_new_capacity_production[(node,)+index] = thisInvestmentOptimization.model_instance.delta_new_capacity_production[index].value
                # save objective value
                iterative_objective_values[node] = pe.value(thisInvestmentOptimization.model_instance.Objective/thisInvestmentOptimization.model_instance.scaling_objective)
            # after each iteration
            # get objective values for each node
            objective_values = {item[0]:pe.value(item[1]) for item in thisInvestmentOptimization.model_instance.ShortULObjective.items()}
            # calculate convergence norm
            relative_deviation = []
            for index in delta_new_capacity_production:
                if delta_new_capacity_production[index] != 0:
                    relative_deviation.append((input_dictionary[None]["predicted_new_capacity_production"][index]-predicted_new_capacity_old[index])/delta_new_capacity_production[index])
                else:
                    relative_deviation.append((input_dictionary[None]["predicted_new_capacity_production"][index]-predicted_new_capacity_old[index]))
            # calculate convergence norm of objective values
            if iteration >= 1:
                array_objective_values_old = np.array(list(previous_objective_values.values()))
                array_objective_values_new = np.array(list(objective_values.values()))
                convergence_norm = np.linalg.norm((array_objective_values_old-array_objective_values_new)/array_objective_values_old,2)

                array_iterative_objective_values_old = np.array(list(previous_iterative_objective_values.values()))
                array_iterative_objective_values_new = np.array(list(iterative_objective_values.values()))
                iterative_convergence_norm = np.linalg.norm((array_iterative_objective_values_old-array_iterative_objective_values_new)/array_iterative_objective_values_old,2)
            # calculate convergence norm of predicted new capacity
            capacity_convergence_norm = np.linalg.norm(relative_deviation,2)
            # print the three convergence norms:
            # 1. convergence of objective values, evaluated after each iteration
            # 2. convergence of iterative objective values, saved after each node
            # 3. convergence of predicted new capacity
            helpers.log_heading("convergence norm after iteration {} is {}".format(iteration,convergence_norm))
            logging.info("objective values after iteration {} is {}\n".format(iteration,objective_values))
            logging.info("iterative convergence norm after iteration {} is {}\n".format(iteration,iterative_convergence_norm))
            logging.info("iterative objective values after iteration {} is {}\n".format(iteration,iterative_objective_values))
            logging.info("capacity convergence norm after iteration {} is {}\n".format(iteration,capacity_convergence_norm))
            previous_objective_values = copy.deepcopy(objective_values)
            previous_iterative_objective_values = copy.deepcopy(iterative_objective_values)
            
            # hold for num_hold_iterations 
            if convergence_norm < config.bilevel_options["convergence_criteria"]:
                hold_iteration += 1
            else:
                hold_iteration = 0
            
            logging.info("Previous new capacity before iteration {} is {}".format(iteration,input_dictionary[None]["predicted_new_capacity_production"]))
            # stop if convergence norm of capacity is zero after the first iteration 
            if capacity_convergence_norm == 0 and iteration == 0:
                logging.info("capacity did not change in the first iteration. Nash equilibrium already reached")
                break
            # update iteration
            iteration += 1
        with open(working_directory / "SecMOD" / "01-MODEL-RESULTS" / "InvestmentModel_{0}.pickle".format(min(ProcessImpacts.invest_years)), "wb") as input_file:
            pickle.dump(thisInvestmentOptimization.model_instance, input_file)
        # dump solution_dict
        with open(working_directory / "SecMOD" / "01-MODEL-RESULTS" / "SolutionDict_{0}.pickle".format(min(ProcessImpacts.invest_years)), "wb") as input_file:
            pickle.dump(solution_dict, input_file)
        # add new capacity
        data_processing.add_new_capacity_from_results(thisInvestmentOptimization)

    logging.info("\n\n")
    logging.info("========================================")
    logging.info("=               THE END!               =")
    logging.info("========================================")
    
    logging.getLogger().handlers.clear()
    logging.shutdown()

if __name__ == "__main__":
    main()
