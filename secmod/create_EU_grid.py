import os
from pathlib import Path
import pandas as pd
import pickle
import copy
from datetime import timedelta
import numpy as np
import update_ecoinvent_3_7_1_EU_AllProcesses as update_ecoinvent_processes
from extract_transmission_properties import ENTSOE_Grid

class Grid():
    """ This is the class to calculate the EU Grid"""
    def __init__(self,folder_name,create_grid = True, create_processes = True,create_products = True,create_ecoinvent = True,create_impact_categories = True):
        """ Initializes Grid """
        self.folder_name = folder_name
        self.create_grid = create_grid
        self.create_processes = create_processes
        self.create_products = create_products
        self.create_ecoinvent = create_ecoinvent
        self.create_impact_categories = create_impact_categories

        self.create_EU_grid()

    def set_mapped_sets(self):
        """ this method defines essential sets """
        ## invest years
        self.invest_years = [2015,2020,2025,2030,2035,2040,2045,2050]
        ## construction years
        self.construction_years = [2000,2005,2010,2015]
        ## countries
        # only MRC
        countries_MRC =  {"AT": "Austria", 
                        "BE": "Belgium", 
                        "BG": "Bulgaria", 
                        "HR": "Croatia", 
                        "DK": "Denmark", 
                        "EE": "Estonia", 
                        "FI": "Finland", 
                        "FR": "France",  
                        "DE": "Germany", 
                        "GR": "Greece", 
                        "IE": "Ireland", 
                        "IT": "Italy", 
                        "LV": "Latvia", 
                        "LT": "Lithuania", 
                        "LU": "Luxembourg", 
                        "NL": "The Netherlands",  
                        # "NO": "Norway", 
                        "PL": "Poland", 
                        "PT": "Portugal", 
                        "SI": "Slovenia", 
                        "ES": "Spain", 
                        "SE": "Sweden"}
        self.countries = countries_MRC
        # connecting nodes to switzerland
        self.connecting_countries_switzerland = ["AT00","DE00","FR00","ITN1"]
        # additional nordic connections which are not included in XMLs
        self.additional_connections = ["FI00-SE01",
                                    "FI00-SE03",
                                    "DE00-SE04",
                                    "DKE1-SE04",
                                    "DKW1-SE03",
                                    "EE00-FI00",
                                    "LT00-SE04",
                                    "PL00-SE04",
                                    "FR00-IE00"]
        ## processes (key is JRC nomenclature, value is secmod nomenclature)
        # in \Potencia\Central_2018_<country>\<country>\Annual_reports\PowerGeneration\Central_2018_<country>_pg_det_yearly
        processes = {"Nuclear": "nuclear",
                        "Coal fired": "hard_coal",
                        "Lignite fired": "lignite",
                        "Gas fired": "natural_gas_turbine",
                        "Onshore": "wind_onshore",
                        "Offshore": "wind_offshore",
                        "Run-of-river": "run-of-river_hydro",
                        "Geothermal": "geothermal",
                        "Solar photovoltaics": "photovoltaics",
                        "Solar thermal": "solar_thermal"
                        }
        # add DAC if DAC in folder name
        if "DAC" in self.folder_name:
            processes["DAC"] = "DAC"

        self.processes = processes
        # products
        products = {"Nuclear": "uranium",
                    "Coal fired": "hard coal",
                    "Lignite fired": "lignite",
                    "Gas fired": "natural gas",
                    "Onshore": None,
                    "Offshore": None,
                    "Run-of-river": None,
                    "Geothermal": None,
                    "Solar photovoltaics": None,
                    "Solar thermal": None
                    }
        self.products = products

    def load_ecoinvent_data(self):
        """ This method loads the ecoinvent data """
        # load ecoinvent data
        # Load updated ecoinvent.csv file:
        ecoinvent = pd.read_csv( self.input_path_secmod / "04-ECOINVENT" / "ecoinvent.csv")
        # Define if investment and/or operation impacts should be investigated:
        list_of_impact_types = ['invest','operation'] # Options: ['invest','operation']
        # Define self.locations that should be considered.
        # The algorithm picks the self.locations with priority from first (highest) to last (lowest).
        self.locations = ['Europe_without_Switzerland','RER', "ENTSO_E", "WEU"]+list(self.countries.keys())+['GLO', 'RoW']
        # self.locations.extend(['Europe_without_Switzerland','RER', "ENTSO_E", "WEU", 'GLO', 'RoW'])
        # Create a list of all ecoinvent processes in new database:
        self.ecoinvent_processes_unique = ecoinvent['process'].unique()
        # Create a log file in which all steps in the ecoinvent data update are tracked:
        logfile_columns = ['SecMOD process type','SecMOD process name','invest/operation','ecoinvent process [OLD]','ecoinvent process [NEW]','location [OLD]','location [NEW]','change made','reason']
        self.logfile = pd.DataFrame(columns=logfile_columns)

    def get_cost_index(self, process):
        """ this method returns the index to look up cost data in Potencia Assumption """
        size = "M"
        cogeneration = "Electricity only"
        # Nuclear
        if process == "Nuclear":
            type_proc = "Nuclear power plants"
            technology = "Nuclear - current"
        # coal
        elif process == "Coal fired":
            type_proc = "Coal fired power plants"
            technology = "Steam turbine" 
        # lignite
        elif process == "Lignite fired":
            type_proc = "Lignite fired power plants"
            technology = "Steam turbine" 
        # gas
        elif process == "Gas fired":
            type_proc = "Gas fired power plants (Natural gas, biogas)"
            technology = "Gas turbine combined cycle" 
        # onshore
        elif process == "Onshore":
            type_proc = "Wind power plants"
            technology = "Onshore" 
        # offshore
        elif process == "Offshore":
            type_proc = "Wind power plants"
            technology = "Offshore" 
        # run-of-river
        elif process == "Run-of-river":
            type_proc = "Hydro plants"
            technology = "Run-of-river" 
        # geothermal
        elif process == "Geothermal":
            type_proc = "Geothermal power plants"
            technology = "Geothermal power plants"
        # pv 
        elif process == "Solar photovoltaics":
            type_proc = "Solar PV power plants"
            technology = "Solar PV power plants" 
        # solarthermal
        elif process == "Solar thermal":
            type_proc = "Solar thermal power plants"
            technology = "Solar thermal power plants" 
        else:
            type_proc = "-1"
            technology = "-1"
        
        return(type_proc,technology,cogeneration,size)

    def create_DAC_process(self,production_process_path):
        """ This method creates the DAC process from the existing DAC folder """
        # create grid folder
        if not os.path.exists(production_process_path / "DAC" / self.grid_name):
            os.mkdir(production_process_path / "DAC" / self.grid_name)
        # technology matrix
        technologymatrix_input = pd.read_csv(production_process_path / "DAC" / "technologymatrix.csv").set_index("product")
        technologymatrix_index = pd.MultiIndex.from_product([["electricity"],self.nodes.index.values],names=["product","node"])
        technologymatrix = pd.DataFrame(columns = technologymatrix_input.columns,index = technologymatrix_index)
        technologymatrix.index.name = "product"
        technologymatrix["unit"] = "MW"
        technologymatrix["comments"] = "Source: From Deutz.2021, assumed heat pump (COP 2.51)"
        for year in list(map(str,self.invest_years)):
            technologymatrix.loc["electricity",year] = technologymatrix_input.loc["electricity",year] + technologymatrix_input.loc["heat low temperature",year]/self.constants["COP_heat_pump"]
        # write csv
        technologymatrix.to_csv(production_process_path / "DAC" / self.grid_name  / "technologymatrix.csv")
        # potential capacity
        # approach: get emissions of year 2015 for each country
        # compensation of entire emissions is potential capacity
        year_start = self.invest_years[0]
        # potential capacity
        potential_capacity = pd.DataFrame(columns = ["unit"] + self.invest_years + ["comments"],index = self.nodes.index)
        potential_capacity.index.name = "node"
        potential_capacity["unit"] = "gigagram * hour **(-1)"
        potential_capacity["comments"] = "Assumption: able to compensate 2015 emissions of country"
        for node in self.nodes.index:
            if node == "GR":
                node_idees = "EL"
            else:
                node_idees = node
            GHG_emissions_node = pd.read_excel(self.source_path_dict["JRC_IDEES_2015"] / ("JRC-IDEES-2015_All_xlsx_"+node_idees) / ("JRC-IDEES-2015_EmissionBalance_"+node_idees + ".xlsx"),sheet_name = "TITOT").set_index("Transformation input (kt CO2)")
            GHG_emissions_start = GHG_emissions_node.loc[["Solid Fuels","Natural gas"],year_start].sum()/8760 # gigagram/hour
            
            potential_capacity.loc[node,self.invest_years] = GHG_emissions_start
        
        # write csv
        # create grid folder
        potential_capacity.to_csv(production_process_path / "DAC" / self.grid_name / "potential_capacity.csv")
            
        # ecoinvent
        production_process_path_reference = self.input_path_secmod_reference / "02-PROCESSES" / "01-PRODUCTION" / "Archive" / "01-ECOINVENT-BASED"
        # if costs already exist
        if os.path.exists(production_process_path / "DAC" / "ecoinvent_invest.csv"):
            ecoinvent_invest = pd.read_csv(production_process_path / "DAC" / "ecoinvent_invest.csv").set_index("process_name")
        # read from dena18 Grid
        else:
            ecoinvent_invest = pd.read_csv(production_process_path_reference / "DAC" / "ecoinvent_invest.csv").set_index("process_name")
        # change 2016 to 2015
        if "2016" in ecoinvent_invest.columns:
            ecoinvent_invest.rename(columns = {"2016":"2015"},inplace=True)
        # if costs already exist
        if os.path.exists(production_process_path / "DAC" / "ecoinvent_operation.csv"):
            ecoinvent_operation = pd.read_csv(production_process_path / "DAC" / "ecoinvent_operation.csv").set_index("process_name")
        # read from dena18 Grid
        else:
            ecoinvent_operation = pd.read_csv(production_process_path_reference / "DAC" / "ecoinvent_operation.csv").set_index("process_name")
        # change 2016 to 2015
        if "2016" in ecoinvent_operation.columns:
            ecoinvent_operation.rename(columns = {"2016":"2015"},inplace=True)
        # create ecoinvent table for each node
        for node in self.nodes.index:
            locations_node = [node]
            locations_node.extend(self.locations)
            [ecoinvent_invest_node,self.logfile] = update_ecoinvent_processes.check_all_ecoinvent_processes_of_secmod_process("production","DAC","invest",ecoinvent_invest,self.ecoinvent_processes_unique,locations_node,self.logfile)        
            [ecoinvent_operation_node,self.logfile] = update_ecoinvent_processes.check_all_ecoinvent_processes_of_secmod_process("production","DAC","operation",ecoinvent_operation,self.ecoinvent_processes_unique,locations_node,self.logfile)        
            # write csvs for node
            # create node folder in grid folder
            if not os.path.exists(production_process_path / "DAC" / self.grid_name / node):
                os.mkdir(production_process_path / "DAC" / self.grid_name / node)
            ecoinvent_invest_node.to_csv(production_process_path / "DAC" / self.grid_name / node / "ecoinvent_invest.csv")
            ecoinvent_operation_node.to_csv(production_process_path / "DAC" / self.grid_name / node / "ecoinvent_operation.csv")
         
    def create_01_GRID(self):
        """ This method creates the 01-GRID folder """
        # create grid folder
        if not os.path.exists(self.input_path_secmod / "01-GRID" / self.grid_name):
            os.mkdir(self.input_path_secmod / "01-GRID" / self.grid_name)
        # nodes from ENTSOE TYNDP 2020-scenario.xlsx
        filename = "TYNDP-2020-Scenario-Datafile.xlsx"
        # load nodes and connections
        nodes = pd.read_excel(self.source_path_dict["ENTSOE"] / filename, sheet_name = "Nodes - Dict")
        connections_df = pd.read_excel(self.source_path_dict["ENTSOE"] / filename, sheet_name = "Line - Dict")
        capitals = pd.read_csv(self.source_path_dict["ENTSOE"] / "country-capitals.csv").set_index("CountryCode")
        # select only nodes which are in MRC
        nodes = nodes[nodes["country"].apply(lambda x: x in self.countries)]
        # sort and rename columns
        nodes = nodes.reindex(["latitude","longitude","country","Region","node_id","previous_node","Unnamed: 1"],axis=1)
        nodes.rename(columns={"country":"node"},inplace=True)
        nodes.rename(columns={"Unnamed: 1":"country"},inplace=True)
        # reduce to country level
        nodes["node_id"] = nodes.groupby("node")["node_id"].transform(lambda x: x.str.cat(sep =','))
        nodes["previous_node"] = nodes.groupby("node")["previous_node"].transform(lambda x: x.str.cat(sep =','))
        nodes.drop_duplicates(subset = ["node"],inplace=True)
        # set index
        nodes.set_index("node",inplace = True)
        # set the capitals as latitude and longitude
        nodes["latitude"] = nodes.index.map(lambda x: capitals.loc[x,"CapitalLatitude"])
        nodes["longitude"] = nodes.index.map(lambda x: capitals.loc[x,"CapitalLongitude"])
        # remove whitespaces from country name
        nodes["country"] = nodes["country"].apply(lambda x: x.strip())
        # set nodes as attribute
        self.nodes = nodes
        # calculate connections
        if self.b_use_real_transmission: # Keep on False, True does not work yet
            # instantiate ENTSOE Grid
            self.this_ENTSOE_Grid = ENTSOE_Grid(self)
            connections = pd.DataFrame(columns=["node1","node2","manual_distance","manual_distance_unit","node1_long","node2_long"])
            connections.index.name = "connection"
            for connection in self.this_ENTSOE_Grid.connection_dict:
                connections.loc[connection,"node1"] = self.this_ENTSOE_Grid.connection_dict[connection]["node1"]
                connections.loc[connection,"node2"] = self.this_ENTSOE_Grid.connection_dict[connection]["node2"]
                connections.loc[connection,"manual_distance"] = self.this_ENTSOE_Grid.connection_dict[connection]["length"]
                connections.loc[connection,"manual_distance_unit"] = "km"
            # add additional connections
            for additional_connection in self.additional_connections:
                connections.loc[additional_connection,"node1"] = nodes.index[nodes["node_id"].str.contains(connections_df[connections_df["line_id"]==additional_connection]["node_a"].values[0])][0]
                connections.loc[additional_connection,"node2"] = nodes.index[nodes["node_id"].str.contains(connections_df[connections_df["line_id"]==additional_connection]["node_b"].values[0])][0]
                connections.loc[additional_connection,"manual_distance"] = self.constants["default_connection_length"]
                connections.loc[additional_connection,"manual_distance_unit"] = "km"
        else:
            # excluded Exp connections and get country code -> exclude connections whose nodes are not in nodes
            connections = connections_df[~connections_df["line_id"].str.contains("Exp")]
            connections.loc[:,"node1"] = connections["node_a"].apply(lambda x: nodes.index[nodes["node_id"].str.contains(x)].any())
            connections.loc[:,"node2"] = connections["node_b"].apply(lambda x: nodes.index[nodes["node_id"].str.contains(x)].any())
            connections = connections[(connections["node1"]!=False) & (connections["node2"]!=False)]
            # rename
            connections = connections.rename(columns={"line_id":"connection","node_a":"node1_long","node_b":"node2_long"})
            connections["manual_distance"] = self.constants["default_connection_length"] # XXX important! insert value
            connections["manual_distance_unit"] = "km"
            connections = connections.reindex(["connection","node1","node2","manual_distance","manual_distance_unit","node1_long","node2_long"],axis=1)
            connections.set_index("connection",inplace=True)
            # exclude domestic connections
            connections = connections[connections["node1"] != connections["node2"]]
        # add extra connections because of Switzerland
        if self.b_use_switzerlands_capacities:
            for extra_country_1 in self.connecting_countries_switzerland:
                for extra_country_2 in self.connecting_countries_switzerland:
                    if extra_country_1 != extra_country_2:
                        # check if combination of countries already in connections
                        if not (((extra_country_1 == connections["node1_long"]) & (extra_country_2 == connections["node2_long"])) | ((extra_country_2 == connections["node1_long"]) & (extra_country_1 == connections["node2_long"]))).any():
                            connections.loc[extra_country_1+"-"+extra_country_2, "node1_long"] = extra_country_1
                            connections.loc[extra_country_1+"-"+extra_country_2, "node2_long"] = extra_country_2
                            connections.loc[extra_country_1+"-"+extra_country_2, "manual_distance"] = self.constants["default_connection_length"]
                            connections.loc[extra_country_1+"-"+extra_country_2, "manual_distance_unit"] = "km"   
                            connections.loc[extra_country_1+"-"+extra_country_2, "node1"] = extra_country_1[0:2]
                            connections.loc[extra_country_1+"-"+extra_country_2, "node2"] = extra_country_2[0:2]
        # write csv
        nodes.to_csv(self.input_path_secmod / "01-GRID" / self.grid_name / "nodes.csv")
        connections.to_csv(self.input_path_secmod / "01-GRID" / self.grid_name / "connections.csv")
        # set connections as attribute
        
        self.connections = connections
        
    def create_02_PROCESSES(self):
        """ This method creates the 02-PROCESSES folder """
        
        # Transmission processes
        transmission_process_path = self.input_path_secmod / "02-PROCESSES" / "04-TRANSMISSION" / "01-ECOINVENT-BASED"
        # currently only power_line_220_kV
        power_line_types = ["power_line_220_kV"]
        for power_line_type in power_line_types:
            if self.b_use_real_transmission: # Keep on False, True does not work yet
                pass
            else:
                # set up DataFrame
                ENTSOE_years = [2025,2030,2040]
                existing_capacity = pd.DataFrame(columns= ENTSOE_years + ["unit","comments"],index = self.connections.index)
                existing_capacity.index.name = "connection"
                existing_capacity["comments"] = "Source: NTC capacities TYNDP 2020, Reference Grid in first year, Expanded Grid in next years."
                existing_capacity["unit"] = "circuits"
                if power_line_type == "power_line_220_kV":
                    # NTC capacities 2020 ENTSO-E Reference Grid 2025 - TYNDP 2020
                    # Assumption only 220 kV transmission line
                    NTC_values_connections = pd.DataFrame(columns = ["NTC"], index=self.connections.index)
                    # load already calculated NTCs or extract raw data
                    if os.path.exists(self.source_path_dict["ENTSOE"] / "NTC_values_raw.pickle"):
                        with open(self.source_path_dict["ENTSOE"] / "NTC_values_raw.pickle", "rb") as input_file:
                            NTC_values = pickle.load(input_file)
                    else:
                        NTC_values = pd.read_excel(self.source_path_dict["ENTSOE"] / "TYNDP-2020-Scenario-Datafile.xlsx",sheet_name="Line").set_index(["Node/Line","Parameter","Case","Scenario","Climate Year","Year"])
                        NTC_values = NTC_values.loc[:,"Export Capacity",:,["National Trends","Global Ambition"],2007,:]["Value"]
                        NTC_values.index = NTC_values.index.droplevel(["Parameter","Climate Year"])
                        with open(self.source_path_dict["ENTSOE"] / "NTC_values_raw.pickle", "wb") as input_file:
                            pickle.dump(NTC_values,input_file)
                    
                    # calculate circuits
                    power_line_properties = pd.read_csv(transmission_process_path / power_line_type / "properties.csv").set_index("property")    
                    
                    for ENTSOE_year in ENTSOE_years:
                        if ENTSOE_year == ENTSOE_years[0]:
                            case = "Reference Grid"
                            scenario = "National Trends"
                        else:
                            case = "Expanded Grid"
                            scenario = "Global Ambition"
                        # get switzerland and distribute capacity
                        NTC_switzerland = NTC_values.loc[NTC_values.index.get_level_values("Node/Line").map(lambda x: "CH" in x)]
                        connected_nodes_switzerland = {key:{"node1": key.split("-")[0],"node2": key.split("-")[1]} for key in NTC_switzerland.index.get_level_values("Node/Line").unique()}                        
                        switzerland_distribution = pd.DataFrame(columns=["net"],index=self.connecting_countries_switzerland)
                        for key in connected_nodes_switzerland:
                            if connected_nodes_switzerland[key]["node1"] == "CH00":
                                try:
                                    switzerland_distribution.loc[connected_nodes_switzerland[key]["node2"],"net"] = NTC_switzerland.loc[key,case,scenario,ENTSOE_year]
                                # if not expanded grid, no new existing capacity
                                except KeyError:
                                    switzerland_distribution.loc[connected_nodes_switzerland[key]["node2"],"net"] = 0
                            else:
                                try:
                                    switzerland_distribution.loc[connected_nodes_switzerland[key]["node1"],"net"] = NTC_switzerland.loc[key,case,scenario,ENTSOE_year]
                                # if not expanded grid, no new existing capacity
                                except KeyError:
                                    switzerland_distribution.loc[connected_nodes_switzerland[key]["node1"],"net"] = 0
                        NTC_switzerland_dict = {}
                        for key in self.connecting_countries_switzerland:        
                            for key_connector in self.connecting_countries_switzerland:
                                if key_connector != key:
                                    if key+"-"+key_connector in self.connections.index:
                                        connection_index = key+"-"+key_connector
                                    else:
                                        connection_index = key_connector+"-"+key

                                    NTC_switzerland_connection = switzerland_distribution.loc[key_connector,"net"]*switzerland_distribution.loc[key,"net"]/sum(switzerland_distribution.loc[switzerland_distribution.index!= key_connector,"net"])
                                    try:
                                        NTC_switzerland_dict[connection_index] = max(NTC_switzerland_connection,NTC_switzerland_dict[connection_index])
                                    except:
                                        NTC_switzerland_dict[connection_index] = NTC_switzerland_connection
                        # iterate through connections
                        for connection in self.connections.index:
                            if (connection,case,scenario,ENTSOE_year) in NTC_values.index:
                                NTC_value = NTC_values.loc[connection,case,scenario,ENTSOE_year]
                            else:
                                NTC_value = 0
                            if self.b_use_switzerlands_capacities:
                                if connection in NTC_switzerland_dict:
                                    NTC_value += NTC_switzerland_dict[connection]
                            # subsitute value for FR00-IE00 by connection IE00-UK00 in first year
                            if connection == "FR00-IE00" and ENTSOE_year == ENTSOE_years[0]:
                                NTC_value =  NTC_values.loc["IE00-UK00",case,scenario,ENTSOE_year]

                            NTC_values_connections.loc[connection] = NTC_value*self.constants["multiplication_factor_NTC"]
                        # calculate circuit (not rounded to integers)
                        circuits = NTC_values_connections/(float(power_line_properties.loc["power limit","value"])*(1-float(power_line_properties.loc["safety margin","value"])))
                    
                        existing_capacity[ENTSOE_year] = circuits
                    # get new existing capacity for 2040 as difference between 2030 and 2040
                    existing_capacity[ENTSOE_years[2]] = existing_capacity[ENTSOE_years[2]]-existing_capacity[ENTSOE_years[1]]
                # substitute first year
                existing_capacity.rename(columns = {ENTSOE_years[0]:self.invest_years[0]},inplace=True)
                # potential capacity
                potential_capacity = pd.DataFrame(columns=self.invest_years + ["unit","comments"],index = self.connections.index)
                potential_capacity.index.name = "connection"
                potential_capacity["unit"] = "circuits"
                potential_capacity.loc[self.connections.index,self.invest_years] = 0
                # write csv
                # create grid folder
                if not os.path.exists(transmission_process_path / power_line_type / self.grid_name):
                    os.mkdir(transmission_process_path / power_line_type / self.grid_name)
                existing_capacity.to_csv(transmission_process_path / power_line_type / self.grid_name / "existing_capacity.csv")
                potential_capacity.to_csv(transmission_process_path / power_line_type / self.grid_name / "potential_capacity.csv")

        ## Production processes
        production_process_path = self.input_path_secmod / "02-PROCESSES" / "01-PRODUCTION" / "01-ECOINVENT-BASED"
        production_process_path_reference = self.input_path_secmod_reference / "02-PROCESSES" / "01-PRODUCTION" / "Archive" / "01-ECOINVENT-BASED"
        
        # if existing capacity already extracted
        if os.path.exists(self.source_path_dict["Potencia"] / "existing_capacity.pickle"):
            with open(self.source_path_dict["Potencia"] / "existing_capacity.pickle", "rb") as input_file:
                existing_capacity_potencia = pickle.load(input_file)
        else:
            # import xlsx for each country \Potencia\Central_2018_<country>\<country>\Annual_reports\PowerGeneration\Central_2018_<country>_pg_det_yearly - Net Capacities
            existing_capacity_potencia = {}
            for node in self.nodes.index.to_list():
                if "Central_2018_"+node in os.listdir(self.source_path_dict["Potencia"]):
                    node_potencia = node
                # name confusion GR (in demand_nodal_timeseries and demand_hourly) and EL (demand_dict_potencia)
                elif node == "GR":
                    node_potencia = "EL"
                existing_capacity_potencia[node] = pd.read_excel(self.source_path_dict["Potencia"] / ("Central_2018_"+node_potencia) / node_potencia / "Annual_reports" / "PowerGeneration" / ("Central_2018_"+node_potencia+"_pg_det_yearly.xlsx"),sheet_name = "Net Capacities").set_index(node_potencia + ": Net capacities installed (MW)")
            with open(self.source_path_dict["Potencia"] / "existing_capacity.pickle","wb") as input_file:
                pickle.dump(existing_capacity_potencia, input_file)
        # if decommissioned capacity already extracted
        if os.path.exists(self.source_path_dict["Potencia"] / "decommissioned_capacity.pickle"):
            with open(self.source_path_dict["Potencia"] / "decommissioned_capacity.pickle", "rb") as input_file:
                decommissioned_capacity_potencia = pickle.load(input_file)
        else:
            # import xlsx for each country \Potencia\Central_2018_<country>\<country>\Annual_reports\PowerGeneration\Central_2018_<country>_pg_det_yearly - Net Capacities Decommissioned
            decommissioned_capacity_potencia = {}
            for node in self.nodes.index.to_list():
                if "Central_2018_"+node in os.listdir(self.source_path_dict["Potencia"]):
                    node_potencia = node
                # name confusion GR (in demand_nodal_timeseries and demand_hourly) and EL (demand_dict_potencia)
                elif node == "GR":
                    node_potencia = "EL"
                decommissioned_capacity_potencia[node] = pd.read_excel(self.source_path_dict["Potencia"] / ("Central_2018_"+node_potencia) / node_potencia / "Annual_reports" / "PowerGeneration" / ("Central_2018_"+node_potencia+"_pg_det_yearly.xlsx"),sheet_name = "Net Capacities Decommissioned").set_index(node_potencia + ": Net capacities decommissioned (MW)")
            with open(self.source_path_dict["Potencia"] / "decommissioned_capacity.pickle","wb") as input_file:
                pickle.dump(decommissioned_capacity_potencia, input_file)
        # import costs and potential capacity once
        # read excel, originally from PG_technology_Central_2018.xlsx [sheet tech_proj], copied to individual sheets
        costs_potencia_capital = pd.read_excel(self.source_path_dict["Potencia"] / ("Central_2018_EU28") / "EU28" / "Assumptions" / "EU28_Potencia_Capital_Costs.xlsx").set_index(["Type","Technology","Co-generation","Size"])
        costs_potencia_fixed_OM= pd.read_excel(self.source_path_dict["Potencia"] / ("Central_2018_EU28") / "EU28" / "Assumptions" / "EU28_Potencia_Fixed_Costs_OM.xlsx").set_index(["Type","Technology","Co-generation","Size"])
        costs_potencia_variable_OM= pd.read_excel(self.source_path_dict["Potencia"] / ("Central_2018_EU28") / "EU28" / "Assumptions" / "EU28_Potencia_Variable_Costs_OM.xlsx").set_index(["Type","Technology","Co-generation","Size"])
        # potential capacity
        potential_capacity_solar = pd.read_excel(self.source_path_dict["ENSPRESO"] / "ENSPRESO_SOLAR_170_0-03.xlsx")
        potential_capacity_solar.set_index("Surface",inplace= True)
        potential_capacity_solarthermal = potential_capacity_solar.loc[potential_capacity_solar["Technology"]=="CSP - before storage",self.nodes["country"]].sum()
        potential_capacity_solarpv = potential_capacity_solar.loc["TOTAL",self.nodes["country"]] - potential_capacity_solarthermal
            # reindex
        potential_capacity_solarpv = potential_capacity_solarpv.reindex(self.nodes["country"])
        potential_capacity_solarthermal = potential_capacity_solarthermal.reindex(self.nodes["country"])
        potential_capacity_solarpv.index = self.nodes.index
        potential_capacity_solarthermal.index = self.nodes.index
        # iterate through processes
        for process in self.processes:
            # create process folder  
            if not os.path.exists(production_process_path / self.processes[process]):
                os.mkdir(production_process_path / self.processes[process])
            # if DAC, extract information from existing folder
            if process == "DAC":
                self.create_DAC_process(production_process_path)
                continue
            # add not grid-dependent information
            # costs
            cost_phases_unit = {"invest": "Euro * kW ** (-1)",
                                "operation": "Euro * kWh ** (-1)",
                                "maintenance_absolute": "Euro * kW ** (-1)",
                                "maintenance_relative_invest": 1}
            costs = pd.DataFrame(columns = ["unit"] + self.invest_years + ["comments"],index = cost_phases_unit.keys())
            costs.index.name = "phase"
            costs["unit"] = cost_phases_unit.values()
            costs["comments"] = "Source: POTEnCIA Central Scenario 2018, Assumptions for EU28 (does not depend on country)"
            # select index
            cost_index_process = self.get_cost_index(process)
            # get costs
            costs.loc["invest",self.invest_years] = costs_potencia_capital.loc[cost_index_process,self.invest_years]
            costs.loc["maintenance_absolute",self.invest_years] = costs_potencia_fixed_OM.loc[cost_index_process,self.invest_years]
            if self.b_use_non_fuel_operational_costs:
                costs.loc["operation",self.invest_years] = costs_potencia_variable_OM.loc[cost_index_process,self.invest_years]/1000 # in €/MWh in assumptions
            costs[costs.isna()] = 0
            # lifetime 
            lifetime_duration = pd.DataFrame(columns = self.invest_years + ["comments"],index = ["year"])
            lifetime_duration.index.name = "unit"
            lifetime_duration["comments"] = "Source: POTEnCIA Central Scenario 2018, Assumptions for EU28 (does not depend on country)"
            lifetime_and_availability_potencia= pd.read_excel(self.source_path_dict["Potencia"] / ("Central_2018_EU28") / "EU28" / "Assumptions" / "EU28_Potencia_Lifetime.xlsx").set_index(["Type","Technology","Co-generation","Size"])
            lifetime_duration.loc["year",self.invest_years] = lifetime_and_availability_potencia.loc[cost_index_process,"Technical lifetime (years)"]
            # properties
            properties_dict = {"property": ["reference product"],"value": ["electricity"],"unit":["MW"]}
            properties = pd.DataFrame(properties_dict)
            properties.set_index("property",inplace = True)
            # technology_matrix
            efficiency_potencia = pd.read_excel(self.source_path_dict["Potencia"] / ("Central_2018_EU28") / "EU28" / "Assumptions" / "EU28_Potencia_Efficiency.xlsx").set_index(["Type","Technology","Co-generation","Size"])
            if self.b_use_eurostat_efficiencies:
                efficiency_eurostat = pd.read_csv(self.source_path_dict["EUROSTAT"] / "nrg_bal_c__custom_1165863_20210722_144659.sdmx.csv")
                efficiency_eurostat["geo"].replace("EL","GR",inplace=True)
                efficiency_eurostat = efficiency_eurostat.set_index(["nrg_bal","siec","geo","TIME_PERIOD"])
                efficiency_eurostat.drop(["DATAFLOW","LAST UPDATE","freq","unit","OBS_FLAG"],axis=1,inplace=True)
                eurostat_identifier = {"input": "TI_EHG_MAPE_E", "output": "GEP_MAPE", "hard coal": "C0129", "lignite": "C0220","natural gas":"G3000","uranium":"N900H"}
                # specific product consumption = -input/output
                technologymatrix_eurostat = -efficiency_eurostat.loc[eurostat_identifier["input"]]/efficiency_eurostat.loc[eurostat_identifier["output"]]
                technologymatrix_eurostat = technologymatrix_eurostat.unstack()
                technologymatrix_eurostat.columns = technologymatrix_eurostat.columns.droplevel(0)
                efficiency_years = technologymatrix_eurostat.columns.to_list()
                combined_efficiency_years = sorted(set.union(set(efficiency_years),set(self.invest_years)))
                products_process = ["electricity"]
                if self.products[process]:
                    products_process.append(self.products[process])
                technologymatrix_index = pd.MultiIndex.from_product([products_process,self.nodes.index.values],names=["product","node"])
                technologymatrix = pd.DataFrame(columns = ["unit"] + combined_efficiency_years + ["comments"], index = technologymatrix_index)
                # entry for electricity
                technologymatrix.loc[properties.loc["reference product","value"],combined_efficiency_years] = 1
                technologymatrix.loc[properties.loc["reference product","value"],"unit"] = properties.loc["reference product","unit"]
                if self.products[process]:
                    technologymatrix.loc[(self.products[process],self.nodes.index),efficiency_years] = technologymatrix_eurostat.loc[(eurostat_identifier[self.products[process]],self.nodes.index),efficiency_years].values
                    technologymatrix.replace(-np.inf,np.nan,inplace=True)
                    technologymatrix.loc[self.products[process],"unit"] = properties.loc["reference product","unit"]
                    technologymatrix.loc[self.products[process],"comments"] = "Source: Eurostat Complete Energy Balances"
                    # interpolate with nearest value
                    for node in self.nodes.index:
                        technologymatrix.loc[(self.products[process],node),combined_efficiency_years] = technologymatrix.loc[(self.products[process],node),combined_efficiency_years].astype(float).interpolate(limit_direction="both")
                        # if nan for all years, use POTEnCIA values
                        if technologymatrix.loc[(self.products[process],node),combined_efficiency_years].isna().all():
                            technologymatrix.loc[(self.products[process],node),"comments"] = "Source: POTEnCIA Central Scenario 2018, Assumptions for EU28 (does not depend on country)"
                            # fill remaining with POTEnCIA values
                            for year in combined_efficiency_years:
                                absolute_distance_to_potencia = np.abs(efficiency_potencia.columns-year)
                                closest_year = efficiency_potencia.columns[absolute_distance_to_potencia.argmin()]
                                technologymatrix.loc[(self.products[process],node),year]= -1/efficiency_potencia.loc[cost_index_process,closest_year]                         
            else:
                # use POTEnCIA values
                technologymatrix = pd.DataFrame(columns = ["unit"] + self.invest_years + ["comments"])
                technologymatrix.index.name = "product"
                technologymatrix.loc[properties.loc["reference product","value"],self.invest_years] = 1
                technologymatrix.loc[properties.loc["reference product","value"],"unit"] = properties.loc["reference product","unit"]
                if self.products[process]:
                    technologymatrix.loc[self.products[process],self.invest_years] = -1/efficiency_potencia.loc[cost_index_process,self.invest_years]
                    technologymatrix.loc[self.products[process],"unit"] = properties.loc["reference product","unit"]
                    technologymatrix.loc[self.products[process],"comments"] = "Source: POTEnCIA Central Scenario 2018, Assumptions for EU28 (does not depend on country)"
            # technology_matrix timeseries
            if os.path.exists(self.source_path_dict["ENTSOE"] / "timeseries_index.pickle"):
                with open(self.source_path_dict["ENTSOE"] / "timeseries_index.pickle", "rb") as input_file:
                    timeseries_index = pickle.load(input_file)
                
                technologymatrix_timeseries = pd.DataFrame(columns = ["unit"] + products_process,index = timeseries_index)
                technologymatrix_timeseries[["unit"] + products_process] = 1.0
                technologymatrix_timeseries.index.name = "time slice"
            
            ## ECOINVENT
               
            # ecoinvent_invest and ecoinvent_operation
            # for the moment: copy from Dena18
            # no data for solar thermal in Dena18
            if process == "Solar thermal":
                process_identifier = "photovoltaics"
            else:
                process_identifier = self.processes[process]
            # if costs already exist
            if os.path.exists(production_process_path / self.processes[process] / "ecoinvent_invest.csv"):
                ecoinvent_invest = pd.read_csv(production_process_path / self.processes[process] / "ecoinvent_invest.csv").set_index("process_name")
            # read from dena18 Grid
            else:
                ecoinvent_invest = pd.read_csv(production_process_path_reference / process_identifier / "ecoinvent_invest.csv").set_index("process_name")
            # change 2016 to 2015
            if "2016" in ecoinvent_invest.columns:
                ecoinvent_invest.rename(columns = {"2016":"2015"},inplace=True)
            # if costs already exist
            if os.path.exists(production_process_path / self.processes[process] / "ecoinvent_operation.csv"):
                ecoinvent_operation = pd.read_csv(production_process_path / self.processes[process] / "ecoinvent_operation.csv").set_index("process_name")
            # read from dena18 Grid
            else:
                ecoinvent_operation = pd.read_csv(production_process_path_reference / process_identifier / "ecoinvent_operation.csv").set_index("process_name")
            # change 2016 to 2015
            if "2016" in ecoinvent_operation.columns:
                ecoinvent_operation.rename(columns = {"2016":"2015"},inplace=True)
            # create ecoinvent table for each node
            for node in self.nodes.index:
                locations_node = [node]
                locations_node.extend(self.locations)
                [ecoinvent_invest_node,self.logfile] = update_ecoinvent_processes.check_all_ecoinvent_processes_of_secmod_process("production",self.processes[process],"invest",ecoinvent_invest,self.ecoinvent_processes_unique,locations_node,self.logfile)        
                [ecoinvent_operation_node,self.logfile] = update_ecoinvent_processes.check_all_ecoinvent_processes_of_secmod_process("production",self.processes[process],"operation",ecoinvent_operation,self.ecoinvent_processes_unique,locations_node,self.logfile)        
                # write csvs for node
                # create node folder in grid folder
                if not os.path.exists(production_process_path / self.processes[process] / self.grid_name / node):
                    os.mkdir(production_process_path / self.processes[process] / self.grid_name / node)
                ecoinvent_invest_node.to_csv(production_process_path / self.processes[process] / self.grid_name / node / "ecoinvent_invest.csv")
                ecoinvent_operation_node.to_csv(production_process_path / self.processes[process] / self.grid_name / node / "ecoinvent_operation.csv")
            # write csv
            costs.to_csv(production_process_path / self.processes[process] / "costs.csv")
            lifetime_duration.to_csv(production_process_path / self.processes[process] / "lifetime_duration.csv")
            properties.to_csv(production_process_path / self.processes[process] / "properties.csv")
            
            technologymatrix_timeseries.to_csv(production_process_path / self.processes[process] / "technologymatrix_timeseries.csv")
            # technologymatrix is now grid-dependent
            technologymatrix.to_csv(production_process_path / self.processes[process] / self.grid_name  / "technologymatrix.csv")
            # grid dependent
            # create grid folder
            if not os.path.exists(production_process_path / self.processes[process] / self.grid_name):
                os.mkdir(production_process_path / self.processes[process] / self.grid_name)
            # maximum_production_share
            maximum_production_share = pd.DataFrame(columns = ["unit"] + self.invest_years + ["comments"],index = self.nodes.index)
            maximum_production_share.index.name = "node"
            maximum_production_share["unit"] = 1.0
            
            # availability timeseries
            # EMHIRES for PV and Wind Onshore and Offshore for year 2015
            # for other processes POTEnCIA Central Scenario 2018, Assumptions for EU28, assumption constant availability
            # values are capacity factors (fractions of capacity)
            availability_timeseries = pd.DataFrame(columns= ["unit"] + self.nodes.index.to_list() + ["comments"], index =  timeseries_index)
            availability_timeseries.index.name = "time slice"
            availability_timeseries.loc[:, availability_timeseries.columns != "comments"] = 1.0
            # correct time stamp
            correct_dateformat = "%d.%m.%Y %H:%M"
            # process specific
            if process == "Solar photovoltaics" or process == "Solar thermal":
                # from EMHIRESPV_TSh_CF_Country_only2015, reduced version 
                # if already converted
                if os.path.exists(self.source_path_dict["EMHIRES"] / "availability_solar.pickle"):
                    with open(self.source_path_dict["EMHIRES"] / "availability_solar.pickle", "rb") as input_file:
                        availability_solar = pickle.load(input_file)
                else:
                    availability_solar = pd.read_excel(self.source_path_dict["EMHIRES"] / "EMHIRESPV_TSh_CF_Country_only2015.xlsx").set_index("Date")
                    # rename Greece
                    availability_solar.rename(columns = {"EL":"GR"},inplace=True)
                    # only select specific countries
                    availability_solar = availability_solar[self.nodes.index]
                    # dump pickle
                    with open(self.source_path_dict["EMHIRES"] / "availability_solar.pickle", "wb") as input_file:
                        pickle.dump(availability_solar , input_file)
                # set to year 2018 to match timeseries of demand
                availability_solar.index = availability_solar.index.map(lambda x: pd.to_datetime(x) + timedelta(days = (2017-2015)*365+1))
                availability_solar.index = pd.to_datetime(availability_solar.index).strftime(correct_dateformat)
                availability_timeseries[self.nodes.index] = availability_solar
            elif process == "Onshore":
                # from EMHIRES_WIND_COUNTRY_only2015_June2019, reduced version 
                # if already converted
                if os.path.exists(self.source_path_dict["EMHIRES"] / "availability_onshore.pickle"):
                    with open(self.source_path_dict["EMHIRES"] / "availability_onshore.pickle", "rb") as input_file:
                        availability_onshore = pickle.load(input_file)
                else:
                    availability_onshore = pd.read_excel(self.source_path_dict["EMHIRES"] / "EMHIRES_WIND_COUNTRY_only2015_June2019.xlsx").set_index("Date")
                    # rename Greece
                    availability_onshore.rename(columns = {"EL":"GR"},inplace=True)
                    # set to year 2017 to match timeseries of demand
                    availability_onshore.index = availability_onshore.index.map(lambda x: pd.to_datetime(x) + timedelta(days = (2017-2015)*365+1))
                    # add hours to index
                    availability_onshore.index = pd.to_datetime(availability_onshore.index) + availability_onshore["Hour"].astype("timedelta64[h]")
                    availability_onshore.index = pd.to_datetime(availability_onshore.index).strftime(correct_dateformat)
                    # only select specific countries
                    availability_onshore = availability_onshore[self.nodes.index]
                    # dump pickle
                    with open(self.source_path_dict["EMHIRES"] / "availability_onshore.pickle", "wb") as input_file:
                        pickle.dump(availability_onshore , input_file)
                availability_timeseries[self.nodes.index] = availability_onshore
                availability_timeseries["comments"] = "Source: EMHIRES_WIND_COUNTRY_June2019 (year 2015)"
            elif process == "Offshore":
                # from TS_CF_OFFSHORE_30yr_date, reduced version 
                # if already converted
                if os.path.exists(self.source_path_dict["EMHIRES"] / "availability_offshore.pickle"):
                    with open(self.source_path_dict["EMHIRES"] / "availability_offshore.pickle", "rb") as input_file:
                        availability_offshore = pickle.load(input_file)
                else:
                    availability_offshore = pd.read_csv(self.source_path_dict["EMHIRES"] / "TS_CF_OFFSHORE_30yr_date.csv").set_index("Date")
                    # set to year 2018 to match timeseries of demand
                    availability_offshore.index = availability_offshore.index.map(lambda x: pd.to_datetime(x) + timedelta(days = (2017-2015)*365+1))
                    # add hours to index
                    availability_offshore.index = pd.to_datetime(availability_offshore.index) + availability_offshore["Hour"].astype("timedelta64[h]")
                    availability_offshore.index = pd.to_datetime(availability_offshore.index).strftime(correct_dateformat)
                    # only select specific countries
                    availability_offshore = availability_offshore[list(set.intersection(set(availability_offshore.columns),set(self.nodes.index)))]
                    # dump pickle
                    with open(self.source_path_dict["EMHIRES"] / "availability_offshore.pickle", "wb") as input_file:
                        pickle.dump(availability_offshore , input_file)
                # overwrite empty entries, i.e., countries not listed in source, by capacity factor of DE
                nodes_not_in_offshore = list(set(self.nodes.index)-set(availability_offshore.columns))
                for node in nodes_not_in_offshore:
                    availability_offshore[node] = availability_offshore["DE"]
                availability_timeseries[availability_offshore.columns] = availability_offshore
                availability_timeseries["comments"] = "Source: TS_CF_OFFSHORE_30yr_date (year 2015)"
            else:
                # for other processes POTEnCIA Central Scenario 2018, Assumptions for EU28, assumption constant availability
                availability_timeseries[self.nodes.index] = lifetime_and_availability_potencia.loc[cost_index_process,"Technical availability (%)"]
                availability_timeseries["comments"] =  "Source: POTEnCIA Central Scenario 2018, constant assumptions for EU28 (does not depend on country)"

            ## existing capacity
            # create empty dataframe for existing_capacity
            existing_capacity = pd.DataFrame(columns = ["unit"] + self.construction_years + ["comments"],index = self.nodes.index)
            existing_capacity["comments"] = "Source: POTEnCIA Central Scenario 2018, JRC IDEES Database"
            existing_capacity.index.name = "node"
            for node in self.nodes.index.to_list():
                if "Central_2018_"+node in os.listdir(self.source_path_dict["Potencia"]):
                    node_potencia = node
                # name confusion GR (in demand_nodal_timeseries and demand_hourly) and EL (demand_dict_potencia)
                elif node == "GR":
                    node_potencia = "EL"
                # reduce to process
                existing_capacity_node = existing_capacity_potencia[node].loc[process,self.construction_years]
                # drop duplicates for electricity_only and CHP - keep total
                existing_capacity_node = existing_capacity_node[~existing_capacity_node.index.duplicated(keep="first")]
                # read decommissioned_capacity
                decommissioned_capacity_node = decommissioned_capacity_potencia[node].loc[process]
                # drop duplicates for electricity_only and CHP - keep total
                decommissioned_capacity_node = decommissioned_capacity_node[~decommissioned_capacity_node.index.duplicated(keep="first")].dropna(axis=1)
                # calculate difference
                difference_existing_capacity = existing_capacity_node.diff(axis=1)[self.construction_years[1:]]
                new_capacity = copy.deepcopy(difference_existing_capacity)
                new_capacity[new_capacity < 0] = 0
                decommissioned_capacity_2000_2015 = - difference_existing_capacity[difference_existing_capacity<0].sum().sum()
                existing_capacity_node[self.construction_years[1:]] = new_capacity
                existing_capacity_node[self.construction_years[0]] -= decommissioned_capacity_2000_2015
                if not self.b_late_capacity:
                    # exclude decommissioned units before first invest year
                    decommissioned_capacity_node = decommissioned_capacity_node.loc[:,(decommissioned_capacity_node.columns > self.invest_years[0])]
                    # reduce year of decommission by lifetime -> construction year
                    decommissioned_capacity_node.columns = decommissioned_capacity_node.columns-lifetime_and_availability_potencia.loc[cost_index_process,"Technical lifetime (years)"]
                    # floor to previous interval_earlier_years-th year and add capacity
                    decommissioned_capacity_node.columns = decommissioned_capacity_node.columns.map(lambda x: self.constants["interval_earlier_years"]*np.floor(x/self.constants["interval_earlier_years"]))
                    decommissioned_capacity_node = decommissioned_capacity_node.groupby(axis=1,level=0).sum()
                    decommissioned_capacity_node = decommissioned_capacity_node.loc[:,(decommissioned_capacity_node.columns < self.construction_years[0])]
                    decommissioned_capacity_node = decommissioned_capacity_node.loc[:,(decommissioned_capacity_node!=0).any()]
                    
                    if not decommissioned_capacity_node.empty:
                        # if sum of decommissioned units <= existing capacity in first year, add decommissioned units and subtract value from first existing_capacity year
                        if decommissioned_capacity_node.sum(axis=1).sum() <= existing_capacity_node[self.construction_years[0]].sum():
                            existing_capacity_node[decommissioned_capacity_node.columns] = decommissioned_capacity_node
                            existing_capacity_node[self.construction_years[0]] -= decommissioned_capacity_node.sum(axis=1)
                        else:
                            # subtract existing capacity from first construction years from decommissioned capacity
                            sum_remaining = existing_capacity_node[self.construction_years[0]]
                            # drop existing capacity from first construction year
                            existing_capacity_node.drop(self.construction_years[0],axis=1,inplace=True)
                            
                            subtraction_running = True
                            index_old_year = 0
                            while subtraction_running:
                                if (sum_remaining <= decommissioned_capacity_node[decommissioned_capacity_node.columns[index_old_year]]).values[0]:
                                    existing_capacity_node[decommissioned_capacity_node.columns[index_old_year]] = sum_remaining
                                    subtraction_running = False
                                else:
                                    existing_capacity_node[decommissioned_capacity_node.columns[index_old_year]] = decommissioned_capacity_node[decommissioned_capacity_node.columns[index_old_year]]
                                    sum_remaining -= sum(decommissioned_capacity_node[decommissioned_capacity_node.columns[index_old_year]])
                                    index_old_year += 1
                                if index_old_year == len(decommissioned_capacity_node.columns):
                                    existing_capacity_node[self.construction_years[0]] = sum_remaining
                        existing_capacity_node.sort_index(axis=1,inplace=True)
                # paste into existing_capacity
                existing_capacity.loc[node,existing_capacity_node.columns] = existing_capacity_node.loc[process]
                existing_capacity.loc[node,"unit"] = "MW"
            existing_capacity.fillna(0,inplace=True)
            existing_capacity = existing_capacity.loc[:,["unit"]+ list(np.sort(existing_capacity.columns.difference(["unit","comments"]))) +["comments"]]
            existing_capacity = existing_capacity.loc[:,(existing_capacity!=0).any()]
            construction_years_capacity = existing_capacity.columns.difference(["unit","comments"])
            # round off incrementally small capacities
            eps_small_capacity = 1e-6
            existing_capacity[construction_years_capacity] = existing_capacity[construction_years_capacity].where(~(existing_capacity[construction_years_capacity]<eps_small_capacity),0)
            # potential capacity
            potential_capacity = pd.DataFrame(columns = ["unit"] + self.invest_years + ["comments"],index = self.nodes.index)
            potential_capacity.index.name = "node"
            potential_capacity["unit"] = "MW"
            potential_capacity["comments"] = "Assumption"
            
            # solar and wind potential ENSPRESO 
            # solar photovoltaics & CSP - Important: unit is GW!
            # assumption: solar thermal (Potencia) corresponds to CSP (ENSPRESO)
            # follow assumption from paper https://doi.org/10.1016/j.esr.2019.100379 (specific power 170W/m^2 and 3% use of non-artificial land)
            if process == "Solar photovoltaics":
                potential_capacity.loc[self.nodes.index,self.invest_years] = potential_capacity_solarpv.loc[self.nodes.index]*1000 # to convert to MW
                potential_capacity["unit"] = "MW"
                potential_capacity["comments"] = "Source: ENSPRESO Solar Potential, 170 W/m^2, 3% use of non-artificial land"
            elif process == "Solar thermal":
                potential_capacity.loc[self.nodes.index,self.invest_years] = potential_capacity_solarthermal.loc[self.nodes.index]*1000 # to convert to MW
                potential_capacity["unit"] = "MW"
                potential_capacity["comments"] = "Source: ENSPRESO Solar Potential, 170 W/m^2, 3% use of non-artificial land"
            elif process == "Onshore":
                # ENSPRESO, Reference - Large Turbines, capacity factor > 20% (follow assumption from paper https://doi.org/10.1016/j.esr.2019.100379)
                potential_capacity_onshore = pd.read_excel(self.source_path_dict["ENSPRESO"] / "ENSPRESO_ONSHORE_Reference.xlsx")
                potential_capacity_onshore.set_index("[GW]",inplace=True)
                potential_capacity_onshore = potential_capacity_onshore.loc[potential_capacity_onshore.index.isin(self.nodes["country"])]
                potential_capacity_onshore = potential_capacity_onshore.reindex(self.nodes["country"])
                potential_capacity_onshore.index = self.nodes.index
                potential_capacity.loc[self.nodes.index,self.invest_years] = potential_capacity_onshore[["CF > 25%","20% < CF < 25%"]].sum(axis=1).loc[self.nodes.index]*1000 # to convert to MW
                potential_capacity["unit"] = "MW"
                potential_capacity["comments"] = "Source: ENSPRESO Wind Potential, Reference - Large Turbines, CF > 20%"
            elif process == "Offshore":
                # ENSPRESO, Reference - Large Turbines, water depth 0-60m [0-60m is own assumption] (follow assumption from paper https://doi.org/10.1016/j.esr.2019.100379)
                potential_capacity_offshore = pd.read_excel(self.source_path_dict["ENSPRESO"] / "ENSPRESO_OFFSHORE_Reference.xlsx")
                potential_capacity_offshore.set_index("[GW]",inplace=True)
                potential_capacity_offshore = potential_capacity_offshore.loc[potential_capacity_offshore.index.isin(self.nodes["country"])]
                potential_capacity_offshore = potential_capacity_offshore.reindex(self.nodes["country"])
                potential_capacity_offshore.index = self.nodes.index
                potential_capacity.loc[self.nodes.index,self.invest_years] = potential_capacity_offshore[["Water depth 30-60m","Water depth 0-30m"]].sum(axis=1).loc[self.nodes.index]*1000 # to convert to MW
                potential_capacity["unit"] = "MW"
                potential_capacity["comments"] = "Source: ENSPRESO Wind Potential, Reference - Large Turbines, Water depth 0-60m"
            elif process == "Geothermal":
                # assume it can only be held at this level
                potential_capacity.loc[self.nodes.index,self.invest_years] = existing_capacity[construction_years_capacity].sum(axis=1)
            elif process == "Nuclear":
                # Three categories: phase-out, keep, invest
                # based on nuclear energy data by OECD/NEA
                countries_nuclear =  {"AT": "phaseout", 
                        "BE": "phaseout", 
                        "BG": "phaseout", #?
                        "HR": "phaseout", #?
                        "DK": "phaseout", 
                        "EE": "phaseout", 
                        "FI": "keep", 
                        "FR": "keep",  
                        "DE": "phaseout", 
                        "GR": "phaseout", 
                        "IE": "phaseout", 
                        "IT": "phaseout", 
                        "LV": "phaseout", 
                        "LT": "phaseout", #?
                        "LU": "phaseout", 
                        "NL": "phaseout",  
                        "PL": "invest", 
                        "PT": "phaseout", 
                        "SI": "keep", 
                        "ES": "phaseout", 
                        "SE": "keep"}
                for node in self.nodes.index:
                    if countries_nuclear[node] == "phaseout":
                        # if phase-out, potential capacity is 0
                        potential_capacity.loc[node,self.invest_years] = 0
                    elif countries_nuclear[node] == "keep":
                        # if keep, potential capacity is existing capacity
                        potential_capacity.loc[node,self.invest_years] = existing_capacity.loc[node,construction_years_capacity].sum()
                    elif countries_nuclear[node] == "invest":
                        # if invest, potential capacity is infinity
                        pass

            elif process == "Run-of-river":
                # assume it can only be held at this level
                potential_capacity.loc[self.nodes.index,self.invest_years] = existing_capacity[construction_years_capacity].sum(axis=1)
            else:
                print("Potential capacity for {} is currently infinity".format(process))

            # write csv
            existing_capacity.to_csv(production_process_path / self.processes[process] / self.grid_name / "existing_capacity.csv")
            potential_capacity.to_csv(production_process_path / self.processes[process] / self.grid_name / "potential_capacity.csv")
            maximum_production_share.to_csv(production_process_path / self.processes[process] / self.grid_name / "maximum_production_share.csv")
            availability_timeseries.to_csv(production_process_path / self.processes[process] / self.grid_name / "availability_timeseries.csv")

    def create_03_PRODUCTS(self):
        """ This method creates the 03-PRODUCTS folder """
        product_list = ["electricity"]+[value for value in self.products.values() if value]
        for product in product_list:
            product_folder = product.replace(" ","_")
            # create folder
            if not os.path.exists(self.input_path_secmod / "03-PRODUCTS" / product_folder):
                os.mkdir(self.input_path_secmod / "03-PRODUCTS" / product_folder)
            # create grid folder
            if not os.path.exists(self.input_path_secmod / "03-PRODUCTS" / product_folder / self.grid_name):
                os.mkdir(self.input_path_secmod / "03-PRODUCTS" / product_folder / self.grid_name)
            # default unit
            unit = "MW"
            # set default values for required_average_nodal_secured_capacity,required_relative_nodal_secured_capacity,required_total_secured_capacity
            required_average_nodal_secured_capacity = pd.DataFrame(columns=self.invest_years+ ["comments"],index=[unit])
            required_average_nodal_secured_capacity.index.name = "unit"
            required_relative_nodal_secured_capacity = pd.DataFrame(columns=["unit","value","comments"],index=self.nodes.index.to_list())
            required_relative_nodal_secured_capacity.index.name = "node"
            required_relative_nodal_secured_capacity[["unit","value"]] = 1.0
            required_total_secured_capacity = pd.DataFrame(columns=self.invest_years+ ["comments"],index=[unit])
            required_total_secured_capacity.index.name = "unit"
            # write csv
            required_average_nodal_secured_capacity.to_csv(self.input_path_secmod / "03-PRODUCTS" / product_folder / self.grid_name / "required_average_nodal_secured_capacity.csv")
            required_relative_nodal_secured_capacity.to_csv(self.input_path_secmod / "03-PRODUCTS" / product_folder / self.grid_name / "required_relative_nodal_secured_capacity.csv")
            required_total_secured_capacity.to_csv(self.input_path_secmod / "03-PRODUCTS" / product_folder / self.grid_name / "required_total_secured_capacity.csv")
            ## demand
            # create DataFrames
            if os.path.exists(self.source_path_dict["ENTSOE"] / "timeseries_index.pickle"):
                with open(self.source_path_dict["ENTSOE"] / "timeseries_index.pickle", "rb") as input_file:
                    timeseries_index = pickle.load(input_file)
            demand_nodal_average = pd.DataFrame(columns=self.invest_years+ ["comments"],index=[unit])
            # default values
            demand_nodal_average.index.name = "unit"
            demand_nodal_average[self.invest_years] = 0.0
            demand_nodal_timeseries = pd.DataFrame(columns=["unit"]+self.nodes.index.to_list(),index=timeseries_index)
            demand_nodal_timeseries.index.name = "time slice"
            demand_nodal_timeseries.loc[:, demand_nodal_timeseries.columns != "comments"] = 1.0
            # electricity
            if product == "electricity":
                # hourly demand values from "monthly hourly load values 2018/2019" from ENTSO-E https://www.entsoe.eu/data/power-stats/
                # if already converted
                if os.path.exists(self.source_path_dict["ENTSOE"] / "demand_hourly.pickle"):
                    with open(self.source_path_dict["ENTSOE"] / "demand_hourly.pickle", "rb") as input_file:
                        demand_hourly = pickle.load(input_file)
                else:
                    demand_data_entsoe_timeslice = pd.read_excel(self.source_path_dict["ENTSOE"] / "MHLV_data-2015-2017_demand_hourly.xlsx",sheet_name = "2015-2017")
                    # correct time stamp
                    correct_dateformat = "%d.%m.%Y %H:%M"
                    demand_data_entsoe_timeslice["DateUTC"] = demand_data_entsoe_timeslice["DateUTC"].dt.strftime(correct_dateformat)
                    demand_hourly = demand_data_entsoe_timeslice[pd.to_datetime(demand_data_entsoe_timeslice["DateUTC"]).dt.year==self.constants["year_first_demand"]]
                    demand_hourly.drop(["MeasureItem","DateShort","TimeFrom","TimeTo","Value","Cov_ratio"],axis=1,inplace=True)
                    with open(self.source_path_dict["ENTSOE"] / "demand_hourly.pickle", "wb") as input_file:
                        pickle.dump(demand_hourly, input_file)
                # rearrange index and drop duplicate indices
                demand_hourly.set_index(["DateUTC","CountryCode"],inplace=True)
                demand_hourly = demand_hourly[~demand_hourly.index.duplicated()]
                demand_hourly = demand_hourly.unstack()
                demand_hourly.columns = demand_hourly.columns.droplevel(0)
                with open(self.source_path_dict["ENTSOE"] / "timeseries_index.pickle", "wb") as input_file:
                        pickle.dump(demand_hourly.index, input_file)
                missing_values = np.where(demand_hourly.isna())
                # fill missing values
                for row, col in zip(missing_values[0],missing_values[1]):
                    # if last time slice
                    if row == len(demand_hourly)-1 or np.isnan(demand_hourly.iloc[row+1,col]):
                        demand_hourly.iloc[row,col] = demand_hourly.iloc[row-1,col]
                    # if first time slice
                    elif row == 0 or np.isnan(demand_hourly.iloc[row-1,col]): 
                        demand_hourly.iloc[row,col] = demand_hourly.iloc[row+1,col]
                    # if both previous and next value not None
                    elif not np.isnan(demand_hourly.iloc[row-1,col]) and not np.isnan(demand_hourly.iloc[row+1,col]):
                        demand_hourly.iloc[row,col] = (demand_hourly.iloc[row+1,col]+demand_hourly.iloc[row-1,col])/2

                # get demand from POTEnCIA for each country
                # Potencia/Central_2018_<node>/node/Annual_reports/Central_2018_<node>_summary_yearly.xlsx sheet PowerGen
                demand_dict_potencia = {}
                for node in self.nodes.index.to_list():
                    if "Central_2018_"+node in os.listdir(self.source_path_dict["Potencia"]):
                        node_potencia = node
                    # name confusion GR (in demand_nodal_timeseries and demand_hourly) and EL (demand_dict_potencia)
                    elif node == "GR":
                        node_potencia = "EL"
                    else:
                        print("Country {} not in POTEnCIA database. Use constant ENTSO-E values".format(node))
                        continue
                    demand_scenario = pd.read_excel(self.source_path_dict["Potencia"] / ("Central_2018_"+node_potencia) / node_potencia / "Annual_reports" / ("Central_2018_"+node_potencia+"_summary_yearly.xlsx"),sheet_name = "PowerGen")
                    value_2015 = demand_scenario.loc[demand_scenario[node_potencia+" - Power generation"]==self.constants["potencia_demand_name"],self.invest_years[0]].values*self.constants["ktoe_to_MWh"]
                    try:
                        # different country codes e.g. UK and GB
                        print("Difference between ENTSO-E and POTEnCIA electricity demand values for {} for country {} is {}%".format(self.invest_years[0],node,((value_2015-demand_hourly[node].sum())/value_2015)[0]*100))
                    except KeyError:
                        print("Country {} not in ENTSO-E database.".format(node))
                    demand_scenario = demand_scenario.loc[demand_scenario[node_potencia+" - Power generation"]==self.constants["potencia_demand_name"],self.invest_years]*self.constants["ktoe_to_MWh"]
                    demand_dict_potencia[node] = {self.invest_years[0]: value_2015, "invest_years":demand_scenario}
                
                    self.index_timeseries = demand_hourly.index
                    self.index_name_timeseries = "time slice"
                    # set sum of all demands as demand_nodal_average 
                    # get correct 
                    # if b_constant_demand, assume constant demand - reference is first year
                    if self.b_constant_demand:
                        demand_nodal_average[self.invest_years]+=demand_dict_potencia[node]["invest_years"].iloc[0][self.invest_years[0]]/self.constants["hours_per_year"]
                    else:
                        demand_nodal_average[self.invest_years]+=demand_dict_potencia[node]["invest_years"].iloc[0]/self.constants["hours_per_year"]
                # set demand_nodal_timeseries
                for node in demand_nodal_timeseries:
                    # if country exists in ENTSO-E database
                    if node in demand_hourly:
                        # if country exists in POTEnCIA database
                        if node in demand_dict_potencia:
                            demand_nodal_timeseries[node] = demand_hourly[node]*demand_dict_potencia[node][self.invest_years[0]]/(demand_hourly[node].sum()*demand_nodal_average[self.invest_years[0]].values)
                        # if country does not exist in POTEnCIA database, only use ENTSO-E values
                        else:
                            demand_nodal_timeseries[node] = demand_hourly[node]/(demand_nodal_average[self.invest_years[0]].values)
                    # name confusion UK (in demand_nodal_timeseries and demand_dict_potencia) and GB (demand_hourly)
                    # is relict, since UK not in nodes anymore
                    elif node == "UK":
                        demand_nodal_timeseries[node] = demand_hourly["GB"]*demand_dict_potencia[node][self.invest_years[0]]/(demand_hourly["GB"].sum()*demand_nodal_average[self.invest_years[0]].values)

                demand_nodal_timeseries.index.name = self.index_name_timeseries
                # costs_yearly_timeseries
                costs_yearly_timeseries = pd.DataFrame(columns=["unit","value"],index=timeseries_index)
                costs_yearly_timeseries.index.name = "time slice"
                costs_yearly_timeseries.loc[:, costs_yearly_timeseries.columns != "comments"] = 1.0
                costs_yearly_timeseries.to_csv(self.input_path_secmod / "03-PRODUCTS" / product_folder  / "costs_yearly_timeseries.csv")
            
            # Potencia IntFuelPrices_Central_2018 Prices for Coal and Natural Gas - use fuel prices from PG_technology_Central_2018 for Lignite and Uranium
            # hard coal or natural_gas
            # costs data from POTEnCIA Central Scenario 2018, Assumptions for EU28, IntFuelPrices_Central_2018, assume Potencia prices
            elif product == "hard coal" or product == "natural gas":
                fuel_costs_potencia = pd.read_excel(self.source_path_dict["Potencia"] / ("Central_2018_EU28") / "EU28" / "Assumptions" / "IntFuelPrices_Central_2018_Reduced.xlsx").set_index("Year")
                if product == "hard coal":
                    product_identifier = "Coal"
                elif product == "natural gas":
                    product_identifier = "Gas"
                # extract fuel costs, convert to kiloEuro/MWh
                costs_fuel = fuel_costs_potencia.loc[self.invest_years,product_identifier]/self.constants["boe_to_MWh"]/1000
                costs_yearly_average = pd.DataFrame(columns = self.invest_years + ["comments"], index = ["kiloEuro / MWh"])
                costs_yearly_average.index.name = "unit"
                costs_yearly_average["comments"] = "Source: POTEnCIA Central Scenario 2018, Assumptions for EU28, assume POTEnCIA prices"
                costs_yearly_average.loc["kiloEuro / MWh",self.invest_years] = costs_fuel
                costs_yearly_average.to_csv(self.input_path_secmod / "03-PRODUCTS" / product_folder  / "costs_yearly_average.csv")
                # costs_yearly_timeseries
                costs_yearly_timeseries = pd.DataFrame(columns=["unit","value"],index=timeseries_index)
                costs_yearly_timeseries.index.name = "time slice"
                costs_yearly_timeseries.loc[:, costs_yearly_timeseries.columns != "comments"] = 1.0
                costs_yearly_timeseries.to_csv(self.input_path_secmod / "03-PRODUCTS" / product_folder  / "costs_yearly_timeseries.csv")
            # all other products
            else:
                fuel_costs_potencia = pd.read_excel(self.source_path_dict["Potencia"] / ("Central_2018_EU28") / "EU28" / "Assumptions" / "FuelPrices_from_PG_technology_Central_2018.xlsx").set_index("End-user prices (€2010/toe)")
                if product == "hard coal":
                    product_identifier = "Coal fired power plants"
                elif product == "natural gas":
                    product_identifier = "Gas fired power plants (Natural gas, biogas)"
                elif product == "uranium":
                    product_identifier = "Nuclear power plants"
                elif product == "lignite":
                    product_identifier = "Lignite fired power plants"
                else:
                    print("product {} not known. Skip cost calculation.".format(product))
                    continue
                # extract fuel costs, convert to kiloEuro/MWh
                costs_fuel = fuel_costs_potencia.loc[product_identifier,self.invest_years]/self.constants["ktoe_to_MWh"]
                costs_yearly_average = pd.DataFrame(columns = self.invest_years + ["comments"], index = ["kiloEuro / MWh"])
                costs_yearly_average.index.name = "unit"
                costs_yearly_average["comments"] = "Source: POTEnCIA Central Scenario 2018, Assumptions for EU28, assume POTEnCIA prices"
                costs_yearly_average.loc["kiloEuro / MWh",self.invest_years] = costs_fuel
                costs_yearly_average.to_csv(self.input_path_secmod / "03-PRODUCTS" / product_folder  / "costs_yearly_average.csv")
                # costs_yearly_timeseries
                costs_yearly_timeseries = pd.DataFrame(columns=["unit","value"],index=timeseries_index)
                costs_yearly_timeseries.index.name = "time slice"
                costs_yearly_timeseries.loc[:, costs_yearly_timeseries.columns != "comments"] = 1.0
                costs_yearly_timeseries.to_csv(self.input_path_secmod / "03-PRODUCTS" / product_folder  / "costs_yearly_timeseries.csv")
            # write csv
            demand_nodal_average.to_csv(self.input_path_secmod / "03-PRODUCTS" / product_folder / self.grid_name / "demand_nodal_average.csv")
            demand_nodal_timeseries.to_csv(self.input_path_secmod / "03-PRODUCTS" / product_folder / self.grid_name / "demand_nodal_timeseries.csv")

    def create_04_ECOINVENT(self):
        """ This method creates the 04-ECOINVENT folder """
        # change 2016 to 2015
        ecoinvent_path = self.input_path_secmod / "04-ECOINVENT"
        subassemblies = "00-ECOINVENT-SUBASSEMBLIES"
        for subassembly in os.listdir(ecoinvent_path / subassemblies):
            if subassembly != "README.md" and not os.path.isdir(ecoinvent_path / subassemblies / subassembly):
                subassembly_csv = pd.read_csv(ecoinvent_path / subassemblies / subassembly).set_index("process_name")
                if "2016" in subassembly_csv.columns:
                    subassembly_csv.rename(columns = {"2016": "2015"},inplace = True)
                subassembly_csv.to_csv(ecoinvent_path / subassemblies / subassembly)
                # create ecoinvent subassembly for each node
                # nodal subassemblies currently not used in optimization
                if self.b_create_nodal_subassemblies:
                    for node in self.nodes.index:
                        locations_node = [node]
                        locations_node.extend(self.locations)
                        # get proxy for subassembly ecoinvent processes
                        [subassembly_csv_node,self.logfile] = update_ecoinvent_processes.check_all_ecoinvent_processes_of_secmod_process("subassembly","subassembly","subassembly",subassembly_csv,self.ecoinvent_processes_unique,locations_node,self.logfile)        
                        # write csvs for node
                        # create node folder in grid folder
                        if not os.path.exists(ecoinvent_path / subassemblies / self.grid_name):
                            os.mkdir(ecoinvent_path / subassemblies / self.grid_name)
                        
                        if not os.path.exists(ecoinvent_path / subassemblies / self.grid_name / node):
                            os.mkdir(ecoinvent_path / subassemblies / self.grid_name / node)
                        subassembly_csv_node.to_csv(ecoinvent_path / subassemblies / self.grid_name / node / subassembly)


    def create_05_IMPACT_CATEGORIES(self):
        """ This method creates the 05-IMPACT-CATEGORIES folder """
        for impact in os.listdir(self.input_path_secmod / "05-IMPACT-CATEGORIES"):
            if impact == "README.md":
                continue
            # create grid folder
            if not os.path.exists(self.input_path_secmod / "05-IMPACT-CATEGORIES" / impact / self.grid_name):
                os.mkdir(self.input_path_secmod / "05-IMPACT-CATEGORIES" / impact / self.grid_name)
            if impact == "cost":
                unit = "kiloEuro"
            elif impact == "ILCD_2.0_2018_midpoint_climate_change_climate_change_total":
                unit = "kg CO2_eq"
            # setup empty dataframes
            invest_impact_limits = pd.DataFrame(columns = self.invest_years+ ["comments"],index=[unit])
            invest_impact_limits.index.name = "unit"
            operational_impact_limits = pd.DataFrame(columns = self.invest_years+ ["comments"],index=[unit])
            operational_impact_limits.index.name = "unit"
            total_impact_limits = pd.DataFrame(columns = self.invest_years+ ["comments"],index=[unit])
            total_impact_limits.index.name = "unit"
            invest_nodal_impact_limits = pd.DataFrame(columns = ["unit"] + self.invest_years+ ["comments"],index=self.nodes.index)
            invest_nodal_impact_limits["unit"] = unit 
            invest_nodal_impact_limits.index.name = "node"
            operational_nodal_impact_limits = pd.DataFrame(columns = ["unit"] + self.invest_years+ ["comments"],index=self.nodes.index)
            operational_nodal_impact_limits["unit"] = unit
            operational_nodal_impact_limits.index.name = "node"
            total_nodal_impact_limits = pd.DataFrame(columns = ["unit"] + self.invest_years+ ["comments"],index=self.nodes.index)
            total_nodal_impact_limits["unit"] = unit
            total_nodal_impact_limits.index.name = "node"
            # if GHG, set operational_impact_limit
            if impact == "ILCD_2.0_2018_midpoint_climate_change_climate_change_total":
                # Approach: get measured emissions from power generation sector for 2015 from IDEES Database for each country
                # add up and use as limit for first year
                # reduce until -85% in 2050 (in comparison to 1990)
                # IDEES Data start in 2000 -> calculate reduction factor 1990 -> 2000 from European environment agency
                GHG_emissions_start = 0
                GHG_emissions_2000 = 0
                year_start = self.invest_years[0]
                year_end = self.invest_years[-1]
                # sum GHG emissions
                for node in self.nodes.index:
                    if node == "GR":
                        node_idees = "EL"
                    else:
                        node_idees = node
                    GHG_emissions_node = pd.read_excel(self.source_path_dict["JRC_IDEES_2015"] / ("JRC-IDEES-2015_All_xlsx_"+node_idees) / ("JRC-IDEES-2015_EmissionBalance_"+node_idees + ".xlsx"),sheet_name = "TITOT").set_index("Transformation input (kt CO2)")
                    GHG_emissions_start += GHG_emissions_node.loc[["Solid Fuels","Natural gas"],year_start].sum()
                    GHG_emissions_2000 += GHG_emissions_node.loc[["Solid Fuels","Natural gas"],2000].sum()
                # calculate reduction factor from 1990 - 2000, used if not to net zero but -85% until 2050
                # convert from kt_CO2_eq to kg_CO2_eq
                GHG_emissions_start *= 1e6
                GHG_emissions_hist = pd.read_csv(self.source_path_dict["JRC_IDEES_2015"] / "total-ghg-emissions-1.csv").set_index("Year")
                reduction_1990_2000 = GHG_emissions_hist.loc[2000,"EU-28 (GHG inventory scope under UNFCCC)"]/GHG_emissions_hist.loc[1990,"EU-28 (GHG inventory scope under UNFCCC)"]
                reduction_factor = self.constants["GHG_reduction"]*reduction_1990_2000
                    
                # if net zero
                if self.b_net_zero:
                    GHG_emissions_end = 0
                    operational_impact_limits["comments"] = "Source: measured emissions for 2015 from IDEES Database. Reduction to net-zero until 2050"
                else:
                    GHG_emissions_end = GHG_emissions_2000*(1-reduction_factor)*1e6
                    operational_impact_limits["comments"] = "Source: measured emissions for 2015 from IDEES Database. Reduction of 85% from 1990 - 2050"
                # linearly decrease until GHG_emissions_end in 2050
                operational_impact_limits.loc[unit,self.invest_years] = list(map(lambda x: GHG_emissions_start+(GHG_emissions_end-GHG_emissions_start)
                    *(x-year_start)/(year_end-year_start),self.invest_years))
                
                # objective factor operational impact overshoot  
                objective_factor_impact_overshot = pd.DataFrame(columns = self.invest_years+ ["comments"],index=["Euro * (kg CO2_eq) ** (-1)"])
                objective_factor_impact_overshot.index.name = "unit"
                objective_factor_impact_overshot.loc["Euro * (kg CO2_eq) ** (-1)",self.invest_years] = [self.constants["base_carbon_penalty"] * self.constants["inflation_rate"] ** (year - self.constants["base_year_inflation"]) for year in self.invest_years]
                objective_factor_impact_overshot.loc["Euro * (kg CO2_eq) ** (-1)","comments"] = "Source: EU ETS, 100 €/t_CO_2-eq in 2013, increasing with 3\% inflation."
                objective_factor_impact_overshot.to_csv(self.input_path_secmod / "05-IMPACT-CATEGORIES" / impact / "objective_factor_impact_overshot.csv")

            # write csv
            invest_impact_limits.to_csv(self.input_path_secmod / "05-IMPACT-CATEGORIES" / impact / self.grid_name / "invest_impact_limits.csv")
            invest_nodal_impact_limits.to_csv(self.input_path_secmod / "05-IMPACT-CATEGORIES" / impact / self.grid_name / "invest_nodal_impact_limits.csv")
            operational_impact_limits.to_csv(self.input_path_secmod / "05-IMPACT-CATEGORIES" / impact / self.grid_name / "operational_impact_limits.csv")
            operational_nodal_impact_limits.to_csv(self.input_path_secmod / "05-IMPACT-CATEGORIES" / impact / self.grid_name / "operational_nodal_impact_limits.csv")
            total_impact_limits.to_csv(self.input_path_secmod / "05-IMPACT-CATEGORIES" / impact / self.grid_name / "total_impact_limits.csv")
            total_nodal_impact_limits.to_csv(self.input_path_secmod / "05-IMPACT-CATEGORIES" / impact / self.grid_name / "total_nodal_impact_limits.csv")
        
    def create_EU_grid(self):
        """ This method starts the creation of the EU Grid """
        print("Start Creation of EU Grid")
        # get paths of necessary folders
        top_folder = Path.cwd()

        # set constants
        self.set_constants()
        # set scope of model (countries, technologies, ...)
        self.set_mapped_sets()
        # paths
        self.input_path_secmod = top_folder / "Working_Directories" / self.folder_name / "SecMOD" / "00-INPUT" / "00-RAW-INPUT"
        self.input_path_secmod_reference = top_folder / "Working_Directories" / "Dena18_Grid" / "SecMOD" / "00-INPUT" / "00-RAW-INPUT"
        self.source_path = top_folder / "EU_Model_Input_Data"
        # source folders
        self.source_path_dict = {}
        self.source_path_dict["dynELMOD"] = self.source_path / "dynELMOD"
        self.source_path_dict["ENTSOE"] = self.source_path / "ENTSOE"
        self.source_path_dict["ENSPRESO"] = self.source_path / "ENSPRESO"
        self.source_path_dict["JRC_IDEES_2015"] = self.source_path / "JRC_IDEES_2015"
        self.source_path_dict["JRC-EU-TIMES_2019_11"] = self.source_path / "JRC-EU-TIMES_2019_11"
        self.source_path_dict["Potencia"] = self.source_path / "Potencia"
        self.source_path_dict["PyPSA"] = self.source_path / "PyPSA"
        self.source_path_dict["OPSD"] = self.source_path / "OPSD"
        self.source_path_dict["EMHIRES"] = self.source_path / "EMHIRES"
        self.source_path_dict["EUROSTAT"] = self.source_path / "Eurostat"
        # name convention for grid
        self.grid_name = "EU"
        # load ecoinvent data
        print("Load ecoinvent data")
        self.load_ecoinvent_data()
        # create 01-GRID
        if self.create_grid:
            print("Create 01-GRID")
            self.create_01_GRID()
        # create 02-PROCESSES
        if self.create_processes:
            print("Create 02-PROCESSES")
            self.create_02_PROCESSES()
        # create 03-PRODUCTS
        if self.create_products:
            print("Create 03-PRODUCTS")
            self.create_03_PRODUCTS()
        # create 04-ECOINVENT
        if self.create_ecoinvent:
            print("Create 04-ECOINVENT")
            self.create_04_ECOINVENT()
        # create 05-IMPACT-CATEGORIES
        if self.create_impact_categories:
            print("Create 05-IMPACT-CATEGORIES")
            self.create_05_IMPACT_CATEGORIES()
        # write ecoinvent logfile
        if self.create_processes and self.create_ecoinvent:
            self.logfile.to_excel(self.input_path_secmod / "logfile_ecoinvent.xlsx")
        elif self.create_processes:
            self.logfile.to_excel(self.input_path_secmod / "logfile_ecoinvent_processes.xlsx")
        else:
            self.logfile.to_excel(self.input_path_secmod / "logfile_ecoinvent_subassemblies.xlsx")
        # finished
        print("Creation of EU Grid finished")

    def set_constants(self):
        """ this method defines constants and options"""
        # constants
        self.constants = {}
        self.constants["ktoe_to_MWh"] = 11630 # 1 ktoe = 11630 MWh
        self.constants["boe_to_MWh"] = 1.6994064444444 # 1 boe = 1.6994064444444 MWh
        self.constants["hours_per_year"] = 8760
        self.constants["year_first_demand"] = 2017
        self.constants["interval_earlier_years"] = 10
        self.constants["default_connection_length"] = 25
        self.constants["multiplication_factor_NTC"] = 1 # multiply NTC with multiplication_factor_NTC to account for higher NTC
        self.constants["COP_heat_pump"] = 2.51 # COP of DAC process, Deutz.2021
        self.constants["GHG_reduction"] = 0.85
        self.constants["inflation_rate"] = 1.03
        self.constants["base_year_inflation"] = 2013
        self.constants["base_carbon_penalty"] = 0.100 # €/kg_CO_2-eq
        
        # POTEnCIA demand name
        # self.constants["potencia_demand_name"] = "Electricity supply"
        self.constants["potencia_demand_name"] = "Final energy demand"
        # booleans
        # if self.b_late_capacity == True, existing capacities are only built between 2000 and 2005
        if "late_capa" in self.folder_name:
            self.b_late_capacity = True
        else:
            self.b_late_capacity = False
        # if True, use real transmission capacities
        self.b_use_real_transmission = False # Keep on False, True does not work yet
        # if True, use switzerlands transmission capacities
        self.b_use_switzerlands_capacities = True
        # if True, include non-fuel related operational costs
        self.b_use_non_fuel_operational_costs = True
        # if True, calculate eurostat efficiencies
        self.b_use_eurostat_efficiencies = True
        # if True, constant demand
        self.b_constant_demand = True
        # if True, manual net-zero goal
        self.b_net_zero_manual = True
        # net-zero goal, only if DAC included or self.b_net_zero_manual == True
        if "DAC" in self.folder_name or self.b_net_zero_manual:
            self.b_net_zero = True
        else:
            self.b_net_zero = False
        # if True, create nodal specific subassemblies
        self.b_create_nodal_subassemblies = False
        
def main():
    """ This is the main function to create EU_grid """
    # set folder name
    folder_name = "EU_Grid_DAC"
    # enable or disable creation scripts. As for now, create_grid must be set to True
    EU_Grid = Grid(folder_name = folder_name,create_grid = True, create_processes = True,create_products = True,create_ecoinvent = True ,create_impact_categories = True)

if __name__ == "__main__":
    main()