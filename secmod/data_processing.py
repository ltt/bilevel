from pathlib import Path
from secmod.classes import units, Grid, ImpactCategory, Product, ProcessImpacts, EcoinventImpacts, Process, ProductionProcess, ProductionProcessEcoinvent, TransmissionProcess, TransmissionProcessEcoinvent, convert_quantity_to_correct_unit
#from secmod.classes import StorageProcess, StorageProcessEcoinvent, TransshipmentProcess, TransshipmentProcessEcoinvent # storage and transshipment processes are currently not used in the bilevel optimization
from secmod.optimization_BO_SDE import Optimization
import secmod.time_series_aggregation as tsa
import os
import importlib.util
import logging
import pandas as pd
import pickle
import json
import copy
from clint.textui import progress

# Load config.py
spec = importlib.util.spec_from_file_location("config", "SecMOD/00-INPUT/00-RAW-INPUT/config.py")
config = importlib.util.module_from_spec(spec)
spec.loader.exec_module(config)

time_slices = pd.DataFrame()
target_units = json.load(open("SecMOD/00-INPUT/00-RAW-INPUT/01-UNITS-TARGET-OPTIMIZATION.json", 'r'))

def load_raw_input(raw_input_directory: Path):
    """Loads all data from raw input.

    Args:
        raw_input_directory (Path): Path to the raw input directory
    """

    global time_slices
    global target_units

    # Load ecoinvent database
    EcoinventImpacts.load_ecoinvent_database(raw_input_directory / "04-ECOINVENT" / "ecoinvent.csv", raw_input_directory / "04-ECOINVENT" / "ecoinvent_units_to_change.json")
    # Load ecoinvent subassemblies
    EcoinventImpacts.load_ecoinvent_subassemblies(raw_input_directory / "04-ECOINVENT" / "00-ECOINVENT-SUBASSEMBLIES")
    # Load all grids and select the one from config
    Grid.load_grids_from_directory(raw_input_directory / "01-GRID")
    for grid in Grid.grids:
        if grid.name == config.grid_name:
            grid.select()
    # Load all impact categories
    ImpactCategory.load_impact_categories_from_directory(raw_input_directory / "05-IMPACT-CATEGORIES")
    # Load all products
    Product.load_products_from_directory(raw_input_directory / "03-PRODUCTS")
    # Load all processes
    load_all_processes(raw_input_directory / "02-PROCESSES")
    # Load time slices
    if not (hasattr(config,'time_series_aggregation_config') and config.time_series_aggregation_config["use_time_series_aggregation"]):
        time_slices = pd.read_csv(raw_input_directory / "06-AGGREGATED-TIMESERIES" / "time_slices.csv", index_col="time_slice")

def load_all_processes(processes_directory: Path):
    """Loads all processes in their corresponding class.

    Args:
        raw_input_directory (Path): Path to the raw input directory
    """

    logging.info("Load processes")
    directories = os.listdir(processes_directory)
    directories = [processes_directory / directory for directory in directories if (processes_directory / directory).is_dir()]

    for directory in directories:
        subdirectories = os.listdir(directory)
        subdirectories = [directory / subdirectory for subdirectory in subdirectories if (directory / subdirectory).is_dir()]
        
        for subdirectory in subdirectories:
            if subdirectory.name.upper() == "01-ECOINVENT-BASED":
                if directory.name.upper() == "01-PRODUCTION":
                    ProductionProcessEcoinvent.load_processes_from_directory(subdirectory)
                #elif directory.name.upper() == "02-STORAGE":
                #    StorageProcessEcoinvent.load_processes_from_directory(subdirectory)
                #elif directory.name.upper() == "03-TRANSSHIPMENT":
                #    TransshipmentProcessEcoinvent.load_processes_from_directory(subdirectory)
                elif directory.name.upper() == "04-TRANSMISSION":
                    TransmissionProcessEcoinvent.load_processes_from_directory(subdirectory)

    logging.info("All processes loaded")

def generate_input_dictionary(computed_input_path: Path, load: bool = False):
    """Takes all loaded data and generates an input dictionary for the optimization model.

    Args:
        computed_input_path (Path): Path to an already computed input directory for reading
        load (bool), default False: specifies whether an existing input dictionary will be loaded (True) or an input dictionary will be computed (False)
	
    Returns:
        input_dictionary (Dict): returns a Dictionary with all the necessary data for optimization
    """ 
    
    if load and computed_input_path.exists():
        logging.info("Load existing input dictionary...")
        with open(computed_input_path / "input-{0}.pickle".format(str(min(ProcessImpacts.invest_years))), 'rb') as input_file:
            input_dictionary = pickle.load(input_file)
    else:

        logging.info("Start generation of input dictionary...")
        input_dictionary = {}

        input_dictionary["Big_M_primal_dual"]           = {None: ["primal","dual"]}
        logging.info("Setup nodes")
        input_dictionary["nodes"]                       = {None: Grid.selected().get_list_of_node_ids()}
        logging.info("Setup connections")
        input_dictionary["connections"]                 = {None: Grid.selected().get_list_of_connection_ids()}
        logging.info("Setup connection nodes")
        input_dictionary["connection_nodes"]            = Grid.CONNECTION_NODES
        connected_nodes_names                           = Grid.selected().connections[Grid.CONNECTION_NODES[None]].stack()
        input_dictionary["connected_nodes"]             = connected_nodes_names.map(lambda x: Grid.selected().get_list_of_node_ids().index(x) + 1).to_dict() # pyomo sets are 1-indexed
        input_dictionary["connection_length"]           = Grid.selected()._connections["distance"].to_dict()
        logging.info("Setup products")
        input_dictionary["products"]                    = {None: [product.name for product in Product.products]}
        logging.info("Setup process list production")
        input_dictionary["processes_production"]        = {None: [process.name for process in    ProductionProcess.processes]}
        #logging.info("Setup process list storage")
        #input_dictionary["processes_storage"]           = {None: [process.name for process in       StorageProcess.processes]}
        #logging.info("Setup process list transshipment")
        #input_dictionary["processes_transshipment"]     = {None: [process.name for process in TransshipmentProcess.processes]}
        logging.info("Setup process list transmission")
        input_dictionary["processes_transmission"]      = {None: [process.name for process in  TransmissionProcess.processes]}

        logging.info("Setup constant factors")
        #input_dictionary["directions_transshipment"]    = TransshipmentProcess.DIRECTIONS_TRANSSHIPMENT
        #input_dictionary["factor_transshipment"]        = TransshipmentProcess.DIRECTION_FACTOR_TRANSSHIPMENT
        #input_dictionary["directions_storage"]          = StorageProcess.DIRECTIONS_STORAGE
        #input_dictionary["storage_level_factor"]        = StorageProcess.STORAGE_LEVEL_FACTOR
        input_dictionary["products_transmission"]       = TransmissionProcess.PRODUCTS

        logging.info("Setup years")
        input_dictionary["years"]                           = {None:       ProcessImpacts.invest_years}

        logging.info("Setup existing capacity")
        existing_capacity                                   =   ProductionProcess.get_combined_existing_capacity()
        input_dictionary["existing_capacity_production"]    =   existing_capacity.to_dict()
        existing_capacity_centralized                       =   ProductionProcess.get_combined_existing_capacity_centralized()
        input_dictionary["existing_capacity_production_centralized"]    =   existing_capacity_centralized.to_dict()
        ProductionProcess.setup_construction_years(existing_capacity)
        #existing_capacity                                   =      StorageProcess.get_combined_existing_capacity()
        #input_dictionary["existing_capacity_storage"]       =   existing_capacity.to_dict()
        #StorageProcess.setup_construction_years(existing_capacity)
        #existing_capacity                                   =   TransshipmentProcess.get_combined_existing_capacity()
        #input_dictionary["existing_capacity_transshipment"] =   existing_capacity.to_dict()
        #TransshipmentProcess.setup_construction_years(existing_capacity)
        existing_capacity                                   =   TransmissionProcess.get_combined_existing_capacity()
        input_dictionary["existing_capacity_transmission"]  =   existing_capacity.to_dict()
        TransmissionProcess.setup_construction_years(existing_capacity)

        logging.info("Setup construction years production")
        input_dictionary["construction_years_production"]       = {None: ProductionProcess.construction_years}
        #logging.info("Setup construction years storage")
        #input_dictionary["construction_years_storage"]          = {None: StorageProcess.construction_years}
        #logging.info("Setup construction years transshipment")
        #input_dictionary["construction_years_transshipment"]    = {None: TransshipmentProcess.construction_years}
        logging.info("Setup construction years transmission")
        input_dictionary["construction_years_transmission"]     = {None: TransmissionProcess.construction_years}

        logging.info("Setup lifetime duration production")
        input_dictionary["lifetime_duration_production"]        = ProductionProcess.get_combined_lifetime_duration()[0].to_dict()
        #logging.info("Setup lifetime duration storage")
        #input_dictionary["lifetime_duration_storage"]           = StorageProcess.get_combined_lifetime_duration()[0].to_dict()
        #logging.info("Setup lifetime duration transshipment")
        #input_dictionary["lifetime_duration_transshipment"]     = TransshipmentProcess.get_combined_lifetime_duration()[0].to_dict()
        logging.info("Setup lifetime duration transmission")
        input_dictionary["lifetime_duration_transmission"]      = TransmissionProcess.get_combined_lifetime_duration()[0].to_dict()

        logging.info("Setup impact categories and sources")
        input_dictionary["impact_categories"]               = {None: ImpactCategory.get_list_of_names_of_active_impact_categories()}
        input_dictionary["impact_sources"]                  = ProcessImpacts.IMPACT_SOURCES
        
        logging.info("Setup technology matrices")
        input_dictionary["technology_matrix_production"]    = ProductionProcess.get_combined_technology_matrix(input_dictionary["products"][None]).to_dict()
        #input_dictionary["efficiency_matrix_storage"]       = StorageProcess.get_combined_technology_matrix(input_dictionary["products"][None]).to_dict()
        
        #logging.info("Setup stored products")
        #input_dictionary["storage_products"]                = StorageProcess.get_combined_storage_products(input_dictionary["products"][None])
        
        #logging.info("Setup transshipment parameters")
        #input_dictionary["transshipment_efficiency"]        = TransshipmentProcess.get_combined_transshipment_efficiency()["efficiency"].to_dict()
        #input_dictionary["transshipment_products"]          = TransshipmentProcess.get_combined_transhipment_products()
        
        logging.info("Setup power transmission properties")
        input_dictionary["power_limit_per_circuit"]         = TransmissionProcess.get_combined_power_limit_per_circuit()
        input_dictionary["power_line_property_categories"]  = {None: ["power limit", "susceptance per unit"]}
        input_dictionary["power_line_properties"]           = TransmissionProcess.get_connection_properties()[["power limit", "susceptance per unit"]].stack().to_dict()
        input_dictionary["transmission_reference_node"]     = {None: "DE"}
        input_dictionary["per_unit_base"]                   = {None: TransmissionProcess.per_unit_base}

        logging.info("Setup impact matrices")
        input_dictionary["impact_matrix_production"]        = ProductionProcess.get_combined_impact_matrix().to_dict()[0]
        #input_dictionary["impact_matrix_storage"]           = StorageProcess.get_combined_impact_matrix().to_dict()[0]
        #input_dictionary["impact_matrix_transshipment"]     = TransshipmentProcess.get_combined_impact_matrix().to_dict()[0]
        input_dictionary["impact_matrix_transmission"]      = TransmissionProcess.get_combined_impact_matrix().to_dict()[0]

        logging.info("Setup potential capacity")
        input_dictionary["potential_capacity_production"]       = ProductionProcess.get_combined_potential_capacity().to_dict()
        #input_dictionary["potential_capacity_storage"]          = StorageProcess.get_combined_potential_capacity().to_dict()
        #input_dictionary["potential_capacity_transshipment"]    = TransshipmentProcess.get_combined_potential_capacity().to_dict()
        input_dictionary["potential_capacity_transmission"]     = TransmissionProcess.get_combined_potential_capacity().to_dict()

        logging.info("Setup transmission reliability margins")
        input_dictionary["reliability_margin_transmission"]     = TransmissionProcess.get_combined_safety_margin()

        #logging.info("Setup the flow to storage capacity factor for storages")
        #input_dictionary["flow_to_storage_capacity_factor"]     = StorageProcess.get_combined_flow_to_storage_capacity_factor().to_dict()  

        input_dictionary["operational_nodal_impact_limits"]     = ImpactCategory.get_combined_operational_nodal_impact_limits().to_dict()
        input_dictionary["operational_impact_limits"]           = ImpactCategory.get_combined_operational_impact_limits().to_dict()
        input_dictionary["invest_nodal_impact_limits"]          = ImpactCategory.get_combined_invest_nodal_impact_limits().to_dict()
        input_dictionary["invest_impact_limits"]                = ImpactCategory.get_combined_invest_impact_limits().to_dict()
        input_dictionary["total_nodal_impact_limits"]           = ImpactCategory.get_combined_total_nodal_impact_limits().to_dict()
        input_dictionary["total_impact_limits"]                 = ImpactCategory.get_combined_total_impact_limits().to_dict()

        input_dictionary["objective_factor_impacts"]            = ImpactCategory.get_combined_objective_factor_impact().to_dict()
        input_dictionary["objective_factor_impact_overshoot"]   = ImpactCategory.get_combined_objective_factor_impact_overshoot().to_dict()

        input_dictionary["secured_capacity_factors_production"] = ProductionProcess.get_combined_secured_capacity_factor().to_dict()
        #input_dictionary["secured_capacity_factors_storage"]    = StorageProcess.get_combined_secured_capacity_factor().to_dict()
        input_dictionary["required_total_secured_capacity"]     = Product.get_combined_required_total_secured_capacity().to_dict()
        input_dictionary["required_nodal_secured_capacity"]     = Product.get_combined_required_nodal_secured_capacity().to_dict()
        
        logging.info("Get target Units of Processes, Products and Impacts")
        input_dictionary["target_units"]={}
        input_dictionary["target_units"]["processes"]           = Process.get_target_units()
        input_dictionary["target_units"]["products"]            = Product.get_target_units()
        input_dictionary["target_units"]["impacts"]             = ImpactCategory.get_target_units()

        # bilevel optimization
        input_dictionary["traded_products"]                     = {None: ["electricity"]}
        input_dictionary["capped_impacts"]                     = {None: [impact for impact in ImpactCategory.get_list_of_names_of_active_impact_categories() if impact != "cost"]}
        # binary expansion
        input_dictionary["sampling_points_binary_expansion"]    = {None: range(config.bilevel_options["sampling_points_binary_expansion"] + 1)}
        input_dictionary["range_binary_expansion"]              = {None: 2**config.bilevel_options["sampling_points_binary_expansion"]}
        input_dictionary["surplus_capacity_factor_production"]  = {None: 1}
        
        # select if timeseries aggregation is used
        logging.info("Setup time series aggregation dependent variables")
        if hasattr(config,'time_series_aggregation_config') and config.time_series_aggregation_config["use_time_series_aggregation"]:
            time_series_data = tsa.get_time_series_data()
            input_dictionary["time_slices"]                                         = {None: time_series_data["aggregated_time_slices"]["time_slice"]}
            input_dictionary["time_slice_yearly_weight"]                            = time_series_data["aggregated_time_slices"]["time_slice_yearly_weights"]
            input_dictionary["time_slice_duration"]                                 = time_series_data["aggregated_time_slices"]["time_slice_duration"]
            input_dictionary["next_time_slice"]                                     = time_series_data["aggregated_time_slices"]["next_time_slice"]
            input_dictionary["time_series_aggregation_info"]                        = {"cluster_order": None, "hours_per_period": None, "no_typical_periods": None}
            input_dictionary["time_series_aggregation_info"]["cluster_order"]       = time_series_data['aggregation'].clusterOrder
            input_dictionary["time_series_aggregation_info"]["hours_per_period"]    = time_series_data['aggregation'].hoursPerPeriod
            input_dictionary["time_series_aggregation_info"]["no_typical_periods"]  = time_series_data['aggregation'].noTypicalPeriods

            input_dictionary["technology_matrix_production_timeseries"]             = time_series_data["aggregated_time_series"]["technologymatrix"].to_dict()
            input_dictionary["usable_capacity_factor_timeseries_production"]        = time_series_data["aggregated_time_series"]["availability"].to_dict()
            input_dictionary["impact_matrix_non_served_demand"]                     = time_series_data["aggregated_time_series"]["impact_non_served_demand"].to_dict()
            input_dictionary["demand"]                                              = time_series_data["aggregated_time_series"]["demand"].to_dict()
        else:
            input_dictionary["time_slices"]                 = {None: list(time_slices.index)}
            input_dictionary["time_slice_yearly_weight"]    = time_slices["yearly_hours"].to_dict()
            input_dictionary["time_slice_duration"]         = time_slices["duration"].to_dict()
            input_dictionary["next_time_slice"]             = time_slices["next_time_slice"].to_dict()

            input_dictionary["technology_matrix_production_timeseries"]             = ProductionProcess.get_combined_technology_matrix_timeseries().to_dict()["value"]
            input_dictionary["usable_capacity_factor_timeseries_production"]        = ProductionProcess.get_combined_availability_timeseries().to_dict()["value"] 
            input_dictionary["impact_matrix_non_served_demand"]                     = Product.get_combined_impact_non_served_demand().to_dict()[0]
            input_dictionary["demand"]                                              = Product.get_combined_demand_time_series().to_dict()
        logging.info("Generation of input dictionary finished.")

        logging.info("Store input dictionary in {0}".format(computed_input_path / "input-{0}.pickle".format(str(min(ProcessImpacts.invest_years)))))
        if not os.path.exists(computed_input_path):
            os.mkdir(computed_input_path)

        with open(computed_input_path / "input-{0}.pickle".format(str(min(ProcessImpacts.invest_years))), 'wb') as input_file:
            pickle.dump(input_dictionary, input_file, protocol=pickle.HIGHEST_PROTOCOL)

        with open(computed_input_path / "Dimensions-{0}.json".format(str(min(ProcessImpacts.invest_years))), "w") as dimensions:
            json.dump(get_dimensionalities_from_input_dictionary(input_dictionary), dimensions)

    return {None: input_dictionary}


def get_dimensionalities_from_input_dictionary(input_dictionary: dict):
    """Returns a dictionary with all unique dimensionalities and an example unit.

    Args:
        input_dictionary (Dict): a Dictionary with all mandatory inputs
    
    Returns:
        dimensions (Dict): returns a Dictionary with dimensions used in the Input Dictionary
    """

    dimensions = {}
    example_unit = units("50 MW")
    for dictionary in input_dictionary:
        for element in input_dictionary[dictionary]:
            if type(input_dictionary[dictionary][element]) == type(example_unit):
                dimensions[str(input_dictionary[dictionary][element].dimensionality)] = str(input_dictionary[dictionary][element].units)
    
    return dimensions

def add_new_capacity_from_results(optimization_model: Optimization, years: list = None):
    """Add the returned new capacity from the optimization to the existing capacity in the data.

    Args:
        optimization_model (Optimization): Instance of optimization class with all the computed results
        years (list), default None: a list with all years considered in the investment period 
    """ 

    logging.info("Add new capacity to processes from optimization")
    if years == None:
        years = [min(optimization_model.model_instance.years.ordered_data())]
    new_capacity = optimization_model.get_new_capacity_from_bilevel_result()

    idx = pd.IndexSlice
    for year in progress.bar(years):
        logging.info("Add new capacity from {0} to production processes".format(year))
        for process in progress.bar(ProductionProcess.processes):
            multiplication_factor = convert_quantity_to_correct_unit(process._existing_capacity.loc[Grid.get_list_of_node_ids()[0],"unit"])
            process.add_capacity(year, new_capacity["production"].loc[idx[:,process.name],year].sum(level='node') / multiplication_factor)
                
    # Reset construction years of all process classes
    ProductionProcess.construction_years = []
    #StorageProcess.construction_years = []
    #TransshipmentProcess.construction_years = []
    TransmissionProcess.construction_years = []

def add_new_capacity_centralized_from_results(optimization_model: Optimization, years: list = None):
    """Add the returned new capacity from the optimization to the existing centralized capacity in the data.

    Args:
        optimization_model (Optimization): Instance of optimization class with all the computed results
        years (list), default None: a list with all years considered in the investment period 
    """ 
    
    logging.info("Add new centralized capacity to processes from optimization")
    if years == None:
        years = [min(optimization_model.model_instance.years.ordered_data())]
    new_capacity = optimization_model.get_new_capacity_from_result()

    idx = pd.IndexSlice
    for year in progress.bar(years):
        logging.info("Add new centralized capacity from {0} to production processes".format(year))
        for process in progress.bar(ProductionProcess.processes):
            multiplication_factor = convert_quantity_to_correct_unit(process._existing_capacity_centralized.loc[Grid.get_list_of_node_ids()[0],"unit"])
            # for node in Grid.selected().get_list_of_node_ids():
            process.add_capacity_centralized(year, new_capacity["production"].loc[idx[:,process.name],year].sum(level='node') / multiplication_factor)
        
    # Reset construction years of all process classes
    ProductionProcess.construction_years = []
    #StorageProcess.construction_years = []
    #TransshipmentProcess.construction_years = []
    TransmissionProcess.construction_years = []
