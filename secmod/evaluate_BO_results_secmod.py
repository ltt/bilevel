from pathlib import Path
import pickle
import pyomo.environ as pe
from pyomo.core.expr import current as EXPR
from secmod.optimization_BO_SDE import Optimization as Optimization_BO
from secmod.optimization_centralized import Optimization as Optimization_centralized
import pandas as pd
import numpy as np
import os
from clint.textui import progress

import cartopy.crs as ccrs
import cartopy.feature as cf
import cartopy.io.shapereader as shpreader
import matplotlib
from matplotlib.colors import LinearSegmentedColormap as LSC
from matplotlib.colors import to_rgba_array
from matplotlib.patches import PathPatch
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import pyproj
import itertools

matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams.update({'font.size': 16})
matplotlib.rcParams.update({'font.family': 'serif', 'font.serif': ['Computer Modern']})

def flip(items, ncol):
    """ This function flips the items in a legend"""
    return itertools.chain(*[items[i::ncol] for i in range(ncol)])

def cost_function_terms(model,node, costtype):
    """This method is used as an pe.Expression for the optimization, using weight factors for all impact categories.

    Args:
        model: The equivalent of "self" in pyomo optimization models
        node: Node for which the objective is calculated
        costtype: invest, operational, trade, carbon
    """
    if costtype == "trade":
        return(
            # sum over all years 
            model.scaling_objective*(sum(
                sum(# Multiply everything with the yearly weight of the time slice
                    model.time_slice_yearly_weight[time_slice]*(
                    # sum over all products
                    sum(
                        model.demand[product, year, time_slice, node]*
                        # market clearing price
                        model.dual_variable_product_balance[node,product,year,time_slice]*model.scaling_dual_variable_product_balance[node,product,year,time_slice]
                    -
                    # subtract trading revenue 
                        sum(
                            sum(
                                model.technology_matrix_production[product,node, process_production, year_construction]*
                                model.used_production[node, process_production, year, year_construction, time_slice]
                                * model.scaling_used_production[node, process_production, year, year_construction, time_slice]
                                * model.dual_variable_product_balance[node,product,year,time_slice]
                                * model.scaling_dual_variable_product_balance[node,product,year,time_slice]
                            for year_construction in model.construction_years_production)
                        for process_production in model.processes_production)
                    for product in model.traded_products)
                    
                )
                for time_slice in model.time_slices)
            for year in model.years))
        )

    elif costtype == "carbon":
        return(
            # sum over all years 
            model.scaling_objective*(sum(
                +
                model.operational_nodal_impact[node,model.capped_impacts.ordered_data()[0], year]
                *model.scaling_operational_nodal_impact[node,model.capped_impacts.ordered_data()[0], year]
                *model.dual_variable_operational_impact_limits[model.capped_impacts.ordered_data()[0], year]
                *model.scaling_dual_variable_operational_impact_limits[model.capped_impacts.ordered_data()[0], year]
            for year in model.years))
        )

    elif costtype == "operational":
        return(
            # sum over all years 
            model.scaling_objective*(sum(
                # add operational costs
                model.operational_nodal_impact[node,"cost", year]
                *model.scaling_operational_nodal_impact[node,"cost", year]
            for year in model.years))
        )

    elif costtype == "invest":
        return(
            # sum over all years 
            model.scaling_objective*(sum(
                # add invest costs
                model.invest_nodal_impact[node, "cost", year]*model.scaling_invest_nodal_impact[node, "cost", year]
            for year in model.years))
        )
    else:
        return(0)


def get_sub_results(model,component_name,input_list,multiplicator:float = None,compare_to_zero:str = None ):
    """ This method returns a subdict of a result component, dependent on the input_list 
    subresults = get_sub_results(InvestmentModel[year],"dual_component_product_balance",[("nodes","DE"),("time_slices",2)]) 
    
    Args:
    model: pe.ConcreteModel
    component_name(str): name of component
    input_list(list of 2-tuple): list of checked indices (2-tuple). First element is string of index, second the included index
    multiplicator(float): value with the result value is multiplied
    compare_to_zero(str): either eq or ineq, return only values which are eq/ineq to zero
    """
    # find component from model
    component = model.find_component(component_name)
    condition_list=[]
    # generate list of conditions which reduce result dictionary
    for item in input_list:
        key_index = component._implicit_subsets.index(model.find_component(item[0]))
        condition_list.append((key_index,item[1]))
    if not multiplicator:
        multiplicator = 1
    # extract results
    if not compare_to_zero:
        subresult = {key:component.extract_values()[key]*multiplicator for key in component if all(key[key_index]==key_value for key_index,key_value in condition_list)}
    elif compare_to_zero == "eq":
        subresult = {key:component.extract_values()[key]*multiplicator for key in component if all(key[key_index]==key_value for key_index,key_value in condition_list) and component.extract_values()[key] == 0}
    elif compare_to_zero == "ineq":
        subresult = {key:component.extract_values()[key]*multiplicator for key in component if all(key[key_index]==key_value for key_index,key_value in condition_list) and component.extract_values()[key] != 0}
    else:
        print("compare to zero command {} is invalid. Please only choose from 'eq' and 'ineq'".format(compare_to_zero))
        raise KeyError
    return(subresult)

class Results():
    def __init__(self, working_directory, save_folder: str = None,manual_years = None, multiplicator = 1,save_figs = False):
        """ this method initializes Results"""
        self.save_figs = save_figs
        # setup paths and constants
        self.setup_utilities(working_directory,save_folder,manual_years,multiplicator)
        # import results
        self.import_results()

    def setup_utilities(self, working_directory, save_folder: str = None,manual_years = None,multiplicator = 1):
        """ this methos sets up utlities, such as paths, constant etc."""
         # set folder path
        if save_folder:
            self.folder_path = working_directory/"SecMOD"/"01-MODEL-RESULTS"/save_folder
        else:
            self.folder_path = working_directory/"SecMOD"/"01-MODEL-RESULTS"
        # set input path
        self.input_path = working_directory/"SecMOD"/"00-INPUT"/"00-RAW-INPUT"
        self.input_dict_path = working_directory/"SecMOD"/"00-INPUT"/"01-COMPUTED-INPUT"
        # set manual years
        self.manual_years = manual_years
        # select models
        self.input_models = {
            "InvestmentModel":True,
            "CentralizedResultInvestmentModel":False,
            "CentralizedPathwayResultInvestmentModel":True,
            "CentralizedInvestmentModel":False,
            "CentralizedPathwayInvestmentModel":False,
            "SolutionDict":True
        }
        # model names
        self.model_names = ["Bilevel Optimization", "Centralized Optimization"]
        # energy market models
        market_models = ["InvestmentModel","CentralizedResultInvestmentModel","CentralizedPathwayResultInvestmentModel"]
        self.market_models = [model for model in market_models if self.input_models[model]]
        # countries in real electricity price xlsx
        self.real_price_countries = {
            "DE": {2015: "Deutschland/Österreich/Luxemburg[€/MWh]", 2020: "Deutschland/Luxemburg[€/MWh]"},
            "AT": {2015: "Deutschland/Österreich/Luxemburg[€/MWh]", 2020: "Österreich[€/MWh]"},
            "LU": {2015: "Deutschland/Österreich/Luxemburg[€/MWh]", 2020: "Deutschland/Luxemburg[€/MWh]"},
            "DK": ["Dänemark 1[€/MWh]","Dänemark 2[€/MWh]"],
            "FR": "Frankreich[€/MWh]",
            "IT": "Italien (Nord)[€/MWh]",
            "NL": "Niederlande[€/MWh]",
            "PL": "Polen[€/MWh]",
            "SE": "Schweden 4[€/MWh]",
            "SI": "Slowenien[€/MWh]",
            "BE": "Belgien[€/MWh]"
        }
        self.shape_countries =  {"AUT": "AT", 
                        "BEL": "BE", 
                        "BGR": "BG", 
                        "HRV": "HR", 
                        "DNK": "DK", 
                        "EST": "EE", 
                        "FIN": "FI", 
                        "FRA": "FR",  
                        "DEU": "DE", 
                        "GRC": "GR", 
                        "IRL": "IE", 
                        "ITA": "IT", 
                        "LVA": "LV", 
                        "LTU": "LT", 
                        "LUX": "LU", 
                        "NLD": "NL",  
                        # "NO": "Norway", 
                        "POL": "PL", 
                        "PRT": "PT", 
                        "SVN": "SI", 
                        "ESP": "ES", 
                        "SWE": "SE"}

        self.real_price_years = [2015,2020]
        self.real_price_path = working_directory.parent.parent / "EU_Model_Input_Data" / "Bundesnetzagentur"
        self._real_electricity_prices = {}
        # extract real electricity prices
        for year in self.real_price_years:
            self._real_electricity_prices[year] = pd.read_excel(self.real_price_path / "real_prices_{}.xlsx".format(year))
        # set conversion factors
        self.factors = {}
        self.factors["electricity_price"] = multiplicator
        self.factors["carbon_price"] = multiplicator

        # set plot properties
        self.plot_prop = {"size_h":(7,4.5),"size_v":(7,10)}
        self.plot_prop = {"size_h":(10.5,7),"size_h_small":(10.5,6),"size_v":(10.5,15)}
        self.b_market_clearing_total = True 
        # get RWTH colors
        self.set_RWTH_colors(working_directory.parent)
        # create savepath and folder
        self.save_path = working_directory.parent / "Results"
        if not os.path.exists(self.save_path):
            os.mkdir(self.save_path)
        if not os.path.exists(self.save_path / "SVG"):
            os.mkdir(self.save_path / "SVG")
        if not os.path.exists(self.save_path / "PDF"):
            os.mkdir(self.save_path / "PDF")

    def set_RWTH_colors(self,parent_path):
        """ This method sets the RWTH colors """
        self.colors = pd.read_excel("{}/RWTH_Farben.xlsx".format(parent_path)).set_index("Hex")/255

        self.process_names = {"hard coal":"Hard Coal" ,
                        "lignite":"Lignite", 
                        "natural gas turbine":"Natural Gas Turbine", 
                        "nuclear":"Nuclear", 
                        "wind onshore":"Onshore Wind", 
                        "wind offshore":"Offshore Wind", 
                        "photovoltaics":"Photovoltaics", 
                        "solar thermal":"CSP", 
                        "run-of-river hydro":"Run-of-River Hydro", 
                        "geothermal": "Geothermal"}
        self.process_colors ={"hard coal":"#808080", 
                        "lignite":"#CD8B87", 
                        "natural gas turbine":"#7DA4A7", 
                        "nuclear":"#A8859E", 
                        "wind onshore":"#E0E69A", 
                        "wind offshore":"#B8D698", 
                        "photovoltaics":"#FDD48F", 
                        "solar thermal":"#E69679", 
                        "run-of-river hydro":"#89CCCF", 
                        "geothermal":"#BCB5D7"}
        
        self.cost_colors_bilevel ={"invest":"#8EBAE5", 
                        "operational":"#7DA4A7", 
                        "trade":"#CD8B87", 
                        "carbon":"#A8859E"}
        self.cost_colors_centralized ={"invest":"#8EBAE5", 
                        "operational":"#7DA4A7", 
                        "trade_grey":"#D9CECE",
                        "carbon_grey": "#BFB6BD"}

        self.model_colors ={"InvestmentModel":"#8EBAE5", 
                        "CentralizedPathwayResultInvestmentModel":"#CD8B87", 
                        "real":"#9C9E9F"}

        self.years_colors ={2020:"#00549F", 
                        2030:"#407FB7", 
                        2040:"#8EBAE5",
                        2050:"#C7DDF2"
                        }
        
        self.model_colors_bg ={"InvestmentModel":"#E8F1FA",         
                        "CentralizedPathwayResultInvestmentModel":"#F5E8E5"}

        self.market_clearing_colors ={"fuel_costs":["#E8F1FA","#00549F"], 
                        "non_fuel_costs":["#F2F7EC","#57AB27"], 
                        "carbon_costs":["#FFF7EA","#F6A800"],
                        "demand": "#CC071E"}
        
        self.map_colors ={"in":"#8EBAE5", 
                        "out":"#ECEDED", 
                        "edge":"#FFFFFE",
                        "demand": "#CC071E"}

        self.color_map ={"dark_green":"#DDEBCE", 
                        "green":"#F0F3D0", 
                        "yellow":"#FFFAD1",
                        "orange": "#FEEAC9",
                        "red":"#F3CDBB"#,
                        # "dark_red": "#E5C5C0"
                        }

        self.additional_colors = {"lowkey":"#CFD1D2"}
        # generate color map
        self.create_colormap()

    def create_colormap(self):
        """ This method creates a custom colormap for the congestion of connections """
        _colors_colormap = self.colors.loc[self.color_map.values(),["R","G","B"]].values
        num_el = len(_colors_colormap)
        steps = np.linspace(0,1,num_el)
        cdict= {}
        cdict["red"] = np.concatenate((steps.reshape(num_el,1),_colors_colormap[:,0].reshape(num_el,1),_colors_colormap[:,0].reshape(num_el,1)),axis=1)
        cdict["green"] = np.concatenate((steps.reshape(num_el,1),_colors_colormap[:,1].reshape(num_el,1),_colors_colormap[:,1].reshape(num_el,1)),axis=1)
        cdict["blue"] = np.concatenate((steps.reshape(num_el,1),_colors_colormap[:,2].reshape(num_el,1),_colors_colormap[:,2].reshape(num_el,1)),axis=1)
        self.custom_cmp = LSC("rwth_cmp",segmentdata = cdict,N=256)

    def import_results(self):
        """ the method imports the results """
        print("Import raw results")
        # import connections
        self.nodes = pd.read_csv("{}/01-GRID/EU/nodes.csv".format(self.input_path)).set_index("node")
        self.connections = pd.read_csv("{}/01-GRID/EU/connections.csv".format(self.input_path)).set_index("connection")
        # import results
        self.raw_results = {}
        self.time_slice_yearly_weight = {}
        self.construction_years = {}
        for model in self.input_models:
            if self.input_models[model]:
                self.raw_results[model] = {}
            
        if self.manual_years:
            self.years = self.manual_years
        else:
            # find years in folder
            self.years = [int(file.replace("CentralizedInvestmentModel_","").replace(".pickle","")) for file in os.listdir(self.folder_path) if "CentralizedInvestmentModel" in file]
        
        for year in progress.bar(self.years):
            for model in self.raw_results:
                if model != "SolutionDict":
                    with open(self.folder_path/(model+"_"+str(year)+".pickle"), "rb") as input_file:
                        self.raw_results[model][year] = pickle.load(input_file)
                        # get capped impact
                        if not hasattr(self,"capped_impact"):
                            self.capped_impact = self.raw_results[model][year].capped_impacts.ordered_data()[0]
                        # get time slices
                        if not hasattr(self, "time_slices"):
                            self.time_slices = self.raw_results[model][year].time_slices.ordered_data()
                        # get processes production
                        if not hasattr(self, "processes_production"):
                            self.processes_production = self.raw_results[model][year].processes_production.ordered_data()
                        # get products
                        if not hasattr(self, "products"):
                            self.products = self.raw_results[model][year].products.ordered_data()
                        # get construction years
                        self.construction_years[year] = self.raw_results[model][year].construction_years_production.ordered_data()
                        # get time slice weights
                        self.time_slice_yearly_weight[year] = self.raw_results[model][year].time_slice_yearly_weight.extract_values()
                else:
                    with open(self.folder_path/(model+"_"+str(year)+".pickle"), "rb") as input_file:
                        self.raw_results[model][year] = pickle.load(input_file)
    
    def calculate_results(self):
        """ this method calculates the results"""
        print("Calculate costs and prices")
        self.results = {}
        # calculate costs
        self.calculate_costs()
        # calculate capacities
        print("Calculate capacities")
        self.calculate_capacities()
        # calculate transmission
        print("Calculate transmission")
        self.calculate_transmission()
        # calculate marginal costs

    def calculate_costs(self):
        """ this method calculates the costs """
        costtypes = ["invest","operational","trade","carbon"]
        cost_index = pd.MultiIndex.from_product([self.market_models,self.nodes.index,costtypes,self.years],names = ["model","node","costtype","year"]).sort_values()
        electricity_price_index = pd.MultiIndex.from_product([self.market_models,self.nodes.index,self.time_slices,self.years],names = ["model","node","time_slice","year"]).sort_values()
        carbon_price_index = pd.MultiIndex.from_product([self.market_models,self.years],names = ["model","year"]).sort_values()
        demand_index = pd.MultiIndex.from_product([self.market_models,self.nodes.index,self.time_slices,self.years],names = ["model","node","time_slice","year"]).sort_values()
    
        # calculate costs
        self.results["costs"] = pd.Series(list(map(lambda multiindex: pe.value(cost_function_terms(self.raw_results[multiindex[0]][multiindex[3]],multiindex[1],multiindex[2])),cost_index.values)),index=cost_index)
        # add overshoot
        self.results["costs"].loc[(slice(None),slice(None),"carbon",slice(None))] = self.results["costs"].loc[(slice(None),slice(None),"carbon",slice(None))].add(list(map(lambda multiindex: self.add_overshoot_cost(multiindex[0],multiindex[1],multiindex[2]),self.results["costs"].loc[(slice(None),slice(None),"carbon",slice(None))].index.values))).values
        self.results["costs"] = self.results["costs"].unstack(level="year")
        # calculate prices
        self.results["electricity_price"] = pd.Series(list(map(
            lambda multiindex: self.raw_results[multiindex[0]][multiindex[3]].dual_variable_product_balance[multiindex[1],"electricity",multiindex[3],multiindex[2]].value*self.factors["electricity_price"],electricity_price_index.values)),index=electricity_price_index).unstack(level="year")
        self.results["carbon_price"] = pd.Series(list(map(
            lambda multiindex: self.raw_results[multiindex[0]][multiindex[1]].dual_variable_operational_impact_limits[self.capped_impact,multiindex[1]].value*self.factors["carbon_price"],carbon_price_index.values)),index=carbon_price_index).unstack(level="year")
        self.results["demand"] = pd.Series(list(map(
            lambda multiindex: self.raw_results[multiindex[0]][multiindex[3]].demand["electricity",multiindex[3],multiindex[2],multiindex[1]],demand_index.values)),index=demand_index).unstack(level="year")
        
    def get_combined_marginal_costs(self,model,node,year,time_slice):
        """ This method calculates the combined marginal costs"""
        reduced_index = pd.MultiIndex.from_product([[model],[node],self.processes_production,self.construction_years[year],[time_slice]]).sort_values()
        _available_capacity = pd.Series(list(map(lambda multiindex: self.get_marginal_cost_data("available_capacity",multiindex,year),reduced_index.values)),index=reduced_index)
        _available_capacity = _available_capacity[_available_capacity!=0]
        _fuel_costs = pd.Series(list(map(lambda multiindex: self.get_marginal_cost_data("fuel_costs",multiindex,year),_available_capacity.index.values)),index=_available_capacity.index)
        _non_fuel_costs = pd.Series(list(map(lambda multiindex: self.get_marginal_cost_data("non_fuel_costs",multiindex,year),_available_capacity.index.values)),index=_available_capacity.index)
        _carbon_costs = pd.Series(list(map(lambda multiindex: self.get_marginal_cost_data("carbon_costs",multiindex,year),_available_capacity.index.values)),index=_available_capacity.index)
        _costs = pd.concat([_fuel_costs,_non_fuel_costs,_carbon_costs],axis=1)
        _costs.columns = ["fuel_costs","non_fuel_costs","carbon_costs"]
        _costs["total"] = _costs.sum(axis=1)
        _costs["available_capacity"] = _available_capacity
        _costs.sort_values("total", inplace=True)
        
        return(_costs)

    def add_overshoot_cost(self,model_name,node,year):
        """ This method returns the cost for operational overshoot, weighted by demand """
        model = self.raw_results[model_name][year]
        overshoot = model.operational_impact_overshoot[self.capped_impact, year].value
        overshoot_price = model.objective_factor_impact_overshoot[self.capped_impact, year]
        total_demand = sum(sum(model.demand["electricity",year,time_slice,demand_node]*model.time_slice_yearly_weight[time_slice] for time_slice in model.time_slices) for demand_node in self.nodes.index)
        nodal_demand = sum(model.demand["electricity",year,time_slice,node]*model.time_slice_yearly_weight[time_slice] for time_slice in model.time_slices)
        overshoot_cost = overshoot*overshoot_price*nodal_demand/total_demand

        return(overshoot_cost)            

    def calculate_capacities(self):
        """ this method calculates the capacities """
        existing_capacity_temp = {}
        for year in self.years:
            construction_years = self.raw_results[self.market_models[0]][year].construction_years_production.ordered_data()
            existing_capacity_temp_index = pd.MultiIndex.from_product([self.market_models,self.nodes.index,self.processes_production,construction_years],names=["model","node","process","construction_year"])
            existing_capacity_temp[year] = pd.DataFrame(index=existing_capacity_temp_index,columns=[year]).sort_index()
            for model in self.market_models:
                existing_capacity_temp[year].loc[model,year] = pd.Series(self.raw_results[model][year].existing_capacity_production.extract_values()).unstack(level=2).sort_index().values
                _new_capacity = pd.Series(self.raw_results[model][year].predicted_new_capacity_production.extract_values()).unstack(level=2).sort_index()
                _new_capacity.loc[self.raw_results[model][year].nodes[self.raw_results[model][year].current_node.value],year] = pd.Series(self.raw_results[model][year].new_capacity_production.extract_values()).sort_index().values
                existing_capacity_temp[year].loc[model,year]
                existing_capacity_temp[year].loc[(model,slice(None),slice(None),year),year] += _new_capacity[year].values
        self.results["existing_capacity"] = pd.concat([existing_capacity_temp[year] for year in self.years],axis=1).fillna(0)
        self.results["absolute_difference_capacity"] = (self.results["existing_capacity"].loc[self.market_models[0]]-self.results["existing_capacity"].loc[self.market_models[1]])
        self.results["relative_difference_capacity"] = (self.results["existing_capacity"].loc[self.market_models[0]]-self.results["existing_capacity"].loc[self.market_models[1]])/self.results["existing_capacity"].loc[self.market_models[0]]-self.results["existing_capacity"].loc[self.market_models[1]]

    def calculate_transmission(self):
        """ this method calculates the transmission """
        _used_transmission_temp = {}
        _congestion_temp = {}
        for model in progress.bar(self.market_models):
            _used_transmission_temp[model] = {} 
            _congestion_temp[model] = {}
            for year in self.years:
                # used transmission of year and model
                _used_transmission_temp[model][year] = pd.Series(self.raw_results[model][year].used_transmission.extract_values()).unstack(level=1).sort_index()
                # calculate congestion
                _transmission_limit = pd.Series(get_sub_results(self.raw_results[model][year],"power_line_properties",[("power_line_property_categories","power limit")])).droplevel(level=[1,2])
                _congestion_temp[model][year] = _used_transmission_temp[model][year].unstack(level=1).apply(lambda x: abs(x/_transmission_limit.loc[x.name]),axis=1).fillna(0)
            _used_transmission_temp[model] = pd.concat({model:pd.concat([_used_transmission_temp[model][year] for year in self.years],axis=1)})
            _congestion_temp[model] = pd.concat({model:pd.concat([_congestion_temp[model][year] for year in self.years],axis=1)})
        # save in results
        self.results["used_transmission"] = pd.concat([_used_transmission_temp[model] for model in self.market_models],axis=0)
        self.results["congestion"] = pd.concat([_congestion_temp[model] for model in self.market_models],axis=0)

    def calculate_transmission_balance(self,node,year=None):
        """ This method calculates the transmission balance of a country """
        conout = self.connections.index[self.connections["node1"]==node]
        conin = self.connections.index[self.connections["node2"]==node]
        # transmission balance is out - in -> negative values are import, positive values are export
        if len(conin) != 0 and len(conout) != 0:
            transmission_balance = self.results["used_transmission"].loc[(slice(None),list(conout.values)),:].groupby(level=[0,2]).sum()-self.results["used_transmission"].loc[(slice(None),list(conin.values)),:].groupby(level=[0,2]).sum()
        elif len(conin) == 0:
            transmission_balance = self.results["used_transmission"].loc[(slice(None),list(conout.values)),:].groupby(level=[0,2]).sum()
        else:
            transmission_balance = -self.results["used_transmission"].loc[(slice(None),list(conin.values)),:].groupby(level=[0,2]).sum()
        # calculate transmission saldo 
        transmission_saldo = transmission_balance.apply(lambda x: x*self.time_slice_yearly_weight[x.index[0]][x.name[1]],axis=1).groupby(level=0).sum()
        return (transmission_balance,transmission_saldo)

    def get_marginal_cost_data(self, variable, multiindex,year):
        """ This function calculates the marginal cost data and available capacity. Select different cost data term by variable """
        model = multiindex[0]
        node = multiindex[1]
        process = multiindex[2]
        construction_year = multiindex[3]
        time_slice = multiindex[4]
        if variable == "available_capacity":
            return(
                pe.value(self.results["existing_capacity"].loc[(model,node,process,construction_year),year]*\
                    self.raw_results[model][year].usable_capacity_factor_timeseries_production[node,process,time_slice])
            )
        elif variable == "fuel_costs":
            return(
                pe.value(sum(-self.raw_results[model][year].technology_matrix_production[product,node,process,construction_year]*
                    self.raw_results[model][year].impact_matrix_non_served_demand["cost",product,year,time_slice] 
                    for product in self.products if self.raw_results[model][year].technology_matrix_production[product,node,process,construction_year] < 0))*self.factors["electricity_price"]
            )
        elif variable == "non_fuel_costs":
            return(
                pe.value(-self.raw_results[model][year].impact_matrix_production["cost","operation",node,process,year,construction_year]*\
                    self.raw_results[model][year].dual_variable_operational_nodal_impact[node,"cost",year])*self.factors["electricity_price"]
            )
        elif variable == "carbon_costs":
            return(
                pe.value(-self.raw_results[model][year].impact_matrix_production[self.capped_impact,"operation",node,process,year,construction_year]*\
                    self.raw_results[model][year].dual_variable_operational_nodal_impact[node,self.capped_impact,year])*self.factors["electricity_price"]
            )

    ## plot
    def plot_results(self):
        """ this method plots the results """
        print("Plot results")
        # set up plot years
        self.plot_years = list(np.unique(10*np.ceil(np.array(self.years)/10)).astype(int))
        # Total costs
        self.plot_costs()
        # Total capacities
        self.plot_capacity()
        # German capacities
        self.plot_capacity("DE")
        # evolution of electricity price
        self.plot_electricity_evolution()
        # show plot
        plt.show()

    def plot_capacity(self,node=None):
        """ this method plots the capacity """
        order_processes = list(self.process_colors.keys())
        colors_processes = self.colors.loc[self.process_colors.values(),["R","G","B"]].values
        # create two subplots
        fig,ax = plt.subplots(1,2)
        for idx_model,model in enumerate(self.market_models):
            # ceil to next 100 GW
            _ceiling_factor = 100
            # if node, specific country
            if node:
                file_suffix = node
                _max_capa = _ceiling_factor*np.ceil(self.results["existing_capacity"].loc[(slice(None),node),:].groupby(level=["model"]).sum().max().max()/_ceiling_factor)
                try:
                    self.results["existing_capacity"].loc[(model,node,order_processes),:].groupby(level=["process"],sort=False).sum().T.plot.area(stacked=True,ax=ax[idx_model],color=colors_processes,legend=False)
                    ax[idx_model].set_ylim([0,_max_capa])
                    ax[idx_model].set_xlim([self.years[0],self.years[-1]])
                    ax[idx_model].set_xticks(self.plot_years)
                except ValueError:
                    self.results["existing_capacity"].loc[(model,node,order_processes),:].groupby(level=["process"],sort=False).sum().T.plot.bar(stacked=True,ax=ax[idx_model],color=colors_processes,legend=False)
                ax[idx_model].set_title("node {}".format(node))                
            else:
                file_suffix = "total"
                _max_capa = _ceiling_factor*np.ceil(self.results["existing_capacity"].groupby(level=["model"]).sum().max().max()/_ceiling_factor)
                try:
                    self.results["existing_capacity"].loc[(model,slice(None),order_processes),:].groupby(level=["process"],sort=False).sum().T.plot.area(stacked=True,ax=ax[idx_model],color=colors_processes,legend=False)
                    ax[idx_model].set_ylim([0,_max_capa])
                    ax[idx_model].set_xlim([self.years[0],self.years[-1]])
                    ax[idx_model].set_xticks(self.plot_years)
                except ValueError:
                    self.results["existing_capacity"].loc[(model,slice(None),order_processes),:].groupby(level=["process"],sort=False).sum().T.plot.bar(stacked=True,ax=ax[idx_model],color=colors_processes,legend=False)
            # set linestyle to none
            for line in ax[idx_model].lines:
                line.set_linestyle("none")
            
            ax[idx_model].set_xlabel("Years")
            ax[idx_model].set_title(self.model_names[idx_model])
            if idx_model == 0:
                ax[idx_model].set_ylabel("Installed Capacity [GW]")

        fig.set_size_inches(self.plot_prop["size_h"])
        fig.legend(ax[idx_model].collections,self.process_names.values(),loc=8,ncol=3)
        fig.tight_layout()
        fig.subplots_adjust(bottom=0.30)
        if self.save_figs:
            # save
            plt.savefig(self.save_path / "SVG" / "capacity_{}.svg".format(file_suffix))
            plt.savefig(self.save_path / "PDF" / "capacity_{}.pdf".format(file_suffix))

    def plot_costs(self,node=None):
        """ this method plots the capacity """
        order_costs = ["invest","operational","trade","carbon"]
        # create two subplots
        fig,ax = plt.subplots(1,2)
        for idx_model,model in enumerate(self.market_models):
            _ceiling_factor = 10
            # if unit is megaEuro
            if self.factors["electricity_price"] == 1000:
                _conversion_factor = 1e3
            # if unit is kiloEuro
            else:
                _conversion_factor = 1e6
            # select greyed-out colors for centralized model
            if idx_model == 0:
                colors_costs = self.colors.loc[self.cost_colors_bilevel.values(),["R","G","B"]].values
            else:
                colors_costs = self.colors.loc[self.cost_colors_centralized.values(),["R","G","B"]].values
            if node:
                file_suffix = node
                _max_costs = _ceiling_factor*np.ceil(self.results["costs"].loc[(slice(None),node,slice(order_costs)),:].groupby(level=["model"]).sum().max().max()/_conversion_factor/_ceiling_factor)
                (self.results["costs"].loc[(model,node,order_costs),:].groupby(level=["costtype"],sort=False).sum()/_conversion_factor).T.plot.area(stacked=True,ax=ax[idx_model],color=colors_costs,legend=False)
                ax[idx_model].set_ylim([0,_max_costs])
                ax[idx_model].set_xlim([self.years[0],self.years[-1]])
                ax[idx_model].set_xticks(self.plot_years)
                ax[idx_model].set_title(self.model_names[idx_model])
            else:
                file_suffix = "total"
                _max_costs = _ceiling_factor*np.ceil(self.results["costs"].groupby(level=["model"]).sum().max().max()/_conversion_factor/_ceiling_factor)
                (self.results["costs"].loc[(model,slice(None),order_costs),:].groupby(level=["costtype"],sort=False).sum()/_conversion_factor).T.plot.area(stacked=True,ax=ax[idx_model],color=colors_costs,legend=False)
                ax[idx_model].set_ylim([0,_max_costs])
                ax[idx_model].set_xlim([self.years[0],self.years[-1]])
                ax[idx_model].set_xticks(self.plot_years)
                ax[idx_model].set_title(self.model_names[idx_model])
            # set linestyle to none
            for line in ax[idx_model].lines:
                line.set_linestyle("none")
            ax[idx_model].set_xlabel("Years")
            if idx_model == 0:
                ax[idx_model].set_ylabel("Total Costs [bn. EUR/a]")
            # hatch out the carbon and trade in centralized_optimization
            if idx_model == 1:
                for el in [2,3]:
                    col = ax[idx_model].collections[el]
                    for path in col.get_paths():
                        patch = PathPatch(path, hatch='/', facecolor='none',edgecolor = self.colors.loc[self.model_colors["real"],["R","G","B"]].values)
                        ax[idx_model].add_patch(patch)
        fig.set_size_inches(self.plot_prop["size_h"])
        fig.legend(ax[0].collections,["Investment Costs","Operational Costs","Trading Costs","Carbon Allowance Costs"],loc=8,ncol=2)
        fig.tight_layout()
        fig.subplots_adjust(bottom=0.25)
        if self.save_figs:
            # save
            plt.savefig(self.save_path / "SVG" / "costs_{}.svg".format(file_suffix))
            plt.savefig(self.save_path / "PDF" / "costs_{}.pdf".format(file_suffix))

    def plot_electricity_evolution(self):
        """ This method plots the evolution of the yearly average electricity prices"""
        fig,ax = plt.subplots(1,2)
        _average_price_df = pd.DataFrame()
        p={}
        _summed_demand = self.results["demand"].groupby(["model","time_slice"]).sum()
        _average_price = (self.results["electricity_price"]*self.results["demand"]).groupby(["model","time_slice"]).sum()/_summed_demand
        _annual_average_price = _average_price.apply(lambda x: x*self.raw_results[x.name[0]][x.index[0]].time_slice_yearly_weight[x.name[1]],axis=1).groupby("model").sum()/8760
        for year in self.years:
            _average_price_df[year] = self.results["electricity_price"].apply(lambda x: x[year]*self.raw_results[x.name[0]][year].time_slice_yearly_weight[x.name[2]],axis=1).groupby(["model","node"]).sum()/8760

        _max_price = 10*np.ceil(np.quantile(_average_price_df.values,0.975)/10)
        for idx_model,model in enumerate(self.market_models):
            _average_price_df.loc[model].T.plot(ax=ax[idx_model],color=self.colors.loc[self.additional_colors.values(),["R","G","B"]].values,linewidth=0.25,legend=False)
            p[idx_model]=_annual_average_price.loc[model].T.plot(ax=ax[idx_model],color=[self.colors.loc[self.model_colors.values(),["R","G","B"]].values[idx_model,:]],linewidth=1.5,legend=False).lines[-1]
            ax[idx_model].set_ylim([0,_max_price])
            ax[idx_model].set_xlim([self.years[0],self.years[-1]])
            ax[idx_model].set_xticks(self.plot_years)
            ax[idx_model].set_xlabel("Years")
            ax[idx_model].set_title(self.model_names[idx_model])

        ax[0].set_ylabel("Average Electricity Price [EUR/MWh]")
        
        fig.set_size_inches(self.plot_prop["size_h_small"])
        if self.save_figs:
            # save
            #plt.savefig(self.save_path / "SVG" / "electricity_price_evolution.svg")
            plt.savefig(self.save_path / "PDF" / "electricity_price_evolution.pdf")

def start_evaluation():
    """ this function starts the evaluation """
    working_directory = Path.cwd()
    save_folder = "final_results"
    #manual_years = [2035]
    manual_years = None
    multiplicator = 1000 # multiplicator to match units (compare to 01-UNITS-TARGET-OPTIMIZATION.json), units after multiplication are kiloEUR/GWh and kiloEuro/gigagram 
    save_figs = True
    OptimizationResults = Results(working_directory,save_folder,manual_years,multiplicator,save_figs)
    # calculate results 
    OptimizationResults.calculate_results()
    # plot results
    OptimizationResults.plot_results()

if __name__ == "__main__":
    """ start main """
    start_evaluation()
