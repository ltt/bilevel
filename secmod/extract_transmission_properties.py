from xml.dom import minidom
import xml.etree.ElementTree as ET
# from lxml import etree
import pandas as pd
import numpy as np
import os
import pickle 
import sys
import copy 

def get_namespaces():
    """ This function returns all namespaces used """
    ns = {"cim":"http://iec.ch/TC57/2013/CIM-schema-cim16#",
        "cims":"http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#",
        "entsoe":"http://entsoe.eu/CIM/SchemaExtension/3/1#", 
        "md":"http://iec.ch/TC57/61970-552/ModelDescription/1#",
        "rdf":"http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        "rdfs":"http://www.w3.org/2000/01/rdf-schema#", 
        "xsd":"http://www.w3.org/2001/XMLSchema#"}

    return ns


class ENTSOE_Grid():
    """ This class derives the important Grid Properties from the ENTSOE TYNDP files """
    def __init__(self,input_grid_properties):
        """ This method initializes the ENTSOE Grid """
        self.entsoe_dataset = {}
        self.load_existing_dataset = True

        self.grid_properties = input_grid_properties
        self.nodes = self.grid_properties.nodes.index
        self.tyndp_path = self.grid_properties.source_path_dict["ENTSOE"] / "TYNDP_dataset_level_3"
        self.ns = get_namespaces()
        self.entsoe_dataset["ns"] = self.ns

        if os.path.exists(self.tyndp_path / "entsoe_dataset.pickle") and self.load_existing_dataset:
            with open(self.tyndp_path / "entsoe_dataset.pickle", "rb") as input_file:
                self.entsoe_dataset = pickle.load(input_file)
        else:
            self.topological_nodes = {}
            # parse files
            self.parse_xml_files()    
            # extract base voltages
            self.get_voltage_levels()
            # extract cross border connections
            self.get_cross_border_connections()
            # extract connected terminals
            self.get_terminals()
            self.entsoe_dataset["topological_nodes"] = self.topological_nodes
            self.entsoe_dataset["included_countries"] = self.included_countries
            self.entsoe_dataset["missing_countries"] = self.missing_countries
            
            with open(self.tyndp_path / "entsoe_dataset.pickle", "wb") as input_file:
                pickle.dump(self.entsoe_dataset,input_file)

        topological_nodes=self.entsoe_dataset["topological_nodes"]
        df_topological_nodes = pd.DataFrame.from_dict({(id_node, country, terminal):topological_nodes[id_node][country][terminal] for id_node in topological_nodes for country in ["from_node","to_node"] for terminal in ["terminal1","terminal2"]}, orient ="index")
        df_topological_nodes.sort_index(inplace=True)
        for node in topological_nodes:
            df_topological_nodes.loc[node,"x_node_name"] = topological_nodes[node]["x_node_name"]
            try:
                df_topological_nodes.loc[(node,"from_node"),"country"] = topological_nodes[node]["from_node_ISO"]
            except:
                pass
            try:
                df_topological_nodes.loc[(node,"to_node"),"country"] = topological_nodes[node]["to_node_ISO"]
            except:
                pass
            df_topological_nodes.loc[node,"base_voltage"] = topological_nodes[node]["base_voltage"]
            for country in ["from_node","to_node"]:
                terminal1 = self.entsoe_dataset["topological_nodes"][node][country]["terminal1"]
                if "b_ch" not in terminal1 or terminal1["b_ch"] == -1:
                    del self.entsoe_dataset["topological_nodes"][node][country]["terminal1"]
                terminal2 = self.entsoe_dataset["topological_nodes"][node][country]["terminal2"]
                if "b_ch" not in terminal2 or terminal2["b_ch"] == -1:
                    del self.entsoe_dataset["topological_nodes"][node][country]["terminal2"]
        # copy and reduce further
        df_topological_nodes_reduced = copy.deepcopy(df_topological_nodes)
        df_topological_nodes_reduced = df_topological_nodes_reduced[~df_topological_nodes_reduced["id"].isna()]
        df_topological_nodes_reduced = df_topological_nodes_reduced[df_topological_nodes_reduced["x"]!=-1]
        # delete nodes which only have one connection
        df_topological_nodes_full_connection = copy.deepcopy(df_topological_nodes_reduced)
        for index in df_topological_nodes_reduced.index:
            if len(df_topological_nodes_reduced.loc[index[0]].index) == 1:
                df_topological_nodes_full_connection.drop(labels=index,inplace=True)
        # calculate specific values and power limit
        df_topological_nodes_full_connection["b_spez"] = df_topological_nodes_full_connection["b_ch"]/df_topological_nodes_full_connection["length"] 
        df_topological_nodes_full_connection["g_spez"] = df_topological_nodes_full_connection["g_ch"]/df_topological_nodes_full_connection["length"]
        df_topological_nodes_full_connection["r_spez"] = df_topological_nodes_full_connection["r"]/df_topological_nodes_full_connection["length"]
        df_topological_nodes_full_connection["x_spez"] = df_topological_nodes_full_connection["x"]/df_topological_nodes_full_connection["length"]
        df_topological_nodes_full_connection["power_limit"] = df_topological_nodes_full_connection["current_limit"]*df_topological_nodes_full_connection["base_voltage"]/1000 # MW
        # drop terminal level
        df_topological_nodes_full_connection = df_topological_nodes_full_connection.droplevel(2)
        # 220 kV: everything with base_voltage <= 220 kV
        # 380 kV: everything with base_voltage > 380 kV
        self.connection_dict = {}
        for connection in np.unique(df_topological_nodes_full_connection.index.get_level_values(0)):
            connection_name = df_topological_nodes_full_connection.loc[(connection,"from_node"),"x_node_name"].to_list()[0]
            self.connection_dict[connection_name] = {}
            if df_topological_nodes_full_connection.loc[(connection,"from_node"),"base_voltage"].to_list()[0] <= 220:
                self.connection_dict[connection_name]["voltage"] = 220
            else:
                self.connection_dict[connection_name]["voltage"] = 380
            self.connection_dict[connection_name]["node1"] = df_topological_nodes_full_connection.loc[(connection,"from_node"),"country"].to_list()[0]
            self.connection_dict[connection_name]["node2"] = df_topological_nodes_full_connection.loc[(connection,"to_node"),"country"].to_list()[0]
            # min distance 1 km
            self.connection_dict[connection_name]["length"] = max(df_topological_nodes_full_connection.loc[(connection),"length"].sum(),1)
            self.connection_dict[connection_name]["power_limit"] = df_topological_nodes_full_connection.loc[(connection),"power_limit"][df_topological_nodes_full_connection.loc[(connection),"power_limit"]<9000].max()

    def parse_xml_files(self):
        """ this method imports all necessary files from the xml documents """
        print("Start Parsing XML Files")
        self.files = {}
        self.files["BF_TP"] = ET.parse(str(self.tyndp_path / "Boundary_file" / "20200117T1239Z_ENTSO-E_BD_1164" / "20200117T1239Z_ENTSO-E_TP_BD_1164.xml")).getroot()
        self.files["BF_EQ"] = ET.parse(str(self.tyndp_path / "Boundary_file" / "20200117T1239Z_ENTSO-E_BD_1164" / "20200117T1239Z_ENTSO-E_EQ_BD_1164.xml")).getroot()
        # list countries
        country_folders = os.listdir(self.tyndp_path / "Countries")
        self.missing_countries = []
        self.included_countries = []
        self.selected_nodes = self.nodes
        # self.selected_nodes = ["BE","FR"]
        for country in self.selected_nodes:
            folder_of_country = list(filter(lambda x: country in x, country_folders))
            # if country folder exists
            if folder_of_country:
                self.included_countries.append(country)
                self.files[country] = {}
                _country_path = self.tyndp_path / "Countries" / folder_of_country[0]
                _TP_file_name = list(filter(lambda x: "_TP" in x, os.listdir(_country_path)))
                _EQ_file_name = list(filter(lambda x: "_EQ" in x, os.listdir(_country_path)))
                # assert that unique TP and EQ files
                assert len(_TP_file_name)==1, "confusing or missing TP file for {}. Found these files: {}".format(country,_TP_file_name)
                assert len(_EQ_file_name)==1, "confusing or missing EQ file for {}. Found these files: {}".format(country,_EQ_file_name)

                self.files[country]["TP"] = ET.parse(str(self.tyndp_path / "Countries" / folder_of_country[0] / _TP_file_name[0])).getroot()
                self.files[country]["EQ"] = ET.parse(str(self.tyndp_path / "Countries" / folder_of_country[0] / _EQ_file_name[0])).getroot()

            # if country folder does not exist
            else:
                print("Country {} not in ENTSO-E Data".format(country))
                self.missing_countries.append(country)
        print("Done Parsing XML Files")
    
    def get_voltage_levels(self):
        """ this method gets all voltage levels by ID """
        self.base_voltages = {}
        for base_voltage in self.files["BF_EQ"].findall('cim:BaseVoltage',self.ns):
            self.base_voltages[base_voltage.attrib[self.format_ns("rdf","ID")]] = base_voltage.find('cim:BaseVoltage.nominalVoltage',self.ns).text

    def get_cross_border_connections(self):
        """ this method extracts the border connections from the ENTSOE TYNDP files """
        print("Start Retrieving Topological Nodes")
        BF_TP_topological_nodes = self.files["BF_TP"].findall('cim:TopologicalNode',self.ns)
        # iterate through cross border connection nodes
        for topological_node in BF_TP_topological_nodes:
            from_ISO = topological_node.find('entsoe:TopologicalNode.fromEndIsoCode',self.ns).text
            to_ISO = topological_node.find('entsoe:TopologicalNode.toEndIsoCode',self.ns).text
            # if start and end node in self.nodes
            if from_ISO in self.included_countries and to_ISO in self.included_countries and from_ISO != to_ISO:
                # create new topological node with rdf:ID
                topo_node_ID = topological_node.attrib[self.format_ns("rdf","ID")]
                self.topological_nodes[topo_node_ID] = {}
                self.topological_nodes[topo_node_ID]["from_node_ISO"] = from_ISO
                self.topological_nodes[topo_node_ID]["to_node_ISO"] = to_ISO
                self.topological_nodes[topo_node_ID]["x_node_name"] = topological_node.find('cim:IdentifiedObject.name',self.ns).text
                self.topological_nodes[topo_node_ID]["base_voltage"] = float(self.base_voltages[topological_node.find('cim:TopologicalNode.BaseVoltage',self.ns).attrib[self.format_ns("rdf","resource")].replace("#","")])
                self.topological_nodes[topo_node_ID]["from_node"] = {"terminal1":{},"terminal2":{}}
                self.topological_nodes[topo_node_ID]["to_node"] = {"terminal1":{},"terminal2":{}}
        print("Done Retrieving Topological Nodes")

    def get_terminals(self):
        """ this method iterates through all country files and retrieves the connected terminals """
        print("Start Retrieving Terminals")
        for country in self.included_countries:
            for terminal in self.files[country]["TP"].findall('cim:Terminal',self.ns):
                node_of_terminal = terminal.find('cim:Terminal.TopologicalNode',self.ns).attrib[self.format_ns("rdf","resource")].replace("#","")
                # if node of terminal in topological nodes
                if node_of_terminal in self.topological_nodes.keys():
                    terminal_id = terminal.attrib[self.format_ns("rdf","about")]
                    if country == self.topological_nodes[node_of_terminal]["from_node_ISO"]:
                        idx_country = "from_node"
                    elif country == self.topological_nodes[node_of_terminal]["to_node_ISO"]:
                        idx_country = "to_node"
                    else:
                        raise KeyError
                    # set terminal 1 or 2
                    if "id" in self.topological_nodes[node_of_terminal][idx_country]["terminal1"]:
                        num_terminal = "terminal2"
                    else:
                        num_terminal = "terminal1"
                    # set index of terminal
                    self.topological_nodes[node_of_terminal][idx_country][num_terminal]["id"] = terminal_id
                    # find properties
                    # find ID of OperationalLimitSet which has terminal as its OperationalLimitSet.Terminal
                    try:
                        operationalLimitSet = self.files[country]["EQ"].find("cim:OperationalLimitSet/cim:OperationalLimitSet.Terminal[@{}='{}']..".format(self.format_ns("rdf","resource"),terminal_id),self.ns).attrib[self.format_ns("rdf","ID")]
                        self.topological_nodes[node_of_terminal][idx_country][num_terminal]["current_limit"] =float(self.files[country]["EQ"].find("cim:CurrentLimit/cim:OperationalLimit.OperationalLimitSet[@{}='{}']..".format(self.format_ns("rdf","resource"),"#"+operationalLimitSet),self.ns).find("cim:CurrentLimit.value",self.ns).text)
                    except AttributeError:
                        print("There does not exist a OperationalLimitSet for the Terminal {} in EQ of country {}".format(terminal_id,country))
                        self.topological_nodes[node_of_terminal][idx_country][num_terminal]["current_limit"] = -1
                    # get Terminal.ConductingEquipment Resource attribute of corresponding Terminal from country EQ
                    conducting_equipment = self.files[country]["EQ"].find("cim:Terminal[@{}='{}']".format(self.format_ns("rdf","ID"),terminal_id.replace("#","")),self.ns).find("cim:Terminal.ConductingEquipment",self.ns).attrib[self.format_ns("rdf","resource")]
                    self.topological_nodes[node_of_terminal][idx_country][num_terminal]["conducting_equipment"] = conducting_equipment
                    # get properties
                    self.topological_nodes[node_of_terminal][idx_country][num_terminal]["b_ch"] = self.get_property(self.files[country]["EQ"],conducting_equipment,"bch")
                    self.topological_nodes[node_of_terminal][idx_country][num_terminal]["g_ch"] = self.get_property(self.files[country]["EQ"],conducting_equipment,"gch")
                    self.topological_nodes[node_of_terminal][idx_country][num_terminal]["r"] = self.get_property(self.files[country]["EQ"],conducting_equipment,"r")
                    self.topological_nodes[node_of_terminal][idx_country][num_terminal]["x"] = self.get_property(self.files[country]["EQ"],conducting_equipment,"x")
                    self.topological_nodes[node_of_terminal][idx_country][num_terminal]["length"] = self.get_property(self.files[country]["EQ"],conducting_equipment,"length")


        print("Done Retrieving Terminals")
    
    def get_child_value(self,node, child: str, attr:str = None):
        """ This method returns the value of the child of a node """
        if not attr:
            return(node.getElementsByTagName(child)[0].firstChild.data)
        else:
            return(node.getElementsByTagName(child)[0].getAttribute(attr))

    def get_property(self,xml_file,conducting_equipment,property_el):
        """ This method returns an electrical property for conducting_equipment from xml_file """
        try:
            if property_el == "length":
                return(float(xml_file.find("cim:ACLineSegment[@{}='{}']/cim:Conductor.{}".format(self.format_ns("rdf","ID"),conducting_equipment.replace("#",""),property_el),self.ns).text))
            else:
                return(float(xml_file.find("cim:ACLineSegment[@{}='{}']/cim:ACLineSegment.{}".format(self.format_ns("rdf","ID"),conducting_equipment.replace("#",""),property_el),self.ns).text))
        except AttributeError:
            if property_el == "length": 
                print("There does not exist a length correspondent to the conducting_equipment {}".format(conducting_equipment))
            return(-1)

    def find_ID_of_corresponding_node(self,xml,id_input: str, node_type: str, child:str, attr:str = None):
        """ This method returns a node ID corresponding to a given ID of a child attribute.
        <xml>
            <node_type rdf:ID>
                <child attr=id_input>
            < node_type>
        <xml>
        
        --> return rdf:ID
        """
        for node in xml.getElementsByTagName(node_type):
            if self.get_child_value(node,child,attr) == id_input:
                return node.getAttribute("rdf:ID")
        return None

    def find_childID_of_same_node(self,xml,id_input: str, node_type: str, child:str, attr: str = None):
        """ This method is similar to the method find_corresponding_node, but in this case,
        the node_type is the input node type and it returns the ID/value of the child attribute
        <xml>
            <node_type id_input>
                <child attr= ID>
            < node_type>
        <xml>
        --> return ID
        """
        for node in xml.getElementsByTagName(node_type):
            if node.getAttribute("rdf:ID") == id_input.replace("#",""):
                try:
                    return self.get_child_value(node,child,attr)
                except IndexError:
                    print("{} of {} {} in {} cannot be found".format(child,node_type,id_input,xml))
                    return -1
        return -1
    
    def format_ns(self,prefix, attr):
        """ this method converts the prefix and attr to the namespace string """
        return("{{{0}}}{1}".format(self.ns[prefix],attr))