# import namespace for abstract classes
import abc
import logging
from datetime import datetime
import pandas as pd
import importlib.util
import numpy as np
import scipy.stats.mstats as sp
import cProfile, pstats
from copy import deepcopy

from pyomo.solvers.plugins.solvers.GAMS import GAMSShell
# import necessary elements of pyomo - alternatively use "from pyomo.environ import *", if you are missing something
import pyomo.environ as pe
from pyomo.opt import TerminationCondition as TerminationCondition
from pyomo.core.expr import current as EXPR
from pyomo.core.expr import differentiate # .calculus.derivatives
from pyomo.core.expr.calculus.derivatives import Modes 
from pyomo.core.expr.numvalue import NumericConstant

# Load config.py
spec = importlib.util.spec_from_file_location("config", "SecMOD/00-INPUT/00-RAW-INPUT/config.py")
config = importlib.util.module_from_spec(spec)
spec.loader.exec_module(config)

class Optimization(abc.ABC):
    """This is the class for optimization models.

    It includes all declarations of Sets, Parameters, Variables and Constraints. 
    Furthermore it contains all optimization methods.
    """
    # PROPERTIES #
    # empty properties later set
    _variable_dict = {}
    _num_constraints = {}
    numerics = {}
    _name_buffer = {}
    _variables_in_constraint = {}
    _dual_constraint_expression = {}
    def __init__(self):
        """This method initializes the model as an abstract pyomo model.
        
        Hence, all necessary pe.Sets, pe.Params (including scaling parameters), pe.Vars, pe.Constraints as well as the Objective Function are setup here"""
        # set necessary properties
        setattr(self.__class__,'_variable_dict',{})
        setattr(self.__class__,'_num_constraints',{'evaluated':0,'skipped':0})
        setattr(self.__class__,'numerics',{'unscaled':{'max_val':{'var':None,'val': None},'min_val':{'var':None,'val': None},'sum_coeff_sq':0},'scaled':{'max_val':{'var':None,'val': None},'min_val':{'var':None,'val': None},'sum_coeff_sq':0}})
        setattr(self.__class__, '_name_buffer',{})
        setattr(self.__class__, '_variables_in_constraint',{})

        # set dict of increased Big-M values and set boolean of large Big-M values to False
        self.increased_big_M_values = {}

        # create Pyomo AbstractModel
        self.model = pe.AbstractModel()
        
        # SETS # Declaration of sets used in the optimization
        self.setupSets()

        # PARAMETERS # Declaration of parameters used in the optimization
        self.setupParameters()
        
        # PARAMETERS # Declaration of scaling parameters used in the optimization
        self.setupScalingParameters()

        # PARAMETERS
        self.setupDualScalingParameters()

        # VARIABLES # Declaration of primal variables used in the optimization
        self.setupPrimalVariables()

        # VARIABLES # Definition of UL variables used in the optimization
        self.setupULVariables()
        
        # CONSTRAINTS # Definition of primal constraints used in the optimization
        self.setupPrimalConstraints()

        # OBJECTIVE # Definition of LL objective used in the optimization - only to compute Dual Constraints
        self.setupLLObjective()

        # VARIABLES # Declaration of dual variables used in the optimization
        self.setupDualVariables()

        # DUAL CONSTRAINTS # Definition of dual constraints used in the optimization
        self.setupDualConstraints()


        # STRONG DUALITY CONSTRAINT #  Definition of strong duality constraint
        self.setupStrongDualityConstraint()

        # CONSTRAINTS # Definition of UL constraints used in the optimization
        self.setupULConstraints()

        # OBJECTIVE # Definition of UL objective used in the optimization
        self.setupULObjective()

        # OBJECTIVE (EXpression) # Definition of Short and Long UL objective used in the optimization
        # Only for result consistency examination
        self.setupShortULObjective()

        self.setupLongULObjective()
        
    # METHODS #
    @staticmethod
    def calculate_scaling_factor(scaling_vector):
        """ returns scaling factor r or c for scaling_vector as Arg and scaling algorithm selected in config
        Args: 
            scaling_vector(list): list with coefficients

        Returns:
            scaling_factor(float): scaling factor corresponding to scaling_vector
        """
        scaling_vector = np.abs(scaling_vector)
        # if scaling_vector not empty vector
        if not scaling_vector.size == 0:
            # if all coefficients == 1
            if np.all(scaling_vector == 1):
                scaling_factor = 1
            # else, apply one of the following algorithms
            else:
                if config.scaling_options['algorithm'] == 'approx_geom_mean': # geometric mean scaling
                    scaling_factor_til = np.sqrt(1/(scaling_vector.max()*scaling_vector.min()))
                elif config.scaling_options['algorithm'] == 'geom_mean': # exact geometric mean scaling
                    scaling_factor_til = 1/sp.gmean(scaling_vector)
                elif config.scaling_options['algorithm'] == 'arith_mean': # arithmetic mean scaling
                    scaling_factor_til = 1/np.mean(scaling_vector)
                elif config.scaling_options['algorithm'] == 'equi': # equilibrium scaling
                    scaling_factor_til = 1/np.max(scaling_vector)
                else:
                    raise KeyError('Scaling Algorithm {} not found'.format(config.scaling_options['algorithm']))

                if config.scaling_options['Round_to_power_of_2'] and not config.scaling_options['algorithm'] == 'equi':
                    # round to the closest power of two
                    scaling_factor = 2**(np.round(np.log2(scaling_factor_til)))
                else:
                    scaling_factor = scaling_factor_til
        else:
            scaling_factor = None
        # return scaling factor
        return scaling_factor
        
    @classmethod
    def add_evaluated_constraint(cls):
        """ this method increases the counter of evaluated constraints by one """
        cls._num_constraints['evaluated'] += 1

    @classmethod
    def add_skipped_constraint(cls):
        """ this method increases the counter of skipped constraints by one """
        cls._num_constraints['skipped'] += 1

    @classmethod
    def calculate_matrix_properties(cls):
        """ this classmethod calculates the highest and lowest matrix coefficient, and the frobenius norm for the scaled and unscaled model
        Performed after initialization of model.
        Args: 
            cls: class Optimization
        """
        # unscaled model
        logging.info('Numeric properties of unscaled model:')
        # if properties exist for unscaled model --> scaling enabled
        if cls.numerics['unscaled']['max_val']['val'] is not None:
            _model_type = 'unscaled'
            logging.info('Largest matrix coefficient: {:.3e} in {}'.format(cls.numerics[_model_type]['max_val']['val'],cls.numerics[_model_type]['max_val']['var']))
            logging.info('Smallest (nonzero) matrix coefficient: {:.3e} in {}'.format(cls.numerics[_model_type]['min_val']['val'],cls.numerics[_model_type]['min_val']['var']))
            _matrix_range = np.round(np.log10(cls.numerics[_model_type]['max_val']['val']))-np.round(np.log10(cls.numerics[_model_type]['min_val']['val']))
            logging.info('Range of order of magnitude: {}'.format(_matrix_range))
            logging.info('Frobenius norm: {:.3e}'.format(np.sqrt(cls.numerics[_model_type]['sum_coeff_sq'])))
            # scaled model
            logging.info('Numeric properties of scaled model:')
        _model_type = 'scaled'
        logging.info('Largest matrix coefficient: {:.3e} in {}'.format(cls.numerics[_model_type]['max_val']['val'],cls.numerics[_model_type]['max_val']['var']))
        logging.info('Smallest (nonzero) matrix coefficient: {:.3e} in {}'.format(cls.numerics[_model_type]['min_val']['val'],cls.numerics[_model_type]['min_val']['var']))
        _matrix_range = np.round(np.log10(cls.numerics[_model_type]['max_val']['val']))-np.round(np.log10(cls.numerics[_model_type]['min_val']['val']))
        logging.info('Range of order of magnitude: {}'.format(_matrix_range))
        logging.info('Frobenius norm: {:.3e}'.format(np.sqrt(cls.numerics[_model_type]['sum_coeff_sq'])))

    @classmethod
    def set_component_name_to_name_buffer(cls,component):
        """ This classmethod receives a component and sets the component name and id in the _name_buffer """
        cls._name_buffer[id(component)] = component.getname()

    @classmethod
    def set_dual_expression_of_variable(cls,variable_id,dual_expression):
        """ This classmethod sets the term of the dual_expression of a variable correspondent to a constraint """
        try:
            cls._dual_constraint_expression[variable_id] += dual_expression
        except KeyError:
            cls._dual_constraint_expression[variable_id] = dual_expression

    @classmethod
    def get_dual_expression_of_variable(cls,variable_id = None):
        """ This classmethod gets the dual_expression of a variable.
        
        If no variable_id passed, it checks whether cls._dual_constraint_expression is empty (hence not yet iterated) """
        if variable_id:
            try:
                return(cls._dual_constraint_expression[variable_id])
            except KeyError:
                return None
        # check if already iterated
        else:
            if not cls._dual_constraint_expression:
                return False
            else:
                return True

    @classmethod
    def set_variable_to_constraint(cls,constraint_id,variable_id,coefficient):
        """ This classmethod saves the coefficients of the variables in each constraint, so that EXPR.decompose_term has to be called as little as possible """
        try:
            cls._variables_in_constraint[constraint_id][variable_id] = coefficient
        except KeyError:
            cls._variables_in_constraint[constraint_id] = {variable_id: coefficient} 
    
    @classmethod
    def get_variable_of_constraint(cls,constraint_id):
        """ This classmethod returns the variable ids and coefficients of each constraint. If constraint is not yet saved, returns None """
        try:
            return(cls._variables_in_constraint[constraint_id])
        except KeyError:
            return None

    @staticmethod
    def get_dual_constraint_expression_for_variable(model,variable,data):
        """ This method receives a variable and calculates the dual cosntraint expression for this variable 
        
        Let L(x) = f(x) + \lambda*g(x) be the lagrange equation to 
        f(x)
        s.t. g(x) <= 0
        
        Then this method returns d(L(x))/dx = 0 
        
        If variable has implicit lower bound (e.g. because within = pe.NonNegativeReals) then add -dual_variable_lower_bound_<variable>
        
        Note: Unlike in original SecMOD framework, scaling parameters are not included here."""
        # skip constraint for fixed variable
        if variable[data].value == 0:
            return pe.Constraint.Skip

        dual_constraint_expr = 0 
        objective = model.LLObjectiveExpression.expr
        objective_coefficient = Optimization.get_variable_of_constraint(id(objective))
        if objective_coefficient:
            if id(variable[data]) in objective_coefficient:
                dual_constraint_expr += objective_coefficient[id(variable[data])]
        else:
            for item in EXPR.decompose_term(objective)[1]:
                if item[1] is not None:
                    Optimization.set_variable_to_constraint(id(objective),id(item[1]),item[0])
                    if id(variable[data]) == id(item[1]):
                        dual_constraint_expr += item[0]
        # if constraints not yet iterated (_dual_constraint_expression is empty), iterate and fill cls._dual_constraint_expression
        if not Optimization.get_dual_expression_of_variable():
            for constraint in model.component_objects(pe.Constraint, active =True):
                if "dual_constraint" not in constraint.getname(name_buffer = Optimization._name_buffer) and "binary_expansion" not in constraint.getname(name_buffer = Optimization._name_buffer):
                    _index = [id(index) for index in constraint.index_set().subsets()]
                    for data_constraint in constraint:
                        if id(model.time_slices) in _index:
                            time_slice_weight = model.time_slice_yearly_weight[data_constraint[_index.index(id(model.time_slices))]]
                        else:
                            time_slice_weight = 1
                        dual_variable = model.find_component(constraint.getname(name_buffer = Optimization._name_buffer).replace("constraint","dual_variable"))[data_constraint]
                        if type(constraint[data_constraint].expr) is EXPR.InequalityExpression and constraint[data_constraint].lower is not None:
                            multiplicator = -1
                        else:
                            multiplicator = 1
                        # iterate through variables in constraint
                        for item in EXPR.decompose_term(constraint[data_constraint].body)[1]:
                            if item[1] is not None:
                                _term_dual_expression = item[0]*dual_variable*multiplicator*time_slice_weight
                                Optimization.set_dual_expression_of_variable(id(item[1]),_term_dual_expression)
        # get dual constraint
        dual_constraint_expr += Optimization.get_dual_expression_of_variable(id(variable[data]))
        # if variable has lower bound, add dual_variable_lower_bound
        if variable[data].lb is not None:
            if variable[data].lb == 0:
                _index = [id(index) for index in variable.index_set().subsets()]
                if id(model.time_slices) in _index:
                    time_slice_weight = model.time_slice_yearly_weight[data[_index.index(id(model.time_slices))]]
                else:
                    time_slice_weight = 1
                dual_constraint_expr -= model.find_component("dual_variable_lower_bound_{}".format(variable.getname(name_buffer = Optimization._name_buffer)))[data]*time_slice_weight       
            else:
                raise NotImplementedError('Not implemented for implicit lower bound of not zero but {}'.format(variable[data].lb))
        # if reference node
        if "phase_difference" in variable.name and data[0] == model.transmission_reference_node.value:
            dual_constraint_expr += model.dual_variable_phase_difference*model.scaling_dual_variable_phase_difference

        return(dual_constraint_expr==0)

    @staticmethod
    def fix_variables(*args):
        """ This method sets the variables that need to be fixed to 0 at initialization 
        
        By setting the default value to 0, the fixed variables can be identified before explicitly fixing them. 
        If condition is met, function returns 0, otherwise None. This is subsequently set as the variable's default value
        Args:
            *args: 
                args[0](str): string of variable name to identify variable
                args[1](pe.ConcreteModel): ConcreteModel with necessary sets and parameters
                args[2:]: Set indizes which define the current variable index (e.g. year, product, production_process, node etc.) 
        """
        # select items from *args, see variable definition for order of data
        variable_name = args[0]
        model = args[1]
        data = args[2:]
        # get type_of_optimization
        type_of_optimization = model.type_of_optimization.data()[0]
        # fix binary variables to zero if dual_variable is zero (because variable has no lower bound or constraint is skipped) 
        # or if domain of dual variable is pe.Reals (because primal constraint is equality constraint)
        if "binary_" in variable_name and "binary_expansion" not in variable_name:
            if model.find_component(variable_name.replace("binary_","dual_variable_"))[data].value == 0 or model.find_component(variable_name.replace("binary_","dual_variable_"))[data].lb is None:
                return 0
        # fix dual_variable_lower_bound to zero 
        elif "dual_variable" in variable_name:
            if "lower_bound" in variable_name:
                # if variable has no lower bound
                if model.find_component(variable_name.replace("dual_variable_lower_bound_",""))[data].lb is None:
                    return 0
                # if variable is fixed
                elif model.find_component(variable_name.replace("dual_variable_lower_bound_",""))[data].value == 0:
                    return 0
            # fix dual variable to zero if constraint is skipped
            elif data not in model.find_component(variable_name.replace("dual_variable_","constraint_")):
                return 0

        elif "overshoot" in variable_name:
            # fix overshoot only if both all impacts fixed and especially operational impact fixed as well
            if config.fix_impact_overshoot:
                if not (not config.fix_operational_impact_overshoot and "operational_impact_overshoot" in variable_name):
                    return 0
                elif data[0] not in model.capped_impacts:
                    return 0
        # fix auxiliary_variable if new_capacity_production fixed or CF == 0
        elif "auxiliary_variable" in variable_name:
            if model.new_capacity_production[data[1], data[3]].value == 0:
                return 0
            elif model.usable_capacity_factor_timeseries_production[model.nodes[model.current_node.value],data[1],data[4]] == 0:
                return 0
        # fix binary_variable_binary_expansion if new_capacity_production fixed
        elif "binary_variable_binary_expansion" in variable_name:
            if model.new_capacity_production[data[1:3]].value == 0:
                return 0
        # used_production
        elif variable_name == 'used_production':
            # year - construction_year >= lifetime
            if ((data[2] - data[3]) >= model.lifetime_duration_production[data[1], data[3]]): 
                return 0
            # year < construction_year       
            elif data[2] < data[3]:
                return 0
            # existing_capacity == 0 
            elif model.existing_capacity_production[data[0], data[1], data[2], data[3]] == 0:
                # additional fixed variables for InvestmentOptimization
                if type_of_optimization == 'InvestmentOptimization':
                    if (sum([model.existing_capacity_production[data[0], data[1], data[2], construction_year] for construction_year in model.construction_years_production]) >= model.potential_capacity_production[data[0], data[1], data[2]]):
                        # sum(existing capacity) >= potential capacity
                        return 0
                    elif data[3] not in model.years:
                        # construction_year not in invest_year
                        return 0 
                    elif data[0] != model.nodes[model.current_node.value] and model.predicted_new_capacity_production[data[0:3]] == 0:
                        # node is not current node and predicted_new_capacity == 0
                        return 0
                # additional fixed variables for OperationalOptimization
                elif type_of_optimization == 'OperationalOptimization':
                    return 0
                # optimization type unknown
                else:
                    raise NotImplementedError('Not implemented for {}'.format(type_of_optimization))
            elif model.usable_capacity_factor_timeseries_production[data[0],data[1],data[4]] == 0:
                # usable capacity factor timeseries == 0
                return 0
        # used_storage
        elif variable_name == 'used_storage':
            # year - construction_year >= lifetime
            if ((data[3] - data[4]) >= model.lifetime_duration_storage[data[1], data[4]]):
                return 0
            # year < construction_year       
            elif data[3] < data[4]:
                return 0
            elif model.existing_capacity_storage[data[0], data[1], data[3], data[4]] == 0:
                # additional fixed variables for 'InvestmentOptimization'
                if type_of_optimization == 'InvestmentOptimization':   
                    if (sum([model.existing_capacity_storage[data[0], data[1], data[3], construction_year] for construction_year in model.construction_years_storage]) >= model.potential_capacity_storage[data[0], data[1], data[3]]):
                        # sum(existing capacity) >= potential capacity
                        return 0
                    elif data[3] not in model.years:
                        # construction_year not in invest_year
                        return 0 
                # additional fixed variables for OperationalOptimization
                elif type_of_optimization == 'OperationalOptimization':
                    return 0
                else:
                    raise NotImplementedError('Not implemented for {}'.format(type_of_optimization))
        # used_transshipment
        elif variable_name == 'used_transshipment':
            # year - construction_year >= lifetime
            if ((data[3] - data[4]) >= model.lifetime_duration_transshipment[data[1], data[4]]):
                return 0
            # year < construction_year       
            elif data[3] < data[4]:
                return 0
            # existing_capacity == 0
            elif model.existing_capacity_transshipment[data[0], data[1], data[3], data[4]] == 0:
                # additional fixed variables for 'InvestmentOptimization'
                if type_of_optimization == 'InvestmentOptimization':   
                    if (sum([model.existing_capacity_transshipment[data[0], data[1], data[3], construction_year] for construction_year in model.construction_years_transshipment]) >= model.potential_capacity_transshipment[data[0], data[1], data[3]]):
                        # sum(existing capacity) >= potential capacity
                        return 0
                    elif data[3] not in model.years:
                        # construction_year not in invest_year
                        return 0 
                elif type_of_optimization == 'OperationalOptimization':
                    return 0
                else:
                    raise NotImplementedError('Not implemented for {}'.format(type_of_optimization))
        # fixing the transmission phase difference of one node to zero.
        elif variable_name == 'phase_difference':
            # if node == transmission_reference_node
            if data[0] == pe.value(model.transmission_reference_node):
                return 0
        # different Optimization subclasses
        if type_of_optimization == 'InvestmentOptimization':
            # Fixes the new capacity variables for all processes which have no potential left and the used capacity variables which therefore have no existing capacity either.
            if variable_name == 'new_capacity_production':
                # if sum(existing_capacity) >= potential capacity
                if (sum(model.existing_capacity_production[model.nodes[model.current_node.value], data[0], data[1], construction_year] for construction_year in model.construction_years_production) >= model.potential_capacity_production[model.nodes[model.current_node.value], data[0], data[1]]):
                    return 0
        # if OperationalOptimization, fix new capacities
        elif type_of_optimization == 'OperationalOptimization':
            if variable_name == 'new_capacity_production':
                return 0
            
        # unknown type of optimization
        else:
            raise NotImplementedError('Not implemented for {}'.format(type_of_optimization))
        # if not fixed, return None
        return None

    @staticmethod
    def get_domain_of_dual_variable(*args):
        """ This method sets the domain of the dual variables correspondent to the primal constraint. 
        If the constraint is an inequality constraint, the dual variable is >= 0 (pe. NonNegativeReals) 
        
        Args:
            *args: 
                args[0](str): string of variable name to identify variable
                args[1](pe.ConcreteModel): ConcreteModel with necessary sets and parameters
                args[2:]: Set indizes which define the current variable index (e.g. year, product, production_process, node etc.) 
        """
        # select items from *args, see variable definition for order of data
        variable_name = args[0]
        model = args[1]
        data = args[2:]

        # skipped constraint
        if data not in model.find_component(variable_name.replace("dual_variable_","constraint_")):
            return(pe.Reals)
        # inequality constraint
        if type(model.find_component(variable_name.replace("dual_variable_","constraint_"))[data].expr) is EXPR.InequalityExpression:
            return(pe.NonNegativeReals)
        # equality constraint
        else:
            return(pe.Reals)
       
    def setupSets(self):
        """This method sets up all Sets required the optimization model.

        Some Sets are initialized with default values, if no other values are specified in the input file.
        """
        # pe.Set of the type of optimization
        self.model.type_of_optimization = pe.Set()
        # pe.Set of primal and dual indizes for Big M parameters 
        self.model.Big_M_primal_dual = pe.Set(
            initialize = {"primal","dual"})
        # pe.Set of the nodes of the grid
        self.model.nodes = pe.Set()
        # pe.Set of the connections between the nodes of the grid
        self.model.connections = pe.Set()
        # pe.Set of two nodes to identify both nodes of a connection
        self.model.connection_nodes = pe.Set(
            initialize={"node1", "node2"})
        # pe.Set of the used products for which there is a demand
        self.model.products = pe.Set()
        # pe.Set of the traded products
        self.model.traded_products = pe.Set()
        # pe.Set of the capped impacts
        self.model.capped_impacts = pe.Set()
        # pe.Set of the used processes for production
        self.model.processes_production = pe.Set()
        # pe.Set of the used processes for storage of products
        self.model.processes_storage = pe.Set()
        # pe.Set of the used processes for transshipment of products between nodes
        self.model.processes_transshipment = pe.Set()
        # pe.Set of the used processes for transshipment of products between nodes
        self.model.processes_transmission = pe.Set()
        # pe.Set of storage directions (deposit and withdraw)
        self.model.directions_storage = pe.Set(
            initialize={"deposit", "withdraw"})
        # pe.Set of transshipment direction (forward and backward)
        self.model.directions_transshipment = pe.Set(
            initialize={"forward", "backward"})
        # pe.Set of the properties of a transmission process/power line
        self.model.power_line_property_categories = pe.Set(
            initialize={
                "power limit",
                "susceptance per unit"})
        # pe.Set of products using transmission via DC load flow
        self.model.products_transmission = pe.Set(
            initialize={"electricity"})
        # pe.Set of the years of construction of existing capacity
        self.model.construction_years_production = pe.Set(
            within=pe.NonNegativeIntegers)
        # pe.Set of the years of construction of existing capacity
        self.model.construction_years_storage = pe.Set(
            within=pe.NonNegativeIntegers)
        # pe.Set of the years of construction of existing capacity
        self.model.construction_years_transshipment = pe.Set(
            within=pe.NonNegativeIntegers)
        # pe.Set of the years of construction of existing capacity
        self.model.construction_years_transmission = pe.Set(
            within=pe.NonNegativeIntegers)
        # pe.Set of the years in the current optimization period
        self.model.years = pe.Set(
            within=pe.NonNegativeIntegers)
        # pe.Set of the time slices of a year
        self.model.time_slices = pe.Set(
            within=pe.NonNegativeIntegers)
        # pe.Set of the impact categories, including environmental and monetary costs
        self.model.impact_categories = pe.Set()
        # pe.Set of the impact sources, e.g. whether the impacts result from invest or operation
        self.model.impact_sources = pe.Set(
            initialize={"operation", "invest"})

    def setupParameters(self):
        """This method sets up all Parameters required by the optimization model.

        """
        # pe.Param for current node (i.e., player)
        self.model.current_node = pe.Param(
            default = 1,
            within = pe.NonNegativeIntegers,
            doc="Parameter which contains the current node.")

        # predicted new capacity of all technologies
        # Nash assumption of each player that the strategy (i.e., the new capacity of every other player is known and constant)
        # new installed capacity of production technologies based on nodes and investment years
        self.model.predicted_new_capacity_production = pe.Param(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            within=pe.NonNegativeReals,
            default=0,
            doc="Parameter which contains the data about the predicted new capacity of a production process. \
                Nash assumption of each player that the strategy (i.e., the new capacity of every other player is known and constant) \
                First 'year' marks the year for which the value is valid. Second 'year' marks the \
                year of construction.")

        # length of the connections of the grid
        self.model.connection_length = pe.Param(
            self.model.connections,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the length of all connections of the grid.")
        # technology matrices, which define how much of any product is produces by a process
        # technology matrix of production processes like power plant processes
        self.model.technology_matrix_production = pe.Param(
            self.model.products,
            self.model.nodes,
            self.model.processes_production,
            self.model.construction_years_production,
            default=0,
            doc="Parameter which contains the data about which products take part in a production process.")
        # technology matrix of storage processes like pumped hydro storage
        self.model.efficiency_matrix_storage = pe.Param(
            self.model.products,
            self.model.processes_storage,
            self.model.directions_storage,
            self.model.construction_years_storage,
            default=0,
            doc="Parameter which contains the data about which products take part in a storage process.")
        
        # products stored by storage processes
        self.model.storage_products = pe.Param(
            self.model.processes_storage,
            within=self.model.products,
            doc="Parameter which contains the product stored by a storage process.")
        # efficiency of transshipment processes along all connections
        self.model.transshipment_efficiency = pe.Param(
            self.model.processes_transshipment,
            self.model.connections,
            default=0,
            doc="Parameter which contains the efficiency of a transshipment process along a connection.")
        # products which are transshipped by the corresponding transshipment processes
        self.model.transshipment_products = pe.Param(
            self.model.processes_transshipment,
            within = pe.Any,
            default=None,
            doc="Parameter which contains the transshipped product of each transshipment process.")
        # properties of each existing power line, regarding power limit and susceptance per unit
        self.model.power_line_properties = pe.Param(
            self.model.connections,
            self.model.years,
            self.model.power_line_property_categories,
            within=pe.NonNegativeReals,
            default=0,
            doc="Parameter which contains the power limit and susceptance per unit of the existing capacity.")

        # timeseries multiplier of the technology matrix of production processes
        self.model.technology_matrix_production_timeseries = pe.Param(
            self.model.products,
            self.model.processes_production,
            self.model.time_slices,
            default=1,
            doc="Parameter which contains the time series factor for the technology matrix of a production process.")
        
        # impact matrices, which define how big the life cycle impact of any process is - includes monetary costs
        # impact matrix of production processes
        self.model.impact_matrix_production = pe.Param(
            self.model.impact_categories,
            self.model.impact_sources,
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            doc="Parameter which contains the data about the impacts of a production process. \
                First 'year' marks the year for which the value is valid. Second 'year' marks the \
                year of construction.")
        # impact matrix of storage processes
        self.model.impact_matrix_storage = pe.Param(
            self.model.impact_categories,
            self.model.impact_sources,
            self.model.processes_storage,
            self.model.years,
            self.model.construction_years_storage,
            doc="Parameter which contains the data about the impacts of a storage process. \
                Operational impact data is based on the **withdrawn** product. \
                First 'year' marks the year for which the value is valid. Second 'year' marks the \
                year of construction.")
        # impact matrix of transshipment processes
        self.model.impact_matrix_transshipment = pe.Param(
            self.model.impact_categories,
            self.model.impact_sources,
            self.model.processes_transshipment,
            self.model.years,
            self.model.construction_years_transshipment,
            doc="Parameter which contains the data about the impacts of a transshipment process. \
                First 'year' marks the year for which the value is valid. Second 'year' marks the \
                year of construction.")
        # impact matrix of transmssion processes
        self.model.impact_matrix_transmission = pe.Param(
            self.model.impact_categories,
            self.model.impact_sources,
            self.model.processes_transmission,
            self.model.years,
            self.model.construction_years_transmission,
            doc="Parameter which contains the data about the impacts of transmission processes \
                in the transmission grid. First 'year' marks the year for which the value is valid. \
                Second 'year' marks the year of construction.")
        # impact matrix of non-served demand of a product
        self.model.impact_matrix_non_served_demand = pe.Param(
            self.model.impact_categories,
            self.model.products,
            self.model.years,
            self.model.time_slices,
            doc="Parameter which contains the data about the impacts of non-served demand.")
        
        # lifetime duration of production process
        self.model.lifetime_duration_production = pe.Param(
            self.model.processes_production,
            self.model.construction_years_production,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the lifetime duration of each production process\
                in the corresponding construction year.")
        # lifetime duration of storage process
        self.model.lifetime_duration_storage = pe.Param(
            self.model.processes_storage,
            self.model.construction_years_storage,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the lifetime duration of each storage process\
                in the corresponding construction year.")
        # lifetime duration of transshipment process
        self.model.lifetime_duration_transshipment = pe.Param(
            self.model.processes_transshipment,
            self.model.construction_years_transshipment,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the lifetime duration of each production process\
                in the corresponding construction year.")
        # lifetime duration of transmission process
        self.model.lifetime_duration_transmission = pe.Param(
            self.model.processes_transmission,
            self.model.construction_years_transmission,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the lifetime duration of each production process\
                in the corresponding construction year.")
        
        # existing capacity of all technologies
        # installed capacity of production technologies based on nodes and investment years
        self.model.existing_capacity_production = pe.Param(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            within=pe.NonNegativeReals,
            default=0,
            doc="Parameter which contains the data about the existing capacity of a production process. \
                First 'year' marks the year for which the value is valid. Second 'year' marks the \
                year of construction.")

        
        # installed capacity of transshipment technologies based on nodes and investment years
        self.model.existing_capacity_transmission = pe.Param(
            self.model.connections,
            self.model.processes_transmission,
            self.model.years,
            self.model.construction_years_transmission,
            default=0,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the existing capacity of a transshipment process. \
                First 'year' marks the year for which the value is valid. Second 'year' marks the \
                year of construction.")
        # product flow to storage capacity factor based on processes and investment years
        self.model.flow_to_storage_capacity_factor = pe.Param(
            self.model.processes_storage,
            self.model.construction_years_storage,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the relation between the maximum input \
                and output of a storage process and its storage capacity.")
        # lists of nodes connected by connections
        self.model.connected_nodes = pe.Param(
            self.model.connections,
            self.model.connection_nodes,
            within = pe.NonNegativeIntegers,
            doc="Parameter which contains the two nodes connected by a connection process.")
        # reference node of which the phase difference is pe.Set to zero
        self.model.transmission_reference_node = pe.Param(
            within=self.model.nodes,
            doc="Parameter which contains one node which is used as reference node for the \
                calculation of the phase difference between the nodes. Therefore, its \
                phase difference is set to zero.")
        # apparent power base value for the per-unit calculations
        self.model.per_unit_base = pe.Param(
            within=pe.NonNegativeReals,
            default=500,
            doc="Parameter which contains the base value in MVA for the per-unit calculations.")
        # time dependent parameters
        # weight of every time slice regarding the full year - sum of all shares equals 8760 hours
        self.model.time_slice_yearly_weight = pe.Param(
            self.model.time_slices,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the share of a year represented by each time slice.")
        # actual duration of one time slice - necessary for storage level calculations
        self.model.time_slice_duration = pe.Param(
            self.model.time_slices,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the duration of each time slice.")
        # next connected time slice of the current time slice, used for storage level boundaries
        self.model.next_time_slice = pe.Param(
            self.model.time_slices,
            doc="Parameter which contains the data about the connections between the time slices.")
        # timeseries for the available share of any capacity e.g. production from renewable energy
        self.model.usable_capacity_factor_timeseries_production = pe.Param(
            self.model.nodes,
            self.model.processes_production,
            self.model.time_slices,
            within=pe.NonNegativeReals,
            default=1,
            doc="Parameter which contains the data about the available share of capacity of a production \
                process in a specific time slice.")
        # demand for every product
        self.model.demand = pe.Param(
            self.model.products,
            self.model.years,
            self.model.time_slices,
            self.model.nodes,
            default=0,
            doc="Parameter which contains the data about the demand for each product at each node.")
        # parameters regarding the impact of the system
        # limits for operational impact
        # nodal limits for the operational impacts
        self.model.operational_nodal_impact_limits = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default=float("inf"),
            doc="Parameter which contains all limits for the operational impacts on nodal base.")
        # overall limits for the operational impacts
        self.model.operational_impact_limits = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default=float("inf"),
            doc="Parameter which contains all limits for the operational impacts overall.")
        # limits for invest impact
        # nodal limits for the invest impacts
        self.model.invest_nodal_impact_limits = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default=float("inf"),
            doc="Parameter which contains all limits for the invest impacts on nodal base.")
        # overall limits for the invest impacts
        self.model.invest_impact_limits = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default=float("inf"),
            doc="Parameter which contains all limits for the invest impacts overall.")
        # limits for total impact
        # nodal limits for the total impacts
        self.model.total_nodal_impact_limits = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default=float("inf"),
            doc="Parameter which contains all limits for the total impacts on nodal base.")
        # overall limits for the total impacts
        self.model.total_impact_limits = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default=float("inf"),
            doc="Parameter which contains all limits for the total impacts overall.")
        # parameters which specify with which weight impacts is taken into account in the pe.Objective rule
        self.model.objective_factor_impacts = pe.Param(
            self.model.impact_sources,
            self.model.impact_categories,
            self.model.years,
            default=0,
            doc="Parameter which defines the factor used to multiply an impact category with in the objective rule.")
        # parameters for the weight of overshoot and non-served demand variables
        # parameters which specify with which weight impact overshoots are taken into account in the pe.Objective rule
        self.model.objective_factor_impact_overshoot = pe.Param(
            self.model.impact_categories,
            self.model.years,
            doc="Parameters which specify with which weight impact overshoots are taken into account in the objective rule.")
        # parameter used for the transshipment in different directions. 
        # Therefore its values need to be 
        self.model.factor_transshipment = pe.Param(
            self.model.directions_transshipment,
            initialize={"forward": 1, "backward": -1},
            doc="Parameter which is used for transshipment equations and does not need further initialization.")
        # additional parameter to be used for the calculation of the impact of storage usage on the storage level
        # therefore its values need to be {"withdraw": -1, "deposit": 1}
        self.model.storage_level_factor = pe.Param(
            self.model.directions_storage,
            initialize={"withdraw": -1, "deposit": 1},
            doc="Parameter which is used for storage equations and does not need further initialization.")
        # capacity limits of all processes on nodal base
        # capacity limits of production processes
        self.model.potential_capacity_production = pe.Param(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            within=pe.NonNegativeReals,
            default=float("inf"),
            doc="Parameter which contains the data about the capacity limit of a production process at each node.")
        # capacity limits of storage processes
        self.model.potential_capacity_storage = pe.Param(
            self.model.nodes,
            self.model.processes_storage,
            self.model.years,
            within=pe.NonNegativeReals,
            default=float("inf"),
            doc="Parameter which contains the data about the capacity limit of a storage process at each node.")
        # capacity limits of transshipment processes
        self.model.potential_capacity_transshipment = pe.Param(
            self.model.connections,
            self.model.processes_transshipment,
            self.model.years,
            within=pe.NonNegativeReals,
            default=float("inf"),
            doc="Parameter which contains the data about the capacity limit of a transshipment process at each node.")
        # capacity limits of transmission processes
        self.model.potential_capacity_transmission = pe.Param(
            self.model.connections,
            self.model.processes_transmission,
            self.model.years,
            within=pe.NonNegativeReals,
            default=float("inf"),
            doc="Parameter which contains the data about the capacity limit of a transmission process at each connection.")
        # power limit per circuit of each transmission process
        self.model.power_limit_per_circuit = pe.Param(
            self.model.processes_transmission,
            default=0,
            doc="Parameter which contains the power limit per circuit of each transmission process.")
        # reliability margin for a transmission process
        self.model.reliability_margin_transmission = pe.Param(
            self.model.processes_transmission,
            within=pe.NonNegativeReals,
            default=0,
            doc="Parameter which contains the data about the share of the power limit of a process that can be used.")
        # parameters necessary for reaching the secured capacity
        self.model.surplus_capacity_factor_production = pe.Param(
            default = 1.01,
            within = pe.NonNegativeReals,
            doc = "Parameter which contains the data about the surplus capacity factor, i.e., the factor by which the capacity of all nodes \
                has to exceed the total demand. Default is 1% (1.01)")
        # secured capacity factor for production processes
        self.model.secured_capacity_factors_production = pe.Param(
            self.model.processes_production,
            self.model.construction_years_production,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the secured capacity factor of a production process.")
        # secured capacity factor for storage processes
        self.model.secured_capacity_factors_storage = pe.Param(
            self.model.processes_storage,
            self.model.construction_years_storage,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the secured capacity factor of a storage process.")
        # required total secured capacity e.g. peak load
        self.model.required_total_secured_capacity = pe.Param(
            self.model.products,
            self.model.years,
            default=0,
            doc="Parameter which contains the data about the required total secured capacity for a product")
        # required nodal secured capacity
        self.model.required_nodal_secured_capacity = pe.Param(
            self.model.products,
            self.model.nodes,
            self.model.years,
            default=0,
            doc="Parameter which contains the data about the required nodal secured capacity for a product")
        # NEW capacities
        # newly installed transmission capacity from switching
        self.model.new_capacity_transmission = pe.Param(
            self.model.connections,
            self.model.processes_transmission,
            self.model.years,
            initialize = 0.0,
            within=pe.NonNegativeReals)
    def setupScalingParameters(self):
        """ This functions sets up the scaling parameters of the variables required by the optimization model.

        The notation is the following: The scaling parameter for the component <component> is self.model.scaling_<component> 
        It is crucial to select the same pe.Sets as the corresponding component, to select default = 1, and mutable=True.
        Check setupVariables() for details on the variable declaration """    

        ## set up Parameters for scaling variables
        # slack variables
        self.model.scaling_nodal_slack_variable = pe.Param( 
            self.model.products,
            self.model.years,
            default = 1,
            mutable = True)
        self.model.scaling_total_slack_variable = pe.Param(
            self.model.products,
            self.model.years,
            default = 1,
            mutable = True)
        # usage of production technologies based on nodes and time
        self.model.scaling_used_production = pe.Param(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # usage of storage technologies based on nodes and time
        self.model.scaling_used_storage = pe.Param(
            self.model.nodes,
            self.model.processes_storage,
            self.model.directions_storage,
            self.model.years,
            self.model.construction_years_storage,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # usage of transshipment technologies based on nodes and time
        self.model.scaling_used_transshipment = pe.Param(
            self.model.connections,
            self.model.processes_transshipment,
            self.model.directions_transshipment,
            self.model.years,
            self.model.construction_years_transshipment,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # non-served demand of a product based on nodes and time
        self.model.scaling_non_served_demand = pe.Param(
            self.model.nodes,
            self.model.products,
            self.model.years,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # storage level of storage technologies based on nodes, years and time slices
        self.model.scaling_storage_level = pe.Param(
            self.model.nodes,
            self.model.processes_storage,
            self.model.years,
            self.model.construction_years_storage,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # DC load flow - transmission related variables
        self.model.scaling_phase_difference = pe.Param(
            self.model.nodes,
            self.model.years,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # transmission power
        self.model.scaling_used_transmission = pe.Param(
            self.model.connections,
            self.model.years,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # impact related variables
        # operational impact
        # nodal operational environmental and monetary impact based on impact categories and years
        self.model.scaling_operational_nodal_impact = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overshoot of nodal operational environmental and monetary impact based on impact categories and years
        self.model.scaling_operational_nodal_impact_overshoot = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overall operational environmental and monetary impact based on impact categories and years
        self.model.scaling_operational_impact = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overshoot of overall operational environmental and monetary impact based on impact categories and years
        self.model.scaling_operational_impact_overshoot = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # invest impact
        # nodal invest environmental and monetary impact based on impact categories and years
        self.model.scaling_invest_nodal_impact = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overshoot of nodal invest environmental and monetary impact based on impact categories and years
        self.model.scaling_invest_nodal_impact_overshoot = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overall invest environmental and monetary impact based on impact categories and years
        self.model.scaling_invest_impact = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overshoot of overall invest environmental and monetary impact based on impact categories and years
        self.model.scaling_invest_impact_overshoot = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # total impact
        # nodal total environmental and monetary impact based on impact categories and years
        self.model.scaling_total_nodal_impact = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overshoot of nodal total environmental and monetary impact based on impact categories and years
        self.model.scaling_total_nodal_impact_overshoot = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overall total environmental and monetary impact based on impact categories and years
        self.model.scaling_total_impact = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overshoot of overall total environmental and monetary impact based on impact categories and years
        self.model.scaling_total_impact_overshoot = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # newly installed capacity of production technologies based on nodes and investment years
        self.model.scaling_new_capacity_production = pe.Param(
            self.model.processes_production,
            self.model.years,
            default = 1,
            mutable = True)
        # newly installed capacity of storage technologies based on nodes and investment years
        self.model.scaling_new_capacity_storage = pe.Param(
            self.model.nodes,
            self.model.processes_storage,
            self.model.years,
            default = 1,
            mutable = True)
        # newly installed capacity of transshipment technologies based on nodes and investment years
        self.model.scaling_new_capacity_transshipment = pe.Param(
            self.model.connections,
            self.model.processes_transshipment,
            self.model.years,
            default = 1,
            mutable = True)
        # newly installed transmission capacity from switching
        self.model.scaling_new_capacity_transmission = pe.Param(
            self.model.connections,
            self.model.processes_transmission,
            self.model.years,
            default = 1,
            mutable = True)

    def setupDualScalingParameters(self):
        """This method sets up all dual Scaling Parameters required by the optimization model.

        """
        #----------------------------------------------------------------------------#   
        ## Dual variables for each constraint, if constraint is inequality constraint,
        #  set domain to NonNegativeReals
        #----------------------------------------------------------------------------#   
        # declaring the balance equation for all products used in the optimization
        self.model.scaling_dual_variable_product_balance = pe.Param(
            self.model.nodes,
            self.model.products,
            self.model.years,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # declaring the limit of production, limited by the capacity
        self.model.scaling_dual_variable_production_limit_by_capacity = pe.Param(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # dual_variables for DC load flow modelling of transmission
        # calculating the used_transmission from the phase difference
        self.model.scaling_dual_variable_used_transmission_from_phase_difference = pe.Param(
            self.model.connections,
            self.model.years,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # declaring the limit of positive transmission, limited by the capacity
        self.model.scaling_dual_variable_transmission_limit_by_capacity_positive = pe.Param(
            self.model.connections,
            self.model.years,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # declaring the limit of negative transmission, limited by the capacity
        self.model.scaling_dual_variable_transmission_limit_by_capacity_negative = pe.Param(
            self.model.connections,
            self.model.years,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # impact calculation and limitation
        # operational impacts nodal based
        # declaring the calculation of all operational impact categories,
        # summed over all processes on nodal base
        self.model.scaling_dual_variable_operational_nodal_impact = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # declaring the calculation of all operational impact categories,
        # summed over all processes on nodal base
        self.model.scaling_dual_variable_operational_nodal_impact_limits = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # operational impacts over all nodes
        # declaring the calculation of all operational impact categories,
        # summed over all processes
        self.model.scaling_dual_variable_operational_impact = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # declaring the calculation of all operational impact categories,
        # summed over all processes
        self.model.scaling_dual_variable_operational_impact_limits = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # invest impacts nodal based
        # declaring the calculation of all invest impact categories,
        # summed over all processes on nodal base
        self.model.scaling_dual_variable_invest_nodal_impact = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # declaring the calculation of all invest impact categories,
        # summed over all processes on nodal base
        self.model.scaling_dual_variable_invest_nodal_impact_limits = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # invest impacts over all nodes
        # declaring the calculation of all invest impact categories,
        # summed over all processes
        self.model.scaling_dual_variable_invest_impact = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # declaring the calculation of all invest impact categories,
        # summed over all processes
        self.model.scaling_dual_variable_invest_impact_limits = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # total impacts nodal based
        # declaring the calculation of all impact categories,
        # summed over all processes on nodal base
        self.model.scaling_dual_variable_total_nodal_impact = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # declaring the limitation of all impact categories on nodal scale
        self.model.scaling_dual_variable_total_nodal_impact_limits = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # total impacts over all nodes
        # declaring the calculation of all impact categories,
        # summed over all processes and nodes
        self.model.scaling_dual_variable_total_impact = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # declaring the limitation of all impact categories on global scale
        self.model.scaling_dual_variable_total_impact_limits = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        #----------------------------------------------------------------------------#    
        ## Dual variable for each lower bound on variable, fix to 0 if no lower bound
        #----------------------------------------------------------------------------#   
        self.model.scaling_dual_variable_lower_bound_used_production = pe.Param(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # non-served demand of a product based on nodes and time
        self.model.scaling_dual_variable_lower_bound_non_served_demand = pe.Param(
            self.model.nodes,
            self.model.products,
            self.model.years,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # DC load flow - transmission related variables
        self.model.scaling_dual_variable_lower_bound_phase_difference = pe.Param(
            self.model.nodes,
            self.model.years,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # impact related variables
        # operational impact
        # nodal operational environmental and monetary impact based on impact categories and years
        self.model.scaling_dual_variable_lower_bound_operational_nodal_impact = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overshoot of nodal operational environmental and monetary impact based on impact categories and years
        self.model.scaling_dual_variable_lower_bound_operational_nodal_impact_overshoot = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overall operational environmental and monetary impact based on impact categories and years
        self.model.scaling_dual_variable_lower_bound_operational_impact = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overshoot of overall operational environmental and monetary impact based on impact categories and years
        self.model.scaling_dual_variable_lower_bound_operational_impact_overshoot = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # invest impact
        # nodal invest environmental and monetary impact based on impact categories and years
        self.model.scaling_dual_variable_lower_bound_invest_nodal_impact = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overshoot of nodal invest environmental and monetary impact based on impact categories and years
        self.model.scaling_dual_variable_lower_bound_invest_nodal_impact_overshoot = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overall invest environmental and monetary impact based on impact categories and years
        self.model.scaling_dual_variable_lower_bound_invest_impact = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overshoot of overall invest environmental and monetary impact based on impact categories and years
        self.model.scaling_dual_variable_lower_bound_invest_impact_overshoot = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # total impact
        # nodal total environmental and monetary impact based on impact categories and years
        self.model.scaling_dual_variable_lower_bound_total_nodal_impact = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overshoot of nodal total environmental and monetary impact based on impact categories and years
        self.model.scaling_dual_variable_lower_bound_total_nodal_impact_overshoot = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overall total environmental and monetary impact based on impact categories and years
        self.model.scaling_dual_variable_lower_bound_total_impact = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overshoot of overall total environmental and monetary impact based on impact categories and years
        self.model.scaling_dual_variable_lower_bound_total_impact_overshoot = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overshoot of overall total environmental and monetary impact based on impact categories and years
        self.model.scaling_dual_variable_phase_difference = pe.Param(
            default = 1,
            mutable = True)
        #----------------------------------------------------------------------------#    
        ## Scaling Parameters for Primal Constraints
        #----------------------------------------------------------------------------#   
        # declaring the balance equation for all products used in the optimization
        self.model.scaling_constraint_product_balance = pe.Param(
            self.model.nodes,
            self.model.products,
            self.model.years,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # declaring the limit of production, limited by the capacity
        self.model.scaling_constraint_production_limit_by_capacity = pe.Param(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            self.model.time_slices,
            default = 1,
            mutable = True)
        
        # constraints for DC load flow modelling of transmission
        # calculating the used_transmission from the phase difference
        self.model.scaling_constraint_used_transmission_from_phase_difference = pe.Param(
            self.model.connections,
            self.model.years,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # declaring the limit of positive transmission, limited by the capacity
        self.model.scaling_constraint_transmission_limit_by_capacity_positive = pe.Param(
            self.model.connections,
            self.model.years,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # declaring the limit of negative transmission, limited by the capacity
        self.model.scaling_constraint_transmission_limit_by_capacity_negative = pe.Param(
            self.model.connections,
            self.model.years,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # impact calculation and limitation
        # operational impacts nodal based
        # declaring the calculation of all operational impact categories,
        # summed over all processes on nodal base
        self.model.scaling_constraint_operational_nodal_impact = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # declaring the calculation of all operational impact categories,
        # summed over all processes on nodal base
        self.model.scaling_constraint_operational_nodal_impact_limits = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # operational impacts over all nodes
        # declaring the calculation of all operational impact categories,
        # summed over all processes
        self.model.scaling_constraint_operational_impact = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # declaring the calculation of all operational impact categories,
        # summed over all processes
        self.model.scaling_constraint_operational_impact_limits = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # invest impacts nodal based
        # declaring the calculation of all invest impact categories,
        # summed over all processes on nodal base
        self.model.scaling_constraint_invest_nodal_impact = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # declaring the calculation of all invest impact categories,
        # summed over all processes on nodal base
        self.model.scaling_constraint_invest_nodal_impact_limits = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # invest impacts over all nodes
        # declaring the calculation of all invest impact categories,
        # summed over all processes
        self.model.scaling_constraint_invest_impact = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # declaring the calculation of all invest impact categories,
        # summed over all processes
        self.model.scaling_constraint_invest_impact_limits = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # total impacts nodal based
        # declaring the calculation of all impact categories,
        # summed over all processes on nodal base
        self.model.scaling_constraint_total_nodal_impact = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # declaring the limitation of all impact categories on nodal scale
        self.model.scaling_constraint_total_nodal_impact_limits = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # total impacts over all nodes
        # declaring the calculation of all impact categories,
        # summed over all processes and nodes
        self.model.scaling_constraint_total_impact = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # declaring the limitation of all impact categories on global scale
        self.model.scaling_constraint_total_impact_limits = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        
        ### UL constraints
        # declaring the limit of production capacity, limited by the provided input
        self.model.scaling_constraint_production_potential_capacity_by_input = pe.Param(
            self.model.processes_production,
            self.model.years,
            default = 1,
            mutable = True)

        # declaring the necessity of providing the secured capacity
        self.model.scaling_constraint_nodal_secured_capacity = pe.Param(
            self.model.products,
            self.model.years,
            default = 1,
            mutable = True)

        # declaring the necessity of providing the secured capacity
        self.model.scaling_constraint_total_secured_capacity = pe.Param(
            self.model.products,
            self.model.years,
            default = 1,
            mutable = True)
        #----------------------------------------------------------------------------#    
        ## Scaling Parameters for Dual Constraints
        #----------------------------------------------------------------------------#   
        # usage of production technologies based on nodes and time
        self.model.scaling_dual_constraint_used_production = pe.Param(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # non-served demand of a product based on nodes and time
        self.model.scaling_dual_constraint_non_served_demand = pe.Param(
            self.model.nodes,
            self.model.products,
            self.model.years,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # DC load flow - transmission related variables
        self.model.scaling_dual_constraint_phase_difference = pe.Param(
            self.model.nodes,
            self.model.years,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # used transmission power
        self.model.scaling_dual_constraint_used_transmission = pe.Param(
            self.model.connections,
            self.model.years,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # impact related variables
        # operational impact
        # nodal operational environmental and monetary impact based on impact categories and years
        self.model.scaling_dual_constraint_operational_nodal_impact = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overshoot of nodal operational environmental and monetary impact based on impact categories and years
        self.model.scaling_dual_constraint_operational_nodal_impact_overshoot = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overall operational environmental and monetary impact based on impact categories and years
        self.model.scaling_dual_constraint_operational_impact = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overshoot of overall operational environmental and monetary impact based on impact categories and years
        self.model.scaling_dual_constraint_operational_impact_overshoot = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # invest impact
        # nodal invest environmental and monetary impact based on impact categories and years
        self.model.scaling_dual_constraint_invest_nodal_impact = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overshoot of nodal invest environmental and monetary impact based on impact categories and years
        self.model.scaling_dual_constraint_invest_nodal_impact_overshoot = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overall invest environmental and monetary impact based on impact categories and years
        self.model.scaling_dual_constraint_invest_impact = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overshoot of overall invest environmental and monetary impact based on impact categories and years
        self.model.scaling_dual_constraint_invest_impact_overshoot = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # total impact
        # nodal total environmental and monetary impact based on impact categories and years
        self.model.scaling_dual_constraint_total_nodal_impact = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overshoot of nodal total environmental and monetary impact based on impact categories and years
        self.model.scaling_dual_constraint_total_nodal_impact_overshoot = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overall total environmental and monetary impact based on impact categories and years
        self.model.scaling_dual_constraint_total_impact = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overshoot of overall total environmental and monetary impact based on impact categories and years
        self.model.scaling_dual_constraint_total_impact_overshoot = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)

        ###############################
        ## Scaing Objective Function ##
        ###############################
        self.model.scaling_objective = pe.Param(
            default = 1,
            mutable = True)

    def setupPrimalVariables(self):
        """This method sets up all primal Variables required by the optimization model.

        """
        # slack variable required for constraint_nodal_secured_capacity of CURRENT node
        self.model.nodal_slack_variable = pe.Var( 
            self.model.products,
            self.model.years,
            within=pe.NonNegativeReals)
        # usage of production technologies based on nodes and time
        self.model.used_production = pe.Var(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            self.model.time_slices,
            rule = lambda *args: self.fix_variables('used_production',*args),
            within=pe.NonNegativeReals)
        # non-served demand of a product based on nodes and time
        self.model.non_served_demand = pe.Var(
            self.model.nodes,
            self.model.products,
            self.model.years,
            self.model.time_slices,
            within=pe.NonNegativeReals)
        # DC load flow - transmission related variables
        self.model.phase_difference = pe.Var(
            self.model.nodes,
            self.model.years,
            self.model.time_slices,
            rule = lambda *args: self.fix_variables('phase_difference',*args),
            within=pe.Reals)
        # transmission power
        self.model.used_transmission = pe.Var(
            self.model.connections,
            self.model.years,
            self.model.time_slices,
            within=pe.Reals)
        # impact related variables
        # operational impact
        # nodal operational environmental and monetary impact based on impact categories and years
        self.model.operational_nodal_impact = pe.Var(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            within=pe.Reals)
        # overshoot of nodal operational environmental and monetary impact based on impact categories and years
        self.model.operational_nodal_impact_overshoot = pe.Var(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule = lambda *args: self.fix_variables('operational_nodal_impact_overshoot',*args),
            within=pe.NonNegativeReals)
        # overall operational environmental and monetary impact based on impact categories and years
        self.model.operational_impact = pe.Var(
            self.model.impact_categories,
            self.model.years,
            within=pe.Reals)
        # overshoot of overall operational environmental and monetary impact based on impact categories and years
        self.model.operational_impact_overshoot = pe.Var(
            self.model.impact_categories,
            self.model.years,
            rule = lambda *args: self.fix_variables('operational_impact_overshoot',*args),
            within=pe.NonNegativeReals)
        # invest impact
        # nodal invest environmental and monetary impact based on impact categories and years
        self.model.invest_nodal_impact = pe.Var(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            within=pe.Reals)
        # overshoot of nodal invest environmental and monetary impact based on impact categories and years
        self.model.invest_nodal_impact_overshoot = pe.Var(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule = lambda *args: self.fix_variables('invest_nodal_impact_overshoot',*args),
            within=pe.NonNegativeReals)
        # overall invest environmental and monetary impact based on impact categories and years
        self.model.invest_impact = pe.Var(
            self.model.impact_categories,
            self.model.years,
            within=pe.Reals)
        # overshoot of overall invest environmental and monetary impact based on impact categories and years
        self.model.invest_impact_overshoot = pe.Var(
            self.model.impact_categories,
            self.model.years,
            rule = lambda *args: self.fix_variables('invest_impact_overshoot',*args),
            within=pe.NonNegativeReals)
        # total impact
        # nodal total environmental and monetary impact based on impact categories and years
        self.model.total_nodal_impact = pe.Var(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            within=pe.Reals)
        # overshoot of nodal total environmental and monetary impact based on impact categories and years
        self.model.total_nodal_impact_overshoot = pe.Var(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule = lambda *args: self.fix_variables('total_nodal_impact_overshoot',*args),
            within=pe.NonNegativeReals)
        # overall total environmental and monetary impact based on impact categories and years
        self.model.total_impact = pe.Var(
            self.model.impact_categories,
            self.model.years,
            within=pe.Reals)
        # overshoot of overall total environmental and monetary impact based on impact categories and years
        self.model.total_impact_overshoot = pe.Var(
            self.model.impact_categories,
            self.model.years,
            rule = lambda *args: self.fix_variables('total_impact_overshoot',*args),
            within=pe.NonNegativeReals)

    def setupULVariables(self):
        """This method sets up all Upper Level Variables required by the optimization model.

        """
        # NEW capacities
        # newly installed capacity of production technologies for CURRENT UL node and investment years
        self.model.new_capacity_production = pe.Var(
            self.model.processes_production,
            self.model.years,
            rule = lambda *args: self.fix_variables('new_capacity_production',*args),
            within=pe.NonNegativeReals)

    def setupPrimalConstraints(self):
        """This method sets up all Primal Constraints required by the optimization model.

        """
        # declaring the balance equation for all products used in the optimization
        self.model.constraint_product_balance = pe.Constraint(
            self.model.nodes,
            self.model.products,
            self.model.years,
            self.model.time_slices,
            rule=self.constraint_product_balance_rule)
        # declaring the limit of production, limited by the capacity
        self.model.constraint_production_limit_by_capacity = pe.Constraint(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            self.model.time_slices,
            rule=self.constraint_production_limit_by_capacity_rule)
        
        # constraints for DC load flow modelling of transmission
        # calculating the used_transmission from the phase difference
        self.model.constraint_used_transmission_from_phase_difference = pe.Constraint(
            self.model.connections,
            self.model.years,
            self.model.time_slices,
            rule = self.constraint_used_transmission_from_phase_difference_rule)
        # declaring the limit of positive transmission, limited by the capacity
        self.model.constraint_transmission_limit_by_capacity_positive = pe.Constraint(
            self.model.connections,
            self.model.years,
            self.model.time_slices,
            rule=self.constraint_transmission_limit_by_capacity_positive_rule)
        # declaring the limit of negative transmission, limited by the capacity
        self.model.constraint_transmission_limit_by_capacity_negative = pe.Constraint(
            self.model.connections,
            self.model.years,
            self.model.time_slices,
            rule=self.constraint_transmission_limit_by_capacity_negative_rule)
        # impact calculation and limitation
        # operational impacts nodal based
        # declaring the calculation of all operational impact categories,
        # summed over all processes on nodal base
        self.model.constraint_operational_nodal_impact = pe.Constraint(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_operational_nodal_impact_rule)
        # declaring the calculation of all operational impact categories,
        # summed over all processes on nodal base
        self.model.constraint_operational_nodal_impact_limits = pe.Constraint(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_operational_nodal_impact_limits_rule)
        # operational impacts over all nodes
        # declaring the calculation of all operational impact categories,
        # summed over all processes
        self.model.constraint_operational_impact = pe.Constraint(
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_operational_impact_rule)
        # declaring the calculation of all operational impact categories,
        # summed over all processes
        self.model.constraint_operational_impact_limits = pe.Constraint(
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_operational_impact_limits_rule)
    
    def setupULConstraints(self):
        """ This method sets up all Upper Level Constraints used in the Optimization """
        # declaring the limit of production capacity, limited by the provided input
        self.model.constraint_production_potential_capacity_by_input = pe.Constraint(
            self.model.processes_production,
            self.model.years,
            rule=self.constraint_production_potential_capacity_by_input_rule)

        # declaring the necessity of providing the secured capacity
        self.model.constraint_nodal_secured_capacity = pe.Constraint(
            self.model.products,
            self.model.years,
            rule=self.constraint_nodal_secured_capacity_rule)

        # declaring the necessity of providing the secured capacity
        self.model.constraint_total_secured_capacity = pe.Constraint(
            self.model.products,
            self.model.years,
            rule=self.constraint_total_secured_capacity_rule)
        
        # invest impacts nodal based
        # declaring the calculation of all invest impact categories,
        # summed over all processes on nodal base
        self.model.constraint_invest_nodal_impact = pe.Constraint(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_invest_nodal_impact_rule)
        # declaring the calculation of all invest impact categories,
        # summed over all processes on nodal base
        self.model.constraint_invest_nodal_impact_limits = pe.Constraint(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_invest_nodal_impact_limits_rule)
        # invest impacts over all nodes
        # declaring the calculation of all invest impact categories,
        # summed over all processes
        self.model.constraint_invest_impact = pe.Constraint(
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_invest_impact_rule)
        # declaring the calculation of all invest impact categories,
        # summed over all processes
        self.model.constraint_invest_impact_limits = pe.Constraint(
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_invest_impact_limits_rule)
        # total impacts nodal based
        # declaring the calculation of all impact categories,
        # summed over all processes on nodal base
        self.model.constraint_total_nodal_impact = pe.Constraint(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_total_nodal_impact_rule)
        # declaring the limitation of all impact categories on nodal scale
        self.model.constraint_total_nodal_impact_limits = pe.Constraint(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_total_nodal_impact_limits_rule)
        # total impacts over all nodes
        # declaring the calculation of all impact categories,
        # summed over all processes and nodes
        self.model.constraint_total_impact = pe.Constraint(
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_total_impact_rule)
        # declaring the limitation of all impact categories on global scale
        self.model.constraint_total_impact_limits = pe.Constraint(
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_total_impact_limits_rule)

    def setupDualVariables(self):
        """This method sets up all dual Variables required by the optimization model.

        """
        #----------------------------------------------------------------------------#   
        ## Dual variables for each constraint, if constraint is inequality constraint,
        #  set domain to NonNegativeReals
        #----------------------------------------------------------------------------#   
        # declaring the balance equation for all products used in the optimization
        self.model.dual_variable_product_balance = pe.Var(
            self.model.nodes,
            self.model.products,
            self.model.years,
            self.model.time_slices,
            rule = lambda *args: self.fix_variables('dual_variable_product_balance',*args),
            domain = lambda *args: self.get_domain_of_dual_variable('dual_variable_product_balance',*args))
        # declaring the limit of production, limited by the capacity
        self.model.dual_variable_production_limit_by_capacity = pe.Var(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            self.model.time_slices,
            rule = lambda *args: self.fix_variables('dual_variable_production_limit_by_capacity',*args),
            domain = lambda *args: self.get_domain_of_dual_variable('dual_variable_production_limit_by_capacity',*args))
        # dual_variables for DC load flow modelling of transmission
        # calculating the used_transmission from the phase difference
        self.model.dual_variable_used_transmission_from_phase_difference = pe.Var(
            self.model.connections,
            self.model.years,
            self.model.time_slices,
            rule = lambda *args: self.fix_variables('dual_variable_used_transmission_from_phase_difference',*args),
            domain = lambda *args: self.get_domain_of_dual_variable('dual_variable_used_transmission_from_phase_difference',*args))
        # declaring the limit of positive transmission, limited by the capacity
        self.model.dual_variable_transmission_limit_by_capacity_positive = pe.Var(
            self.model.connections,
            self.model.years,
            self.model.time_slices,
            rule = lambda *args: self.fix_variables('dual_variable_transmission_limit_by_capacity_positive',*args),
            domain = lambda *args: self.get_domain_of_dual_variable('dual_variable_transmission_limit_by_capacity_positive',*args))
        # declaring the limit of negative transmission, limited by the capacity
        self.model.dual_variable_transmission_limit_by_capacity_negative = pe.Var(
            self.model.connections,
            self.model.years,
            self.model.time_slices,
            rule = lambda *args: self.fix_variables('dual_variable_transmission_limit_by_capacity_negative',*args),
            domain = lambda *args: self.get_domain_of_dual_variable('dual_variable_transmission_limit_by_capacity_negative',*args))
        # impact calculation and limitation
        # operational impacts nodal based
        # declaring the calculation of all operational impact categories,
        # summed over all processes on nodal base
        self.model.dual_variable_operational_nodal_impact = pe.Var(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule = lambda *args: self.fix_variables('dual_variable_operational_nodal_impact',*args),
            domain = lambda *args: self.get_domain_of_dual_variable('dual_variable_operational_nodal_impact',*args))
        # declaring the calculation of all operational impact categories,
        # summed over all processes on nodal base
        self.model.dual_variable_operational_nodal_impact_limits = pe.Var(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule = lambda *args: self.fix_variables('dual_variable_operational_nodal_impact_limits',*args),
            domain = lambda *args: self.get_domain_of_dual_variable('dual_variable_operational_nodal_impact_limits',*args))
        # operational impacts over all nodes
        # declaring the calculation of all operational impact categories,
        # summed over all processes
        self.model.dual_variable_operational_impact = pe.Var(
            self.model.impact_categories,
            self.model.years,
            rule = lambda *args: self.fix_variables('dual_variable_operational_impact',*args),
            domain = lambda *args: self.get_domain_of_dual_variable('dual_variable_operational_impact',*args))
        # declaring the calculation of all operational impact categories,
        # summed over all processes
        self.model.dual_variable_operational_impact_limits = pe.Var(
            self.model.impact_categories,
            self.model.years,
            rule = lambda *args: self.fix_variables('dual_variable_operational_impact_limits',*args),
            domain = lambda *args: self.get_domain_of_dual_variable('dual_variable_operational_impact_limits',*args))
        #----------------------------------------------------------------------------#    
        ## Dual variable for each lower bound on variable, fix to 0 if no lower bound
        #----------------------------------------------------------------------------#   
        self.model.dual_variable_lower_bound_used_production = pe.Var(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            self.model.time_slices,
            rule = lambda *args: self.fix_variables('dual_variable_lower_bound_used_production',*args),
            within=pe.NonNegativeReals)
        # non-served demand of a product based on nodes and time
        self.model.dual_variable_lower_bound_non_served_demand = pe.Var(
            self.model.nodes,
            self.model.products,
            self.model.years,
            self.model.time_slices,
            rule = lambda *args: self.fix_variables('dual_variable_lower_bound_non_served_demand',*args),
            within=pe.NonNegativeReals)
        # DC load flow - transmission related variables
        self.model.dual_variable_lower_bound_phase_difference = pe.Var(
            self.model.nodes,
            self.model.years,
            self.model.time_slices,
            rule = lambda *args: self.fix_variables('dual_variable_lower_bound_phase_difference',*args),
            within=pe.NonNegativeReals)
        # impact related variables
        # operational impact
        # nodal operational environmental and monetary impact based on impact categories and years
        self.model.dual_variable_lower_bound_operational_nodal_impact = pe.Var(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule = lambda *args: self.fix_variables('dual_variable_lower_bound_operational_nodal_impact',*args),
            within=pe.NonNegativeReals)
        # overshoot of nodal operational environmental and monetary impact based on impact categories and years
        self.model.dual_variable_lower_bound_operational_nodal_impact_overshoot = pe.Var(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule = lambda *args: self.fix_variables('dual_variable_lower_bound_operational_nodal_impact_overshoot',*args),
            within=pe.NonNegativeReals)
        # overall operational environmental and monetary impact based on impact categories and years
        self.model.dual_variable_lower_bound_operational_impact = pe.Var(
            self.model.impact_categories,
            self.model.years,
            rule = lambda *args: self.fix_variables('dual_variable_lower_bound_operational_impact',*args),
            within=pe.NonNegativeReals)
        # overshoot of overall operational environmental and monetary impact based on impact categories and years
        self.model.dual_variable_lower_bound_operational_impact_overshoot = pe.Var(
            self.model.impact_categories,
            self.model.years,
            rule = lambda *args: self.fix_variables('dual_variable_lower_bound_operational_impact_overshoot',*args),
            within=pe.NonNegativeReals)

        #----------------------------------------------------------------------------#    
        ## Dual variable for fixed phase difference @ model.transmission_reference_node
        #----------------------------------------------------------------------------#   
        self.model.dual_variable_phase_difference = pe.Var(
            within=pe.Reals)

    def setupDualConstraints(self):
        """This method sets up all Dual Constraints required by the optimization model.
        """
        # usage of production technologies based on nodes and time
        self.model.dual_constraint_used_production = pe.Constraint(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            self.model.time_slices,
            rule=self.dual_constraint_used_production_rule)
        # non-served demand of a product based on nodes and time
        self.model.dual_constraint_non_served_demand = pe.Constraint(
            self.model.nodes,
            self.model.products,
            self.model.years,
            self.model.time_slices,
            rule=self.dual_constraint_non_served_demand_rule)
        # DC load flow - transmission related variables
        self.model.dual_constraint_phase_difference = pe.Constraint(
            self.model.nodes,
            self.model.years,
            self.model.time_slices,
            rule=self.dual_constraint_phase_difference_rule)
        # used transmission power
        self.model.dual_constraint_used_transmission = pe.Constraint(
            self.model.connections,
            self.model.years,
            self.model.time_slices,
            rule=self.dual_constraint_used_transmission_rule)
        # impact related variables
        # operational impact
        # nodal operational environmental and monetary impact based on impact categories and years
        self.model.dual_constraint_operational_nodal_impact = pe.Constraint(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule=self.dual_constraint_operational_nodal_impact_rule)
        # overshoot of nodal operational environmental and monetary impact based on impact categories and years
        self.model.dual_constraint_operational_nodal_impact_overshoot = pe.Constraint(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule=self.dual_constraint_operational_nodal_impact_overshoot_rule)
        # overall operational environmental and monetary impact based on impact categories and years
        self.model.dual_constraint_operational_impact = pe.Constraint(
            self.model.impact_categories,
            self.model.years,
            rule=self.dual_constraint_operational_impact_rule)
        # overshoot of overall operational environmental and monetary impact based on impact categories and years
        self.model.dual_constraint_operational_impact_overshoot = pe.Constraint(
            self.model.impact_categories,
            self.model.years,
            rule=self.dual_constraint_operational_impact_overshoot_rule)
        
    def setupStrongDualityConstraint(self):
        """ This method declares the Strong Duality Constraint 
        min c^T x s.t. Ax <= b
        -->
        - c^T x = \lambda^T b"""
        self.model.strong_duality_equation = pe.Constraint(
            rule = self.strong_duality_equation_rule)


    ## Objectives
    def setupLLObjective(self):
        """This method declares the pe.Expression for the Lower Level Objective required by the optimization model."""

        self.model.LLObjectiveExpression = pe.Expression(rule=self.objectiveLLRule)      

    def setupShortULObjective(self):
        """ This method declares the pe.Expression for the Short Upper Level Objective to compare against the Objective.
        The values should be equal"""

        self.model.ShortULObjective = pe.Expression(self.model.nodes,rule=self.objectiveShortULRule)

    def setupLongULObjective(self):
        """ This method declares the pe.Expression for the long Upper Level Objective to compare against the Objective.
        The values should be equal"""

        self.model.LongULObjective = pe.Expression(rule=self.objectiveLongULRule)

    def setupULObjective(self):
        """This method declares the pe.Objective required by the optimization model."""

        self.model.Objective = pe.Objective(rule=self.objectiveULRule)    

    # utility methods
    def set_name_buffer(self):
        """ This method iterates once through all variables and constraints (not indizes) and saves the id and name in the _name_buffer """
        for variable in self.model.component_objects(pe.Var):
            self.__class__.set_component_name_to_name_buffer(variable)
        for constraint in self.model.component_objects(pe.Constraint):
            self.__class__.set_component_name_to_name_buffer(constraint)

    def get_upper_lower_bound(self,term: tuple,input_dictionary: dict):
        """ This method gets upper or lower bound corresponding to the variable term[1],
        dependent whether coefficient term [0] is positive or negative.

        Returns the bound multiplied with coefficient
        
        Mainly for Big-M Formulation, but needed for new_capacity_production in binary expansion, thus included."""
        if pe.value(term[0]) < 0:
            # get lower bounds
            # if variable has explicit lower bound (normally 0)
            if term[1].lb is not None:
                _bound = term[1].lb
                return(_bound*pe.value(term[0]))
            # if variable does not have explicit bound, get by physical properties
            elif "used_transmission" in term[1].parent_component().getname(name_buffer = Optimization._name_buffer):
                # TODO CHECK CORRECT INDIZES
                # get power limit from input_dictionary with node (index 0) and year (index 1) from term[1].index()
                _bound = -input_dictionary[None]["power_line_properties"][term[1].index()[0:2]+("power limit",)]
                return(_bound*pe.value(term[0]))
            elif "operational_impact" in term[1].parent_component().getname(name_buffer = Optimization._name_buffer):
                # calculate most negative emission
                # CURRENTLY NO CCUS, so minimum impact = 0
                _bound = 0 
                return(_bound*pe.value(term[0]))
            else:
                logging.info("Missing lower bound for {}. This will set the Big-M constant for the corresponding constraints to the generic value.".format(term[1].parent_component().getname(name_buffer = Optimization._name_buffer)))
                return None
        # if coefficient > 0, upper bound needed
        else:
            # get upper bounds
            if term[1].ub is not None:
                _bound = term[1].ub
                return(_bound*pe.value(term[0]))
            # if variable does not have explicit bound, get by physical properties
            elif "used_production" in term[1].parent_component().getname(name_buffer = Optimization._name_buffer):
                # node (index 0), process (index 1), year (index 2), construction_year (index 3), time_slice (index 4)
                if term[1].index()[3] < term[1].index()[2]: # existing capacity is upper bound if construction_year < year
                    _bound = input_dictionary[None]["existing_capacity_production"][term[1].index()[0:4]]
                    return(_bound*pe.value(term[0]))
                if term[1].index()[0] != input_dictionary[None]["nodes"][None][input_dictionary[None]["current_node"][None]-1]: # existing capacity + predicted new capacity is upper bound if node != current_node
                    # if existing capacity exists for this construction year
                    if term[1].index()[0:4] in input_dictionary[None]["existing_capacity_production"]:
                        _bound = input_dictionary[None]["existing_capacity_production"][term[1].index()[0:4]] + input_dictionary[None]["predicted_new_capacity_production"][term[1].index()[0:3]]
                    else:
                        _bound = input_dictionary[None]["predicted_new_capacity_production"][term[1].index()[0:3]]
                    return(_bound*pe.value(term[0]))
                # get potential capacity limit from input_dictionary with node (index 0), process (index 1) and year (index 2) from term[1].index()
                _max_used_production_by_potential_capacity = input_dictionary[None]["potential_capacity_production"][term[1].index()[0:3]]
                if _max_used_production_by_potential_capacity == np.inf or _max_used_production_by_potential_capacity == 0:
                    _max_used_production_by_demand = 0
                    for _other_product in filter(lambda _other_product: min(input_dictionary[None]["technology_matrix_production"][_other_product,input_dictionary[None]["nodes"][None][input_dictionary[None]["current_node"][None]-1],term[1].index()[1],_construction_year] for _construction_year in input_dictionary[None]["construction_years_production"][None]) >0,input_dictionary[None]["products"][None]):
                        _worst_conversion_efficiency_other_product =  min(input_dictionary[None]["technology_matrix_production"][_other_product,input_dictionary[None]["nodes"][None][input_dictionary[None]["current_node"][None]-1],term[1].index()[1],_construction_year] for _construction_year in input_dictionary[None]["construction_years_production"][None])
                        if _other_product in input_dictionary[None]["products_transmission"][None]:
                            _cum_demand_other_node = max(sum(input_dictionary[None]["demand"][(_other_product,term[1].index()[2],time_slice,node)] for node in input_dictionary[None]["nodes"][None]) for time_slice in input_dictionary[None]["time_slices"][None])
                            _max_used_production_by_demand += _cum_demand_other_node/_worst_conversion_efficiency_other_product
                        else:
                            _cum_demand_this_node = max(input_dictionary[None]["demand"][(_other_product,term[1].index()[2],time_slice,input_dictionary[None]["nodes"][None][input_dictionary[None]["current_node"][None]-1])] for time_slice in input_dictionary[None]["time_slices"][None])
                            _max_used_production_by_demand += _cum_demand_this_node/_worst_conversion_efficiency_other_product
                    _bound = _max_used_production_by_demand
                else:
                    _bound = _max_used_production_by_potential_capacity

                return(_bound*pe.value(term[0]))
            elif "used_transmission" in term[1].parent_component().getname(name_buffer = Optimization._name_buffer):
                # get power limit from input_dictionary with node (index 0) and year (index 1) from term[1].index()
                _bound = input_dictionary[None]["power_line_properties"][term[1].index()[0:2]+("power limit",)]
                return(_bound*pe.value(term[0]))
            elif "new_capacity_production" in term[1].parent_component().getname(name_buffer = Optimization._name_buffer):
                # get potential capacity limit from input_dictionary with process (index 0) and year (index 1) from term[1].index() for current_node
                _max_new_capacity_by_potential_capacity = input_dictionary[None]["potential_capacity_production"][(input_dictionary[None]["nodes"][None][input_dictionary[None]["current_node"][None]-1],)+term[1].index()[0:2]]
                if _max_new_capacity_by_potential_capacity == np.inf:
                    # get maximum new capacity from possible demand
                    _max_new_capacity_by_demand = 0
                    _process_production = term[1].index()[0]
                    for _other_product in filter(lambda _other_product: min(input_dictionary[None]["technology_matrix_production"][_other_product,input_dictionary[None]["nodes"][None][input_dictionary[None]["current_node"][None]-1],_process_production,_construction_year] for _construction_year in input_dictionary[None]["construction_years_production"][None]) >0,input_dictionary[None]["products"][None]):
                        _worst_conversion_efficiency_other_product =  min(input_dictionary[None]["technology_matrix_production"][_other_product,input_dictionary[None]["nodes"][None][input_dictionary[None]["current_node"][None]-1],_process_production,_construction_year] for _construction_year in input_dictionary[None]["construction_years_production"][None])
                        if _other_product in input_dictionary[None]["products_transmission"][None]:
                            _cum_demand_other_node = max(sum(input_dictionary[None]["demand"][(_other_product,term[1].index()[1],time_slice,node)] for node in input_dictionary[None]["nodes"][None]) for time_slice in input_dictionary[None]["time_slices"][None])
                            # get maximum transmittable power
                            _list_of_connected_lines = [key[0] for key in input_dictionary[None]["connected_nodes"] if input_dictionary[None]["connected_nodes"][key]==input_dictionary[None]["current_node"][None]]
                            _maximum_transmittable_power = sum(input_dictionary[None]["power_line_properties"][(key,term[1].index()[1],"power limit")] for key in _list_of_connected_lines)
                            _cum_demand_this_node = max(input_dictionary[None]["demand"][(_other_product,term[1].index()[1],time_slice,input_dictionary[None]["nodes"][None][input_dictionary[None]["current_node"][None]-1])] for time_slice in input_dictionary[None]["time_slices"][None])
                            _max_served_demand_other_nodes = min(_maximum_transmittable_power,_cum_demand_other_node) 
                            _max_new_capacity_by_demand += (_max_served_demand_other_nodes+_cum_demand_this_node)/_worst_conversion_efficiency_other_product
                        else:
                            _cum_demand_this_node = max(input_dictionary[None]["demand"][(_other_product,term[1].index()[1],time_slice,input_dictionary[None]["nodes"][None][input_dictionary[None]["current_node"][None]-1])] for time_slice in input_dictionary[None]["time_slices"][None])
                            _max_new_capacity_by_demand += _cum_demand_this_node/_worst_conversion_efficiency_other_product
                    _bound = _max_new_capacity_by_demand
                else:
                    # subtract total existing capacity
                    # set maximum of (potential_capacity-sum(existing_capacity) and 0
                    _combined_existing_capacity_production = sum(input_dictionary[None]["existing_capacity_production"][(input_dictionary[None]["nodes"][None][input_dictionary[None]["current_node"][None]-1],)+term[1].index()[0:2] + (year_construction,)]
                        for year_construction in filter(lambda year_construction: (year_construction <= term[1].index()[1]) and (term[1].index()[1] - year_construction < input_dictionary[None]["lifetime_duration_production"][term[1].index()[0], year_construction])and (((input_dictionary[None]["nodes"][None][input_dictionary[None]["current_node"][None]-1],)+term[1].index()[0:2] + (year_construction,)) in input_dictionary[None]["existing_capacity_production"]), input_dictionary[None]["construction_years_production"][None]) )
                    _bound = max(_max_new_capacity_by_potential_capacity-_combined_existing_capacity_production,0)
                return(_bound*pe.value(term[0]))
            elif "non_served_demand" in term[1].parent_component().getname(name_buffer = Optimization._name_buffer):
                _product = term[1].index()[1]
                # if product is transmittable (electricity), get demand for all nodes from input_dictionary with node (index 0), product (index 1), year (index 2) and time slice (index 3)
                if _product in input_dictionary[None]["products_transmission"][None]:
                    _bound = sum(input_dictionary[None]["demand"][term[1].index()[1:4]+(node,)] for node in input_dictionary[None]["nodes"][None])
                # else only demand at current_node
                else:
                    _bound = input_dictionary[None]["demand"][term[1].index()[1:4]+(term[1].index()[0],)]
                # add potential consumption of product
                _max_nsd_by_potential_capacity = 0
                _max_nsd_by_demand = 0 
                
                for _process_production in filter(lambda _process_production: min(input_dictionary[None]["technology_matrix_production"][_product,term[1].index()[0],_process_production,_construction_year] for _construction_year in input_dictionary[None]["construction_years_production"][None]) <0,input_dictionary[None]["processes_production"][None]):
                    _worst_conversion_efficiency_product =  min(input_dictionary[None]["technology_matrix_production"][_product,term[1].index()[0],_process_production,_construction_year] for _construction_year in input_dictionary[None]["construction_years_production"][None])
                    # maximum non served demand if full potential capacity is used to deplete the product
                    _max_nsd_by_potential_capacity +=  input_dictionary[None]["potential_capacity_production"][term[1].index()[0],_process_production,term[1].index()[2]]*_worst_conversion_efficiency_product
                    # entire positive demand of all products is supplied by the production processes associated with this product at this node
                    for _other_product in filter(lambda _other_product: min(input_dictionary[None]["technology_matrix_production"][_other_product,term[1].index()[0],_process_production,_construction_year] for _construction_year in input_dictionary[None]["construction_years_production"][None]) >0,input_dictionary[None]["products"][None]):
                        _worst_conversion_efficiency_other_product =  min(input_dictionary[None]["technology_matrix_production"][_other_product,term[1].index()[0],_process_production,_construction_year] for _construction_year in input_dictionary[None]["construction_years_production"][None])
                        if _other_product in input_dictionary[None]["products_transmission"][None]:
                            _cum_demand_other_node = sum(input_dictionary[None]["demand"][(_other_product,)+term[1].index()[2:4]+(node,)] for node in input_dictionary[None]["nodes"][None])
                            _max_nsd_by_demand += _cum_demand_other_node/_worst_conversion_efficiency_other_product*_worst_conversion_efficiency_product
                        else:
                            _cum_demand_this_node = input_dictionary[None]["demand"][(_other_product,)+term[1].index()[2:4]+(term[1].index()[0],)]
                            _max_nsd_by_demand += _cum_demand_this_node/_worst_conversion_efficiency_other_product*_worst_conversion_efficiency_product
                if np.isfinite(_max_nsd_by_potential_capacity):
                    # maximum non served demand is maximum of the two possibilities
                    _bound -= min(_max_nsd_by_demand,_max_nsd_by_potential_capacity)
                else:
                    _bound -= _max_nsd_by_demand
                return(_bound*pe.value(term[0]))
            elif "operational_nodal_impact" in term[1].parent_component().getname(name_buffer = Optimization._name_buffer):
                _bound = input_dictionary[None]["operational_impact_limits"][term[1].index()[1:3]]
                return _bound
            else:
                logging.info("Missing upper bound for {}. This will set the Big-M constant for the corresponding constraints to the generic value.".format(term[1].parent_component().getname(name_buffer = Optimization._name_buffer)))
                return None
    
    def set_coefficients_for_numerics(self,item,scaled_model:bool,constraint_name='<constraint unknown>'):
        """ this classmethod receives an item containing a tuple of a variable and its corresponding coefficient. 
        It subsequently compares the coefficient to the saved max and min value and saves the coefficient if it is larger/smaller.
        Furthermore it sums up the coefficients in order to calculate the frobenius norm.
        
        Args: 
            item(tuple): tuple which contains coefficient [0] and variable [1]
            scaled_model(bool): boolean whether scaled or unscaled model
            constraint_name(str): str which contains name of constraint. 
                During the construction of the constraint, it is impossible and impractical to pass the constraint name. 
                If constraint of min/max coefficient can not be derived from context, disable scaling 
        """
        # exclude binary variable, because Big_M should always be higher
        # if 'binary' not in item[1].name:
            # get if coefficients from unscaled or scaled model
        if scaled_model:
            _model_type = 'scaled'
        else:
            _model_type = 'unscaled'
        item_val = abs(pe.value(item[0]))
        # ||A||_F = \sqrt(\sum(\abs(a_ij)^2))
        self.numerics[_model_type]['sum_coeff_sq'] += item_val**2
        if self.numerics[_model_type]['max_val']['var'] is None: # set first item as min max
            self.numerics[_model_type]['max_val']['var'] = item[1].name + ' in ' + constraint_name
            self.numerics[_model_type]['max_val']['val'] = item_val
            self.numerics[_model_type]['min_val']['var'] = item[1].name + ' in ' + constraint_name
            self.numerics[_model_type]['min_val']['val'] = item_val
        elif item_val > self.numerics[_model_type]['max_val']['val']:
            self.numerics[_model_type]['max_val']['var'] = item[1].name + ' in ' + constraint_name
            self.numerics[_model_type]['max_val']['val'] = item_val
        elif item_val < self.numerics[_model_type]['min_val']['val']:
            self.numerics[_model_type]['min_val']['var'] = item[1].name + ' in ' + constraint_name
            self.numerics[_model_type]['min_val']['val'] = item_val

    def set_variable_dict(self,variable,variable_coefficient,constraint_scaling_factor):
        """ appends variable_coefficient to _variable_dict 
        Args: 
            variable(pe.Var): indexed variable 
            variable_coefficient(float): value of coefficient corresponding to variable
            constraint_scaling_factor(float): scaling factor of constraint
        """
        # if id(variable) already in _variable_dict, append to key
        try:
            self._variable_dict[id(variable)].append(variable_coefficient*constraint_scaling_factor)
        # if not yet in _variable_dict, create new key
        except KeyError:
            self._variable_dict[id(variable)] = [variable_coefficient*constraint_scaling_factor]

    def scale_expr(self,unscaled_constraint):
        """ this method scales an constraint expression, if config.scaling_options['scale_constraints'] or config.scaling_options['scale_variables'] selected. 
        If config.scaling_options['scale_constraints'] the constraint is scaled.
        If config.scaling_options['scale_variables'] selected, this methods saves the variable coefficients in cls._variable_dict
        
        Args: 
            unscaled_constraint: unscaled inequality or equality constraint, with expr Expression
        """
        unscaled_expr = unscaled_constraint.expr
        # if either constraints or variables scaled
        merged_expr = unscaled_expr.args[0] + unscaled_expr.args[1]
        variable_coefficient_list = EXPR.decompose_term(merged_expr)[1]
        # if constraints not scaled
        if not config.scaling_options['scale_constraints']: 
            if config.scaling_options['scale_variables']:
                for item in variable_coefficient_list:
                    # if item[1] is variable, which is not fixed
                    if item[1] is not None and item[1].value != 0: 
                        # multiply variable coefficient with 1, since constraint not scaled
                        self.set_variable_dict(item[1],pe.value(item[0]),1) 
                        # collect coefficient information for numerical analysis
                        if config.scaling_options['analyze_numerics']:
                            self.set_coefficients_for_numerics(item,scaled_model=False,constraint_name = unscaled_constraint.name)
        else:
            # if constraints scaled
            # calculate constraint_scaling_factor
            constraint_scaling_factor = self.calculate_scaling_factor([pe.value(item[0]) for item in variable_coefficient_list if item[1] is not None and item[1].value != 0 and "binary" not in item[1].name])
            if constraint_scaling_factor:              
                for item in variable_coefficient_list:
                    # if item[1] is variable, which is not fixed
                    if item[1] is not None and item[1].value != 0: 
                        # if variables scaled
                        if pe.value(item[0]) == 0:
                            a=1
                        if config.scaling_options['scale_variables']:
                            # multiply variable coefficient with scaling factor
                            self.set_variable_dict(item[1],pe.value(item[0]),constraint_scaling_factor) 
                        # collect coefficient information for numerical analysis
                        if config.scaling_options['analyze_numerics']:
                            self.set_coefficients_for_numerics(item,scaled_model=False,constraint_name = unscaled_constraint.name)
                # if constraints_scaling_factor == 1, no scaling necessary
                if constraint_scaling_factor != 1:
                    # scale lhs and rhs of unscaled_expr with constraint_scaling_factor
                    self.model_instance.find_component("scaling_" + unscaled_constraint.parent_component().getname(name_buffer = Optimization._name_buffer))[unscaled_constraint.index()] = constraint_scaling_factor

    def scale_model_constraints(self):
        """ This method scales the model constraints by iterating through constraints and resetting the scaling_<constraint> pe.Param """
        if config.scaling_options['scale_constraints'] or config.scaling_options['scale_variables']:
            # iterate through constraints
            for constraint in self.model_instance.component_objects(pe.Constraint):
                # if "complementarity_constraint" not in constraint.getname(name_buffer = Optimization._name_buffer):# and "dual_constraint" not in constraint.getname(name_buffer = Optimization._name_buffer):
                for data in constraint:
                    self.scale_expr(constraint[data])
        

    def scale_model_variables(self):
        """ scales model variables, which are in Optimization._variable_dict """
        logging.info('Scale Variables')
        if config.debug_optimization:
            _time_start = datetime.now()
        _variable_dict = self._variable_dict
        # iterate through variables in model_instance
        for variable in self.model_instance.component_objects(pe.Var, active =True):
            for data in variable:
                try: 
                    # try to retrieve coefficient of variable from _variable_dict
                    variable_scaling_factor = self.calculate_scaling_factor(_variable_dict[id(variable[data])])
                    # if scaling factor exists
                    if variable_scaling_factor:
                        # overwrite scaling_<variable> with scaling factor
                        self.model_instance.find_component('scaling_'+variable.getname(name_buffer = Optimization._name_buffer))[data] = variable_scaling_factor
                # Tries to retrieve coefficient from _variable_dict. Passes to next iterate, if retrievement fails 
                except KeyError:
                    pass
                except TypeError:
                    pass
        # log time to scale variables
        if config.debug_optimization:
            time_delta_1 = datetime.now() -_time_start
            logging.info('Scale Variables: {:,.2f} s for {:,d} variables - {:,.2f} ms/variable'.format(time_delta_1.total_seconds(),len(_variable_dict),time_delta_1.total_seconds()*1000/len(_variable_dict)))

    def scale_model_objective(self):
        """ This model scales the model_instance.Objective if config.scaling_options['scale_objective'] """
        variable_coefficient_list = EXPR.decompose_term(self.model_instance.Objective.expr)[1]
        objective_scaling_factor = self.calculate_scaling_factor([pe.value(item[0]) for item in variable_coefficient_list if item[1] is not None and item[1].value != 0])
        if objective_scaling_factor and objective_scaling_factor != 1:
            self.model_instance.scaling_objective = objective_scaling_factor
            logging.info("Objective Scaling enabled. Objective scaling factor is {}".format(objective_scaling_factor))

    def retrieve_small_bounds_and_calculate_numerics(self):
        """ retrieves and prints bound that are smaller than eps_bound. 
        Furthermore set coefficients for numerics of scaled model and calculate matrix properties        
        """
        # set threshold to print coefficients 
        if 'eps_bound' in config.scaling_options:
            eps_bound = config.scaling_options['eps_bound']
        else:
            eps_bound = 1e-8

        if 'eps_large_bound' in config.scaling_options:
            eps_large_bound = config.scaling_options['eps_large_bound']
        else:
            eps_large_bound = 1e7
        small_bound_list = []
        large_bound_list = []
        for constraint in self.model_instance.component_objects(pe.Constraint,active=True):
            if constraint.name == "strong_duality_equation":
                continue
            for data in constraint:
                # calculate bound
                const_lhs = 0
                for item in EXPR.decompose_term(constraint[data].body)[1]:
                    if item[1] is None:
                        const_lhs += item[0]
                        
                    elif item[1].value != 0:
                        # retrieves scaled coefficients
                        self.set_coefficients_for_numerics(item, scaled_model=True,constraint_name = constraint[data].name)
                # check if 0 < bound < eps_bound
                if constraint[data].has_ub():
                    const = constraint[data].upper - const_lhs
                elif constraint[data].has_lb():
                    const = constraint[data].lower - const_lhs
                if abs(pe.value(const)) < eps_bound and abs(pe.value(const)) >0:
                    small_bound_list.append((constraint[data], pe.value(const))) 
                if abs(pe.value(const)) >= eps_large_bound:
                    large_bound_list.append((constraint[data], pe.value(const))) 
        # if bound < eps_bound exists, print bounds
        if len(small_bound_list) > 0:
            print("Constraints with non-zero bounds lower than {:.0e}:".format(eps_bound))
            for small_bound in small_bound_list:
                print('{}: {:.3e}'.format(small_bound[0],small_bound[1]))
        if len(large_bound_list) > 0:
            print("Constraints with bounds higher than {:.0e}:".format(eps_large_bound))
            for large_bound in large_bound_list:
                print('{}: {:.3e}'.format(large_bound[0],large_bound[1]))
        # calculate norm and log
        logging.info('Numerical Properties of Optimization Problem')
        Optimization.calculate_matrix_properties()

    def fix_unneeded_variables(self,model_instance = None):
        """Fixes variables to 0. The variables have to been labeled as to be fixed in self.fix_variables 
        Here, we iterate through each variable and check if the default value is 0. If yes, the variable is fixed
        """

        logging.info("Fix unneeded variables")
        if not model_instance:
            model_instance = self.model_instance

        number_of_fixed_variables = 0
        number_of_variables = 0
        # iterate through variables in model
        for variable in model_instance.component_objects(pe.Var,active=True):
            
            for data in variable:
                number_of_variables += 1
                
                # if variable was set to be fixed in construction
                if variable[data].value is not None:
                    variable[data].fix(variable[data].value)
                    number_of_fixed_variables += 1

        logging.info("{} of {} variables fixed in Optimization - {:,.2f} %".format(number_of_fixed_variables, number_of_variables, number_of_fixed_variables / number_of_variables * 100))
    
    def fix_slack_variables(self,fix_products):
        """
        Fixes the non_served_demand of specified products( e.g. heat, electricity, and mobility) to 0. 
        Although the import of these products may be undesired in some models, 
        their import is usually allowed at arbitrarily high environmental or financial impacts to keep the optimization feasible.
        By fixing the non-served-demand to 0, these variables are no longer used as slack variables and their import is strictly prevented
        """
        logging.info("Fix slack variables")
        number_of_variables = 0
        # iterate through variables in model    
        for variable in self.model_instance.component_objects(pe.Var,active=True):
            if variable.name=='non_served_demand':
                for data in variable:
                    if data[1] in fix_products:
                        variable[data].fix(0)
                        number_of_variables += 1
        logging.info("{} slack variables in non_served_demand were fixed.".format(number_of_variables))

    def fix_impact_overshoot(self):
        """
        Fixes the impact overshoot variables, thus impact limits must be obeyed
        """
        logging.info("Fix impact overshoot variables")
        # iterate through variables in model    
        for variable in self.model_instance.component_objects(pe.Var,active=True):
            if "overshoot" in variable.name:
                for data in variable:
                    variable[data].fix(0)
        logging.info("Overshoot variables were fixed.")

    def instantiate_model(self, input_dict: dict = None, filepath: str = None, skip_instantiation: bool = False):
        """ This method creates a model instance, fixes variables, and, if selected, analyzes numerics 
        
        Args:
            input_dict(dict): input dictionary
            filepath(str): filepath to input dictionary 
            skip_instantiation(bool): bool if instantiation is skipped. Default is False
        """
        if config.debug_optimization:
            _time_start = datetime.now()
        if not skip_instantiation:
            # set _name_buffer to quickly access vairable and constraint names
            self.set_name_buffer()
            # create instance
            if input_dict:
                self.model_instance = self.model.create_instance(data=input_dict)
            elif filepath:
                self.model_instance = self.model.create_instance(filename=filepath)
            else:
                raise AttributeError("Please provide input data either as input dictionary for pyomo or as filepath to an input file.")
            
            # log skipped constraints
            logging.info("{} out of {} constraints skipped (trivially feasible) - {:,.2f} %".format(Optimization._num_constraints['skipped'],Optimization._num_constraints['skipped']+Optimization._num_constraints['evaluated'],Optimization._num_constraints['skipped']/(Optimization._num_constraints['skipped']+Optimization._num_constraints['evaluated'])*100))
            # fix unneeded_variables
            self.fix_unneeded_variables()       
            if config.fix_slack_variables:
                # if true, fix the non served demand of the products specified in config.fix_products to 0 to prevent using them as slack variable
                self.fix_slack_variables(fix_products=config.fix_products)
            # scale constraints
            self.scale_model_constraints()
            # scale variables
            if config.scaling_options['scale_variables']:
                self.scale_model_variables()
            if config.scaling_options['scale_objective']:
                self.scale_model_objective()

            if config.scaling_options['analyze_numerics']:
                self.retrieve_small_bounds_and_calculate_numerics()
            if config.debug_optimization:
                logging.info('Instantiation of Model Instance took {:,.2f}s.'.format((datetime.now()-_time_start).total_seconds()))
            
            # delete value init rule of variables, because it can not be pickled into the result file
            for variable in self.model_instance.component_objects(pe.Var, active =True):
                variable._value_init_rule = None
                variable._domain_init_rule = None

    def run(self, input_dict: dict = None, filepath: str = None, solver: str = 'glpk', solver_options: dict = None, debug: bool = False, skip_instantiation: bool = False, previous_optimization = None, warmstart: bool = False, keepfiles: bool = True, b_fix_node: bool = False):
        """This method is used to start an optimization.

        To start an optimization it is necessary to provide a file with the necessary input data.
        This file includes all information required to instantiate the optimization problem.

        Args:
            input_dict (dict): Input dictionary that contains all the necessary data for the optimization
            filename (str): Filename of the previously generated input file
            solve (str): Name of the solve to be used. Standard is 'gurobi'
            solver_options (dict): dict with options directly forwarded to the solver
            debug (bool): bool if solver is debugged. Default is False
            skip_instantiation (bool): bool if instantiation is skipped. Default is False
        """

        logging.info("Instantiate optimization")
        # log scaling type
        if not config.scaling_options['scale_constraints'] and not config.scaling_options['scale_variables']:
            logging.info('Scaling disabled')
        else:
            if not config.scaling_options['scale_variables']:
                logging.info('Row (Constraint) scaling enabled')
            elif not config.scaling_options['scale_constraints']:
                logging.info('Column (Variable) scaling enabled')
            else:
                logging.info('Row (Constraint) and Column (Variable) scaling enabled')

        self.instantiate_model(input_dict,filepath,skip_instantiation)
        if config.bilevel_options["set_termination_gap"]:
            logging.info("Set Absolute Termination MIP Gap to {}".format(pe.value(self.model_instance.BinaryExpansionGap)))
            solver_options.update({"MIPGapAbs":pe.value(self.model_instance.BinaryExpansionGap)})
        
        # if b_fix_node, fix new_capacity_production to predicted value and optimize again to retrieve objectives for each node
        if b_fix_node:
            for data in self.model_instance.new_capacity_production:
                self.model_instance.new_capacity_production[data].fix(input_dict[None]["predicted_new_capacity_production"][(self.model_instance.nodes[self.model_instance.current_node.value],)+data])
        
        if (solver == "gurobi_persistent"):
            self.mysolver = pe.SolverFactory(solver, options=solver_options)
            self.mysolver.set_instance(self.model_instance, symbolic_solver_labels=True)
            self.results = self.mysolver.solve(tee=debug, warmstart = warmstart)
        elif (solver == "gams"):
            gms = GAMSShell()
            self.results = gms.solve(self.model_instance, io_options={"symbolic_solver_labels": True, "warmstart": warmstart,"add_options": solver_options}, tee=debug, keepfiles = keepfiles)#solver = config.solver_gams_BO
        else:
            self.mysolver = pe.SolverFactory(solver, options=solver_options)
            self.mysolver.set_instance(self.model_instance, symbolic_solver_labels=False)
            self.results = self.mysolver.solve(self.model_instance, tee=debug, keepfiles = keepfiles)
        logging.info("Termination condition is {}".format(self.results.solver.termination_condition))
        self.b_Big_M_too_small = False
        self.b_Big_M_changed = False
        
    # METHODS #
    # Evaluation methods
    def get_new_capacity_from_result(self):
        """Return a dictionary of dataframes with all new capacities from the optimization."""

        # Create empty dictionary
        new_capacity_categories = ["production", "storage", "transshipment", "transmission"]
        unstack = {"production": [2], "storage": [2], "transshipment": [2], "transmission": [2]}
        index = {"production": ["node", "process"], "storage": ["node", "process"], "transshipment": ["connection", "process"], "transmission": ["connection", "process"]}
        new_capacity = {}
        for category in new_capacity_categories:
            new_capacity[category] = self.get_dataframe_from_result(getattr(self.model_instance, "new_capacity_" + category),getattr(self.model_instance, "scaling_new_capacity_" + category), unstack=unstack[category])
            new_capacity[category].index.names = index[category]


        return new_capacity
    
    def get_new_capacity_from_bilevel_result(self):
        """Return a dictionary of dataframes with all new capacities from the bilevel optimization."""
        # Create empty dictionary
        new_capacity_categories = ["production", "storage", "transshipment", "transmission"]
        unstack_new_capacity = {"production": [1], "storage": [1], "transshipment": [1], "transmission": [1]}
        unstack_predicted_new_capacity = {"production": [2], "storage": [2], "transshipment": [2], "transmission": [2]}
        index = {"production": ["node", "process"], "storage": ["node", "process"], "transshipment": ["connection", "process"], "transmission": ["connection", "process"]}
        new_capacity = {}
        for category in new_capacity_categories:
            # currently only production implemented
            if category ==  "production":
                new_capacity[category] = self.get_dataframe_from_parameter(getattr(self.model_instance, "predicted_new_capacity_" + category), unstack=unstack_predicted_new_capacity[category])
                new_capacity[category].loc[self.model_instance.nodes[self.model_instance.current_node.value]][:] = self.get_dataframe_from_result(getattr(self.model_instance, "new_capacity_" + category),getattr(self.model_instance, "scaling_new_capacity_" + category), unstack=unstack_new_capacity[category])
                new_capacity[category].index.names = index[category]

        return new_capacity

    @staticmethod
    def get_dataframe_from_result(model_instance_variable, variable_scaling_factors, unstack: list = None):
        """Returns a dataframe with the results from the optimization model."""

        # Create empty dictionary
        variable = {}
        # for all indices in the model_instance_variable put its key-value pair in the dictionary
        for index in model_instance_variable:
            if "new_capacity" in model_instance_variable.name:
                if pe.value(model_instance_variable[index])*pe.value(variable_scaling_factors[index]) <= config.bilevel_options["eps_new_capacity"]:
                    variable[index] = 0
                else:
                    variable[index] = pe.value(model_instance_variable[index])*pe.value(variable_scaling_factors[index])
        
        # Create pandas Series from 
        variable = pd.Series(variable)
        if unstack:
            variable = variable.unstack(unstack)

        return variable
    
    @staticmethod
    def get_dataframe_from_parameter(model_instance_variable, unstack: list = None):
        """Returns a dataframe with the results from the optimization model."""

        # Create empty dictionary
        variable = {}
        # for all indices in the model_instance_variable put its key-value pair in the dictionary
        for index in model_instance_variable:
            variable[index] = model_instance_variable[index]
        
        # Create pandas Series from 
        variable = pd.Series(variable)
        if unstack:
            variable = variable.unstack(unstack)

        return variable

    # RULES FOR pe.Objective AND CONSTRAINTS #
    # CONSTRAINTS # methods used as rules for constraints
    @staticmethod
    def constraint_product_balance_rule(model, node: int, product: str, year: int, time_slice: str) -> "pyomo rule": # LL constraint
        """This method is used as rule for the product balance equation.

        This expression represents the product balance equation which is unique for each node, product, year and time slice.
        It can be summarized as::

            Production + Transshipment + Storage + Transmission >= Demand
        
        Therefore it takes the named above as parameters.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            node (int): A node at which the pe.Constraint is active
            product (str): A product at which is constrained
            year (int): A year at which the pe.Constraint is active
            time_slice (str): A time_slice at which the pe.Constraint is active
        """
        Optimization.add_evaluated_constraint()
        # returns the expression which is the product balance equation
        return (
            # Sets the demand for a product at a node in a specific year and time slice equal to
            # the sum of all process contributions to that product balance
            
            -model.demand[product, year, time_slice, node]*model.scaling_constraint_product_balance[node, product, year, time_slice] ==
            # sum of the production of a product by all used production processes
            -model.scaling_constraint_product_balance[node, product, year, time_slice]
            *(sum(
                # sum over all construction years to differentiate between the used capacities
                sum(model.technology_matrix_production[product,node, process_production, year_construction]
                    * model.used_production[node, process_production, year, year_construction, time_slice]*model.scaling_used_production[node, process_production, year, year_construction, time_slice]
                    for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_production[process_production, year_construction]), model.construction_years_production))
                for process_production in model.processes_production)
            
            # adding the power from DC load flow transmission, if the product is electricity
            + ( # Sum of the power from all connections which have this node as first node
                - sum(
                    model.used_transmission[connection,year,time_slice]*model.scaling_used_transmission[connection,year,time_slice]
                    for connection in filter(lambda connection: (model.nodes[model.connected_nodes[connection, "node1"]] == node) and (product in model.products_transmission), model.connections))
                # Sum of the power from all connections which have this node as second node
                + sum(
                    model.used_transmission[connection,year,time_slice]*model.scaling_used_transmission[connection,year,time_slice]
                    for connection in filter(lambda connection: (model.nodes[model.connected_nodes[connection, "node2"]] == node) and (product in model.products_transmission), model.connections))
            )
            # variable for non-served demand to avoid infeasibilities
            + model.non_served_demand[node, product, year, time_slice]*model.scaling_non_served_demand[node, product, year, time_slice]
            )
        )

    @staticmethod # LL constraint
    def constraint_used_transmission_from_phase_difference_rule(model, connection: int, year: int, time_slice: int) -> "pyomo rule":
        """ This method calculates the transmission power from the phase difference. 
        This is mainly done to decrease the numerical effort because the susceptance and unit base are high, 
        which increases the coefficients in the other constraints. 

        Args:
            model: The equivalent of "self" in pyomo optimization models
            connection (int): A power line 
            year (int): A year at which the pe.Constraint is active
            time_slice (str): A time_slice at which the pe.Constraint is active
        """
        
        Optimization.add_evaluated_constraint()
        return (
            model.scaling_constraint_used_transmission_from_phase_difference[connection,year,time_slice]*
            model.used_transmission[connection,year,time_slice]*model.scaling_used_transmission[connection,year,time_slice] == 
            model.scaling_constraint_used_transmission_from_phase_difference[connection,year,time_slice]*
            model.power_line_properties[connection, year, "susceptance per unit"]
            * (model.phase_difference[model.nodes[model.connected_nodes[connection, "node1"]], year, time_slice]*model.scaling_phase_difference[model.nodes[model.connected_nodes[connection, "node1"]], year, time_slice]
            - model.phase_difference[model.nodes[model.connected_nodes[connection, "node2"]], year, time_slice]*model.scaling_phase_difference[model.nodes[model.connected_nodes[connection, "node2"]], year, time_slice])*model.per_unit_base
        )

    @staticmethod # LL constraint
    def constraint_production_limit_by_capacity_rule(model, node: int, process_production: str, year: int, year_construction: int, time_slice: str) -> "pyomo rule": 
        """This method is used as rule for limitting the production to the sum of existing and new capacity.

        The rule is applied to every year of operation AND every year of construction.
        This means that for every year the capacity of existing infrastructure is used as limit,
        creating a constraint for every year of construction as well. This way the operation can
        differentiate between older and newer capacities which might have different impacts.
        Additionally for the years of construction which also are investment years, 
        the newly build capacities are added to the limit.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            node (int): A node at which the pe.Constraint is active
            process_production (str): A production process which is constrained
            year (int): A year at which the pe.Constraint is active
            year_construction (int): The year of construction of a specific capacity
            time_slice (str): A time_slice at which the pe.Constraint is active
        """
        if not model.used_production[node, process_production, year, year_construction, time_slice].value == 0:
            Optimization.add_evaluated_constraint()
            if year_construction not in model.years:
                return (
                    model.scaling_constraint_production_limit_by_capacity[node,process_production,year,year_construction,time_slice]*
                    model.used_production[node, process_production, year, year_construction, time_slice]*model.scaling_used_production[node, process_production, year, year_construction, time_slice]
                    <= model.scaling_constraint_production_limit_by_capacity[node,process_production,year,year_construction,time_slice]
                    * model.existing_capacity_production[node, process_production, year, year_construction]
                    * model.usable_capacity_factor_timeseries_production[node, process_production, time_slice]
                )
            else:
                # node is current node
                if node == model.nodes[model.current_node.value]:
                    return (
                        model.scaling_constraint_production_limit_by_capacity[node,process_production,year,year_construction,time_slice]*
                        model.used_production[node, process_production, year, year_construction, time_slice]*model.scaling_used_production[node, process_production, year, year_construction, time_slice]
                        <= model.scaling_constraint_production_limit_by_capacity[node,process_production,year,year_construction,time_slice]
                        * (model.existing_capacity_production[node, process_production, year, year_construction]
                            + model.new_capacity_production[process_production, year_construction]*model.scaling_new_capacity_production[process_production, year_construction])
                            * model.usable_capacity_factor_timeseries_production[node, process_production, time_slice]
                    )
                # node is any other node
                else:
                    return (
                        model.scaling_constraint_production_limit_by_capacity[node,process_production,year,year_construction,time_slice]*
                        model.used_production[node, process_production, year, year_construction, time_slice]*model.scaling_used_production[node, process_production, year, year_construction, time_slice]
                        <= model.scaling_constraint_production_limit_by_capacity[node,process_production,year,year_construction,time_slice]*
                        (model.existing_capacity_production[node, process_production, year, year_construction]
                            + model.predicted_new_capacity_production[node, process_production, year_construction])
                            * model.usable_capacity_factor_timeseries_production[node, process_production, time_slice]
                    )
        else:
            Optimization.add_skipped_constraint()
            return pe.Constraint.Skip

  
    @staticmethod # LL constraint
    def constraint_transmission_limit_by_capacity_positive_rule(model, connection: int, year: int, time_slice: str) -> "pyomo rule":
        """This method is used as rule for limiting the load flow through a transmission power line.

        The load flow needs to be limited by the available capacity, which is why this abstract rule
        exists and is used for a constraint in the base class for all optimizations.
        Although the available capacity can be defined differently, e.g.::

            load flow <= installed capacity

        or::

            load flow <= installed capacity + capacity from switching + capacity from new power lines

        Because of this, the abstract pe.Constraint rule needs to be defined in a subclass.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            connection (int): A power line which is constrained
            year (int): A year at which the pe.Constraint is active
            time_slice (str): A time_slice at which the pe.Constraint is active
        """
        Optimization.add_evaluated_constraint()
        return (
            model.scaling_constraint_transmission_limit_by_capacity_positive[connection, year, time_slice]*
            # Positive power flow through connection
            model.used_transmission[connection,year,time_slice]*model.scaling_used_transmission[connection,year,time_slice]
            # has to be smaller or equal than the power limit of the existing capacity of that connection
            <= model.scaling_constraint_transmission_limit_by_capacity_positive[connection, year, time_slice]*
            (model.power_line_properties[connection, year, "power limit"]
            # adding the sum of the new power limit capacity from all transmission processes
            + sum(
                # sum over all invest years which are earlier or equal to the current invest years
                sum(
                    # new installed circuits of each transmission technology
                    model.new_capacity_transmission[connection, process_transmission, earlier_year]*model.scaling_new_capacity_transmission[connection, process_transmission, earlier_year]
                    # multiplied by their power limit per circuit
                    * model.power_limit_per_circuit[process_transmission]
                    # multiplied by 1 minus the safety margin
                    * (1 - model.reliability_margin_transmission[process_transmission])
                    for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year <= model.lifetime_duration_transmission[process_transmission, earlier_year]), model.years))
                for process_transmission in model.processes_transmission))
        )

    @staticmethod # LL constraint
    def constraint_transmission_limit_by_capacity_negative_rule(model, connection: int, year: int, time_slice: str) -> "pyomo rule":
        """This method is used as rule for limiting the load flow through a transmission power line.

        The load flow needs to be limited by the available capacity, which is why this abstract rule
        exists and is used for a constraint in the base class for all optimizations.
        Although the available capacity can be defined differently, e.g.::

            load flow <= installed capacity

        or::

            load flow <= installed capacity + capacity from switching + capacity from new power lines

        Because of this, the abstract pe.Constraint rule needs to be defined in a subclass.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            power_line (int): A power line which is constrained
            year (int): A year at which the pe.Constraint is active
            time_slice (str): A time_slice at which the pe.Constraint is active
        """
        Optimization.add_evaluated_constraint()
        return (
            model.scaling_constraint_transmission_limit_by_capacity_negative[connection,year,time_slice]*
            # Negative power flow through connection
            (- model.used_transmission[connection,year,time_slice]*model.scaling_used_transmission[connection,year,time_slice])
            # has to be smaller or equal than the power limit of the existing capacity of that connection
            <= model.scaling_constraint_transmission_limit_by_capacity_negative[connection,year,time_slice]*
            (model.power_line_properties[connection, year, "power limit"]
            # adding the sum of the new power limit capacity from all transmission processes
            + sum(
                # sum over all invest years which are earlier or equal to the current invest years
                sum(
                    # new installed circuits of each transmission technology
                    model.new_capacity_transmission[connection, process_transmission, earlier_year]*model.scaling_new_capacity_transmission[connection, process_transmission, earlier_year]
                    # multiplied by their power limit per circuit
                    * model.power_limit_per_circuit[process_transmission]
                    # multiplied by 1 minus the safety margin
                    * (1 - model.reliability_margin_transmission[process_transmission])
                    for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year <= model.lifetime_duration_transmission[process_transmission, earlier_year]), model.years))
                for process_transmission in model.processes_transmission))
        )            

    # constraints for impacts
    # nodal pe.Constraint rules for the operational impact
    @staticmethod # LL constraint
    def constraint_operational_nodal_impact_rule(model, node: int, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the calculation of the operational impact vector summed over all processes.

        The operational impact of one node over all time slices is calculated in this method.
        By summing over the construction years of all capacities, the corresponding impact matrices from
        the actual construction year can be used. Furthermore the impact calculated for each time slice
        is multiplied with the weight in hours of a year the time slice has.

        The operational impact of production processes equals the sum of the products of process usage and
        the corresponding impact matrix. The same method is used for calculating the impact of transshipment
        processes, but the resulting impact is shared equally between the corresponding nodes.

        The operational impact of storage processes is calculated based on the **withdrawn** product flow.

        For the electric transmission grid it is assumed that the operational impact is negligible.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            node (int): A node at which the pe.Constraint is active
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """
        Optimization.add_evaluated_constraint()
        return (
            model.scaling_constraint_operational_nodal_impact[node,impact_category,year]*
            # calculate the operational nodal impact
            model.operational_nodal_impact[node, impact_category, year]*model.scaling_operational_nodal_impact[node, impact_category, year] 
            ==
            model.scaling_constraint_operational_nodal_impact[node,impact_category,year]*(
                # sum over all time slices
                sum(
                    # Sum over the impacts of all production processes
                    (sum(
                        # sum over the construction years of the production process
                        sum(model.used_production[node, process_production, year, year_construction, time_slice]*model.scaling_used_production[node, process_production, year, year_construction, time_slice]
                            * model.impact_matrix_production[impact_category, 'operation', node, process_production, year, year_construction]
                            for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_production[process_production, year_construction]), model.construction_years_production))    
                        for process_production in model.processes_production)

                    # adding the sum of all non-served demands multiplied by the corresponding impacts
                    # sum over all products
                    + sum(model.non_served_demand[node, product, year, time_slice]*model.scaling_non_served_demand[node, product, year, time_slice]
                        * model.impact_matrix_non_served_demand[impact_category, product, year, time_slice]
                        for product in model.products)
                    )
                    # multiply the sum of all impacts with the weight of a time slice regarding the full year
                    * model.time_slice_yearly_weight[time_slice]
                    for time_slice in model.time_slices)

            )
        )

    @staticmethod # LL constraint
    def constraint_operational_nodal_impact_limits_rule(model, node: int, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the limitation of the operational impact vector summed over all processes

        Args:
            model: The equivalent of "self" in pyomo optimization models
            node (int): A node at which the pe.Constraint is active
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """
        if model.operational_nodal_impact_limits[node, impact_category, year] != np.inf:
            Optimization.add_evaluated_constraint()
            return (
                model.scaling_constraint_operational_nodal_impact_limits[node,impact_category,year]*
                # limit nodal operational impact
                model.operational_nodal_impact[node, impact_category, year]*model.scaling_operational_nodal_impact[node, impact_category, year]
                <=
                model.scaling_constraint_operational_nodal_impact_limits[node,impact_category,year]*
                (model.operational_nodal_impact_limits[node, impact_category, year]
                # adding an overshoot variable mainly for debugging infeasibility issues
                + 
                model.operational_nodal_impact_overshoot[node, impact_category, year]*model.scaling_operational_nodal_impact_overshoot[node, impact_category, year])
            )
        else:
            Optimization.add_skipped_constraint()
            return pe.Constraint.Skip

    # overall pe.Constraint rules for the operational impact
    @staticmethod # LL constraint
    def constraint_operational_impact_rule(model, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the calculation of the operational impact vector summed over all processes and nodes

        Args:
            model: The equivalent of "self" in pyomo optimization models
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """
        Optimization.add_evaluated_constraint()
        return (
            model.scaling_constraint_operational_impact[impact_category,year]*
            model.operational_impact[impact_category, year]*model.scaling_operational_impact[impact_category, year] ==
            model.scaling_constraint_operational_impact[impact_category,year]*
            sum(model.operational_nodal_impact[node, impact_category, year]*model.scaling_operational_nodal_impact[node, impact_category, year]
                for node in model.nodes)
        )

    @staticmethod # LL constraint
    def constraint_operational_impact_limits_rule(model, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the limitation of the operational impact vector summed over all processes and nodes

        Args:
            model: The equivalent of "self" in pyomo optimization models
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """
        if model.operational_impact_limits[impact_category, year] != np.inf:
            Optimization.add_evaluated_constraint()
            return (
                model.scaling_constraint_operational_impact_limits[impact_category,year]*
                # limit overall operational impact
                model.operational_impact[impact_category, year]*model.scaling_operational_impact[impact_category, year] <=
                model.scaling_constraint_operational_impact_limits[impact_category,year]*(model.operational_impact_limits[impact_category, year]
                # adding an overshoot variable mainly for debugging feasibility issues
                + 
                model.operational_impact_overshoot[impact_category, year]*model.scaling_operational_impact_overshoot[impact_category, year])
            )
        else:
            Optimization.add_skipped_constraint()
            return pe.Constraint.Skip
    
    #--------------------------------------------------------------------#
    # UPPER LEVEL CONSTRAINTS #
    #--------------------------------------------------------------------#

    @staticmethod # UL constraint
    def constraint_production_potential_capacity_by_input_rule(model, process_production: str, year: int) -> "pyomo rule":
        """This method is used as rule for limitting the capacity of production processes by limits 
        from the input e.g. maximum potential.

        Some production technologies like renewable energies have local capacity limits based on
        the availability of ressources or regulatory restrictions. These limits are given to the
        optimization as parameter and can change over time.

        Only for current_node

        Args;
            model: The equivalent of "self" in pyomo optimization models
            process_production (str): A production process which is constrained
            year (int): A year at which the pe.Constraint is active
        """
        if model.potential_capacity_production[model.nodes[model.current_node.value], process_production, year] != np.inf: 
            Optimization.add_evaluated_constraint()
            combined_existing_capacity_production = sum(model.existing_capacity_production[model.nodes[model.current_node.value],process_production, year, year_construction]
                for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_production[process_production, year_construction]), model.construction_years_production))
            # if combined existing capacity >= potential capacity --> new capacity == 0
            if combined_existing_capacity_production >= model.potential_capacity_production[model.nodes[model.current_node.value], process_production, year]:
                return (0 >= 
                    model.scaling_constraint_production_potential_capacity_by_input[process_production, year]*
                    sum(model.new_capacity_production[process_production, earlier_year]*model.scaling_new_capacity_production[process_production, earlier_year]
                        for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year <= model.lifetime_duration_production[process_production, earlier_year]), model.years))
                )
            # else new capacity <= potential capacity - existing capacity
            else: 
                return (
                    model.scaling_constraint_production_potential_capacity_by_input[process_production, year]*
                    (model.potential_capacity_production[model.nodes[model.current_node.value], process_production, year] - combined_existing_capacity_production)
                    >= model.scaling_constraint_production_potential_capacity_by_input[process_production, year]* 
                    sum(model.new_capacity_production[process_production, earlier_year]*model.scaling_new_capacity_production[process_production, earlier_year]
                        for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year <= model.lifetime_duration_production[process_production, earlier_year]), model.years))
                )
        else:
            Optimization.add_skipped_constraint()
            return pe.Constraint.Skip

    @staticmethod
    def constraint_nodal_secured_capacity_rule(model, product: str, year: int) -> "pyomo rule":
        """This method is used as rule for providing the necessary secured capacity for a product.

        A baseline of required secured capacity can be given as parameter. This required secured
        capacity needs to be lower or equal to the sum of capacity secured by existing and new
        capacities.

        Since some technologies might increase the need for secured capacity for a product,
        their impact is taken into account as well. 

        Only production processes are taken into account for the secured capacity calculation.

        Only for current_node

        Args:
            model: The equivalent of "self" in pyomo optimization models
            product (str): A product for which the pe.Constraint is active
            year (int): A year at which the pe.Constraint is active
        """
        if model.required_nodal_secured_capacity[product, model.nodes[model.current_node.value], year] != -np.inf:
            Optimization.add_evaluated_constraint()
            return (
                model.scaling_constraint_nodal_secured_capacity[product, year]*
                # The required secured capacity has to be smaller or equal than the existing secured capacity
                (model.required_nodal_secured_capacity[product, model.nodes[model.current_node.value], year]
                + model.nodal_slack_variable[product, year]*model.scaling_nodal_slack_variable[product, year])
                <=  model.scaling_constraint_nodal_secured_capacity[product, year]*
                # Sum over all production processes
                    sum(
                        # Sum over all existing capacity
                        sum(# Existing capacity of a production process
                            model.existing_capacity_production[model.nodes[model.current_node.value], process_production, year, year_construction]
                            # Multiplied by the secured capacity factor of its construction year
                            * model.secured_capacity_factors_production[process_production, year_construction]
                            # Multiplied by the value for the product in the technology matrix
                            # Note, that negative values can exist and therefore increase the need for secured capacity
                            * model.technology_matrix_production[product,model.nodes[model.current_node.value], process_production, year_construction]
                            for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_production[process_production, year_construction]), model.construction_years_production))
                        # Adding the sum over all new capacity built before or in this year
                        + sum(# Existing capacity of a production process
                            model.new_capacity_production[process_production, earlier_year]*model.scaling_new_capacity_production[process_production, earlier_year]
                            # Multiplied by the secured capacity factor of its construction year
                            * model.secured_capacity_factors_production[process_production, earlier_year]
                            # Multiplied by the value for the product in the technology matrix
                            # Note, that negative values can exist and therefore increase the need for secured capacity
                            * model.technology_matrix_production[product,model.nodes[model.current_node.value], process_production, earlier_year]
                            for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year <= model.lifetime_duration_production[process_production, earlier_year]), model.years))
                        for process_production in model.processes_production)
            )
        else:
            Optimization.add_skipped_constraint()
            return pe.Constraint.Skip
    
    @staticmethod
    def constraint_total_secured_capacity_rule(model, product: str, year: int) -> "pyomo rule":
        """This method is used as rule for providing the necessary secured capacity for a product.

        A baseline of required secured capacity can be given as parameter. This required secured
        capacity needs to be lower or equal to the sum of capacity secured by existing and new
        capacities.

        Since some technologies might increase the need for secured capacity for a product,
        their impact is taken into account as well. 

        Only production processes are taken into account for the secured capacity calculation.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            product (str): A product for which the pe.Constraint is active
            year (int): A year at which the pe.Constraint is active
        """
        total_demand_year = sum(
            sum(
                model.demand[product,year,time_slice,node]
                for node in model.nodes)
            * model.time_slice_yearly_weight[time_slice]
            for time_slice in model.time_slices)

        if total_demand_year != 0 and model.surplus_capacity_factor_production.value > 1:
            Optimization.add_evaluated_constraint()
            return (
                model.scaling_constraint_total_secured_capacity[product, year]*
                # The required secured capacity has to be smaller or equal than the existing secured capacity
                float(total_demand_year)*model.surplus_capacity_factor_production
                <= model.scaling_constraint_total_secured_capacity[product, year]*
                # Sum over all nodes to aggregate the total secured capacity
                    (sum(
                        # Sum over all production processes
                        sum(
                            # sum over all time_slices
                            sum(
                                model.usable_capacity_factor_timeseries_production[node, process_production, time_slice]*(
                                # Sum over all existing capacity
                                sum(# Existing capacity of a production process
                                    model.existing_capacity_production[node, process_production, year, year_construction]
                                    # Multiplied by the value for the product in the technology matrix
                                    # Note, that negative values can exist and therefore increase the need for secured capacity
                                    * model.technology_matrix_production[product,node, process_production, year_construction]
                                    # Multiplied by the sum of the 
                                    for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_production[process_production, year_construction]), model.construction_years_production))
                                # Adding the sum over all new capacity built before or in this year
                                + sum(# Existing capacity of a production process
                                    model.predicted_new_capacity_production[node, process_production, earlier_year]
                                    # Multiplied by the value for the product in the technology matrix
                                    # Note, that negative values can exist and therefore increase the need for secured capacity
                                    * model.technology_matrix_production[product,node, process_production, earlier_year]
                                    for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year <= model.lifetime_duration_production[process_production, earlier_year]), model.years)))
                                * model.time_slice_yearly_weight[time_slice]
                                for time_slice in model.time_slices)
                            for process_production in model.processes_production)
                        for node in model.nodes if node != model.nodes[model.current_node.value])
                    # CURRENT_NODE
                    + # Sum over all production processes
                    sum(
                        # sum over all time_slices
                        sum(
                            model.usable_capacity_factor_timeseries_production[model.nodes[model.current_node.value], process_production, time_slice]*(
                            # Sum over all existing capacity
                            sum(# Existing capacity of a production process
                                model.existing_capacity_production[model.nodes[model.current_node.value], process_production, year, year_construction]
                                # Multiplied by the value for the product in the technology matrix
                                # Note, that negative values can exist and therefore increase the need for secured capacity
                                * model.technology_matrix_production[product,model.nodes[model.current_node.value], process_production, year_construction]
                                for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_production[process_production, year_construction]), model.construction_years_production))
                            # Adding the sum over all new capacity built before or in this year
                            + sum(# Existing capacity of a production process
                                model.new_capacity_production[process_production, earlier_year]*model.scaling_new_capacity_production[process_production, earlier_year]
                                # Multiplied by the value for the product in the technology matrix
                                # Note, that negative values can exist and therefore increase the need for secured capacity
                                * model.technology_matrix_production[product,model.nodes[model.current_node.value], process_production, earlier_year]
                                for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year <= model.lifetime_duration_production[process_production, earlier_year]), model.years)))
                            * model.time_slice_yearly_weight[time_slice]
                            for time_slice in model.time_slices)
                        for process_production in model.processes_production))
            )
        else:
            Optimization.add_skipped_constraint()
            return pe.Constraint.Skip
    
    # pe.Constraint rules for the invest impact
    # nodal pe.Constraint rules for the invest impact
    @staticmethod 
    def constraint_invest_nodal_impact_rule(model, node: int, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the calculation of the invest impact vector
        summed over all processes.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """
        Optimization.add_evaluated_constraint()
        return (
            model.scaling_constraint_invest_nodal_impact[node,impact_category,year]*
            # calculate the invest nodal impact
            model.invest_nodal_impact[node, impact_category, year]*model.scaling_invest_nodal_impact[node, impact_category, year] ==
            model.scaling_constraint_invest_nodal_impact[node,impact_category,year]*
                # existing infrastructure
                # Sum over the impacts of all production processes
                (sum(
                    # sum over the construction years of the production process
                    sum(model.existing_capacity_production[node, process_production, year, year_construction]
                        * model.impact_matrix_production[impact_category, 'invest', node,process_production, year, year_construction]
                        for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_production[process_production, year_construction]), model.construction_years_production))    
                    for process_production in model.processes_production)

                # adding NEW CAPACITIES multiplied with the corresponding impact
                # sum over all invest years until the current year
                # Sum over the impacts of all production processes
                + sum(
                    # sum over the construction years of the production process
                    sum(model.new_capacity_production[process_production, earlier_year]*model.scaling_new_capacity_production[process_production, earlier_year]
                        * model.impact_matrix_production[impact_category, 'invest', node, process_production, year, earlier_year]
                        for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year <= model.lifetime_duration_production[process_production, earlier_year]), model.years))    
                    for process_production in model.processes_production if node == model.nodes[model.current_node.value])
                # if node is not current_node
                + sum(
                    # sum over the construction years of the production process
                    sum(model.predicted_new_capacity_production[node, process_production, earlier_year]
                        * model.impact_matrix_production[impact_category, 'invest', node, process_production, year, earlier_year]
                        for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year <= model.lifetime_duration_production[process_production, earlier_year]), model.years))    
                    for process_production in model.processes_production if node != model.nodes[model.current_node.value])
            )
        )

    @staticmethod
    def constraint_invest_nodal_impact_limits_rule(model, node: int, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the limitation of the invest impact vector summed over all processes

        Args:
            model: The equivalent of "self" in pyomo optimization models
            node (int): A node at which the pe.Constraint is active
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """
        if model.invest_nodal_impact_limits[node, impact_category, year] != np.inf:
            Optimization.add_evaluated_constraint()
            return (
                model.scaling_constraint_invest_nodal_impact_limits(node,impact_category,year)*
                # limit nodal invest impact
                model.invest_nodal_impact[node, impact_category, year]*model.scaling_invest_nodal_impact[node, impact_category, year] <=
                model.scaling_constraint_invest_nodal_impact_limits(node,impact_category,year)*
                (model.invest_nodal_impact_limits[node, impact_category, year]
                # adding an overshoot variable mainly for debugging feasibility issues
                + model.invest_nodal_impact_overshoot[node, impact_category, year]*model.scaling_invest_nodal_impact_overshoot[node, impact_category, year])
                )
        else:
            Optimization.add_skipped_constraint() 
            return pe.Constraint.Skip

    # overall pe.Constraint rules for the invest impact
    @staticmethod
    def constraint_invest_impact_rule(model, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the calculation of the invest impact vector summed over all processes and nodes

        Args:
            model: The equivalent of "self" in pyomo optimization models
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """
        Optimization.add_evaluated_constraint()
        return (
            model.scaling_constraint_invest_impact[impact_category,year]*
            model.invest_impact[impact_category, year]*model.scaling_invest_impact[impact_category, year] ==
            model.scaling_constraint_invest_impact[impact_category,year]*
            sum(model.invest_nodal_impact[node, impact_category, year]*model.scaling_invest_nodal_impact[node, impact_category, year]
                for node in model.nodes)
        )

    @staticmethod
    def constraint_invest_impact_limits_rule(model, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the limitation of the invest impact vector summed over all processes and nodes

        Args:
            model: The equivalent of "self" in pyomo optimization models
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """
        if model.invest_impact_limits[impact_category, year] != np.inf:
            Optimization.add_evaluated_constraint()
            return (
                model.scaling_constraint_invest_impact_limits[impact_category,year]*
                # limit overall operational impact
                model.invest_impact[impact_category, year]*model.scaling_invest_impact[impact_category, year] <=
                model.scaling_constraint_invest_impact_limits[impact_category,year]*
                (model.invest_impact_limits[impact_category, year]
                # adding an overshoot variable mainly for debugging feasibility issues
                + model.invest_impact_overshoot[impact_category, year]*model.scaling_invest_impact_overshoot[impact_category, year])
            )
        else:
            Optimization.add_skipped_constraint()
            return pe.Constraint.Skip

    # pe.Constraint rules for the total impact
    # nodal pe.Constraint rules for the total impact
    @staticmethod
    def constraint_total_nodal_impact_rule(model, node: int, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the calculation of the total impact vector summed over all processes

        Args:
            model: The equivalent of "self" in pyomo optimization models
            node (int): A node at which the pe.Constraint is active
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """
        Optimization.add_evaluated_constraint()
        return (
            model.scaling_constraint_total_nodal_impact[node,impact_category,year]*
            model.total_nodal_impact[node, impact_category, year]*model.scaling_total_nodal_impact[node, impact_category, year] ==
            model.scaling_constraint_total_nodal_impact[node,impact_category,year]*
            (model.operational_nodal_impact[node, impact_category, year]*model.scaling_operational_nodal_impact[node, impact_category, year]
            + model.invest_nodal_impact[node, impact_category, year]*model.scaling_invest_nodal_impact[node, impact_category, year])
        )

    @staticmethod
    def constraint_total_nodal_impact_limits_rule(model, node: int, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the limitation of the total impact vector summed over all processes

        Args:
            model: The equivalent of "self" in pyomo optimization models
            node (int): A node at which the pe.Constraint is active
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """
        if model.total_nodal_impact_limits[node, impact_category, year] != np.inf:
            Optimization.add_evaluated_constraint()
            return (
                model.scaling_constraint_total_nodal_impact_limits[node,impact_category,year]*
                # limit nodal total impact
                model.total_nodal_impact[node, impact_category, year]*model.scaling_total_nodal_impact[node, impact_category, year] <=
                model.scaling_constraint_total_nodal_impact_limits[node,impact_category,year]*
                (model.total_nodal_impact_limits[node, impact_category, year]
                # adding an overshoot variable mainly for debugging feasibility issues
                + model.total_nodal_impact_overshoot[node, impact_category, year]*model.scaling_total_nodal_impact_overshoot[node, impact_category, year])
            )
        else:
            Optimization.add_skipped_constraint()
            return pe.Constraint.Skip

    # overall pe.Constraint rules for the total impact
    @staticmethod
    def constraint_total_impact_rule(model, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the calculation of the total impact vector summed over all processes and nodes

        Args:
            model: The equivalent of "self" in pyomo optimization models
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """
        Optimization.add_evaluated_constraint()
        return (
            model.scaling_constraint_total_impact[impact_category,year]*
            model.total_impact[impact_category, year]*model.scaling_total_impact[impact_category, year] ==
            model.scaling_constraint_total_impact[impact_category,year]*
            sum(model.total_nodal_impact[node, impact_category, year]*model.scaling_total_nodal_impact[node, impact_category, year]
                for node in model.nodes)
        )

    @staticmethod
    def constraint_total_impact_limits_rule(model, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the limitation of the total impact vector summed over all processes and nodes

        Args:
            model: The equivalent of "self" in pyomo optimization models
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """
        if model.total_impact_limits[impact_category, year] != np.inf:
            Optimization.add_evaluated_constraint()
            return (
                model.scaling_constraint_total_impact_limits[impact_category,year]*
                # limit overall total impact
                model.total_impact[impact_category, year]*model.scaling_total_impact[impact_category, year] <=
                model.scaling_constraint_total_impact_limits[impact_category,year]*
                (model.total_impact_limits[impact_category, year]
                # adding an overshoot variable mainly for debugging feasibility issues
                + model.total_impact_overshoot[impact_category, year]*model.scaling_total_impact_overshoot[impact_category, year])
            )
        else:
            Optimization.add_skipped_constraint()
            return pe.Constraint.Skip

    ## DUAL CONSTRAINTS
    @staticmethod
    def dual_constraint_used_production_rule(model,node: int, process_production: str, year: int, construction_year_production: int, time_slice: int) -> "pyomo rule":
        """ This method is the dual constraint correspondent to the pe.Var used_production """
        variable = model.used_production
        data = (node,process_production,year,construction_year_production,time_slice)
        
        return(Optimization.get_dual_constraint_expression_for_variable(model,variable,data))

    @staticmethod
    def dual_constraint_non_served_demand_rule(model,node: int, product: str, year: int, time_slice: int) -> "pyomo rule":
        """ This method is the dual constraint correspondent to the pe.Var non_served_demand """
        variable = model.non_served_demand
        data = (node, product, year, time_slice)
        
        return(Optimization.get_dual_constraint_expression_for_variable(model,variable,data))

    @staticmethod
    def dual_constraint_phase_difference_rule(model,node: int, year: int, time_slice: int) -> "pyomo rule":
        """ This method is the dual constraint correspondent to the pe.Var phase_difference """
        variable = model.phase_difference
        data = (node, year, time_slice)
        
        return(Optimization.get_dual_constraint_expression_for_variable(model,variable,data))

    @staticmethod
    def dual_constraint_used_transmission_rule(model,connection: int, year: int, time_slice: int) -> "pyomo rule":
        """ This method is the dual constraint correspondent to the pe.Var used_transmission """
        variable = model.used_transmission
        data = (connection, year, time_slice)
        
        return(Optimization.get_dual_constraint_expression_for_variable(model,variable,data))

    @staticmethod
    def dual_constraint_operational_nodal_impact_rule(model,node: int, impact_category: str, year: int) -> "pyomo rule":
        """ This method is the dual constraint correspondent to the pe.Var operational_nodal_impact """
        variable = model.operational_nodal_impact
        data = (node, impact_category, year)
        
        return(Optimization.get_dual_constraint_expression_for_variable(model,variable,data))

    @staticmethod
    def dual_constraint_operational_nodal_impact_overshoot_rule(model,node: int, impact_category: str, year: int) -> "pyomo rule":
        """ This method is the dual constraint correspondent to the pe.Var operational_nodal_impact_overshoot """
        variable = model.operational_nodal_impact_overshoot
        data = (node, impact_category, year)

        return(Optimization.get_dual_constraint_expression_for_variable(model,variable,data))

    @staticmethod
    def dual_constraint_operational_impact_rule(model,impact_category: str, year: int) -> "pyomo rule":
        """ This method is the dual constraint correspondent to the pe.Var operational_impact """
        variable = model.operational_impact
        data = (impact_category, year)
        
        return(Optimization.get_dual_constraint_expression_for_variable(model,variable,data))

    @staticmethod
    def dual_constraint_operational_impact_overshoot_rule(model,impact_category: str, year: int) -> "pyomo rule":
        """ This method is the dual constraint correspondent to the pe.Var operational_impact_overshoot """
        variable = model.operational_impact_overshoot
        data = (impact_category, year)
        
        return(Optimization.get_dual_constraint_expression_for_variable(model,variable,data))

    @staticmethod
    def dual_constraint_invest_nodal_impact_rule(model,node: int, impact_category: str, year: int) -> "pyomo rule":
        """ This method is the dual constraint correspondent to the pe.Var invest_nodal_impact """
        variable = model.invest_nodal_impact
        data = (node, impact_category, year)
        
        return(Optimization.get_dual_constraint_expression_for_variable(model,variable,data))

    @staticmethod
    def dual_constraint_invest_nodal_impact_overshoot_rule(model,node: int, impact_category: str, year: int) -> "pyomo rule":
        """ This method is the dual constraint correspondent to the pe.Var invest_nodal_impact_overshoot """
        variable = model.invest_nodal_impact_overshoot
        data = (node, impact_category, year)
        
        return(Optimization.get_dual_constraint_expression_for_variable(model,variable,data))

    @staticmethod
    def dual_constraint_invest_impact_rule(model,impact_category: str, year: int) -> "pyomo rule":
        """ This method is the dual constraint correspondent to the pe.Var invest_impact """
        variable = model.invest_impact
        data = (impact_category, year)
        
        return(Optimization.get_dual_constraint_expression_for_variable(model,variable,data))

    @staticmethod
    def dual_constraint_invest_impact_overshoot_rule(model,impact_category: str, year: int) -> "pyomo rule":
        """ This method is the dual constraint correspondent to the pe.Var invest_impact_overshoot """
        variable = model.invest_impact_overshoot
        data = (impact_category, year)
        
        return(Optimization.get_dual_constraint_expression_for_variable(model,variable,data))

    @staticmethod
    def dual_constraint_total_nodal_impact_rule(model,node: int, impact_category: str, year: int) -> "pyomo rule":
        """ This method is the dual constraint correspondent to the pe.Var total_nodal_impact """
        variable = model.total_nodal_impact
        data = (node, impact_category, year)
        
        return(Optimization.get_dual_constraint_expression_for_variable(model,variable,data))

    @staticmethod
    def dual_constraint_total_nodal_impact_overshoot_rule(model,node: int, impact_category: str, year: int) -> "pyomo rule":
        """ This method is the dual constraint correspondent to the pe.Var total_nodal_impact_overshoot """
        variable = model.total_nodal_impact_overshoot
        data = (node, impact_category, year)
        
        return(Optimization.get_dual_constraint_expression_for_variable(model,variable,data))

    @staticmethod
    def dual_constraint_total_impact_rule(model,impact_category: str, year: int) -> "pyomo rule":
        """ This method is the dual constraint correspondent to the pe.Var total_impact """
        variable = model.total_impact
        data = (impact_category, year)
        
        return(Optimization.get_dual_constraint_expression_for_variable(model,variable,data))

    @staticmethod
    def dual_constraint_total_impact_overshoot_rule(model,impact_category: str, year: int) -> "pyomo rule":
        """ This method is the dual constraint correspondent to the pe.Var total_impact_overshoot """
        variable = model.total_impact_overshoot
        data = (impact_category, year)
        
        return(Optimization.get_dual_constraint_expression_for_variable(model,variable,data))
    
    # strong duality constraint
    @staticmethod
    def strong_duality_equation_rule(model):
        """ This method declares the Strong Duality Constraint 
        min c^T x s.t. Ax >= b
        -->
        - c^T x = \lambda^T b"""
        # first term: LL objective expression
        strong_duality_equation = model.LLObjectiveExpression.expr

        # second term: RHS*dual_variable
        # iterate through all constraints, except for dual_constraints and binary_expansion constraints
        for constraint in model.component_objects(pe.Constraint):
            if "dual_constraint" not in constraint.name and "binary_expansion" not in constraint.name:
                # iterate through data in constraints
                for data in constraint:
                    # get dual variable and scaling factor
                    dual_variable = model.find_component(constraint.name.replace("constraint","dual_variable"))[data]
                    dual_variable *= model.find_component(constraint.name.replace("constraint","scaling_dual_variable"))[data]
                    # multiply with time_slice_weight
                    _index = [id(index) for index in constraint.index_set().subsets()]
                    if id(model.time_slices) in _index:
                        dual_variable *= model.time_slice_yearly_weight[data[_index.index(id(model.time_slices))]]
                    # if equality expression
                    if isinstance(constraint[data].expr,EXPR.EqualityExpression):
                        multiplicator = 1
                        RHS = constraint[data].upper*dual_variable
                    # if inequality expression with upper bound
                    elif constraint[data].upper is not None:
                        multiplicator = 1
                        RHS = constraint[data].upper*dual_variable
                    # if inequality expression with lower bound
                    else:
                        multiplicator = -1
                        RHS = constraint[data].lower*dual_variable
                    
                    for term in EXPR.decompose_term(constraint[data].body)[1]:
                        # find constant terms in body and add it to RHS
                        if term[1] is None:
                            RHS -= term[0]*dual_variable
                        elif term[1].parent_component().name == "new_capacity_production":
                            # calculate binary expansion of product of new_capacity_production and dual_variable_production_limit_by_capacity
                            RHS -=  term[0]*term[1]*dual_variable
                            
                    # add RHS*dual_variable*multiplicator to SDE
                    strong_duality_equation += RHS*multiplicator
        # return SDE==0
        return (strong_duality_equation == 0)


        
    # pe.Objective #
    @staticmethod
    def objectiveLLRule(model):
        """This method is used as an Lower Level pe.Objective for the optimization, minimizing the social cost (i.e., minimizing the sum of all operational costs)

        Args:
            model: The equivalent of "self" in pyomo optimization models
        """

        return (
            # sum over all years
            sum(
                # sum over all impact categories
                sum(
                    # sum of the operational impact over all impact categories multiplied by the corresponding weight factor
                        model.operational_impact[impact_category, year]
                        * model.scaling_operational_impact[impact_category, year]
                    for impact_category in model.impact_categories if impact_category == "cost")
                # add operational impact overshoot
                + 
                model.operational_impact_overshoot[model.capped_impacts.ordered_data()[0], year]
                *model.scaling_operational_impact_overshoot[model.capped_impacts.ordered_data()[0], year]
                *model.objective_factor_impact_overshoot[model.capped_impacts.ordered_data()[0], year]
                for year in model.years)
        )
    
    @staticmethod
    def objectiveULRule(model):
        """This method is used as an Upper Level pe.Objective for the optimization.

        Args:
            model: The equivalent of "self" in pyomo optimization models
        """
        # calculate total and nodal demand
        _nodal_demand = {}
        _total_demand = {}
        for year in model.years:
            _total_demand[year] = sum(sum(model.demand[model.traded_products.ordered_data()[0],year,time_slice,demand_node]*model.time_slice_yearly_weight[time_slice] for time_slice in model.time_slices) for demand_node in model.nodes)
            for node in model.nodes:
                _nodal_demand[(year,node)] = sum(model.demand[model.traded_products.ordered_data()[0],year,time_slice,node]*model.time_slice_yearly_weight[time_slice] for time_slice in model.time_slices)
        
        return (
            
            # sum over all years 
            model.scaling_objective*(sum(
                sum(# Multiply everything with the yearly weight of the time slice
                    model.time_slice_yearly_weight[time_slice]*(
                    # sum over all products
                    sum(
                        model.demand[product, year, time_slice, model.nodes[model.current_node.value]]
                        # market clearing price
                        * model.dual_variable_product_balance[model.nodes[model.current_node.value],product,year,time_slice]
                        * model.scaling_dual_variable_product_balance[model.nodes[model.current_node.value],product,year,time_slice]
                    for product in model.products)
                    # )
                    -
                    # add total capacity * dual variable of constraint_production_limit_by_capacity
                    sum(
                        # existing capacity
                        sum(
                            model.existing_capacity_production[model.nodes[model.current_node.value], process_production, year, year_construction]
                            * model.usable_capacity_factor_timeseries_production[model.nodes[model.current_node.value], process_production, time_slice] 
                            * model.dual_variable_production_limit_by_capacity[model.nodes[model.current_node.value],process_production,year,year_construction,time_slice]
                            * model.scaling_dual_variable_production_limit_by_capacity[model.nodes[model.current_node.value],process_production,year,year_construction,time_slice]
                        for year_construction in model.construction_years_production)
                        + 
                        # new capacity
                        sum(
                            model.new_capacity_production[process_production, year_construction]*model.scaling_new_capacity_production[process_production, year_construction]
                            * model.usable_capacity_factor_timeseries_production[model.nodes[model.current_node.value], process_production, time_slice] 
                            * model.dual_variable_production_limit_by_capacity[model.nodes[model.current_node.value],process_production,year,year_construction,time_slice]
                            * model.scaling_dual_variable_production_limit_by_capacity[model.nodes[model.current_node.value],process_production,year,year_construction,time_slice]
                        for year_construction in model.years)
                    for process_production in model.processes_production)
                    -
                    # if impact_matrix_non_served_demand[emission impact category] == 0, equals to zero
                    # kept if changed in the future
                    sum(model.dual_variable_operational_nodal_impact[model.nodes[model.current_node.value],model.capped_impacts.ordered_data()[0],year]
                        *model.scaling_dual_variable_operational_nodal_impact[model.nodes[model.current_node.value],model.capped_impacts.ordered_data()[0],year]
                        *model.non_served_demand[model.nodes[model.current_node.value],product,year,time_slice]
                        *model.scaling_non_served_demand[model.nodes[model.current_node.value],product,year,time_slice]
                        *model.impact_matrix_non_served_demand[model.capped_impacts.ordered_data()[0],product,year,time_slice]
                    for product in model.products)
                    +
                    sum(model.non_served_demand[model.nodes[model.current_node.value],product,year,time_slice]
                        *model.scaling_non_served_demand[model.nodes[model.current_node.value],product,year,time_slice]
                        *model.impact_matrix_non_served_demand["cost",product,year,time_slice]
                    for product in model.traded_products))
                for time_slice in model.time_slices)
                + 
                # add invest costs
                model.invest_nodal_impact[model.nodes[model.current_node.value], "cost", year]*model.scaling_invest_nodal_impact[model.nodes[model.current_node.value], "cost", year]
                
                # add operational impact overshoot
                + 
                model.operational_impact_overshoot[model.capped_impacts.ordered_data()[0], year]
                *model.scaling_operational_impact_overshoot[model.capped_impacts.ordered_data()[0], year]
                *model.objective_factor_impact_overshoot[model.capped_impacts.ordered_data()[0], year]
                # multiply with fraction of demand
                *_nodal_demand[(year,model.nodes[model.current_node.value])]/_total_demand[year]
            for year in model.years))
        )
        
    @staticmethod
    def objectiveShortULRule(model, node):
        """This method is used as an pe.Expression for the UL objective function.

        It is not used in the optimization, just to evaluate if the reduced form above produces the same result as the original formulation below and to calculate the objective value for all nodes.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            node: node/player for which the objective is evaluated
        """
        # calculate total and nodal demand
        _nodal_demand = {}
        _total_demand = {}
        for year in model.years:
            _total_demand[year] = sum(sum(model.demand[model.traded_products.ordered_data()[0],year,time_slice,demand_node]*model.time_slice_yearly_weight[time_slice] for time_slice in model.time_slices) for demand_node in model.nodes)
            for node in model.nodes:
                _nodal_demand[(year,node)] = sum(model.demand[model.traded_products.ordered_data()[0],year,time_slice,node]*model.time_slice_yearly_weight[time_slice] for time_slice in model.time_slices)

        if node == model.nodes[model.current_node.value]:
            return (
                
                # sum over all years 
                sum(
                    sum(# Multiply everything with the yearly weight of the time slice
                        model.time_slice_yearly_weight[time_slice]*(
                        # sum over all products
                        sum(
                            model.demand[product, year, time_slice, node]
                            # market clearing price
                            * model.dual_variable_product_balance[node,product,year,time_slice]
                            * model.scaling_dual_variable_product_balance[node,product,year,time_slice]
                        for product in model.products)
                        # )
                        -
                        # add total capacity * dual variable of constraint_production_limit_by_capacity
                        sum(
                            # existing capacity
                            sum(
                                model.existing_capacity_production[node, process_production, year, year_construction]
                                * model.usable_capacity_factor_timeseries_production[node, process_production, time_slice] 
                                * model.dual_variable_production_limit_by_capacity[node,process_production,year,year_construction,time_slice]
                                * model.scaling_dual_variable_production_limit_by_capacity[node,process_production,year,year_construction,time_slice]
                            for year_construction in model.construction_years_production)
                            + 
                            # new capacity
                            sum(
                                model.new_capacity_production[process_production, year_construction]*model.scaling_new_capacity_production[process_production, year_construction]
                                * model.usable_capacity_factor_timeseries_production[node, process_production, time_slice] 
                                * model.dual_variable_production_limit_by_capacity[node,process_production,year,year_construction,time_slice]
                                * model.scaling_dual_variable_production_limit_by_capacity[node,process_production,year,year_construction,time_slice]
                            for year_construction in model.years)
                        for process_production in model.processes_production)
                        -
                        # if impact_matrix_non_served_demand[emission impact category] == 0, equals to zero
                        # kept if changed in the future
                        sum(model.dual_variable_operational_nodal_impact[node,model.capped_impacts.ordered_data()[0],year]
                            *model.scaling_dual_variable_operational_nodal_impact[node,model.capped_impacts.ordered_data()[0],year]
                            *model.non_served_demand[node,product,year,time_slice]
                            *model.scaling_non_served_demand[node,product,year,time_slice]
                            *model.impact_matrix_non_served_demand[model.capped_impacts.ordered_data()[0],product,year,time_slice]
                        for product in model.products)
                        
                        +
                        # dual_variable_operational_nodal_impact 
                        sum(model.non_served_demand[node,product,year,time_slice]
                            *model.scaling_non_served_demand[node,product,year,time_slice]
                            *model.impact_matrix_non_served_demand["cost",product,year,time_slice]
                        for product in model.traded_products)) 
                    for time_slice in model.time_slices)
                    + 
                    # add invest costs
                    model.invest_nodal_impact[node, "cost", year]*model.scaling_invest_nodal_impact[node, "cost", year]
                    
                    # add operational impact overshoot
                    + 
                    model.operational_impact_overshoot[model.capped_impacts.ordered_data()[0], year]
                    *model.scaling_operational_impact_overshoot[model.capped_impacts.ordered_data()[0], year]
                    *model.objective_factor_impact_overshoot[model.capped_impacts.ordered_data()[0], year]
                    # multiply with fraction of demand
                    *_nodal_demand[(year,node)]/_total_demand[year]
                    for year in model.years)
            )
        else:
            return (
                
                # sum over all years 
                sum(
                    sum(# Multiply everything with the yearly weight of the time slice
                        model.time_slice_yearly_weight[time_slice]*(
                        # sum over all products
                        sum(
                            model.demand[product, year, time_slice, node]
                            # market clearing price
                            * model.dual_variable_product_balance[node,product,year,time_slice]
                            * model.scaling_dual_variable_product_balance[node,product,year,time_slice]
                        for product in model.products)
                        # )
                        -
                        # add total capacity * dual variable of constraint_production_limit_by_capacity
                        sum(
                            # existing capacity
                            sum(
                                model.existing_capacity_production[node, process_production, year, year_construction]
                                * model.usable_capacity_factor_timeseries_production[node, process_production, time_slice] 
                                * model.dual_variable_production_limit_by_capacity[node,process_production,year,year_construction,time_slice]
                                * model.scaling_dual_variable_production_limit_by_capacity[node,process_production,year,year_construction,time_slice]
                            for year_construction in model.construction_years_production)
                            + 
                            # new capacity
                            sum(
                                model.predicted_new_capacity_production[node,process_production, year_construction]
                                * model.usable_capacity_factor_timeseries_production[node, process_production, time_slice] 
                                * model.dual_variable_production_limit_by_capacity[node,process_production,year,year_construction,time_slice]
                                * model.scaling_dual_variable_production_limit_by_capacity[node,process_production,year,year_construction,time_slice]
                            for year_construction in model.years)
                        for process_production in model.processes_production)
                        -
                        # if impact_matrix_non_served_demand[emission impact category] == 0, equals to zero
                        # kept if changed in the future
                        sum(model.dual_variable_operational_nodal_impact[node,model.capped_impacts.ordered_data()[0],year]
                            *model.scaling_dual_variable_operational_nodal_impact[node,model.capped_impacts.ordered_data()[0],year]
                            *model.non_served_demand[node,product,year,time_slice]
                            *model.scaling_non_served_demand[node,product,year,time_slice]
                            *model.impact_matrix_non_served_demand[model.capped_impacts.ordered_data()[0],product,year,time_slice]
                        for product in model.products)
                        
                        +
                        # dual_variable_operational_nodal_impact 
                        sum(model.non_served_demand[node,product,year,time_slice]
                            *model.scaling_non_served_demand[node,product,year,time_slice]
                            *model.impact_matrix_non_served_demand["cost",product,year,time_slice]
                        for product in model.traded_products)) 
                    for time_slice in model.time_slices)
                    + 
                    # add invest costs
                    model.invest_nodal_impact[node, "cost", year]*model.scaling_invest_nodal_impact[node, "cost", year]
                    
                    # add operational impact overshoot
                    + 
                    model.operational_impact_overshoot[model.capped_impacts.ordered_data()[0], year]
                    *model.scaling_operational_impact_overshoot[model.capped_impacts.ordered_data()[0], year]
                    *model.objective_factor_impact_overshoot[model.capped_impacts.ordered_data()[0], year]
                    # multiply with fraction of demand
                    *_nodal_demand[(year,node)]/_total_demand[year]
                    for year in model.years)
            )
    @staticmethod
    def objectiveLongULRule(model):
        """This method is used as an pe.Expression for the UL objective function.

        It is not used in the optimization, just to evaluate if the reduced form above produces the same result as the original formulation here.

        Args:
            model: The equivalent of "self" in pyomo optimization models
        """
        _nodal_demand = {}
        _total_demand = {}
        for year in model.years:
            _total_demand[year] = sum(sum(model.demand[model.traded_products.ordered_data()[0],year,time_slice,demand_node]*model.time_slice_yearly_weight[time_slice] for time_slice in model.time_slices) for demand_node in model.nodes)
            for node in model.nodes:
                _nodal_demand[(year,node)] = sum(model.demand[model.traded_products.ordered_data()[0],year,time_slice,node]*model.time_slice_yearly_weight[time_slice] for time_slice in model.time_slices)

        return(
            # sum over all years 
            model.scaling_objective*(sum(
                sum(# Multiply everything with the yearly weight of the time slice
                    model.time_slice_yearly_weight[time_slice]*(
                    # sum over all products
                    sum(
                        model.demand[product, year, time_slice, model.nodes[model.current_node.value]]*
                        # market clearing price
                        model.dual_variable_product_balance[model.nodes[model.current_node.value],product,year,time_slice]*model.scaling_dual_variable_product_balance[model.nodes[model.current_node.value],product,year,time_slice]
                    -
                    # subtract trading revenue 
                        sum(
                            sum(
                                model.technology_matrix_production[product,model.nodes[model.current_node.value], process_production, year_construction]*
                                model.used_production[model.nodes[model.current_node.value], process_production, year, year_construction, time_slice]
                                * model.scaling_used_production[model.nodes[model.current_node.value], process_production, year, year_construction, time_slice]
                                * model.dual_variable_product_balance[model.nodes[model.current_node.value],product,year,time_slice]
                                * model.scaling_dual_variable_product_balance[model.nodes[model.current_node.value],product,year,time_slice]
                            for year_construction in model.construction_years_production)
                        for process_production in model.processes_production)
                    for product in model.traded_products)
                    
                )
                for time_slice in model.time_slices)
                +
                # add allowance costs
                model.operational_nodal_impact[model.nodes[model.current_node.value],model.capped_impacts.ordered_data()[0], year]
                *model.scaling_operational_nodal_impact[model.nodes[model.current_node.value],model.capped_impacts.ordered_data()[0], year]
                *model.dual_variable_operational_impact_limits[model.capped_impacts.ordered_data()[0], year]
                *model.scaling_dual_variable_operational_impact_limits[model.capped_impacts.ordered_data()[0], year]
                + 
                # add operational costs
                model.operational_nodal_impact[model.nodes[model.current_node.value],"cost", year]
                *model.scaling_operational_nodal_impact[model.nodes[model.current_node.value],"cost", year]
                # add invest costs
                +
                model.invest_nodal_impact[model.nodes[model.current_node.value], "cost", year]*model.scaling_invest_nodal_impact[model.nodes[model.current_node.value], "cost", year]
                # add operational impact overshoot
                + 
                model.operational_impact_overshoot[model.capped_impacts.ordered_data()[0], year]
                *model.scaling_operational_impact_overshoot[model.capped_impacts.ordered_data()[0], year]
                *model.objective_factor_impact_overshoot[model.capped_impacts.ordered_data()[0], year]
                # multiply with fraction of demand
                *_nodal_demand[(year,node)]/_total_demand[year]
            for year in model.years))
        )
    
    @staticmethod
    def BinaryExpansionGapRule(model):
        """ This method calculates the upper bound on the gap in the obejctive function, due to the binary expansion"""
        return(
            model.scaling_objective*(sum(
                sum(# Multiply everything with the yearly weight of the time slice
                    model.time_slice_yearly_weight[time_slice]*
                        sum(
                        # new capacity (binary expansion)
                        sum(
                            model.usable_capacity_factor_timeseries_production[model.nodes[model.current_node.value], process_production, time_slice] 
                            * model.delta_new_capacity_production[process_production,year_construction]
                            * model.dual_variable_production_limit_by_capacity[model.nodes[model.current_node.value],process_production,year,year_construction,time_slice]
                            * model.scaling_dual_variable_production_limit_by_capacity[model.nodes[model.current_node.value],process_production,year,year_construction,time_slice]
                            # model.big_M_binary_expansion_new_capacity_production[process_production,year,year,time_slice]
                        for year_construction in model.years)
                    for process_production in model.processes_production)
                for time_slice in model.time_slices)
            for year in model.years))
        )
        
