# import namespace for abstract classes
import abc
import logging
import secmod.scaling as scaling
import secmod.helpers as helpers
from datetime import datetime
import pandas as pd
import importlib.util
import numpy as np
import scipy.stats.mstats as sp
import cProfile

# import necessary elements of pyomo - alternatively use "from pyomo.environ import *", if you are missing something
import pyomo.environ as pe
from pyomo.opt import TerminationCondition as TerminationCondition
from pyomo.core.expr import current as EXPR
from pyomo.core.expr.numvalue import NumericConstant

# Load config.py
spec = importlib.util.spec_from_file_location("config", "SecMOD/00-INPUT/00-RAW-INPUT/config.py")
config = importlib.util.module_from_spec(spec)
spec.loader.exec_module(config)

class Optimization(abc.ABC):
    """This is the class for optimization models.

    It includes all declarations of Sets, Parameters, Variables and Constraints. 
    Furthermore it contains all optimization methods.
    """
    # PROPERTIES #
    # empty properties later set
    variable_dict = {}
    _num_constraints = {}
    numerics = {}

    def __init__(self):
        """This method initializes the model as an abstract pyomo model.
        
        Hence, all necessary pe.Sets, pe.Params (including scaling parameters), pe.Vars, pe.Constraints as well as the Objective Function are setup here"""
        # set necessary properties
        setattr(Optimization,'variable_dict',{})
        setattr(Optimization,'_num_constraints',{'evaluated':0,'skipped':0})
        setattr(Optimization,'numerics',{'unscaled':{'max_val':{'var':None,'val': None},'min_val':{'var':None,'val': None},'sum_coeff_sq':0},'scaled':{'max_val':{'var':None,'val': None},'min_val':{'var':None,'val': None},'sum_coeff_sq':0}})
    
        # create Pyomo AbstractModel
        self.model = pe.AbstractModel()
        
        # SETS # Declaration of sets used in the optimization
        self.setupSets()

        # PARAMETERS # Declaration of parameters used in the optimization
        self.setupParameters()
        
        # PARAMETERS # Declaration of scaling parameters used in the optimization
        self.setupScalingParameters()

        # VARIABLES # Declaration of variables used in the optimization
        self.setupVariables()
        
        # CONSTRAINTS # Definition of constraints used in the optimization
        self.setupConstraints()
    
        # pe.Objective
        self.setupObjective()
        
    # METHODS #
    @staticmethod
    def calculate_scaling_factor(scaling_vector):
        """ returns scaling factor r or c for scaling_vector as Arg and scaling algorithm selected in config
        Args: 
            scaling_vector(list): list with coefficients

        Returns:
            scaling_factor(float): scaling factor corresponding to scaling_vector
        """
        scaling_vector = np.abs(scaling_vector)
        # if scaling_vector not empty vector
        if not scaling_vector.size == 0:
            # if all coefficients == 1
            if np.all(scaling_vector == 1):
                scaling_factor = 1
            # else, apply one of the following algorithms
            else:
                if config.scaling_options['algorithm'] == 'approx_geom_mean': # geometric mean scaling
                    scaling_factor_til = np.sqrt(1/(scaling_vector.max()*scaling_vector.min()))
                elif config.scaling_options['algorithm'] == 'geom_mean': # exact geometric mean scaling
                    scaling_factor_til = 1/sp.gmean(scaling_vector)
                elif config.scaling_options['algorithm'] == 'arith_mean': # arithmetic mean scaling
                    scaling_factor_til = 1/np.mean(scaling_vector)
                elif config.scaling_options['algorithm'] == 'equi': # equilibrium scaling
                    scaling_factor_til = 1/np.max(scaling_vector)
                else:
                    raise KeyError('Scaling Algorithm {} not found'.format(config.scaling_options['algorithm']))

                if config.scaling_options['Round_to_power_of_2'] and not config.scaling_options['algorithm'] == 'equi':
                    # round to the closest power of two
                    scaling_factor = 2**(np.round(np.log2(scaling_factor_til)))
                else:
                    scaling_factor = scaling_factor_til
        else:
            scaling_factor = None
        # return scaling factor
        return scaling_factor

    @classmethod
    def set_coefficients_for_numerics(cls,item,scaled_model:bool,constraint_name='<constraint unknown>'):
        """ this classmethod receives an item containing a tuple of a variable and its corresponding coefficient. 
        It subsequently compares the coefficient to the saved max and min value and saves the coefficient if it is larger/smaller.
        Furthermore it sums up the coefficients in order to calculate the frobenius norm.
        
        Args: 
            cls: class Optimization
            item(tuple): tuple which contains coefficient [0] and variable [1]
            scaled_model(bool): boolean whether scaled or unscaled model
            constraint_name(str): str which contains name of constraint. 
                During the construction of the constraint, it is impossible and impractical to pass the constraint name. 
                If constraint of min/max coefficient can not be derived from context, disable scaling 
        """
        # get if coefficients from unscaled or scaled model
        if scaled_model:
            _model_type = 'scaled'
        else:
            _model_type = 'unscaled'
        item_val = abs(pe.value(item[0]))
        # ||A||_F = \sqrt(\sum(\abs(a_ij)^2))
        cls.numerics[_model_type]['sum_coeff_sq'] += item_val**2
        if cls.numerics[_model_type]['max_val']['var'] is None: # set first item as min max
            cls.numerics[_model_type]['max_val']['var'] = item[1].name + ' in ' + constraint_name
            cls.numerics[_model_type]['max_val']['val'] = item_val
            cls.numerics[_model_type]['min_val']['var'] = item[1].name + ' in ' + constraint_name
            cls.numerics[_model_type]['min_val']['val'] = item_val
        elif item_val > cls.numerics[_model_type]['max_val']['val']:
            cls.numerics[_model_type]['max_val']['var'] = item[1].name + ' in ' + constraint_name
            cls.numerics[_model_type]['max_val']['val'] = item_val
        elif item_val < cls.numerics[_model_type]['min_val']['val']:
            cls.numerics[_model_type]['min_val']['var'] = item[1].name + ' in ' + constraint_name
            cls.numerics[_model_type]['min_val']['val'] = item_val

    @classmethod
    def calculate_matrix_properties(cls):
        """ this classmethod calculates the highest and lowest matrix coefficient, and the frobenius norm for the scaled and unscaled model
        Performed after initialization of model.
        Args: 
            cls: class Optimization
        """
        # unscaled model
        logging.info('Numeric properties of unscaled model:')
        # if properties exist for unscaled model --> scaling enabled
        if cls.numerics['unscaled']['max_val']['val'] is not None:
            _model_type = 'unscaled'
            logging.info('Largest matrix coefficient: {:.3e} in {}'.format(cls.numerics[_model_type]['max_val']['val'],cls.numerics[_model_type]['max_val']['var']))
            logging.info('Smallest (nonzero) matrix coefficient: {:.3e} in {}'.format(cls.numerics[_model_type]['min_val']['val'],cls.numerics[_model_type]['min_val']['var']))
            _matrix_range = np.round(np.log10(cls.numerics[_model_type]['max_val']['val']))-np.round(np.log10(cls.numerics[_model_type]['min_val']['val']))
            logging.info('Range of order of magnitude: {}'.format(_matrix_range))
            logging.info('Frobenius norm: {:.3e}'.format(np.sqrt(cls.numerics[_model_type]['sum_coeff_sq'])))
            # scaled model
            logging.info('Numeric properties of scaled model:')
        _model_type = 'scaled'
        logging.info('Largest matrix coefficient: {:.3e} in {}'.format(cls.numerics[_model_type]['max_val']['val'],cls.numerics[_model_type]['max_val']['var']))
        logging.info('Smallest (nonzero) matrix coefficient: {:.3e} in {}'.format(cls.numerics[_model_type]['min_val']['val'],cls.numerics[_model_type]['min_val']['var']))
        _matrix_range = np.round(np.log10(cls.numerics[_model_type]['max_val']['val']))-np.round(np.log10(cls.numerics[_model_type]['min_val']['val']))
        logging.info('Range of order of magnitude: {}'.format(_matrix_range))
        logging.info('Frobenius norm: {:.3e}'.format(np.sqrt(cls.numerics[_model_type]['sum_coeff_sq'])))
    
    @classmethod
    def set_variable_dict(cls,variable,variable_coefficient,constraint_scaling_factor):
        """ appends variable_coefficient to variable_dict 
        Args: 
            cls: class Optimization
            variable(pe.Var): indexed variable 
            variable_coefficient(float): value of coefficient corresponding to variable
            constraint_scaling_factor(float): scaling factor of constraint
        """
        # if id(variable) already in variable_dict, append to key
        try:
            cls.variable_dict[id(variable)].append(variable_coefficient*constraint_scaling_factor)
        # if not yet in variable_dict, create new key
        except KeyError:
            cls.variable_dict[id(variable)] = [variable_coefficient*constraint_scaling_factor]

    @classmethod
    def add_evaluated_constraint(cls):
        """ this method increases the counter of evaluated constraints by one """
        cls._num_constraints['evaluated'] += 1

    @classmethod
    def add_skipped_constraint(cls):
        """ this method increases the counter of skipped constraints by one """
        cls._num_constraints['skipped'] += 1

    @classmethod
    def scale_expr(cls,unscaled_expr):
        """ this method scales an constraint expression, if config.scaling_options['scale_constraints'] or config.scaling_options['scale_variables'] selected. 
        If config.scaling_options['scale_constraints'] the constraint is scaled.
        If config.scaling_options['scale_variables'] selected, this methods saves the variable coefficients in cls.variable_dict
        
        Args: 
            cls: class Optimization
            unscaled_expr(EXPR.InequalityExpression/EXPR.EqualityExpression): unscaled inequality or equality expression, 
                which is generated by the calling constraint

        Returns:
            scaled_expr(EXPR.InequalityExpression/EXPR.EqualityExpression): unscaled inequality or equality expression, 
                used in the calling constraint
        """
        # if either constraints or variables scaled
        if config.scaling_options['scale_constraints'] or config.scaling_options['scale_variables']:
            merged_expr = unscaled_expr.args[0] + unscaled_expr.args[1]
            variable_coefficient_list = EXPR.decompose_term(merged_expr)[1]
            # if constraints not scaled
            if not config.scaling_options['scale_constraints']: 
                if config.scaling_options['scale_variables']:
                    for item in variable_coefficient_list:
                        # if item[1] is variable, which is not fixed
                        if item[1] is not None and item[1].value != 0: 
                            # multiply variable coefficient with 1, since constraint not scaled
                            cls.set_variable_dict(item[1],pe.value(item[0]),1) 
                            # collect coefficient information for numerical analysis
                            if config.scaling_options['analyze_numerics']:
                                cls.set_coefficients_for_numerics(item,scaled_model=False)
                return unscaled_expr
            # if constraints scaled
            # calculate constraint_scaling_factor
            constraint_scaling_factor = cls.calculate_scaling_factor([pe.value(item[0]) for item in variable_coefficient_list if item[1] is not None and item[1].value != 0])
            if constraint_scaling_factor:
                for item in variable_coefficient_list:
                    # if item[1] is variable, which is not fixed
                    if item[1] is not None and item[1].value != 0: 
                        # if variables scaled
                        if config.scaling_options['scale_variables']:
                            # multiply variable coefficient with scaling factor
                            cls.set_variable_dict(item[1],pe.value(item[0]),constraint_scaling_factor) 
                        # collect coefficient information for numerical analysis
                        if config.scaling_options['analyze_numerics']:
                            cls.set_coefficients_for_numerics(item,scaled_model=False)
                # if constraints_scaling_factor == 1, no scaling necessary
                if constraint_scaling_factor != 1:
                    # scale lhs and rhs of unscaled_expr with constraint_scaling_factor
                    if unscaled_expr.args[0].__class__ not in EXPR.native_numeric_types and not isinstance(unscaled_expr.args[0], NumericConstant):
                        # if lhs is not a numeric value (hence it's an expression)
                        scaled_lhs = constraint_scaling_factor*unscaled_expr.args[0]
                    else: 
                        scaled_lhs = NumericConstant(constraint_scaling_factor*unscaled_expr.args[0])
                    if unscaled_expr.args[1].__class__ not in EXPR.native_numeric_types and not isinstance(unscaled_expr.args[1], NumericConstant):
                        # if lhs is not a numeric value (hence it's an expression)
                        scaled_rhs = constraint_scaling_factor*unscaled_expr.args[1]
                    else: 
                        scaled_rhs = NumericConstant(constraint_scaling_factor*unscaled_expr.args[1])
                    # reconstruct expression
                    if isinstance(unscaled_expr, EXPR.InequalityExpression): # if inequality
                        return(EXPR.inequality(scaled_lhs,scaled_rhs))
                    else: # if equality
                        return(scaled_lhs==scaled_rhs)
                else:
                    return unscaled_expr
            else: 
                # if constraint_scaling_factor == 1, return unscaled_expr
                return unscaled_expr
        else:
            # if scaling disabled, return unscaled_expr
            return unscaled_expr

    @staticmethod
    def fix_variables(*args):
        """ This method sets the variables that need to be fixed to 0 at initialization 
        
        By setting the default value to 0, the fixed variables can be identified before explicitly fixing them. 
        If condition is met, function returns 0, otherwise None. This is subsequently set as the variable's default value
        Args:
            *args: 
                args[0](str): string of variable name to identify variable
                args[1](pe.ConcreteModel): ConcreteModel with necessary sets and parameters
                args[2:]: Set indizes which define the current variable index (e.g. year, product, production_process, node etc.) 
        """
        # select items from *args, see variable definition for order of data
        variable_name = args[0]
        model = args[1]
        data = args[2:]
        # get type_of_optimization
        type_of_optimization = model.type_of_optimization.data()[0]
        # fix impact overshoot
        if "overshoot" in variable_name:
            # fix overshoot only if both all impacts fixed and especially operational impact fixed as well
            if config.fix_impact_overshoot:
                if not (not config.fix_operational_impact_overshoot and "operational_impact_overshoot" in variable_name):
                    return 0
                elif data[0] not in model.capped_impacts:
                    return 0
        # used_production
        elif variable_name == 'used_production':
            # year - construction_year >= lifetime
            if ((data[2] - data[3]) >= model.lifetime_duration_production[data[1], data[3]]): 
                return 0       
            # year < construction_year       
            elif data[2] < data[3]:
                return 0
            # existing_capacity == 0 
            elif model.existing_capacity_production[data[0], data[1], data[2], data[3]] == 0:
                # additional fixed variables for InvestmentOptimization
                if type_of_optimization == 'InvestmentOptimization':
                    if (sum([model.existing_capacity_production[data[0], data[1], data[2], construction_year] for construction_year in model.construction_years_production]) >= model.potential_capacity_production[data[0], data[1], data[2]]):
                        # sum(existing capacity) >= potential capacity
                        return 0
                    elif data[3] not in model.years:
                        # construction_year not in invest_year
                        return 0 
                # additional fixed variables for OperationalOptimization
                elif type_of_optimization == 'OperationalOptimization':
                    return 0
                # optimization type unknown
                else:
                    raise NotImplementedError('Not implemented for {}'.format(type_of_optimization))
            elif model.usable_capacity_factor_timeseries_production[data[0],data[1],data[4]] == 0:
                # usable capacity factor timeseries == 0
                return 0
        # used_storage
        elif variable_name == 'used_storage':
            # year - construction_year >= lifetime
            if ((data[3] - data[4]) >= model.lifetime_duration_storage[data[1], data[4]]):
                return 0
            # year < construction_year       
            elif data[3] < data[4]:
                return 0
            elif model.existing_capacity_storage[data[0], data[1], data[3], data[4]] == 0:
                # additional fixed variables for 'InvestmentOptimization'
                if type_of_optimization == 'InvestmentOptimization':   
                    if (sum([model.existing_capacity_storage[data[0], data[1], data[3], construction_year] for construction_year in model.construction_years_storage]) >= model.potential_capacity_storage[data[0], data[1], data[3]]):
                        # sum(existing capacity) >= potential capacity
                        return 0
                    elif data[3] not in model.years:
                        # construction_year not in invest_year
                        return 0 
                # additional fixed variables for OperationalOptimization
                elif type_of_optimization == 'OperationalOptimization':
                    return 0
                else:
                    raise NotImplementedError('Not implemented for {}'.format(type_of_optimization))
        # used_transshipment
        elif variable_name == 'used_transshipment':
            # year - construction_year >= lifetime
            if ((data[3] - data[4]) >= model.lifetime_duration_transshipment[data[1], data[4]]):
                return 0
            # year < construction_year       
            elif data[3] < data[4]:
                return 0
            # existing_capacity == 0
            elif model.existing_capacity_transshipment[data[0], data[1], data[3], data[4]] == 0:
                # additional fixed variables for 'InvestmentOptimization'
                if type_of_optimization == 'InvestmentOptimization':   
                    if (sum([model.existing_capacity_transshipment[data[0], data[1], data[3], construction_year] for construction_year in model.construction_years_transshipment]) >= model.potential_capacity_transshipment[data[0], data[1], data[3]]):
                        # sum(existing capacity) >= potential capacity
                        return 0
                    elif data[3] not in model.years:
                        # construction_year not in invest_year
                        return 0 
                elif type_of_optimization == 'OperationalOptimization':
                    return 0
                else:
                    raise NotImplementedError('Not implemented for {}'.format(type_of_optimization))
        # fixing the transmission phase difference of one node to zero.
        elif variable_name == 'phase_difference':
            # if node == transmission_reference_node
            if data[0] == pe.value(model.transmission_reference_node):
                return 0
        # different Optimization subclasses
        if type_of_optimization == 'InvestmentOptimization':
            # Fixes the new capacity variables for all processes which have no potential left and the used capacity variables which therefore have no existing capacity either.
            if variable_name == 'new_capacity_production':
                # if sum(existing_capacity) >= potential capacity
                if (sum(model.existing_capacity_production[data[0], data[1], data[2], construction_year] for construction_year in model.construction_years_production) >= model.potential_capacity_production[data[0], data[1], data[2]]):
                    return 0
            elif variable_name == 'new_capacity_storage':
                # if sum(existing_capacity) >= potential capacity
                if (sum(model.existing_capacity_storage[data[0], data[1], data[2], construction_year] for construction_year in model.construction_years_storage) >= model.potential_capacity_storage[data[0], data[1], data[2]]):
                    return 0
            elif variable_name == 'new_capacity_transshipment':
                # if sum(existing_capacity) >= potential capacity
                if (sum(model.existing_capacity_transshipment[data[0], data[1], data[2], construction_year] for construction_year in model.construction_years_transshipment) >= model.potential_capacity_transshipment[data[0], data[1], data[2]]):
                    return 0
            elif variable_name == 'new_capacity_transmission':
                # if sum(existing_capacity) >= potential capacity
                if (sum(model.existing_capacity_transmission[data[0], data[1], data[2], construction_year] for construction_year in model.construction_years_transmission) >= model.potential_capacity_transmission[data[0], data[1], data[2]]):
                    return 0
        # if OperationalOptimization, fix new capacities
        elif type_of_optimization == 'OperationalOptimization':
            if variable_name == 'new_capacity_production':
                return 0
            elif variable_name == 'new_capacity_storage':
                return 0
            elif variable_name == 'new_capacity_transshipment':
                return 0
            elif variable_name == 'new_capacity_transmission':
                return 0
        # unknown type of optimization
        else:
            raise NotImplementedError('Not implemented for {}'.format(type_of_optimization))
        # if not fixed, return None
        return None

    def setupSets(self):
        """This method sets up all Sets required the optimization model.

        Some Sets are initialized with default values, if no other values are specified in the input file.
        """
        # pe.Set of the type of optimization
        self.model.type_of_optimization = pe.Set()

        # pe.Set of the nodes of the grid
        self.model.nodes = pe.Set()
        # pe.Set of the connections between the nodes of the grid
        self.model.connections = pe.Set()
        # pe.Set of two nodes to identify both nodes of a connection
        self.model.connection_nodes = pe.Set(
            initialize={"node1", "node2"})
        # pe.Set of the used products for which there is a demand
        self.model.products = pe.Set()
        # pe.Set of the traded products
        self.model.traded_products = pe.Set()
        # pe.Set of the capped impacts
        self.model.capped_impacts = pe.Set()
        # pe.Set of the used processes for production
        self.model.processes_production = pe.Set()
        # pe.Set of the used processes for storage of products
        self.model.processes_storage = pe.Set()
        # pe.Set of the used processes for transshipment of products between nodes
        self.model.processes_transshipment = pe.Set()
        # pe.Set of the used processes for transshipment of products between nodes
        self.model.processes_transmission = pe.Set()
        # pe.Set of storage directions (deposit and withdraw)
        self.model.directions_storage = pe.Set(
            initialize={"deposit", "withdraw"})
        # pe.Set of transshipment direction (forward and backward)
        self.model.directions_transshipment = pe.Set(
            initialize={"forward", "backward"})
        # pe.Set of the properties of a transmission process/power line
        self.model.power_line_property_categories = pe.Set(
            initialize={
                "power limit",
                "susceptance per unit"})
        # pe.Set of products using transmission via DC load flow
        self.model.products_transmission = pe.Set(
            initialize={"electricity"})
        # pe.Set of the years of construction of existing capacity
        self.model.construction_years_production = pe.Set(
            within=pe.NonNegativeIntegers)
        # pe.Set of the years of construction of existing capacity
        self.model.construction_years_storage = pe.Set(
            within=pe.NonNegativeIntegers)
        # pe.Set of the years of construction of existing capacity
        self.model.construction_years_transshipment = pe.Set(
            within=pe.NonNegativeIntegers)
        # pe.Set of the years of construction of existing capacity
        self.model.construction_years_transmission = pe.Set(
            within=pe.NonNegativeIntegers)
        # pe.Set of the years in the current optimization period
        self.model.years = pe.Set(
            within=pe.NonNegativeIntegers)
        # pe.Set of the time slices of a year
        self.model.time_slices = pe.Set(
            within=pe.NonNegativeIntegers)
        # pe.Set of the impact categories, including environmental and monetary costs
        self.model.impact_categories = pe.Set()
        # pe.Set of the impact sources, e.g. whether the impacts result from invest or operation
        self.model.impact_sources = pe.Set(
            initialize={"operation", "invest"})

    def setupParameters(self):
        """This method sets up all Parameters required by the optimization model.

        """
        
        # length of the connections of the grid
        self.model.connection_length = pe.Param(
            self.model.connections,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the length of all connections of the grid.")
        # technology matrices, which define how much of any product is produces by a process
        # technology matrix of production processes like power plant processes
        self.model.technology_matrix_production = pe.Param(
            self.model.products,
            self.model.nodes,
            self.model.processes_production,
            self.model.construction_years_production,
            default=0,
            doc="Parameter which contains the data about which products take part in a production process.")
        # technology matrix of storage processes like pumped hydro storage
        self.model.efficiency_matrix_storage = pe.Param(
            self.model.products,
            self.model.processes_storage,
            self.model.directions_storage,
            self.model.construction_years_storage,
            default=0,
            doc="Parameter which contains the data about which products take part in a storage process.")
        
        # products stored by storage processes
        self.model.storage_products = pe.Param(
            self.model.processes_storage,
            within=self.model.products,
            doc="Parameter which contains the product stored by a storage process.")
        # efficiency of transshipment processes along all connections
        self.model.transshipment_efficiency = pe.Param(
            self.model.processes_transshipment,
            self.model.connections,
            default=0,
            doc="Parameter which contains the efficiency of a transshipment process along a connection.")
        # products which are transshipped by the corresponding transshipment processes
        self.model.transshipment_products = pe.Param(
            self.model.processes_transshipment,
            within = pe.Any,
            default=None,
            doc="Parameter which contains the transshipped product of each transshipment process.")
        # properties of each existing power line, regarding power limit and susceptance per unit
        self.model.power_line_properties = pe.Param(
            self.model.connections,
            self.model.years,
            self.model.power_line_property_categories,
            within=pe.NonNegativeReals,
            default=0,
            doc="Parameter which contains the power limit and susceptance per unit of the existing capacity.")

        # timeseries multiplier of the technology matrix of production processes
        self.model.technology_matrix_production_timeseries = pe.Param(
            self.model.products,
            self.model.processes_production,
            self.model.time_slices,
            default=1,
            doc="Parameter which contains the time series factor for the technology matrix of a production process.")
        
        # impact matrices, which define how big the life cycle impact of any process is - includes monetary costs
        # impact matrix of production processes
        self.model.impact_matrix_production = pe.Param(
            self.model.impact_categories,
            self.model.impact_sources,
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            doc="Parameter which contains the data about the impacts of a production process. \
                First 'year' marks the year for which the value is valid. Second 'year' marks the \
                year of construction.")
        # impact matrix of storage processes
        self.model.impact_matrix_storage = pe.Param(
            self.model.impact_categories,
            self.model.impact_sources,
            self.model.processes_storage,
            self.model.years,
            self.model.construction_years_storage,
            doc="Parameter which contains the data about the impacts of a storage process. \
                Operational impact data is based on the **withdrawn** product. \
                First 'year' marks the year for which the value is valid. Second 'year' marks the \
                year of construction.")
        # impact matrix of transshipment processes
        self.model.impact_matrix_transshipment = pe.Param(
            self.model.impact_categories,
            self.model.impact_sources,
            self.model.processes_transshipment,
            self.model.years,
            self.model.construction_years_transshipment,
            doc="Parameter which contains the data about the impacts of a transshipment process. \
                First 'year' marks the year for which the value is valid. Second 'year' marks the \
                year of construction.")
        # impact matrix of transmssion processes
        self.model.impact_matrix_transmission = pe.Param(
            self.model.impact_categories,
            self.model.impact_sources,
            self.model.processes_transmission,
            self.model.years,
            self.model.construction_years_transmission,
            doc="Parameter which contains the data about the impacts of transmission processes \
                in the transmission grid. First 'year' marks the year for which the value is valid. \
                Second 'year' marks the year of construction.")
        # impact matrix of non-served demand of a product
        self.model.impact_matrix_non_served_demand = pe.Param(
            self.model.impact_categories,
            self.model.products,
            self.model.years,
            self.model.time_slices,
            doc="Parameter which contains the data about the impacts of non-served demand.")
        
        # lifetime duration of production process
        self.model.lifetime_duration_production = pe.Param(
            self.model.processes_production,
            self.model.construction_years_production,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the lifetime duration of each production process\
                in the corresponding construction year.")
        # lifetime duration of storage process
        self.model.lifetime_duration_storage = pe.Param(
            self.model.processes_storage,
            self.model.construction_years_storage,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the lifetime duration of each storage process\
                in the corresponding construction year.")
        # lifetime duration of transshipment process
        self.model.lifetime_duration_transshipment = pe.Param(
            self.model.processes_transshipment,
            self.model.construction_years_transshipment,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the lifetime duration of each production process\
                in the corresponding construction year.")
        # lifetime duration of transmission process
        self.model.lifetime_duration_transmission = pe.Param(
            self.model.processes_transmission,
            self.model.construction_years_transmission,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the lifetime duration of each production process\
                in the corresponding construction year.")
        
        # existing capacity of all technologies
        # installed capacity of production technologies based on nodes and investment years
        self.model.existing_capacity_production = pe.Param(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            within=pe.NonNegativeReals,
            default=0,
            doc="Parameter which contains the data about the existing capacity of a production process. \
                First 'year' marks the year for which the value is valid. Second 'year' marks the \
                year of construction.")
        # installed capacity of transmission technologies based on connections and investment years
        self.model.existing_capacity_transmission = pe.Param(
            self.model.connections,
            self.model.processes_transmission,
            self.model.years,
            self.model.construction_years_transmission,
            default=0,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the existing capacity of a transshipment process. \
                First 'year' marks the year for which the value is valid. Second 'year' marks the \
                year of construction.")
        # product flow to storage capacity factor based on processes and investment years
        self.model.flow_to_storage_capacity_factor = pe.Param(
            self.model.processes_storage,
            self.model.construction_years_storage,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the relation between the maximum input \
                and output of a storage process and its storage capacity.")
        # lists of nodes connected by connections
        self.model.connected_nodes = pe.Param(
            self.model.connections,
            self.model.connection_nodes,
            within = pe.NonNegativeIntegers,
            doc="Parameter which contains the two nodes connected by a connection process.")
        # reference node of which the phase difference is pe.Set to zero
        self.model.transmission_reference_node = pe.Param(
            within=self.model.nodes,
            doc="Parameter which contains one node which is used as reference node for the \
                calculation of the phase difference between the nodes. Therefore, its \
                phase difference is set to zero.")
        # apparent power base value for the per-unit calculations
        self.model.per_unit_base = pe.Param(
            within=pe.NonNegativeReals,
            default=500,
            doc="Parameter which contains the base value in MVA for the per-unit calculations.")
        # time dependent parameters
        # weight of every time slice regarding the full year - sum of all shares equals 8760 hours
        self.model.time_slice_yearly_weight = pe.Param(
            self.model.time_slices,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the share of a year represented by each time slice.")
        # actual duration of one time slice - necessary for storage level calculations
        self.model.time_slice_duration = pe.Param(
            self.model.time_slices,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the duration of each time slice.")
        # next connected time slice of the current time slice, used for storage level boundaries
        self.model.next_time_slice = pe.Param(
            self.model.time_slices,
            doc="Parameter which contains the data about the connections between the time slices.")
        # timeseries for the available share of any capacity e.g. production from renewable energy
        self.model.usable_capacity_factor_timeseries_production = pe.Param(
            self.model.nodes,
            self.model.processes_production,
            self.model.time_slices,
            within=pe.NonNegativeReals,
            default=1,
            doc="Parameter which contains the data about the available share of capacity of a production \
                process in a specific time slice.")
        # demand for every product
        self.model.demand = pe.Param(
            self.model.products,
            self.model.years,
            self.model.time_slices,
            self.model.nodes,
            default=0,
            doc="Parameter which contains the data about the demand for each product at each node.")
        # parameters regarding the impact of the system
        # limits for operational impact
        # nodal limits for the operational impacts
        self.model.operational_nodal_impact_limits = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default=float("inf"),
            doc="Parameter which contains all limits for the operational impacts on nodal base.")
        # overall limits for the operational impacts
        self.model.operational_impact_limits = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default=float("inf"),
            doc="Parameter which contains all limits for the operational impacts overall.")
        # limits for invest impact
        # nodal limits for the invest impacts
        self.model.invest_nodal_impact_limits = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default=float("inf"),
            doc="Parameter which contains all limits for the invest impacts on nodal base.")
        # overall limits for the invest impacts
        self.model.invest_impact_limits = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default=float("inf"),
            doc="Parameter which contains all limits for the invest impacts overall.")
        # limits for total impact
        # nodal limits for the total impacts
        self.model.total_nodal_impact_limits = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default=float("inf"),
            doc="Parameter which contains all limits for the total impacts on nodal base.")
        # overall limits for the total impacts
        self.model.total_impact_limits = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default=float("inf"),
            doc="Parameter which contains all limits for the total impacts overall.")
        # parameters which specify with which weight impacts is taken into account in the pe.Objective rule
        self.model.objective_factor_impacts = pe.Param(
            self.model.impact_sources,
            self.model.impact_categories,
            self.model.years,
            default=0,
            doc="Parameter which defines the factor used to multiply an impact category with in the objective rule.")
        # parameters for the weight of overshoot and non-served demand variables
        # parameters which specify with which weight impact overshoots are taken into account in the pe.Objective rule
        self.model.objective_factor_impact_overshoot = pe.Param(
            self.model.impact_categories,
            self.model.years,
            doc="Parameters which specify with which weight impact overshoots are taken into account in the objective rule.")
        # parameter used for the transshipment in different directions. 
        # Therefore its values need to be 
        self.model.factor_transshipment = pe.Param(
            self.model.directions_transshipment,
            initialize={"forward": 1, "backward": -1},
            doc="Parameter which is used for transshipment equations and does not need further initialization.")
        # additional parameter to be used for the calculation of the impact of storage usage on the storage level
        # therefore its values need to be {"withdraw": -1, "deposit": 1}
        self.model.storage_level_factor = pe.Param(
            self.model.directions_storage,
            initialize={"withdraw": -1, "deposit": 1},
            doc="Parameter which is used for storage equations and does not need further initialization.")
        # capacity limits of all processes on nodal base
        # capacity limits of production processes
        self.model.potential_capacity_production = pe.Param(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            within=pe.NonNegativeReals,
            default=float("inf"),
            doc="Parameter which contains the data about the capacity limit of a production process at each node.")
        # capacity limits of storage processes
        self.model.potential_capacity_storage = pe.Param(
            self.model.nodes,
            self.model.processes_storage,
            self.model.years,
            within=pe.NonNegativeReals,
            default=float("inf"),
            doc="Parameter which contains the data about the capacity limit of a storage process at each node.")
        # capacity limits of transshipment processes
        self.model.potential_capacity_transshipment = pe.Param(
            self.model.connections,
            self.model.processes_transshipment,
            self.model.years,
            within=pe.NonNegativeReals,
            default=float("inf"),
            doc="Parameter which contains the data about the capacity limit of a transshipment process at each node.")
        # capacity limits of transmission processes
        self.model.potential_capacity_transmission = pe.Param(
            self.model.connections,
            self.model.processes_transmission,
            self.model.years,
            within=pe.NonNegativeReals,
            default=float("inf"),
            doc="Parameter which contains the data about the capacity limit of a transmission process at each connection.")
        # power limit per circuit of each transmission process
        self.model.power_limit_per_circuit = pe.Param(
            self.model.processes_transmission,
            default=0,
            doc="Parameter which contains the power limit per circuit of each transmission process.")
        # reliability margin for a transmission process
        self.model.reliability_margin_transmission = pe.Param(
            self.model.processes_transmission,
            within=pe.NonNegativeReals,
            default=0,
            doc="Parameter which contains the data about the share of the power limit of a process that can be used.")
        # parameters necessary for reaching the secured capacity
        self.model.surplus_capacity_factor_production = pe.Param(
            default = 1.01,
            within = pe.NonNegativeReals,
            doc = "Parameter which contains the data about the surplus capacity factor, i.e., the factor by which the capacity of all nodes \
                has to exceed the total demand. Default is 1% (1.01)")
        # parameters necessary for reaching the secured capacity
        # secured capacity factor for production processes
        self.model.secured_capacity_factors_production = pe.Param(
            self.model.processes_production,
            self.model.construction_years_production,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the secured capacity factor of a production process.")
        # secured capacity factor for storage processes
        self.model.secured_capacity_factors_storage = pe.Param(
            self.model.processes_storage,
            self.model.construction_years_storage,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the secured capacity factor of a storage process.")
        # required total secured capacity e.g. peak load
        self.model.required_total_secured_capacity = pe.Param(
            self.model.products,
            self.model.years,
            default=0,
            doc="Parameter which contains the data about the required total secured capacity for a product")
        # required nodal secured capacity
        self.model.required_nodal_secured_capacity = pe.Param(
            self.model.products,
            self.model.nodes,
            self.model.years,
            default=0,
            doc="Parameter which contains the data about the required nodal secured capacity for a product")

    def setupScalingParameters(self):
        """ This functions sets up the scaling parameters of the variables required by the optimization model.

        The notation is the following: The scaling parameter for the component <component> is self.model.scaling_<component> 
        It is crucial to select the same pe.Sets as the corresponding component, to select default = 1, and mutable=True.
        Check setupVariables() for details on the variable declaration """    

        ## set up Parameters for scaling variables
        # slack variables
        self.model.scaling_nodal_slack_variable = pe.Param( 
            self.model.products,
            self.model.nodes,
            self.model.years,
            default = 1,
            mutable = True)
        self.model.scaling_total_slack_variable = pe.Param(
            self.model.products,
            self.model.years,
            default = 1,
            mutable = True)
        # usage of production technologies based on nodes and time
        self.model.scaling_used_production = pe.Param(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # usage of storage technologies based on nodes and time
        self.model.scaling_used_storage = pe.Param(
            self.model.nodes,
            self.model.processes_storage,
            self.model.directions_storage,
            self.model.years,
            self.model.construction_years_storage,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # usage of transshipment technologies based on nodes and time
        self.model.scaling_used_transshipment = pe.Param(
            self.model.connections,
            self.model.processes_transshipment,
            self.model.directions_transshipment,
            self.model.years,
            self.model.construction_years_transshipment,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # non-served demand of a product based on nodes and time
        self.model.scaling_non_served_demand = pe.Param(
            self.model.nodes,
            self.model.products,
            self.model.years,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # storage level of storage technologies based on nodes, years and time slices
        self.model.scaling_storage_level = pe.Param(
            self.model.nodes,
            self.model.processes_storage,
            self.model.years,
            self.model.construction_years_storage,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # DC load flow - transmission related variables
        self.model.scaling_phase_difference = pe.Param(
            self.model.nodes,
            self.model.years,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # transmission power
        self.model.scaling_used_transmission = pe.Param(
            self.model.connections,
            self.model.years,
            self.model.time_slices,
            default = 1,
            mutable = True)
        # impact related variables
        # operational impact
        # nodal operational environmental and monetary impact based on impact categories and years
        self.model.scaling_operational_nodal_impact = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overshoot of nodal operational environmental and monetary impact based on impact categories and years
        self.model.scaling_operational_nodal_impact_overshoot = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overall operational environmental and monetary impact based on impact categories and years
        self.model.scaling_operational_impact = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overshoot of overall operational environmental and monetary impact based on impact categories and years
        self.model.scaling_operational_impact_overshoot = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # invest impact
        # nodal invest environmental and monetary impact based on impact categories and years
        self.model.scaling_invest_nodal_impact = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overshoot of nodal invest environmental and monetary impact based on impact categories and years
        self.model.scaling_invest_nodal_impact_overshoot = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overall invest environmental and monetary impact based on impact categories and years
        self.model.scaling_invest_impact = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overshoot of overall invest environmental and monetary impact based on impact categories and years
        self.model.scaling_invest_impact_overshoot = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # total impact
        # nodal total environmental and monetary impact based on impact categories and years
        self.model.scaling_total_nodal_impact = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overshoot of nodal total environmental and monetary impact based on impact categories and years
        self.model.scaling_total_nodal_impact_overshoot = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overall total environmental and monetary impact based on impact categories and years
        self.model.scaling_total_impact = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # overshoot of overall total environmental and monetary impact based on impact categories and years
        self.model.scaling_total_impact_overshoot = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default = 1,
            mutable = True)
        # newly installed capacity of production technologies based on nodes and investment years
        self.model.scaling_new_capacity_production = pe.Param(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            default = 1,
            mutable = True)
        # newly installed capacity of storage technologies based on nodes and investment years
        self.model.scaling_new_capacity_storage = pe.Param(
            self.model.nodes,
            self.model.processes_storage,
            self.model.years,
            default = 1,
            mutable = True)
        # newly installed capacity of transshipment technologies based on nodes and investment years
        self.model.scaling_new_capacity_transshipment = pe.Param(
            self.model.connections,
            self.model.processes_transshipment,
            self.model.years,
            default = 1,
            mutable = True)
        # newly installed transmission capacity from switching
        self.model.scaling_new_capacity_transmission = pe.Param(
            self.model.connections,
            self.model.processes_transmission,
            self.model.years,
            default = 1,
            mutable = True)

    def setupVariables(self):
        """This method sets up all Variables required by the optimization model.

        """
        # slack variable required for constraint_nodal_secured_capacity
        self.model.nodal_slack_variable = pe.Var( 
            self.model.products,
            self.model.nodes,
            self.model.years,
            within=pe.NonNegativeReals)

        # slack variable required for constraint_total_secured_capacity
        self.model.total_slack_variable = pe.Var(
            self.model.products,
            self.model.years,
            within = pe.NonNegativeReals)

        # usage of production technologies based on nodes and time
        self.model.used_production = pe.Var(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            self.model.time_slices,
            rule = lambda *args: self.fix_variables('used_production',*args),
            within=pe.NonNegativeReals)
        # non-served demand of a product based on nodes and time
        self.model.non_served_demand = pe.Var(
            self.model.nodes,
            self.model.products,
            self.model.years,
            self.model.time_slices,
            within=pe.NonNegativeReals)
        # DC load flow - transmission related variables
        self.model.phase_difference = pe.Var(
            self.model.nodes,
            self.model.years,
            self.model.time_slices,
            rule = lambda *args: self.fix_variables('phase_difference',*args),
            within=pe.Reals)
        # transmission power
        self.model.used_transmission = pe.Var(
            self.model.connections,
            self.model.years,
            self.model.time_slices,
            within=pe.Reals)
        # impact related variables
        # operational impact
        # nodal operational environmental and monetary impact based on impact categories and years
        self.model.operational_nodal_impact = pe.Var(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            within=pe.Reals)
        # overshoot of nodal operational environmental and monetary impact based on impact categories and years
        self.model.operational_nodal_impact_overshoot = pe.Var(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule = lambda *args: self.fix_variables('operational_nodal_impact_overshoot',*args),
            within=pe.NonNegativeReals)
        # overall operational environmental and monetary impact based on impact categories and years
        self.model.operational_impact = pe.Var(
            self.model.impact_categories,
            self.model.years,
            within=pe.Reals)
        # overshoot of overall operational environmental and monetary impact based on impact categories and years
        self.model.operational_impact_overshoot = pe.Var(
            self.model.impact_categories,
            self.model.years,
            rule = lambda *args: self.fix_variables('operational_impact_overshoot',*args),
            within=pe.NonNegativeReals)
        # invest impact
        # nodal invest environmental and monetary impact based on impact categories and years
        self.model.invest_nodal_impact = pe.Var(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            within=pe.Reals)
        # overshoot of nodal invest environmental and monetary impact based on impact categories and years
        self.model.invest_nodal_impact_overshoot = pe.Var(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule = lambda *args: self.fix_variables('invest_nodal_impact_overshoot',*args),
            within=pe.NonNegativeReals)
        # overall invest environmental and monetary impact based on impact categories and years
        self.model.invest_impact = pe.Var(
            self.model.impact_categories,
            self.model.years,
            within=pe.Reals)
        # overshoot of overall invest environmental and monetary impact based on impact categories and years
        self.model.invest_impact_overshoot = pe.Var(
            self.model.impact_categories,
            self.model.years,
            rule = lambda *args: self.fix_variables('invest_impact_overshoot',*args),
            within=pe.NonNegativeReals)
        # total impact
        # nodal total environmental and monetary impact based on impact categories and years
        self.model.total_nodal_impact = pe.Var(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            within=pe.Reals)
        # overshoot of nodal total environmental and monetary impact based on impact categories and years
        self.model.total_nodal_impact_overshoot = pe.Var(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule = lambda *args: self.fix_variables('total_nodal_impact_overshoot',*args),
            within=pe.NonNegativeReals)
        # overall total environmental and monetary impact based on impact categories and years
        self.model.total_impact = pe.Var(
            self.model.impact_categories,
            self.model.years,
            within=pe.Reals)
        # overshoot of overall total environmental and monetary impact based on impact categories and years
        self.model.total_impact_overshoot = pe.Var(
            self.model.impact_categories,
            self.model.years,
            rule = lambda *args: self.fix_variables('total_impact_overshoot',*args),
            within=pe.NonNegativeReals)
        # NEW capacities
        # newly installed capacity of production technologies based on nodes and investment years
        self.model.new_capacity_production = pe.Var(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            rule = lambda *args: self.fix_variables('new_capacity_production',*args),
            within=pe.NonNegativeReals)
        # newly installed transmission capacity from switching
        self.model.new_capacity_transmission = pe.Var(
            self.model.connections,
            self.model.processes_transmission,
            self.model.years,
            rule = lambda *args: self.fix_variables('new_capacity_transmission',*args),
            within=pe.NonNegativeReals)

    def setupConstraints(self):
        """This method sets up all Constraints required by the optimization model.

        """
        # declaring the balance equation for all products used in the optimization
        self.model.constraint_product_balance = pe.Constraint(
            self.model.nodes,
            self.model.products,
            self.model.years,
            self.model.time_slices,
            rule=self.constraint_product_balance_rule)
        # declaring the limit of production, limited by the capacity
        self.model.constraint_production_limit_by_capacity = pe.Constraint(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            self.model.time_slices,
            rule=self.constraint_production_limit_by_capacity_rule)
        # constraints for DC load flow modelling of transmission
        # calculating the used_transmission from the phase difference
        self.model.constraint_used_transmission_from_phase_difference = pe.Constraint(
            self.model.connections,
            self.model.years,
            self.model.time_slices,
            rule = self.constraint_used_transmission_from_phase_difference_rule)        
            # declaring the limit of positive transmission, limited by the capacity
        self.model.constraint_transmission_limit_by_capacity_positive = pe.Constraint(
            self.model.connections,
            self.model.years,
            self.model.time_slices,
            rule=self.constraint_transmission_limit_by_capacity_positive_rule)
        # declaring the limit of negative transmission, limited by the capacity
        self.model.constraint_transmission_limit_by_capacity_negative = pe.Constraint(
            self.model.connections,
            self.model.years,
            self.model.time_slices,
            rule=self.constraint_transmission_limit_by_capacity_negative_rule)
        # impact calculation and limitation
        # operational impacts nodal based
        # declaring the calculation of all operational impact categories,
        # summed over all processes on nodal base
        self.model.constraint_operational_nodal_impact = pe.Constraint(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_operational_nodal_impact_rule)
        # declaring the calculation of all operational impact categories,
        # summed over all processes on nodal base
        self.model.constraint_operational_nodal_impact_limits = pe.Constraint(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_operational_nodal_impact_limits_rule)
        # operational impacts over all nodes
        # declaring the calculation of all operational impact categories,
        # summed over all processes
        self.model.constraint_operational_impact = pe.Constraint(
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_operational_impact_rule)
        # declaring the calculation of all operational impact categories,
        # summed over all processes
        self.model.constraint_operational_impact_limits = pe.Constraint(
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_operational_impact_limits_rule)
        # invest impacts nodal based
        # declaring the calculation of all invest impact categories,
        # summed over all processes on nodal base
        self.model.constraint_invest_nodal_impact = pe.Constraint(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_invest_nodal_impact_rule)
        # declaring the calculation of all invest impact categories,
        # summed over all processes on nodal base
        self.model.constraint_invest_nodal_impact_limits = pe.Constraint(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_invest_nodal_impact_limits_rule)
        # invest impacts over all nodes
        # declaring the calculation of all invest impact categories,
        # summed over all processes
        self.model.constraint_invest_impact = pe.Constraint(
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_invest_impact_rule)
        # declaring the calculation of all invest impact categories,
        # summed over all processes
        self.model.constraint_invest_impact_limits = pe.Constraint(
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_invest_impact_limits_rule)
        # total impacts nodal based
        # declaring the calculation of all impact categories,
        # summed over all processes on nodal base
        self.model.constraint_total_nodal_impact = pe.Constraint(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_total_nodal_impact_rule)
        # declaring the limitation of all impact categories on nodal scale
        self.model.constraint_total_nodal_impact_limits = pe.Constraint(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_total_nodal_impact_limits_rule)
        # total impacts over all nodes
        # declaring the calculation of all impact categories,
        # summed over all processes and nodes
        self.model.constraint_total_impact = pe.Constraint(
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_total_impact_rule)
        # declaring the limitation of all impact categories on global scale
        self.model.constraint_total_impact_limits = pe.Constraint(
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_total_impact_limits_rule)
        # declaring the limit of production capacity, limited by the provided input
        self.model.constraint_production_potential_capacity_by_input = pe.Constraint(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            rule=self.constraint_production_potential_capacity_by_input_rule)
        # declaring the limit of transmission capacity, limited by the provided input
        self.model.constraint_transmission_potential_capacity_by_input_rule = pe.Constraint(
            self.model.connections,
            self.model.processes_transmission,
            self.model.years,
            rule=self.constraint_transmission_potential_capacity_by_input_rule_rule)
        # declaring the necessity of providing the secured capacity
        self.model.constraint_total_secured_capacity = pe.Constraint(
            self.model.products,
            self.model.years,
            rule=self.constraint_total_secured_capacity_rule)
        # declaring the necessity of providing the secured capacity
        self.model.constraint_nodal_secured_capacity = pe.Constraint(
            self.model.nodes,
            self.model.products,
            self.model.years,
            rule=self.constraint_nodal_secured_capacity_rule)

    def setupObjective(self):
        """This method declares the pe.Objective required by the optimization model."""

        self.model.Objective = pe.Objective(rule=self.objectiveRule)        

    def scale_model_variables(self):
        """ scales model variables, which are in Optimization.variable_dict """
        logging.info('Scale Variables')
        if config.debug_optimization:
            _time_start = datetime.now()
        variable_dict = getattr(Optimization,'variable_dict')
        # iterate through variables in model_instance
        for variable in self.model_instance.component_objects(pe.Var, active =True):
            for data in variable:
                try: 
                    # try to retrieve coefficient of variable from variable_dict
                    variable_scaling_factor = self.calculate_scaling_factor(variable_dict[id(variable[data])])
                    # if scaling factor exists
                    if variable_scaling_factor:
                        # overwrite scaling_<variable> with scaling factor
                        self.model_instance.find_component('scaling_'+str(variable.name))[data] = variable_scaling_factor
                # Tries to retrieve coefficient from variable_dict. Passes to next iterate, if retrievement fails 
                except KeyError:
                    pass
        # log time to scale variables
        if config.debug_optimization:
            time_delta_1 = datetime.now() -_time_start
            logging.info('Scale Variables: {:,.2f} s for {:,d} variables - {:,.2f} ms/variable'.format(time_delta_1.total_seconds(),len(variable_dict),time_delta_1.total_seconds()*1000/len(variable_dict)))

    def retrieve_small_bounds_and_calculate_numerics(self):
        """ retrieves and prints bound that are smaller than eps_bound. 
        Furthermore set coefficients for numerics of scaled model and calculate matrix properties        
        """
        # set threshold to print coefficients 
        if 'eps_bound' in config.scaling_options:
            eps_bound = config.scaling_options['eps_bound']
        else:
            eps_bound = 1e-8

        small_bound_list = []
        for cons in self.model_instance.component_objects(pe.Constraint,active=True):
            cons_name = cons.name
            for data in cons:
                # calculate bound
                const_lhs = 0
                for item in EXPR.decompose_term(cons[data].body)[1]:
                    if item[1] is None:
                        const_lhs += item[0]
                    elif item[1].value != 0:
                        # retrieves scaled coefficients
                        Optimization.set_coefficients_for_numerics(item, scaled_model=True,constraint_name = cons_name)
                # check if 0 < bound < eps_bound
                if cons[data].has_ub():
                    const = cons[data].upper - const_lhs
                elif cons[data].has_lb():
                    const = cons[data].lower - const_lhs
                if abs(const) < eps_bound and abs(const) >0:
                    small_bound_list.append((cons[data].name, const)) 
        # if bound < eps_bound exists, print bounds
        if len(small_bound_list) > 0:
            print("Constraints with non-zero bounds lower than {:.0e}:".format(eps_bound))
            for small_bound in small_bound_list:
                print('{}: {:.3e}'.format(small_bound[0],small_bound[1]))
        # calculate norm and log
        logging.info('Numerical Properties of Optimization Problem')
        Optimization.calculate_matrix_properties()

    def instantiate_model(self, input_dict: dict = None, filepath: str = None, skip_instantiation: bool = False):
        """ This method creates a model instance, fixes variables, and, if selected, analyzes numerics 
        
        Args:
            input_dict(dict): input dictionary
            filepath(str): filepath to input dictionary 
            skip_instantiation(bool): bool if instantiation is skipped. Default is False
        """
        if config.debug_optimization:
            _time_start = datetime.now()
        if not skip_instantiation:
            # create instance
            if input_dict:
                self.model_instance = self.model.create_instance(data=input_dict)
                self.model_instance.dual = pe.Suffix(direction=pe.Suffix.IMPORT)
            elif filepath:
                self.model_instance = self.model.create_instance(filename=filepath)
            else:
                raise AttributeError("Please provide input data either as input dictionary for pyomo or as filepath to an input file.")
            # log skipped constraints
            logging.info("{} out of {} constraints skipped (trivially feasible) - {:,.2f} %".format(Optimization._num_constraints['skipped'],Optimization._num_constraints['skipped']+Optimization._num_constraints['evaluated'],Optimization._num_constraints['skipped']/(Optimization._num_constraints['skipped']+Optimization._num_constraints['evaluated'])*100))
            # fix unneeded_variables
            self.fix_unneeded_variables()       
            if config.fix_slack_variables:
                # if true, fix the non served demand of the products specified in config.fix_products to 0 to prevent using them as slack variable
                self.fix_slack_variables(fix_products=config.fix_products)
            # if config.fix_impact_overshoot_after_initialization:
            #     # if true, fix the overshoot variables to zero, so impact limits must be obeyed
            #     self.fix_impact_overshoot()
        # scale variables
        if config.scaling_options['scale_variables']:
            self.scale_model_variables()
        if config.debug_optimization:
            logging.info('Instantiation of Model Instance took {:,.2f}s.'.format((datetime.now()-_time_start).total_seconds()))
        if config.scaling_options['analyze_numerics']:
            self.retrieve_small_bounds_and_calculate_numerics()

    def correct_variable_values_by_scaling_value(self):
        """ This method multiplies the variable values with its correspondent scaling values """
        for variable in self.model_instance.component_objects(pe.Var):
            for data in variable:
                if variable[data].value and variable[data].value != 0:
                    variable[data].value *= pe.value(self.model_instance.find_component("scaling_"+variable.name)[data])

    def run(self, input_dict: dict = None, filepath: str = None, solver: str = 'glpk', solver_options: dict = None, debug: bool = False, skip_instantiation: bool = False, b_fix_node: bool = False):
        """This method is used to start an optimization.

        To start an optimization it is necessary to provide a file with the necessary input data.
        This file includes all information required to instantiate the optimization problem.

        Args:
            input_dict (dict): Input dictionary that contains all the necessary data for the optimization
            filename (str): Filename of the previously generated input file
            solve (str): Name of the solve to be used. Standard is 'gurobi'
            solver_options (dict): dict with options directly forwarded to the solver
            debug (bool): bool if solver is debugged. Default is False
            skip_instantiation (bool): bool if instantiation is skipped. Default is False
        """

        logging.info("Instantiate optimization")
        # log scaling type
        if not config.scaling_options['scale_constraints'] and not config.scaling_options['scale_variables']:
            logging.info('Scaling disabled')
        else:
            if not config.scaling_options['scale_variables']:
                logging.info('Row (Constraint) scaling enabled')
            elif not config.scaling_options['scale_constraints']:
                logging.info('Column (Variable) scaling enabled')
            else:
                logging.info('Row (Constraint) and Column (Variable) scaling enabled')
        self.mysolver = pe.SolverFactory(solver, options=solver_options)
        self.instantiate_model(input_dict,filepath,skip_instantiation)
        # after last node, fix new_capacity_production to predicted value and optimize again to retrieve objectives for each node
        if b_fix_node:
            for data in self.model_instance.new_capacity_production:
                self.model_instance.new_capacity_production[data].fix(input_dict[None]["predicted_new_capacity_production"][data])
        
        # delete value init rule of variables, because it can not be pickled into the result file
        for variable in self.model_instance.component_objects(pe.Var, active =True):
            variable._value_init_rule = None
        if (solver == "gurobi_persistent"):
            self.mysolver.set_instance(self.model_instance, symbolic_solver_labels=True)
            self.results = self.mysolver.solve(tee=debug)
            # write kappa
            logging.info('Condition Number Kappa = {:.2e}'.format(self.mysolver._solver_model.Kappa_exact))
        else:
            self.results = self.mysolver.solve(self.model_instance, tee=debug)
        logging.info("Termination condition is {}".format(self.results.solver.termination_condition))
        if self.results.solver.termination_condition == getattr(TerminationCondition,'optimal'):
            self.model_instance.solutions.load_from(self.results)
            # self.correct_variable_values_by_scaling_value()
       
    def fix_unneeded_variables(self):
        """Fixes variables to 0. The variables have to been labeled as to be fixed in self.fix_variables 
        Here, we iterate through each variable and check if the default value is 0. If yes, the variable is fixed
        """

        logging.info("Fix unneeded variables")
        number_of_fixed_variables = 0
        number_of_variables = 0
        # iterate through variables in model
        for variable in self.model_instance.component_objects(pe.Var,active=True):
            for data in variable:
                number_of_variables += 1
                # if variable was set to be fixed in construction
                if variable[data].value == 0:
                    variable[data].fix(0)
                    number_of_fixed_variables += 1
        logging.info("{} of {} variables fixed in Optimization - {:,.2f} %".format(number_of_fixed_variables, number_of_variables, number_of_fixed_variables / number_of_variables * 100))
    
    def fix_slack_variables(self,fix_products):
        """
        Fixes the non_served_demand of specified products( e.g. heat, electricity, and mobility) to 0. 
        Although the import of these products may be undesired in some models, 
        their import is usually allowed at arbitrarily high environmental or financial impacts to keep the optimization feasible.
        By fixing the non-served-demand to 0, these variables are no longer used as slack variables and their import is strictly prevented
        """
        logging.info("Fix slack variables")
        number_of_variables = 0
        # iterate through variables in model    
        for variable in self.model_instance.component_objects(pe.Var,active=True):
            if variable.name=='non_served_demand':
                for data in variable:
                    if data[1] in fix_products:
                        variable[data].fix(0)
                        number_of_variables += 1
        logging.info("{} slack variables in non_served_demand were fixed.".format(number_of_variables))

    def fix_impact_overshoot(self):
        """
        Fixes the impact overshoot variables, thus impact limits must be obeyed
        """
        logging.info("Fix impact overshoot variables")
        # iterate through variables in model    
        for variable in self.model_instance.component_objects(pe.Var,active=True):
            if "overshoot" in variable.name:
                for data in variable:
                    variable[data].fix(0)
        logging.info("Overshoot variables were fixed.")

    # METHODS #
    # Evaluation methods
    def get_new_capacity_from_result(self):
        """Return a dictionary of dataframes with all new capacities from the optimization."""

        # Create empty dictionary
        new_capacity_categories = ["production", "storage", "transshipment", "transmission"]
        unstack = {"production": [2], "storage": [2], "transshipment": [2], "transmission": [2]}
        index = {"production": ["node", "process"], "storage": ["node", "process"], "transshipment": ["connection", "process"], "transmission": ["connection", "process"]}
        new_capacity = {}
        for category in new_capacity_categories:
            # currently only production implemented
            if category ==  "production":
                new_capacity[category] = self.get_dataframe_from_result(getattr(self.model_instance, "new_capacity_" + category),getattr(self.model_instance, "scaling_new_capacity_" + category), unstack=unstack[category])
                new_capacity[category].index.names = index[category]

        return new_capacity
        
    @staticmethod
    def get_dataframe_from_result(model_instance_variable, variable_scaling_factors, unstack: list = None):
        """Returns a dataframe with the results from the optimization model."""

        # Create empty dictionary
        variable = {}
        # for all indices in the model_instance_variable put its key-value pair in the dictionary
        for index in model_instance_variable:
            variable[index] = pe.value(model_instance_variable[index])*pe.value(variable_scaling_factors[index])
        
        # Create pandas Series from 
        variable = pd.Series(variable)
        if unstack:
            variable = variable.unstack(unstack)

        return variable
    
    @staticmethod
    def get_dataframe_from_parameter(model_instance_variable, unstack: list = None):
        """Returns a dataframe with the results from the optimization model."""

        # Create empty dictionary
        variable = {}
        # for all indices in the model_instance_variable put its key-value pair in the dictionary
        for index in model_instance_variable:
            variable[index] = model_instance_variable[index]
        
        # Create pandas Series from 
        variable = pd.Series(variable)
        if unstack:
            variable = variable.unstack(unstack)

        return variable

    # RULES FOR pe.Objective AND CONSTRAINTS #
    # CONSTRAINTS # methods used as rules for constraints
    @staticmethod
    def constraint_product_balance_rule(model, node: int, product: str, year: int, time_slice: str) -> "pyomo rule":
        """This method is used as rule for the product balance equation.

        This expression represents the product balance equation which is unique for each node, product, year and time slice.
        It can be summarized as::

            Production + Transshipment + Storage + Transmission >= Demand
        
        Therefore it takes the named above as parameters.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            node (int): A node at which the pe.Constraint is active
            product (str): A product at which is constrained
            year (int): A year at which the pe.Constraint is active
            time_slice (str): A time_slice at which the pe.Constraint is active
        """
        Optimization.add_evaluated_constraint()
        # returns the expression which is the product balance equation
        return Optimization.scale_expr(
            # Sets the demand for a product at a node in a specific year and time slice equal to
            # the sum of all process contributions to that product balance
            # -float(model.time_slice_yearly_weight[time_slice])
            -model.demand[product, year, time_slice, node] ==
            # sum of the production of a product by all used production processes
            # -float(model.time_slice_yearly_weight[time_slice])*
            -(sum(
                # sum over all construction years to differentiate between the used capacities
                sum(model.technology_matrix_production[product,node, process_production, year_construction]
                    * model.used_production[node, process_production, year, year_construction, time_slice]*model.scaling_used_production[node, process_production, year, year_construction, time_slice]
                    for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_production[process_production, year_construction]), model.construction_years_production))
                for process_production in model.processes_production)

            # adding the power from DC load flow transmission, if the product is electricity
            + ( # Sum of the power from all connections which have this node as first node
                - sum(
                    model.used_transmission[connection,year,time_slice]*model.scaling_used_transmission[connection,year,time_slice]
                    for connection in filter(lambda connection: (model.nodes[model.connected_nodes[connection, "node1"]] == node) and (product in model.products_transmission), model.connections))
                # Sum of the power from all connections which have this node as second node
                + sum(
                    model.used_transmission[connection,year,time_slice]*model.scaling_used_transmission[connection,year,time_slice]
                    for connection in filter(lambda connection: (model.nodes[model.connected_nodes[connection, "node2"]] == node) and (product in model.products_transmission), model.connections))
            )
            # variable for non-served demand to avoid infeasibilities
            + model.non_served_demand[node, product, year, time_slice]*model.scaling_non_served_demand[node, product, year, time_slice]
            )
        )

    @staticmethod  
    def constraint_used_transmission_from_phase_difference_rule(model, connection: int, year: int, time_slice: int) -> "pyomo rule":
        """ This method calculates the transmission power from the phase difference. 
        This is mainly done to decrease the numerical effort because the susceptance and unit base are high, 
        which increases the coefficients in the other constraints. 

        Args:
            model: The equivalent of "self" in pyomo optimization models
            connection (int): A power line 
            year (int): A year at which the pe.Constraint is active
            time_slice (str): A time_slice at which the pe.Constraint is active
        """
        
        Optimization.add_evaluated_constraint()
        return Optimization.scale_expr(
            model.used_transmission[connection,year,time_slice]*model.scaling_used_transmission[connection,year,time_slice] == 
            model.power_line_properties[connection, year, "susceptance per unit"]
            * (model.phase_difference[model.nodes[model.connected_nodes[connection, "node1"]], year, time_slice]*model.scaling_phase_difference[model.nodes[model.connected_nodes[connection, "node1"]], year, time_slice]
            - model.phase_difference[model.nodes[model.connected_nodes[connection, "node2"]], year, time_slice]*model.scaling_phase_difference[model.nodes[model.connected_nodes[connection, "node2"]], year, time_slice])*model.per_unit_base
        )
        
    @staticmethod
    def constraint_production_limit_by_capacity_rule(model, node: int, process_production: str, year: int, year_construction: int, time_slice: str) -> "pyomo rule":
        """This method is used as rule for limitting the production to the sum of existing and new capacity.

        The rule is applied to every year of operation AND every year of construction.
        This means that for every year the capacity of existing infrastructure is used as limit,
        creating a constraint for every year of construction as well. This way the operation can
        differentiate between older and newer capacities which might have different impacts.
        Additionally for the years of construction which also are investment years, 
        the newly build capacities are added to the limit.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            node (int): A node at which the pe.Constraint is active
            process_production (str): A production process which is constrained
            year (int): A year at which the pe.Constraint is active
            year_construction (int): The year of construction of a specific capacity
            time_slice (str): A time_slice at which the pe.Constraint is active
        """
        if not model.used_production[node, process_production, year, year_construction, time_slice].value == 0:
            Optimization.add_evaluated_constraint()
            if year_construction not in model.years:
                return Optimization.scale_expr(
                    model.used_production[node, process_production, year, year_construction, time_slice]*model.scaling_used_production[node, process_production, year, year_construction, time_slice]
                    <= model.existing_capacity_production[node, process_production, year, year_construction]
                        * model.usable_capacity_factor_timeseries_production[node, process_production, time_slice]
                )
            else:
                return Optimization.scale_expr(
                    model.used_production[node, process_production, year, year_construction, time_slice]*model.scaling_used_production[node, process_production, year, year_construction, time_slice]
                    <= (model.existing_capacity_production[node, process_production, year, year_construction]
                        + model.new_capacity_production[node, process_production, year_construction]*model.scaling_new_capacity_production[node, process_production, year_construction])
                        * model.usable_capacity_factor_timeseries_production[node, process_production, time_slice]
                )
        else:
            Optimization.add_skipped_constraint()
            return pe.Constraint.Skip

    @staticmethod
    def constraint_transmission_limit_by_capacity_positive_rule(model, connection: int, year: int, time_slice: str) -> "pyomo rule":
        """This method is used as rule for limiting the load flow through a transmission power line.

        The load flow needs to be limited by the available capacity, which is why this abstract rule
        exists and is used for a constraint in the base class for all optimizations.
        Although the available capacity can be defined differently, e.g.::

            load flow <= installed capacity

        or::

            load flow <= installed capacity + capacity from switching + capacity from new power lines

        Because of this, the abstract pe.Constraint rule needs to be defined in a subclass.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            connection (int): A power line which is constrained
            year (int): A year at which the pe.Constraint is active
            time_slice (str): A time_slice at which the pe.Constraint is active
        """
        Optimization.add_evaluated_constraint()
        return Optimization.scale_expr(
            # Positive power flow through connection
            model.used_transmission[connection,year,time_slice]*model.scaling_used_transmission[connection,year,time_slice]
            # has to be smaller or equal than the power limit of the existing capacity of that connection
            <= (model.power_line_properties[connection, year, "power limit"]
            # adding the sum of the new power limit capacity from all transmission processes
            + sum(
                # sum over all invest years which are earlier or equal to the current invest years
                sum(
                    # new installed circuits of each transmission technology
                    model.new_capacity_transmission[connection, process_transmission, earlier_year]*model.scaling_new_capacity_transmission[connection, process_transmission, earlier_year]
                    # multiplied by their power limit per circuit
                    * model.power_limit_per_circuit[process_transmission]
                    # multiplied by 1 minus the safety margin
                    * (1 - model.reliability_margin_transmission[process_transmission])
                    for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year <= model.lifetime_duration_transmission[process_transmission, earlier_year]), model.years))
                for process_transmission in model.processes_transmission))
        )

    @staticmethod
    def constraint_transmission_limit_by_capacity_negative_rule(model, connection: int, year: int, time_slice: str) -> "pyomo rule":
        """This method is used as rule for limiting the load flow through a transmission power line.

        The load flow needs to be limited by the available capacity, which is why this abstract rule
        exists and is used for a constraint in the base class for all optimizations.
        Although the available capacity can be defined differently, e.g.::

            load flow <= installed capacity

        or::

            load flow <= installed capacity + capacity from switching + capacity from new power lines

        Because of this, the abstract pe.Constraint rule needs to be defined in a subclass.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            power_line (int): A power line which is constrained
            year (int): A year at which the pe.Constraint is active
            time_slice (str): A time_slice at which the pe.Constraint is active
        """
        Optimization.add_evaluated_constraint()
        return Optimization.scale_expr(
            # Negative power flow through connection
            - model.used_transmission[connection,year,time_slice]*model.scaling_used_transmission[connection,year,time_slice]
            # has to be smaller or equal than the power limit of the existing capacity of that connection
            <= (model.power_line_properties[connection, year, "power limit"]
            # adding the sum of the new power limit capacity from all transmission processes
            + sum(
                # sum over all invest years which are earlier or equal to the current invest years
                sum(
                    # new installed circuits of each transmission technology
                    model.new_capacity_transmission[connection, process_transmission, earlier_year]*model.scaling_new_capacity_transmission[connection, process_transmission, earlier_year]
                    # multiplied by their power limit per circuit
                    * model.power_limit_per_circuit[process_transmission]
                    # multiplied by 1 minus the safety margin
                    * (1 - model.reliability_margin_transmission[process_transmission])
                    for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year <= model.lifetime_duration_transmission[process_transmission, earlier_year]), model.years))
                for process_transmission in model.processes_transmission))
        )          

    # constraints for impacts
    # nodal pe.Constraint rules for the operational impact
    @staticmethod
    def constraint_operational_nodal_impact_rule(model, node: int, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the calculation of the operational impact vector summed over all processes.

        The operational impact of one node over all time slices is calculated in this method.
        By summing over the construction years of all capacities, the corresponding impact matrices from
        the actual construction year can be used. Furthermore the impact calculated for each time slice
        is multiplied with the weight in hours of a year the time slice has.

        The operational impact of production processes equals the sum of the products of process usage and
        the corresponding impact matrix. The same method is used for calculating the impact of transshipment
        processes, but the resulting impact is shared equally between the corresponding nodes.

        The operational impact of storage processes is calculated based on the **withdrawn** product flow.

        For the electric transmission grid it is assumed that the operational impact is negligible.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            node (int): A node at which the pe.Constraint is active
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """
        Optimization.add_evaluated_constraint()
        return Optimization.scale_expr(
            # calculate the operational nodal impact
            model.operational_nodal_impact[node, impact_category, year]*model.scaling_operational_nodal_impact[node, impact_category, year] 
            ==
            # sum over all time slices of the year
            sum(
                (
                    # Sum over the impacts of all production processes
                    sum(
                        # sum over the construction years of the production process
                        sum(model.used_production[node, process_production, year, year_construction, time_slice]*model.scaling_used_production[node, process_production, year, year_construction, time_slice]
                            * model.impact_matrix_production[impact_category, 'operation', node,process_production, year, year_construction]
                            for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_production[process_production, year_construction]), model.construction_years_production))    
                        for process_production in model.processes_production)
                        
                    # adding the sum of all non-served demands multiplied by the corresponding impacts
                    # sum over all products
                    + sum(model.non_served_demand[node, product, year, time_slice]*model.scaling_non_served_demand[node, product, year, time_slice]
                        * model.impact_matrix_non_served_demand[impact_category, product, year, time_slice]
                        for product in model.products)
                )
                # multiply the sum of all impacts with the weight of a time slice regarding the full year
                * model.time_slice_yearly_weight[time_slice]
                for time_slice in model.time_slices)
        )

    @staticmethod
    def constraint_operational_nodal_impact_limits_rule(model, node: int, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the limitation of the operational impact vector summed over all processes

        Args:
            model: The equivalent of "self" in pyomo optimization models
            node (int): A node at which the pe.Constraint is active
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """
        if model.operational_nodal_impact_limits[node, impact_category, year] != np.inf:
            Optimization.add_evaluated_constraint()
            return Optimization.scale_expr(
                # limit nodal operational impact
                model.operational_nodal_impact[node, impact_category, year]*model.scaling_operational_nodal_impact[node, impact_category, year] <=
                (model.operational_nodal_impact_limits[node, impact_category, year]
                # adding an overshoot variable mainly for debugging infeasibility issues
                + model.operational_nodal_impact_overshoot[node, impact_category, year]*model.scaling_operational_nodal_impact_overshoot[node, impact_category, year])
            )
        else:
            Optimization.add_skipped_constraint()
            return pe.Constraint.Skip

    # overall pe.Constraint rules for the operational impact
    @staticmethod
    def constraint_operational_impact_rule(model, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the calculation of the operational impact vector summed over all processes and nodes

        Args:
            model: The equivalent of "self" in pyomo optimization models
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """
        Optimization.add_evaluated_constraint()
        return Optimization.scale_expr(
            model.operational_impact[impact_category, year]*model.scaling_operational_impact[impact_category, year] ==
            sum(model.operational_nodal_impact[node, impact_category, year]*model.scaling_operational_nodal_impact[node, impact_category, year]
                for node in model.nodes)
        )

    @staticmethod
    def constraint_operational_impact_limits_rule(model, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the limitation of the operational impact vector summed over all processes and nodes

        Args:
            model: The equivalent of "self" in pyomo optimization models
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """
        if model.operational_impact_limits[impact_category, year] != np.inf:
            Optimization.add_evaluated_constraint()
            return Optimization.scale_expr(
                # limit overall operational impact
                model.operational_impact[impact_category, year]*model.scaling_operational_impact[impact_category, year] <=
                (model.operational_impact_limits[impact_category, year]
                # adding an overshoot variable mainly for debugging feasibility issues
                + model.operational_impact_overshoot[impact_category, year]*model.scaling_operational_impact_overshoot[impact_category, year])
            )
        else:
            Optimization.add_skipped_constraint()
            return pe.Constraint.Skip

    # pe.Constraint rules for the invest impact
    # nodal pe.Constraint rules for the invest impact
    @staticmethod
    def constraint_invest_nodal_impact_rule(model, node: int, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the calculation of the invest impact vector
        summed over all processes.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """
        Optimization.add_evaluated_constraint()
        return Optimization.scale_expr(
            # calculate the invest nodal impact
            model.invest_nodal_impact[node, impact_category, year]*model.scaling_invest_nodal_impact[node, impact_category, year] ==
            
                # existing infrastructure
                # Sum over the impacts of all production processes
                (sum(
                    # sum over the construction years of the production process
                    sum(model.existing_capacity_production[node, process_production, year, year_construction]
                        * model.impact_matrix_production[impact_category, 'invest',node, process_production, year, year_construction]
                        for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_production[process_production, year_construction]), model.construction_years_production))    
                    for process_production in model.processes_production)

                # adding NEW CAPACITIES multiplied with the corresponding impact
                # sum over all invest years until the current year
                # Sum over the impacts of all production processes
                + sum(
                    # sum over the construction years of the production process
                    sum(model.new_capacity_production[node, process_production, earlier_year]*model.scaling_new_capacity_production[node, process_production, earlier_year]
                        * model.impact_matrix_production[impact_category, 'invest', node, process_production, year, earlier_year]
                        for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year <= model.lifetime_duration_production[process_production, earlier_year]), model.years))    
                    for process_production in model.processes_production)
            )
        )

    @staticmethod
    def constraint_invest_nodal_impact_limits_rule(model, node: int, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the limitation of the invest impact vector summed over all processes

        Args:
            model: The equivalent of "self" in pyomo optimization models
            node (int): A node at which the pe.Constraint is active
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """
        if model.invest_nodal_impact_limits[node, impact_category, year] != np.inf:
            Optimization.add_evaluated_constraint()
            return Optimization.scale_expr(
                # limit nodal invest impact
                model.invest_nodal_impact[node, impact_category, year]*model.scaling_invest_nodal_impact[node, impact_category, year] <=
                (model.invest_nodal_impact_limits[node, impact_category, year]
                # adding an overshoot variable mainly for debugging feasibility issues
                + model.invest_nodal_impact_overshoot[node, impact_category, year]*model.scaling_invest_nodal_impact_overshoot[node, impact_category, year])
            )
        else:
            Optimization.add_skipped_constraint() 
            return pe.Constraint.Skip

    # overall pe.Constraint rules for the invest impact
    @staticmethod
    def constraint_invest_impact_rule(model, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the calculation of the invest impact vector summed over all processes and nodes

        Args:
            model: The equivalent of "self" in pyomo optimization models
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """
        Optimization.add_evaluated_constraint()
        return Optimization.scale_expr(
            model.invest_impact[impact_category, year]*model.scaling_invest_impact[impact_category, year] ==
            sum(model.invest_nodal_impact[node, impact_category, year]*model.scaling_invest_nodal_impact[node, impact_category, year]
                for node in model.nodes)
        )

    @staticmethod
    def constraint_invest_impact_limits_rule(model, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the limitation of the invest impact vector summed over all processes and nodes

        Args:
            model: The equivalent of "self" in pyomo optimization models
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """
        if model.invest_impact_limits[impact_category, year] != np.inf:
            Optimization.add_evaluated_constraint()
            return Optimization.scale_expr(
                # limit overall operational impact
                model.invest_impact[impact_category, year]*model.scaling_invest_impact[impact_category, year] <=
                model.invest_impact_limits[impact_category, year]
                # adding an overshoot variable mainly for debugging feasibility issues
                + model.invest_impact_overshoot[impact_category, year]*model.scaling_invest_impact_overshoot[impact_category, year]
            )
        else:
            Optimization.add_skipped_constraint()
            return pe.Constraint.Skip

    # pe.Constraint rules for the total impact
    # nodal pe.Constraint rules for the total impact
    @staticmethod
    def constraint_total_nodal_impact_rule(model, node: int, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the calculation of the total impact vector summed over all processes

        Args:
            model: The equivalent of "self" in pyomo optimization models
            node (int): A node at which the pe.Constraint is active
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """
        Optimization.add_evaluated_constraint()
        return Optimization.scale_expr(
            model.total_nodal_impact[node, impact_category, year]*model.scaling_total_nodal_impact[node, impact_category, year] ==
            model.operational_nodal_impact[node, impact_category, year]*model.scaling_operational_nodal_impact[node, impact_category, year]
            + model.invest_nodal_impact[node, impact_category, year]*model.scaling_invest_nodal_impact[node, impact_category, year]
        )

    @staticmethod
    def constraint_total_nodal_impact_limits_rule(model, node: int, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the limitation of the total impact vector summed over all processes

        Args:
            model: The equivalent of "self" in pyomo optimization models
            node (int): A node at which the pe.Constraint is active
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """
        if model.total_nodal_impact_limits[node, impact_category, year] != np.inf:
            Optimization.add_evaluated_constraint()
            return Optimization.scale_expr(
                # limit nodal total impact
                model.total_nodal_impact[node, impact_category, year]*model.scaling_total_nodal_impact[node, impact_category, year] <=
                model.total_nodal_impact_limits[node, impact_category, year]
                # adding an overshoot variable mainly for debugging feasibility issues
                + model.total_nodal_impact_overshoot[node, impact_category, year]*model.scaling_total_nodal_impact_overshoot[node, impact_category, year]
            )
        else:
            Optimization.add_skipped_constraint()
            return pe.Constraint.Skip

    # overall pe.Constraint rules for the total impact
    @staticmethod
    def constraint_total_impact_rule(model, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the calculation of the total impact vector summed over all processes and nodes

        Args:
            model: The equivalent of "self" in pyomo optimization models
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """
        Optimization.add_evaluated_constraint()
        return Optimization.scale_expr(
            model.total_impact[impact_category, year]*model.scaling_total_impact[impact_category, year] ==
            sum(model.total_nodal_impact[node, impact_category, year]*model.scaling_total_nodal_impact[node, impact_category, year]
                for node in model.nodes)
        )

    @staticmethod
    def constraint_total_impact_limits_rule(model, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the limitation of the total impact vector summed over all processes and nodes

        Args:
            model: The equivalent of "self" in pyomo optimization models
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """
        if model.total_impact_limits[impact_category, year] != np.inf:
            Optimization.add_evaluated_constraint()
            return Optimization.scale_expr(
                # limit overall total impact
                model.total_impact[impact_category, year]*model.scaling_total_impact[impact_category, year] <=
                model.total_impact_limits[impact_category, year]
                # adding an overshoot variable mainly for debugging feasibility issues
                + model.total_impact_overshoot[impact_category, year]*model.scaling_total_impact_overshoot[impact_category, year]
            )
        else:
            Optimization.add_skipped_constraint()
            return pe.Constraint.Skip

    @staticmethod
    def constraint_production_potential_capacity_by_input_rule(model, node: int, process_production: str, year: int) -> "pyomo rule":
        """This method is used as rule for limitting the capacity of production processes by limits 
        from the input e.g. maximum potential.

        Some production technologies like renewable energies have local capacity limits based on
        the availability of ressources or regulatory restrictions. These limits are given to the
        optimization as parameter and can change over time.

        Args;
            model: The equivalent of "self" in pyomo optimization models
            node (int): A node at which the pe.Constraint is active
            process_production (str): A production process which is constrained
            year (int): A year at which the pe.Constraint is active
        """
        if model.potential_capacity_production[node, process_production, year] != np.inf: 
            Optimization.add_evaluated_constraint()
            combined_existing_capacity_production = sum(model.existing_capacity_production[node,process_production, year, year_construction]
                for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_production[process_production, year_construction]), model.construction_years_production))
            # if combined existing capacity >= potential capacity --> new capacity == 0
            if combined_existing_capacity_production >= model.potential_capacity_production[node, process_production, year]:
                return Optimization.scale_expr(0 >= 
                    sum(model.new_capacity_production[node,process_production, earlier_year]*model.scaling_new_capacity_production[node,process_production, earlier_year]
                        for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year <= model.lifetime_duration_production[process_production, earlier_year]), model.years))
                )
            # else new capacity <= potential capacity - existing capacity
            else: 
                return Optimization.scale_expr(
                    model.potential_capacity_production[node, process_production, year] - combined_existing_capacity_production 
                    >= sum(model.new_capacity_production[node,process_production, earlier_year]*model.scaling_new_capacity_production[node,process_production, earlier_year]
                        for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year <= model.lifetime_duration_production[process_production, earlier_year]), model.years))
                )
        else:
            Optimization.add_skipped_constraint()
            return pe.Constraint.Skip
            
    @staticmethod
    def constraint_transmission_potential_capacity_by_input_rule_rule(model, connection: int, process_transmission: str, year: int) -> "pyomo rule":
        """This method is used as rule for limitting the capacity of transmission processes by limits
        from the input e.g. maximum potential.

        Some transmission technologies have local capacity limits based on the availability of 
        ressources or regulatory restrictions. These limits are given to the optimization as
        parameter and can change by time.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            connection (int): A connection of the grid
            process_transmission (str): A transsmission process which is constrained
            year (int): A year at which the pe.Constraint is active
        """
        if model.potential_capacity_transmission[connection, process_transmission, year] != np.inf:
            Optimization.add_evaluated_constraint()
            combined_existing_capacity_transmission = sum(model.existing_capacity_transmission[connection,process_transmission, year, year_construction]
                for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_transmission[process_transmission, year_construction]), model.construction_years_transmission))
            # if combined existing capacity >= potential capacity --> new capacity == 0
            if combined_existing_capacity_transmission >= model.potential_capacity_transmission[connection, process_transmission, year]:
                return Optimization.scale_expr(0 == 
                    sum(model.new_capacity_transmission[connection,process_transmission, earlier_year]*model.scaling_new_capacity_transmission[connection,process_transmission, earlier_year]
                        for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year <= model.lifetime_duration_transmission[process_transmission, earlier_year]), model.years))
                )
            # else new capacity <= potential capacity - existing capacity
            else: 
                return Optimization.scale_expr(
                    model.potential_capacity_transmission[connection, process_transmission, year] - combined_existing_capacity_transmission 
                    >= sum(model.new_capacity_transmission[connection,process_transmission, earlier_year]*model.scaling_new_capacity_transmission[connection,process_transmission, earlier_year]
                        for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year <= model.lifetime_duration_transmission[process_transmission, earlier_year]), model.years))
                )
        else:
            Optimization.add_skipped_constraint()
            return pe.Constraint.Skip

    @staticmethod
    def constraint_total_secured_capacity_rule(model, product: str, year: int) -> "pyomo rule":
        """This method is used as rule for providing the necessary secured capacity for a product.

        A baseline of required secured capacity can be given as parameter. This required secured
        capacity needs to be lower or equal to the sum of capacity secured by existing and new
        capacities.

        Since some technologies might increase the need for secured capacity for a product,
        their impact is taken into account as well. 

        Only production processes are taken into account for the secured capacity calculation.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            product (str): A product for which the pe.Constraint is active
            year (int): A year at which the pe.Constraint is active
        """
        total_demand_year = sum(
            sum(
                model.demand[product,year,time_slice,node]
                for node in model.nodes)
            * model.time_slice_yearly_weight[time_slice]
            for time_slice in model.time_slices)

        if total_demand_year != 0 and model.surplus_capacity_factor_production.value > 1:
            Optimization.add_evaluated_constraint()
            return Optimization.scale_expr(
                # The required secured capacity has to be smaller or equal than the existing secured capacity
                float(total_demand_year)
                *model.surplus_capacity_factor_production
                <= 
                # Sum over all nodes to aggregate the total secured capacity
                    sum(
                        # Sum over all production processes
                        sum(
                            # sum over all time_slices
                            sum(
                                model.usable_capacity_factor_timeseries_production[node, process_production, time_slice]*(
                                # Sum over all existing capacity
                                sum(# Existing capacity of a production process
                                    model.existing_capacity_production[node, process_production, year, year_construction]
                                    # Multiplied by the secured capacity factor of its construction year
                                    # * model.secured_capacity_factors_production[process_production, year_construction]
                                    # Multiplied by the value for the product in the technology matrix
                                    # Note, that negative values can exist and therefore increase the need for secured capacity
                                    * model.technology_matrix_production[product,node, process_production, year_construction]
                                    # Multiplied by the sum of the 
                                    for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_production[process_production, year_construction]), model.construction_years_production))
                                # Adding the sum over all new capacity built before or in this year
                                + sum(# Existing capacity of a production process
                                    model.new_capacity_production[node, process_production, earlier_year]*model.scaling_new_capacity_production[node, process_production, earlier_year]
                                    # Multiplied by the secured capacity factor of its construction year
                                    # * model.secured_capacity_factors_production[process_production, earlier_year]
                                    # Multiplied by the value for the product in the technology matrix
                                    # Note, that negative values can exist and therefore increase the need for secured capacity
                                    * model.technology_matrix_production[product,node, process_production, earlier_year]
                                    for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year <= model.lifetime_duration_production[process_production, earlier_year]), model.years)))
                                * model.time_slice_yearly_weight[time_slice]
                                for time_slice in model.time_slices)
                            for process_production in model.processes_production)
                        for node in model.nodes)
            )
        else:
            Optimization.add_skipped_constraint()
            return pe.Constraint.Skip

    @staticmethod
    def constraint_nodal_secured_capacity_rule(model, node: int, product: str, year: int) -> "pyomo rule":
        """This method is used as rule for providing the necessary secured capacity for a product.

        A baseline of required secured capacity can be given as parameter. This required secured
        capacity needs to be lower or equal to the sum of capacity secured by existing and new
        capacities.

        Since some technologies might increase the need for secured capacity for a product,
        their impact is taken into account as well. 

        Only production processes are taken into account for the secured capacity calculation.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            product (str): A product for which the pe.Constraint is active
            year (int): A year at which the pe.Constraint is active
        """
        if model.required_nodal_secured_capacity[product, node, year] != -np.inf:
            Optimization.add_evaluated_constraint()
            return Optimization.scale_expr(
                # The required secured capacity has to be smaller or equal than the existing secured capacity
                (model.required_nodal_secured_capacity[product, node, year]
                + model.nodal_slack_variable[product, node, year]*model.scaling_nodal_slack_variable[product, node, year])
                <=  
                # Sum over all production processes
                    sum(
                        # Sum over all existing capacity
                        sum(# Existing capacity of a production process
                            model.existing_capacity_production[node, process_production, year, year_construction]
                            # Multiplied by the secured capacity factor of its construction year
                            * model.secured_capacity_factors_production[process_production, year_construction]
                            # Multiplied by the value for the product in the technology matrix
                            # Note, that negative values can exist and therefore increase the need for secured capacity
                            * model.technology_matrix_production[product,node, process_production, year_construction]
                            for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_production[process_production, year_construction]), model.construction_years_production))
                        # Adding the sum over all new capacity built before or in this year
                        + sum(# Existing capacity of a production process
                            model.new_capacity_production[node, process_production, earlier_year]*model.scaling_new_capacity_production[node, process_production, earlier_year]
                            # Multiplied by the secured capacity factor of its construction year
                            * model.secured_capacity_factors_production[process_production, earlier_year]
                            # Multiplied by the value for the product in the technology matrix
                            # Note, that negative values can exist and therefore increase the need for secured capacity
                            * model.technology_matrix_production[product,node, process_production, earlier_year]
                            for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year <= model.lifetime_duration_production[process_production, earlier_year]), model.years))
                        for process_production in model.processes_production)
            )
        else:
            Optimization.add_skipped_constraint()
            return pe.Constraint.Skip

    # pe.Objective #
    @staticmethod
    def objectiveRule(model):
        """This method is used as an pe.Objective for the optimization, using weight factors for all impact categories.

        Args:
            model: The equivalent of "self" in pyomo optimization models
        """

        return (
            # sum over all years
            sum(
                # sum over all impact categories
                sum(
                    # sum of the operational impact over all impact categories 
                    model.operational_impact[impact_category, year]
                    * model.scaling_operational_impact[impact_category, year]
                    # sum of the invest  impact over all impact categories 
                    + model.invest_impact[impact_category, year]
                    * model.scaling_invest_impact[impact_category, year]
                for impact_category in model.impact_categories if impact_category == "cost")
                # add operational impact overshoot
                + 
                model.operational_impact_overshoot[model.capped_impacts.ordered_data()[0], year]
                *model.scaling_operational_impact_overshoot[model.capped_impacts.ordered_data()[0], year]
                *model.objective_factor_impact_overshoot[model.capped_impacts.ordered_data()[0], year]
                for year in model.years)
        )
