import numpy as np
import scipy.stats.mstats as sp
import importlib.util
# Load config.py
spec = importlib.util.spec_from_file_location("config", "SecMOD/00-INPUT/00-RAW-INPUT/config.py")
config = importlib.util.module_from_spec(spec)
spec.loader.exec_module(config)

def calculate_scaling_factor(scaling_vector):
    """ returns scaling factor r or c for vector
    
    select scaling algorithm"""
    scaling_vector = np.abs(scaling_vector)
    if not scaling_vector.size == 0 and not np.all(scaling_vector==1):
        if config.scaling_options['Algorithm'] == 'approx_geom_mean': # geometric mean scaling
            scaling_factor_til = np.sqrt(1/(scaling_vector.max()*scaling_vector.min()))
        elif config.scaling_options['Algorithm'] == 'geom_mean': # exact geometric mean scaling
            scaling_factor_til = 1/sp.gmean(scaling_vector)
        elif config.scaling_options['Algorithm'] == 'arith_mean': # arithmetic mean scaling
            scaling_factor_til = 1/np.mean(scaling_vector)
        elif config.scaling_options['Algorithm'] == 'equi': # equilibrium scaling
            scaling_factor_til = 1/np.max(scaling_vector)
        else:
            raise KeyError('Scaling Algorithm {} not found'.format(config.scaling_options['Algorithm']))

        if config.scaling_options['Round_to_power_of_2'] and not config.scaling_options['Algorithm'] == 'equi':
            # round to the closest power of two
            scaling_factor = 2**(np.round(np.log2(scaling_factor_til)))
        else:
            scaling_factor = scaling_factor_til
    else:
        scaling_factor = None
    return scaling_factor


