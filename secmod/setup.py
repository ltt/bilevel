import logging
import os
import shutil
import sys
from pathlib import Path

import datapackage as dp
import secmod.data_preprocessing as dataprep


def setup(working_directory: Path, reset: bool = False, download_ext_datapackages: bool = False):
    """This methods sets up SecMOD for the first use in the working directory.

    This method copies the embedded sample data to the working directory.
    Furthermore it downloads the external data packages for the first time.
    Last but not least it creates a python file and a batch file to easily start SecMOD.

    Args:
        working_directory (Path): The folder which is used as working directory
            by SecMOD
        reset (bool): Boolean value which decides whether an existing working directory
            is overwritten
        download_external_datapackages (bool): Boolean value which decides whether 
            external datapackages are downloaded
    """
    if (dataprep.is_setup(working_directory) and not reset):
        logging.info("SecMOD folder found! Setup has already been run.")
    else:
        logging.info("Setting up SecMOD in {0}...".format(str(working_directory)))
        copy_sample_data(working_directory)
        create_startup_helper(working_directory)

        if download_ext_datapackages:
            # Manual download of data packages might be required due to defect meta data
            download_external_datapackages(working_directory)
        logging.info("Setup completed!")


def copy_sample_data(working_directory: Path):
    """This method copies sample data from the package to the working directory.

    Args:
        working_directory (Path): The folder which is used as working directory
            by SecMOD
    """

    # get path of the installed package
    current_path = os.path.dirname(os.path.abspath(__file__))
    # join path to get sample data
    sampledata_path = os.path.join(os.path.dirname(current_path), 'sampledata')
    # remove SecMOD folder from working directory, if existing
    shutil.rmtree(working_directory / "SecMOD", ignore_errors=True)
    # copy sample data to new SecMOD folder in working directory
    logging.info("Copy sample data from package to working directory...")
    shutil.copytree(sampledata_path, working_directory / "SecMOD" / "00-INPUT" /
                    "00-RAW-INPUT", ignore=shutil.ignore_patterns('__init__.py'))
    logging.info("Done!")


def create_startup_helper(working_directory: Path):
    """This method creates startup helpers in the working directory.

    The startup helpers created by this method allow to run SecMOD with one click.

    Args:
        working_directory (Path): The folder which is used as working directory
            by SecMOD
    """

    # creating the startup helper files for SecMOD
    logging.info("Creating startup helper files...")
    content = "@Echo off\nEcho Edit this file to configure your startup!\n:start\nEcho To run SecMOD on default Anaconda environment choose 1\nEcho To run SecMOD on your a specific virtual environment choose 2\nset /p choice=Type number to choose environment:\nif %choice%==1 goto default_environment\nif %choice%==2 goto virtual_environment\nEcho Unvalid choice.\ngoto start\n:default_environment\nZ:\\vEnv\\secmod_venv_3\\python.exe -m secmod\ngoto end\n:virtual_environment\ncall C:\\Anaconda3\\Scripts\\conda env list\nset /p environment_name=Enter the name of the virtual environment you want to choose:\ncall C:\\Anaconda3\\Scripts\\activate %environment_name%\npython -m secmod\ngoto end\n:end\npause"
    file = open(working_directory / "start.bat", "w")
    file.write(content)
    logging.info("Done!")


def download_external_datapackages(working_directory: Path):
    # access data package "external_data_packages" to retrieve source URLs of external data packages
    if (working_directory / "SecMOD" / "00-INPUT" / "00-RAW-INPUT" / "00-EXTERNAL" / "external_data_packages" / "datapackage.json").exists():
        logging.info("Reading external datapackages...")
        package = dp.Package(str(working_directory / "SecMOD" / "00-INPUT" / "00-RAW-INPUT" /
                                "00-EXTERNAL" / "external_data_packages" / "datapackage.json"))
        # read data about external data packages (internal name, source URL)
        external_data_packages = package.get_resource(
            'external_data_packages').read()
        logging.info("Done!")
        # download all external data packages
        for package in external_data_packages:
            logging.info("Downloading external data package: {0}...".format(package[0]))
            if dataprep.download_datapackage(package[1], working_directory / "SecMOD" / "00-INPUT" / "00-RAW-INPUT" / "00-EXTERNAL" / package[0]):
                logging.info("Download of data package {0} finished.".format(package[0]))
            else:
                logging.info(
                    "Download of data package {0} was unsuccessful.".format(package[0]))
                logging.info("You need to retry downloading all necessary data packages later.")
    else:
        logging.warning("No external datapackages set up!")

if __name__ == "__main__":
    setup(Path.cwd(), True, False)
