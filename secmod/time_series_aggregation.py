import pandas as pd
import logging
import numpy as np
import copy as copy
import tsam.timeseriesaggregation as tsam
from secmod.classes import config, Product, ProductionProcess
import secmod.helpers as helpers

def get_all_timeseries():
    """ Returns a dictionary of timeseries dataframes with the timeseries data provided for each product and process
    
    Returns:
	full_time_series (dict)
    """

    logging.info("Get all time series")
    full_time_series = {}
    full_time_series["demand"] = Product.get_combined_demand_time_series()
    full_time_series["impact_non_served_demand"] = Product.get_combined_impact_non_served_demand()
    full_time_series["technologymatrix"] = ProductionProcess.get_combined_technology_matrix_timeseries()
    full_time_series["availability"] = ProductionProcess.get_combined_availability_timeseries()

    for series in full_time_series:
        # convert date string in index
        full_time_series[series] = format_as_timeseries(full_time_series[series], series)
    return full_time_series

def format_as_timeseries(timeseries: pd.Series, series: str):
    """ Formats a parametrized time series obtained from classes as columns of individual time series.

    """

    # make sure that column has no identifier, transforming DataFrames to Series
    if isinstance(timeseries, pd.DataFrame):
        timeseries = timeseries[list(timeseries.columns)[0]]
    # Get the names of all indices of the provided series
    index_names = list(timeseries.index.names)

    # Remove the level "time slice" from the list of indices
    try:
        index_names.remove("time slice")
    except ValueError as e:
        print(e)
        raise ValueError("Index of the provided Series '{0}' does not include the level 'time slice'!".format(series))
    
    # Use the list of indices different from "time slice" to unstack all of them
    unstacked_timeseries = timeseries.unstack(index_names)

    # return the unstacked time series
    return unstacked_timeseries

def get_columns_for_all_timeseries(full_time_series: list):
    """ Return simple indices for all indices of all time series which are about to be joined. """

    simple_columns = {}
    num_of_columns = 0
    for series in full_time_series:
        columns = list(full_time_series[series].columns)
        simple_columns[series] = {}
        for column in columns:
            simple_columns[series][column] = num_of_columns
            num_of_columns += 1

    return simple_columns

def get_combined_timeseries_table(full_time_series: dict):
    """ Returns a dataframe with all timeseries as columns. """

    logging.info("Combine timeseries tables")
    # Get unique identifiers for every column of the combined time series
    column_identifiers = get_columns_for_all_timeseries(full_time_series)
    # Initialize a new dictionary to take in all used time series
    combined_timeseries = {}
    # initialize dictionary to save column multiindex
    column_index_timeseries = {}
    # For all time series in the input data...
    for timeseries in full_time_series:
        # Copy the dataframe in order to not manipulate the input data
        combined_timeseries[timeseries] = full_time_series[timeseries].copy(deep=True)
        # Overwrite the columns index with the unique integer column identifiers in order to have a simple joinable column index
        combined_timeseries[timeseries].columns = [column_identifiers[timeseries][index] for index in column_identifiers[timeseries]]
        # save column multiindex for later reconstruction of index
        column_index_timeseries[timeseries] = full_time_series[timeseries].columns
    list_of_timeseries = [combined_timeseries[index] for index in combined_timeseries]
    combined_timeseries_data = list_of_timeseries.pop(0)
    combined_timeseries_data = combined_timeseries_data.join(list_of_timeseries)

    combined_timeseries = {}
    combined_timeseries["timeseries"] = combined_timeseries_data
    combined_timeseries["column_identifiers"] = column_identifiers 
    combined_timeseries["column_index_timeseries"] = column_index_timeseries

    return combined_timeseries

def separate_timeseries(combined_timeseries: pd.DataFrame, column_identifiers: dict, column_index: dict, time_slices: dict):
    """ Method to separate a combined time series DataFrame into multiple DataFrames.

    The parameter column_identifiers contains a dictionary of dictionaries for every
    targeted DataFrame. These dictionaries contain the information about all columns
    of the combined time series which belong to the targeted DataFrame.
    """

    logging.info("Separate combined time series")
    # Create an empty dictionary for the separated time series
    separated_timeseries = {}
    combined_timeseries.index = time_slices["time_slice"]
    combined_timeseries.index.set_names("time_slice",inplace = True)
    for element in column_identifiers:
        separated_timeseries[element] = pd.DataFrame()
        for index in column_identifiers[element]:
            separated_timeseries[element][index] = combined_timeseries[column_identifiers[element][index]]
        separated_timeseries[element].columns = column_index[element]
        
    return separated_timeseries

def reorder_timeseries(aggregated_time_series: dict):
    """ Reorder indizes of timeseries in order to match format in generate_input_dictionary (data_processing) """
    for timeseries in aggregated_time_series:
        # get names of columns
        column_names = list(aggregated_time_series[timeseries].columns.names)
        desired_order = copy.deepcopy(column_names)
        if timeseries != 'demand': # if not demand, append 'time_slice'
            desired_order.append('time_slice')
        else: # if demand
            desired_order.insert(2, 'time_slice')
        # stack and reorder
        aggregated_time_series[timeseries] = aggregated_time_series[timeseries].stack(column_names).reorder_levels(desired_order)

    return aggregated_time_series
def get_all_weights():
    """ TO-DO: Get all weights for the time series aggregation

    """

    return None

def get_time_slices(aggregation):
    """ return time slices of aggregation. Corresponding to format of time slices without time series aggregation """

    aggregated_time_slices = {"time_slice": [], "next_time_slice": [], "time_slice_duration":[],"time_slice_yearly_weights": []}
    # create array of time slices
    for cluster in aggregation.clusterPeriodIdx:
        # list of time slices for each cluster
        time_slices = list(range(1+cluster*aggregation.hoursPerPeriod,1+aggregation.hoursPerPeriod*(1+cluster)))
        aggregated_time_slices["time_slice"].extend(time_slices)
        # shift index of time slices to get next time slice
        time_slices.append(time_slices.pop(0))
        aggregated_time_slices["next_time_slice"].extend(time_slices)
        # get duration of time slice
        aggregated_time_slices["time_slice_duration"].extend(list(np.ones(aggregation.hoursPerPeriod)*aggregation.clusterPeriodNoOccur[cluster]))
        # get weight of time slice --> not yet fully implemented
        aggregated_time_slices["time_slice_yearly_weights"].extend(list(np.ones(aggregation.hoursPerPeriod)*aggregation.clusterPeriodNoOccur[cluster]))

    # create dicts
    aggregated_time_slices["next_time_slice"] = dict(zip(aggregated_time_slices["time_slice"],aggregated_time_slices["next_time_slice"]))
    aggregated_time_slices["time_slice_duration"] = dict(zip(aggregated_time_slices["time_slice"],aggregated_time_slices["time_slice_duration"]))
    aggregated_time_slices["time_slice_yearly_weights"] = dict(zip(aggregated_time_slices["time_slice"],aggregated_time_slices["time_slice_yearly_weights"]))
    return aggregated_time_slices

def get_aggregated_time_series(combined_timeseries, aggregation, aggregated_time_slices):
    """ Returns all aggregated time_series. """

    # Create typical periods
    aggregated_time_series = aggregation.createTypicalPeriods()
    # Create the individual time series DataFrames
    aggregated_time_series = separate_timeseries(aggregated_time_series, combined_timeseries["column_identifiers"],combined_timeseries["column_index_timeseries"],aggregated_time_slices)
    # Reorder time series
    aggregated_time_series = reorder_timeseries(aggregated_time_series)

    return aggregated_time_series

def get_time_series_data():
    """ Return the aggregated time series data """

    helpers.log_heading("Time Series Aggregation")
    aggregated_time_series = {}

    # Get all time series
    aggregated_time_series["full_time_series"] = get_all_timeseries()
    # Combine time series to a single dataframe
    aggregated_time_series["combined_timeseries"] = get_combined_timeseries_table(aggregated_time_series["full_time_series"])
    # Get weight of every single time series
    weights = get_all_weights()

    logging.info("Aggregate time series")
    # Create aggregation object
    aggregated_time_series["aggregation"] = tsam.TimeSeriesAggregation(
        timeSeries                  = aggregated_time_series["combined_timeseries"]["timeseries"],
        resolution                  = config.time_series_aggregation_config["resolution"],
        noTypicalPeriods            = config.time_series_aggregation_config["noTypicalPeriods"],
        hoursPerPeriod              = config.time_series_aggregation_config["hoursPerPeriod"],
        clusterMethod               = config.time_series_aggregation_config["clusterMethod"],
        evalSumPeriods              = config.time_series_aggregation_config["evalSumPeriods"],
        sortValues                  = config.time_series_aggregation_config["sortValues"],
        sameMean                    = config.time_series_aggregation_config["sameMean"],
        rescaleClusterPeriods       = config.time_series_aggregation_config["rescaleClusterPeriods"],
        # weightDict                  = weights,
        extremePeriodMethod         = config.time_series_aggregation_config["extremePeriodMethod"],
        predefClusterOrder          = config.time_series_aggregation_config["predefClusterOrder"],
        predefClusterCenterIndices  = config.time_series_aggregation_config["predefClusterCenterIndices"],
        solver                      = config.time_series_aggregation_config["solver_tsa"],
        roundOutput                 = config.time_series_aggregation_config["roundOutput"],
        addPeakMin                  = config.time_series_aggregation_config["addPeakMin"],
        addPeakMax                  = config.time_series_aggregation_config["addPeakMax"],
        addMeanMin                  = config.time_series_aggregation_config["addMeanMin"],
        addMeanMax                  = config.time_series_aggregation_config["addMeanMax"]
        )
    aggregated_time_series["aggregated_time_slices"] = get_time_slices(aggregated_time_series["aggregation"])
    aggregated_time_series["aggregated_time_series"] = get_aggregated_time_series(aggregated_time_series["combined_timeseries"], aggregated_time_series["aggregation"], aggregated_time_series["aggregated_time_slices"])

    # TO-DO Get time steps, duration and weight

    return aggregated_time_series